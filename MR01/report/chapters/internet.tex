Ce document consiste à présenter une définition et les enjeux associés de la
neutralité des réseaux appliquée au réseau informatique mondial : Internet. Une
des difficultés de cet objectif est de parvenir à expliquer des problématiques
de nature technique à un lectorat qui ignore peut-être tout du fonctionnement
d'Internet, de son organisation et des raisons qui ont permis sa création et son
développement.

Ce premier chapitre apporte donc les clés potentiellement nécessaires à la
compréhension d'une problématique fine et encore floue, alors que cela fait
plusieurs années que le débat fait rage parmi les spécialistes du domaine !

\section{Internet, le réseaux des réseaux}

Internet est sans aucun doute l'innovation technologique majeure de ces vingt
dernières années. Présent à l'échelle globale, transformant le quotidien d'un
nombre sans cesse croissant d'utilisateurs de tout âge, nationalité et origine
sociale, Internet ou le \textbf{réseau des réseaux} est une magnifique et
dynamique implémentation d'un des besoins le plus élémentaire de l'Homme :
\emph{la communication avec ses pairs}. Pourtant, les origines de ce réseau
n'étaient pas si louables…

\subsection{Origine et histoire}

En 1962, alors que le communisme faisait force, l'US Air Force demande à un
petit groupe de chercheurs de créer un réseau de communication militaire capable
de résister à une attaque nucléaire. Le concept de ce réseau reposait sur un
système décentralisé, permettant au réseau de fonctionner malgré la destruction
d'une une ou plusieurs machines.

\subsection{Caractéristiques techniques}

Paul Baran était un informaticien, un physicien et un mathématicien américain
qui a co-inventé avec Donald Davies la communication sur réseau de données par
paquet. Il est considéré comme un des acteurs principaux de la création
d'Internet.

Il eut l'idée, en 1964, de créer un réseau sous forme de grande toile. Il avait
réalisé qu'un système centralisé était vulnérable car la destruction de son
noyau provoquait l'anéantissement des communications. Il mit donc au point un
réseau hybride d'architectures étoilées et maillées dans lequel les données se
déplaceraient dynamiquement, en «cherchant» le chemin le moins encombré,
et en «patientant» si toutes les routes étaient encombrées. Cette technique
est la \textbf{commutation de paquets} et reste aujourd'hui un pilier
fondamental des réseaux informatiques modernes.

Voici plusieurs schémas illustrant les différentes topologies de réseaux :

\begin{figure}[ht]
  \centering
  \subfloat[Topologie linéaire («bus»)]{\fbox{\scalegraphics{Bus-topology}}}
  \subfloat[Topologie en anneau]{\fbox{\scalegraphics{Ring-topology}}}
  \subfloat[Topologie en arbre]{\fbox{\scalegraphics{Tree-topology}}}
  \caption{Topologies réseau anciennes}
\end{figure}

\begin{figure}[ht]
  \centering
  \subfloat[Topologie en étoile]{\fbox{\scalegraphics{Star-topology}}}
  \subfloat[Topologie en maille, celle de
  l'Internet.]{\fbox{\scalegraphics{Mesh-topology}}}
  \caption{Topologies réseaux modernes}
\end{figure}

Un première implémentation fonctionnelle à grande échelle fut le réseau
\textbf{ARPANET}. En août 1969, indépendamment de tout objectif militaire, le
réseau expérimental ARPANET fut créé par l'ARPA (Advanced Research Projects
Agency) dépendante du DOD (Department of Defense) afin de relier quatre
instituts universitaires :

\begin{itemize}
  \item Le Stanford Institute
  \item L'université de Californie à Los Angeles
  \item L'université de Californie à Santa Barbara
  \item L'université d'Utah
\end{itemize}

\clearpage

Le réseau ARPANET est aujourd'hui considéré comme le réseau précurseur
d'Internet car déjà à l'époque, il comportait certaines caractéristiques
techniques fondamentales du réseau actuel :

\begin{itemize}
\setlength{\itemsep}{8pt}

  \item \textbf{Un réseau résilient.}\\
  Un ou plusieurs nœuds du réseau pouvaient être détruits sans perturber son
  fonctionnement.

  \item \textbf{Un réseau acentré.}\\
  La communication entre machines se faisait sans machine centralisée
  intermédiaire.

  \item \textbf{Un réseau au fonctionnement simplifié.}\\
  Les protocoles de communication et de signalisation utilisés étaient basiques.
\end{itemize}

À ces caractéristiques historiques sont venus ensuite se greffer des composants
clés du réseau actuel :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item \textbf{Le protocole de transport TCP}\\
  Le protocole NCP, utilisé jusqu'alors, ne permettait pas de gérer le contrôle
  d'erreur. Arrivé à l'ARPA en 1972, Bob Kahn élabora un nouveau protocole
  permettant d'acheminer des données sur un réseau en les fragmentant en petits
  paquets. En 1976, le nouveau protocole TCP fut déployé sur le réseau ARPANET,
  composé de 111 machines uniquement ! En 1978, le protocole TCP fut scindé en
  deux protocoles : TCP et IP, explicités dans la section \ref{tcp-ip} page
  \pageref{tcp-ip}.

  \item \textbf{Le protocole DNS (\emph{Domain Name System})}\\
  Le système de nommage DNS, utilisé de nos jours, fut mis en œuvre en 1984,
  afin de pallier le manque de souplesse du nommage par correspondance stockée
  dans un fichier texte présent sur chaque machine, ce qui demandait un travail
  considérable de maintenance.

  \item \textbf{Le \emph{World Wide Web} (langage \emph{HTML} + protocole
  \emph{HTTP})}\\
  Dès 1980, Tim Berners-Lee, un chercheur au CERN de Genève, mit au point un
  système de navigation hypertexte et développa, avec l'aide de Robert Cailliau,
  un logiciel nommé \emph{Enquire} permettant de naviguer selon ce principe. Fin
  1990, Tim Berners-Lee mit au point le protocole HTTP (Hyper Text Transfer
  Protocol), ainsi que le langage HTML (HyperText Markup Language) permettant de
  naviguer à l'aide de liens hypertextes, à travers les réseaux.

\end{itemize}

\section{Organisation et fonctionnement}

Nous connaissons désormais les principes fondateurs du réseau mondial Internet,
mais comment migrer d'une gestion d'une centaine de machines d'une même
organisation à plusieurs milliards de machines et d'appareils connectés à un
réseau composé de plusieurs dizaines de milliers d'acteurs indépendants ?

\subsection{Internet : des hommes et des organisations}

Pour bien comprendre comment s'est organisé Internet depuis la fin des années 80
jusqu'à nos jours, il est nécessaire d'aborder la \emph{gouvernance d'Internet}
et des ressources rattachées. En effet, pour qu'une machine puisse accéder au
réseau et y communiquer, elle a tout d'abord besoin d'une adresse réseau. Ces
adresses, dites IP, sont destinées avant tout à un traitement informatique et ne
sont pas destinées à une utilisation humaine : dans la version 4 du protocole
IP, elles sont constitués de 32 bits, c'est à dire une combinaison unique et
hiérarchique de 32 \texttt{0} et \texttt{1}. 

Ensuite, le système \emph{DNS} permet de faire le lien avec une adresse au
format lisible par un humain : le nom de domaine. Ce nom de domaine est défini
hiérarchiquement à partir d'une racine (\texttt{.}), puis d'un nom de premier
niveau comme le \texttt{.fr} et enfin d'un nom unique identifiant précisément la
machine. Par exemple, le nom de domaine \texttt{utbm.fr.} est en fait interprété
de droite à gauche par les machines connectées sur Internet.

Il existe donc deux types d'adresses organisées selon une hiérarchie en arbre et
dont chaque affectation ou \emph{allocation} se doit d'être unique pour éviter
les conflits d'adressage. Il fut donc nécessaire d'avoir une ou plusieurs
organisations chargées de la gestion de ces ressources. Pour les adresses IP,
c'est tout d'abord l'\textbf{IANA}
%
\footnote{\emph{Internet Assigned Numbers Authority} : Désormais branche de
l'ICANN, cette organisation assure la gestion de l'espace d'adressage IP
d'Internet, et des autres ressources partagées de numérotation requises soit par
les protocoles de communication sur Internet, soit pour l'interconnexion de
réseaux à Internet.}
%
puis ensuite l'\textbf{ICANN}%
\footnote{\emph{Internet Corporation for Assigned Names and Numbers} : autorité
de régulation de l'Internet. C'est une société de droit californien à but non
lucratif contrôlant l'accès à tout domaine virtuel, qu'il soit générique ou
national.}
%
à partir de 1998 qui en assure la gestion à un niveau global. Pendant les années
90 sont créées pour chaque continent une organisation régionale qui effectue ce
travail de gestion par délégation : ce sont les \textbf{RIR}
%
\footnote{\emph{Regional Internet Registry} : c'est un organisme qui alloue les
blocs d'adresses IP (adressage IPv4, IPv6) et des numéros d'Autonomous System
dans sa zone géographique.},
%
dont voici la répartition :

\begin{figure}[ht]
  \centering\scalegraphics{rir}
  \caption{Répartition géographique des différents registres régionaux}
\end{figure}

\textbf{En bref, Internet est sous la gouvernance de l'ICANN, qui délègue un
certain nombre de rôle et de ressources à gérer.} Ce sont ensuite les
organisations, les entreprises, les associations, les opérateurs qui demandent
un identifiant numérique unique et des blocs d'adresses IP (par exemple 1024
adresses formant un bloc contigu). Une fois que les ressources demandées sont
attribuées, ces entités deviennent alors des \textbf{Systèmes Autonomes} ou
\emph{Autonomous Systems} en anglais. À la différence d'un particulier client
d'un FAI et qui n'a rien à faire de particulier pour disposer d'une ou plusieurs
adresses IP, un \emph{AS} a la responsabilité de s'interconnecter avec d'autres
AS pour échanger leurs informations respectives… C'est la réalité d'Internet sur
le plan humain et institutionnel : \textbf{Des autorités de régulation (ICANN)
qui allouent via des délégations régionales (RIR) des ressources à des Systèmes
Autonomes (AS) qui s'interconnectent, annonçant alors quelles ressources leur
sont allouées à leurs AS voisins (ou \emph{pairs}).}

\subsection{… mais aussi des machines et des liens}

Techniquement, Internet est basé sur un maillage d'entités (AS) qui communiquent
de pairs en pairs, en échangeant des informations via le protocole \textbf{BGP}
%
\footnote{\emph{Border Gateway Protocol} : protocole d'échange de route utilisé
notamment sur le réseau Internet. Son objectif est d'échanger des informations
d'accessibilité de réseaux (appelés préfixes) entre Autonomous Systems (AS) car
il a été conçu pour prendre en charge de très grands volumes de données et
dispose de possibilités étendues de choix de la meilleure route.}
%
qui permet d'annoncer quels sont les blocs IP qui sont utilisés par tel ou tel
AS. Par conséquent, si l'on se place du point de vue d'un AS donné, on sait
alors que pour atteindre tel ou tel bloc d'adresses IP, il sera nécessaire de
passer par l'infrastructure de tel autre AS, lui même appairé avec d'autres AS…
Jusqu'à parvenir à l'AS final qui utilise le bloc d'adresses IP recherché.

Dès lors, on peut voir Internet comme \textbf{un maillage de routes}, chaque
route étant \emph{constituée des nœuds ou sauts} entre \emph{un AS source et un
AS de destination}.

Concrètement, on peut imaginer une session de navigation sur le web, avec un
navigateur qui dispose de plusieurs onglets ouverts : un moteur de recherche,
des cartes, Wikipédia, le site de l'UTBM, Youtube, Yahoo mail, une banque en
ligne, le site du journal le monde, etc. De manière schématique, on peut résumer
chaque onglet par une connexion, un seul flux. Des lors, chaque flux traverse
des \textbf{nœuds intermédiaires}. Si les premiers nœuds appartiennent au FAI,
les derniers nœuds appartiennent aux opérateurs de transit et finalement au FAI
qui héberge le serveur web de destination. Et pour chaque destination, ce chemin
peut être complètement différent ! En effet, Internet n'a pas de nœud central,
l'interconnexion entre les différents acteurs du réseau reste à leur entière
discrétion : de l'entité \textbf{A} pour aller vers \textbf{Z}, le trafic peut
passer par \textbf{B-R-N-Q} mais pour aller vers \textbf{Y}, le trafic
empruntera un autre chemin tel que \textbf{B-T-I}. \textbf{Il faut ici
comprendre que chaque AS permet la communication avec les ressources qui lui
sont allouées, mais également fait transiter du trafic réseau ne lui étant pas
destiné et ainsi de jouer le rôle d'intermédiaire.} Cette particularité
d'Internet assure en réalité sa résilience : si un pair disparait du réseau,
alors un autre peut prendre le relai et assurer l'acheminement du trafic !

En fait, la topologie d'Internet entier dépend des accords commerciaux et de
principes qui établissent des liens entre les opérateurs, les FAI, les
hébergeurs de services et de contenu… Internet est finalement le premier
(historiquement et numériquement) réseau social, commercial, technique et
économique du Monde, basé sur la facilité et l'engouement naturels de l'espèce
humaine à vouloir partager et échanger librement.

Néanmoins, malgré le caractère acentré et maillé d'Internet, il existe une
hiérarchie entre les différents AS, qui se caractérise par la taille de
l'entreprise qui soutient l'AS, sa couverture géographique et son cœur
d'activité. Par exemple, un FAI national n'a pas la même importance qu'un
opérateur qui s'occupe des liaisons transocéaniques et transcontinentales. De
même il existe des opérateurs intermédiaires dont le cœur d'activité est
d'interconnecter des petits opérateurs, des FAI ou d'autres AS situés à la
«terminaison» du réseau aux gros opérateurs intercontinentaux.

Il apparait alors une hiérarchie tiercée, qui dans l'étude de la neutralité des
réseau se révèle très importante car ce sont essentiellement les FAI (Tiers 3)
qui sont concernés :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item \textbf{AS de Tiers 1}\\
  Opérateurs de premier ordre, généralement présents sur un continent voire dans
  le monde entier. Ils constituent une dorsale très ramifiée du réseau Internet.

  \item \textbf{AS de Tiers 2}\\
  Entités intermédiaires, généralement à portée nationale : Opérateurs réseaux
  dits \emph{transitaires}, opérateurs professionnels, gros fournisseurs de
  contenus.

  \item \textbf{AS de Tiers 3}\\
  Entités situées à la terminaison du réseau : FAI nationaux ou locaux, grandes
  entreprises, hébergeurs de services, etc.
\end{itemize}

\subsection{Un exemple illustré}

\begin{figure}[ht]
\centering\fbox{\includegraphics[scale=0.46]{as50903}}
\caption{Vue partielle des interconnexions vers l'AS 50903}
\end{figure}

Dans ce cas de figure, \textbf{TRINAPS} est un AS de Tiers 3, \textbf{JAGUAR-AS}
est un AS de Tiers 2 et \textbf{Level3} est un AS de Tiers 1.

\clearpage

\section{Introduction au fonctionnement de TCP/IP}

La suite de protocole TCP/IP est un des piliers porteurs d'Internet. Utilisés
simultanément, ces protocoles permettent à la fois d'identifier les machines
informatiques sur le réseau grâce à leur adresse IP, mais également comment
initier une «discussion» avec elles.

\subsection{\emph{Internet Protocol} : l'adressage logique des ressources}

Ainsi, l'adressage IP permet d'établir une communication basée sur deux adresses
: l'\textbf{adresse source} (celle de l'émetteur) et l'\textbf{adresse de
destination}. En effet une communication informatique peut avoir lieu dans les
deux sens simultanément, il est donc primordial que tous les équipements
traversés aient conscience de la source et du destinataire pour assurer
l'acheminement des paquets.

La structure des adresses IP est la suivante :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item \textbf{IP version 4}\\
  Les adresses sont composés de 32 bits (unité de base fondamentale en
  informatique) et représentées pour la lecture humaine par la \emph{notation
  décimale pointée} : \texttt{8.8.8.8}, \texttt{82.238.123.3} ou encore
  \texttt{91.121.93.194}.

  \item \textbf{IP version 6}\\
  IPv4 ne permet de gérer que 4.2 milliards d'adresses, ce qui devient très
  insuffisant de nos jours avec tous les appareils connectés sur Internet.
  Une nouvelle version fut développée pour, entre autre, pallier ce souci.
  Composée de 128 bits, la version 6 permet de gérer plus de
  340282366920938463463374607431768211456 adresses, soit plus que d'atomes dans
  la galaxie entière ! Les adresses sont représentées par des groupes de
  symboles \emph{hexadécimaux}
%
\footnote{Symboles héxadécimaux : \texttt{0 1 2 3 4 5 6 7 8 9 a b c d e f}} :\\
%
  \texttt{2001:470:c8be::1} ou encore
  \texttt{2001:470:c8be:1:2ff:34ff:fe18:ae2}.
\end{itemize}

Bien entendu, en pratique, il est rare de croiser de telles adresses dans nos
usages quotidiens : le protocole DNS permet d'établir une correspondance entre
adresse IP et nom de domaine : \texttt{lv0.in} $\leftrightarrow$
\texttt{82.238.123.3}.

Ce qu'il faut comprendre ici vis-à-vis de la neutralité des réseaux, c'est que
les différentes entités indépendantes (AS) composant Internet \emph{disposent de
blocs contigus d'adresses IPv4 et IPv6}. Sachant que le registre Internet
régional expose toutes ces informations publiquement, il devient très aisé pour
un FAI donné de \emph{filtrer ou altérer la qualité du trafic selon la
provenance ou la destination}, en fonction des accords commerciaux existants
entre ce FAI et son pair par qui passe le trafic vers et/ou depuis ce bloc IP
donné.

\subsection{\emph{TCP} et \emph{UDP} : transport des données}

Les protocoles \textbf{TCP} (\emph{Transmission Control Protocol}) et
\textbf{UDP} (\emph{User Datagram Protocol}) sont les deux \emph{moyens} de
transport principaux des réseaux informatiques actuels. Si l'IP permet de
connaître source et destination, à l'instar d'une adresse postale, les
protocoles de transports contiennent les données à transmettre et
\emph{l'identifiant de protocole haut niveau}, à l'instar du numéro
d'appartement dans un immeuble.

Cet identifiant de protocole haut niveau, plus communément appelé \textbf{port
applicatif}, permet aux machines émettant ou recevant du trafic réseau de trier
les données contenues, afin de les acheminer ensuite vers le bon programme
informatique : les emails dans le logiciel de messagerie, les données HTTP dans
le navigateur web, etc.

Comme précédemment, l'ICANN maintient à jour une liste de correspondance entre
l'identifiant numérique (de 0 à 65535) à un protocole haut niveau, ce qui rend
très simple la différenciation des flux réseaux pour ordonnancer le trafic à sa
convenance, en violant la neutralité des réseaux. C'est une technique utilisée
par la grande majorité des FAI.

\subsection{Le paquet final : \texttt{adressage(transport(données))}}
\label{tcp-ip}

On désigne ici par \emph{paquet final} l'assemblage par encapsulation de trois
composants dont voici une description simplifiée :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item l'entête IP, qui contient entre autres l'adresse IP source et
  l'adresse IP de destination.

  \item l'entête de transport (UDP ou TCP), qui contient entre autres le port
  applicatif source et le port applicatif de destination.

  \item les données effectives de l'application locale qui seront communiquées à
  l'application distante.
\end{itemize}

\begin{figure}[ht]
  \centering\scalegraphics{tcp-ip-model}
  \caption{Détail du processus d'encapsulation TCP/IP}
\end{figure}

\label{www-is-not-internet}
Un autre point \textbf{très important à retenir ici} pour mieux comprendre le
fonctionnement d'Internet : \textbf{Il n'y a pas que le web, l'email et le chat
sur Internet, cela représente à peine trois ports applicatifs parmi… 65536
possibles !} C'est une richesse incroyable et des perspectives d'innovation
sans précédent dans le domaine des télécommunications et c'est un des points
clés de la neutralité des réseaux.
