La neutralité des réseaux est un débat qui a pris de l'ampleur depuis presque
deux ans. Sans pour autant atteindre la portée et l'exposition populaire d'un
enjeu de société, le sujet est régulièrement traité en France et dans d'autres
pays du monde, notamment les États-Unis d'Amérique. Ceci est certainement le
fait de l'histoire même du réseau Internet, comme présentée dans le chapitre
précédent, ces deux nations étant à l'origine de la plupart des composants
encore utilisés aujourd'hui. Il semble logique de supposer de cette manière que
la neutralité des réseaux est un sujet mieux connu et reconnu comme crucial à la
continuité du développement d'Internet.

\section{Les origines de la neutralité des réseaux}

Les origines de cette problématique, ainsi que l'expression même de \emph{net
neutrality} remontent au début des années 2000 aux États-Unis. C'est un
professeur de droit enseignant le droit d'auteur et les télécommunications à
l'université de Columbia à New York, M. Tim Wu, qui en a popularisé le concept
dans un article paru en 2003 sous le titre de \emph{Network Neutrality,
Broadband Discrimination}
%
\footnote{Cf. Références page \pageref{références} pour un lien vers l'article
complet gratuitement disponible au format PDF.}
%
. Cet article s'attarde
particulièrement sur les extrémités du réseau, à savoir les connexions
haut-débit personnelles telles que proposées par les grands \textbf{FAI}. Cela
couvre donc un aspect limité d'Internet, puisque nous avons vu dans le chapitre
précédent qu'Internet est un regroupement mondial d'organisation et sociétés
autonomes qui n'offrent pas obligatoirement un accès, mais peuvent également
être des transitaires (\emph{Tiers 2}) ou même des géants qui interconnectent des
opérateurs transitaires (\emph{Tiers 1}).
%
\footnote{Fournisseurs d'Accès à Internet (\emph{Internet Service Providers} en
anglais). Sociétés fournissant à un abonné particulier ou professionnel l'accès
physique et logique à un réseau opérateur interconnecté au reste d'Internet.
Cela comprend aussi bien le matériel mis à disposition (modem, routeur) que les
services nécessaires à l'utilisation du réseau (Adresse IP, service DNS), ainsi
que des services subsidiaires apportant une plus-value (filtrage par pare-feu,
contrôle parental, téléphonie, mail, TV, partage de fichiers, hébergement de
pages personnelles…).}
%

Tim Wu expose donc les pratiques des opérateurs dit de \emph{Tiers 3} qui
offrent une connectivité et des services à des utilisateurs finaux
(\emph{eyeball customers} en anglais
%
\footnote{\emph{Eyeball customers} ou simplement \emph{Eyeballs} : désigne les
utilisateurs finaux, servis par les FAI. Le terme fait à la fois référence aux
yeux, medium terminal de l'information numérique, mais comporte également une
connotation péjorative de passivité et de consumérisme béat.}
%
) vraisemblablement de profil non technique. L'auteur soulève donc les
motivations pour un fournisseur de différencier les flux selon les protocoles et
les ressources sollicités par leurs clients, que cela soit pour contrôler la
congestion du réseau sans devoir consacrer d'investissements conséquents à son
évolution ou pour mettre en avant leurs services au détriment des services
concurrents. Des exemples concrets et techniques complètent ce triste tableau
dans un discours se voulant pourtant révélateur et vulgarisant, comme un signal
d'alarme qui a mis plusieurs années avant d'être entendu, y compris par les
professionnels du domaine.

Depuis 2008, la question est remise au goût du jour à peu près simultanément
dans le monde entier, mais particulièrement en France au début des débats sur la
loi HADOPI et les réflexions qui s'organisent alors dans le camps des opposants.
Dès lors et encore à ce jour, la neutralité est utilisée comme un argument lourd
de sens, parfois complètement hors de propos au sein de débats houleux. La
difficulté d'appréhension du principe, sa technicité, les multiples visions
existantes et la courte histoire de l'ère numérique occultent la neutralité des
réseaux dans un flou artistique qui tantôt la soutient tantôt la dessert.

À l'aube de la deuxième décennie de débat, des lois lourdes de conséquences sont
débattues ou votées, en Europe et aux États-Unis notamment, sacralisant la
neutralité ou la diabolisant. Ne faisant pas l'unanimité, se révélant assez
complexe pour être régulièrement malmenée par les politiques, la neutralité des
réseaux exige par conséquent d'être définie clairement pour être comprise du
plus grand nombre. À peine plus vieux que 30 ans, Internet est aujourd'hui un
élément capital à l'échelle globale. Reposant sur des principes d'ouverture et
de partage, sa compréhension par le plus grand nombre devient et s'affirmera
comme un objectif capital, afin que le plus grand réseau démocratique du monde
puisse prospérer et vivre.

\section{Définition(s) ? Pas si simple…}

Néanmoins, la neutralité des réseaux ne se limite pas au constat de M. Wu et la
notion s'est complexifiée depuis 2003. Au même titre qu'Internet, que ses
usages, ses produits et termes à la mode, la neutralité des réseaux est avant
tout un regard vivant sur ce que devrait être ce réseau mondial, sur de nombreux
plans. Une définition fournie par M. Wu :

\begin{center}
  \begin{large}\emph{«L'idée est de considérer un réseau le plus utile et
  pratique possible qui considère tous les contenus, sites et plateformes de
  manière égale, sans discrimination due à un ou plusieurs facteurs d'ordre
  technique, politique, stratégique ou économique. L'intérêt profond ce cette
  non spécialisation du réseau est d'assurer un accès équitable et juste à tous
  les ressources (contenus, protocoles, plateformes) actuelles et futures
  !»}\end{large}
\end{center}

Une autre approche, exprimée par M. Stéphane Bortzmeyer, ingénieur à
\emph{l'AFNIC}
%
\footnote{\emph{Association française pour le nommage Internet en coopération}
: association loi 1901 qui gère les \emph{domaines de premier niveau}
\textbf{.fr.}, \textbf{.re.}, \textbf{.tf.}, etc. Elle est responsable de
l'attribution des noms de domaine se terminant par ces suffixes, mais également
de développer les outils et services pour le développement de l'économie
numérique en France. Enfin, elle doit transmettre compétences et connaissances
au niveau national et international.}
%
, personnalité reconnue dans le domaine en France :

\begin{center}
  \begin{large}\emph{«La neutralité de l'Internet, c'est d'abord l'idée que
  l'Intermédiaire ne doit pas abuser de son rôle.»}\end{large}
\end{center}

Si la première définition tente d'être la plus globale possible avec un énoncé
qui peut paraître incongru à un profane, la seconde quant à elle tente de mettre
en avant la responsabilité de l'humain et des organisations. Si j'ai retenu ces
deux points de vue, il en existe pourtant des dizaines différents qui ressortent
au fur et à mesure des recherches et des lectures : chaque auteur y va de sa
propre interprétation d'un débat en constante évolution et complexification.

Ces visions sont souvent déformées par des biais cognitifs qui témoignent d'une
vision étroite, d'une méconnaissance technique ou au contraire d'une maîtrise
qui occulte d'autres aspects tels que la légalité ou l'économie…

Une autre définition, plus technique et limitée à quelques enjeux seulement est
représentée dans l'infographie suivante :

\begin{figure}[ht]
\centering\fbox{\includegraphics[scale=0.60]{neutralite-image}}
\caption{Infographie de Sébastien Desbenoit, auteur du blog
\emph{Internet\&Moi}. }
\end{figure}

\clearpage

Au final et assez paradoxalement, il est assez compliqué de fournir une
définition neutre qui aborde les différents aspects et acteurs avec équité, sans
avoir étudié la problématique en décrivant les enjeux de la neutralité des
réseaux domaine par domaine, du plus technique au plus proche de l'utilisateur
final, du cybercitoyen le plus aguerri au plus néophyte, sans oublier les aspects
économiques et légaux.

Je propose donc dans les sections suivantes une étude synthétique, fragmentée
selon une série de domaines précis, classés par pertinence : technique,
économique, juridique et politique. L'objectif est d'exposer les enjeux
principaux de la neutralité des réseaux.

\section{Enjeux de la neutralité des réseaux}

L'objectif de cette section est d'exposer au lecteur les enjeux principaux selon
leur appartenance à des domaines particuliers. La liste classée ainsi obtenue
devrait permettre une appréhension de la neutralité des réseaux quel que soit le
niveau technique et les domaines d'intérêt du lecteur.

\subsection{Enjeux techniques et économiques}

Internet repose sur \textbf{la coopération d'intermédiaires}, du client final à
son domicile au serveur de destination, en passant par le fournisseur d'accès
à Internet et les opérateurs réseaux par qui transite le flux. Ainsi, pour
atteindre un site web donné, les paquets réseaux peuvent traverser des
dizaines d'infrastructures et d'opérateurs différents. C'est ce que nous avons
appris dans le chapitre précédent. Ici, il faut surtout entrevoir la complexité
dans la gestion du trafic qui se décompose en deux composantes : routage
(quelles sont les routes que le flux va emprunter ?) et débit (quelle est la
vitesse maximale immédiatement utilisable entre mon ordinateur et le serveur
distant ?).

Néanmoins, deux raisons permettent de simplifier cette apparente complexité.
De manière schématique, Internet est tiercé, hiérarchisé entre les différents
acteurs : \texttt{Tiers 1 > Tiers 2 > Tiers 3}. Généralement, ni les opérateurs
de Tiers 1 ou de Tiers 2 n'ont d'intérêt particulier à porter préjudice à la
neutralité des réseaux. En effet, leurs clients sont les opérateurs de Tiers 3,
c'est-à-dire principalement les FAI grand public et professionnels, dont les
clients ne sont généralement que des acteurs "passifs" et peu impliqués dans la
vie du réseau, qui veulent juste un service sans être forcément capables d'en
évaluer la qualité, la performance et la fiabilité. \textbf{Cette étude se
limite donc désormais aux FAI}.

De plus, il est aisé pour un FAI de \textbf{différencier} le trafic réseau
engendré par ses utilisateurs. En effet, dans la section concernant le
fonctionnement du couple de protocole TCP/IP page \pageref{tcp-ip}, nous avons
vu qu'un protocole réseau est simplement déterminé par la nature du protocole de
transport (TCP ou UDP) et le numéro de port (80 pour HTTP, 443 pour HTTPS, 22
pour SSH, etc). Il suffit de traiter le trafic des utilisateurs selon ce numéro
de port pour lui affecter notamment une priorité de transfert sur le réseau.

Quantitativement, cet unique aspect technique est très dense : Internet repose
sur quelques protocoles majeurs mais des centaines d'autres protocoles furent
développés au dessus, créant tout autant d'usages différents. Ainsi, plusieurs
dizaines de milliers de protocoles différents peuvent coexister.
%
\footnote{Cf. \ref{www-is-not-internet} page \pageref{www-is-not-internet}. Le 
\emph{surf} ou navigation web est un usage majeur en terme de volumétrie de
données échangée, mais techniquement mineur : le protocole HTTP(S) permettant la
navigation n'utilise que deux ports réseaux - 80 et 443 !}
%
Cette prolifération des usages et protocoles complexifie le travail de
l'opérateur, qui sera tenté de défavoriser tous les protocoles sauf les
principaux et les "suffisants". Mais comment un opérateur peut-il décider à la
place de ses utilisateurs ? C'est ici une violation du contrat client, puisque
le client paie pour un accès à \emph{Internet} et pas juste le \emph{web} ou les
jeux en ligne. Nous reviendrons sur cette première violation de la neutralité,
car elle peut révéler un aspect économique et stratégique important pour un FAI.

Un autre point tout aussi important que la différenciation en débit (vitesse de
transmission) et la différenciation en priorité temporelle d'accès au réseau,
plus connu sous le nom de \emph{Qualité de Service}. En effet et contrairement
aux métaphores erronées le comparant \emph{à un réseau fluvial ou à un
réseau autoroutier}, \textbf{Internet est un réseau de routes dont la
performance n'est pas estimée à la vitesse moyenne de ses usagers, mais au
nombre de connexions simultanées que la route (ou lien) peut traiter en
parallèle.} Ce point est très important pour comprendre que la qualité de
service vise non pas à améliorer le débit, mais \emph{diminuer l'impact d'une
saturation du nombre de connexions en parallèle}. Or le FAI, qui propose toute
une panoplie de services et de plus en plus de \emph{services payants} (vidéo à
la demande, sauvegardes en ligne, email, sites thématiques, etc), a tout intérêt
à les rendre prioritaires lors d'une saturation de la connexion du client.
Surtout lorsque la concurrence propose le même type de service.

De cette manière, lors d'une saturation d'une connexion, et cela arrive vite
désormais avec les contenus en haute définition, le client perçoit que le
service concurrent est plus lent voire indisponible. Mais il ne réalise pas que
c'est son FAI qui ne dispose pas d'un réseau adapté à ses besoins, croyant
plutôt qu'il s'agit de la plateforme concurrente qui est de piètre qualité.
C'est un gain double pour l'opérateur qui n'a pas besoin de réinvestir dans de
l'infrastructure et dont la réputation reste inaltérée dans l'esprit de son
client. Fort heureusement, certains sites de la presse spécialisée ont commencé
l'an passé à révéler de telles pratiques discriminatoires.

Enfin, les FAI pratiquent également de la \emph{discrimination entre opérateurs
et fournisseurs de contenus !} Comme abordé dans le chapitre précédent,
Internet était conçu comme \emph{un réseau mondial d'organisations et
d'entreprises désireuses de s'interconnecter pour partager les flux de données}.
Cependant, les usages et les protocoles ont évolué en s'éloignant de cette
philosophie de départ : au lieu d'être un réseau mondial d'échange, avec des
flux quasiment symétriques, \textbf{le contenu
%
\footnote{Note : Ici, la notion de contenu est surtout quantitative que
qualitative : les emails et la communication texte sur les réseaux sociaux
représentent une énorme quantité d'information, mais l'espace de stockage et la
quantité de données binaires transmise sur le réseau est faible. Dans cette
section, il s'agit bien d'aborder les problématiques liées à d'importantes
quantités transférées, comme avec la consultation de vidéos en ligne.}
%
est de plus en plus centralisé}, réparti entre
quelques hébergeurs importants. Les clients \textbf{consomment ce contenu et en
partagent relativement peu} : la situation crée une \textbf{asymétrie} entre le
\emph{trafic entrant} et le \emph{trafic sortant} de l'infrastructure du FAI.
Cependant les liens et l'infrastructure d'Internet ne sont pas gratuits et les
\emph{opérateurs de Tiers 2 et Tiers 1} \textbf{facturent} le débit disponible à
un tarif très élevé. Là où la symétrie permet de négocier un échange de bon
procédé (\emph{« à quoi bon te facturer N puisque tu vas me facturer N ? »}),
l'asymétrie fait naturellement payer la partie qui crée le déséquilibre.

Pour compenser, les FAI veulent faire payer les entreprises qui génèrent ou
hébergent la majeure partie du contenu : plateformes de partage de fichiers, de
vidéo ou encore les moteurs de recherche. Mais c'est là la mise en place d'un
\textbf{marché biface où le FAI, intermédiaire d'Internet, cherche à être payé
deux fois, par ses clients et par les sources de contenu.}

Les ressources et les services les plus utilisés par les clients finaux
sont donc de plus en plus « bridés » et régulés dans le but de limiter la
facture des FAI. Cependant, cette pratique commence à être également connue du
grand public, ou tout du moins des utilisateurs les plus avancés. De plus les
opérateurs concurrents en profitent généralement pour appuyer ce discours pour
promouvoir leur infrastructure, mais ce n'est qu'un jeu de l'arroseur arrosé au
fur et à mesure que ces pratiques discriminatoires sont détectées…

Au final, les enjeux de la neutralité des réseaux \emph{sur le plan technique}
concernent trois aspects distincts mais complémentaires dans l'expérience de
l'utilisateur final :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item \textbf{Non différenciation des flux et des protocoles}\\ En respectant
  une égalité entre tous les \textbf{protocoles}, toutes les \textbf{adresses de
  destination ou d'origine} et tous les \textbf{contenus}, qu'ils représentent
  90\% ou 0.001\% du trafic total. C'est également essentiel pour assurer
  l'essor de nouveaux protocoles permettant de nouveaux usages, le tout dans une
  \textbf{approche décentralisée}, qui ne favorise pas un acteur puissant en
  particulier par rapport à des entreprises ou initiatives plus modestes dans
  leur ambition ou leurs moyens. Il en va de la richesse et de l'ouverture du
  réseau Internet.

  \item \textbf{Juste ordonnancement des flux}\\ En anglais, ce principe
  s'appelle le \emph{Best Effort}, principe neutre par nature, puisqu'il s'agit
  d'appliquer le minimum voire aucune hiérarchie entre les flux en laissant le
  réseau fonctionner au maximum de ses capacités si nécessaire. \textbf{Une
  approche plus pragmatique privilégie les protocoles vitaux au bon
  fonctionnement du réseau}. C'est aujourd'hui le compromis qui est retenu par
  la majorité des acteurs d'Internet.

  \item \textbf{Optimisation de la capacité utilisable des liens}\\ Ceux-ci
  représentent un investissement important et régulier, mais ils correspondent à
  un besoin utilisateur qui évolue en parallèle des nouveaux usages et des
  nouveaux services. C'est la responsabilité de l'opérateur, en plus d'être une
  plus-value face à la concurrence, de permettre un accès à Internet
  garantissant la meilleure expérience possible, sans bridage pour des raisons
  économiques ou stratégiques.
\end{itemize}

\emph{Sur le plan économique}, nous pouvons retenir les points suivants :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item \textbf{Respect du rôle d'intermédiaire, non abusif}\\ Le FAI ne doit
  pas faire payer deux fois son activité d'intermédiaire, par ses utilisateurs
  et les fournisseurs de contenu.

  \item \textbf{Respect du client et de l'accès au réseau}\\ Les différentes
  offres du FAI doivent refléter une vraie plus-value en terme de service ou de
  contenu et certainement pas en terme de «qualité de connexion» ou de
  performance : c'est une violation du principe de \emph{Best Effort} et une
  discrimination basée sur les revenus d'un individu, avec des notions marketing
  ineptes telles que  «accès \emph{premium}» ou «accès dégradé».

  \item \textbf{Établissement de liens logiques et non économiques}\\ Pour
  relier deux FAI français, il est plus économique de passer par Londres…
  Pourtant cela va à l'encontre de la logique la plus élémentaire, tout en
  dégradant la qualité des communications par une plus grande latence et un
  débit moindre.
\end{itemize}

\subsection{Enjeux juridiques}

L'aspect juridique de la neutralité des réseaux est encore aujourd'hui difficile
à aborder car il n'y a pas vraiment de loi en sa faveur. En France, la
plupart des lois régissant certaines pratiques ou limitations autour d'Internet
sont des lois de contrôle, de surveillance ou de censure… Cela donne peu
d'espoir de voir un jour une loi garantir la neutralité des réseaux, même si des
efforts sont fait à l'échelle européenne avec le \textbf{Paquet Télécom} qui
désigne un ensemble de mesures qui délimitent un cadre commun aux états membres
concernant les conditions d'accès et d'utilisation d'Internet, le renforcement
des droits des consommateurs, la réduction de la fracture numérique et le
déploiement du très haut débit. Malheureusement d'autres mesures se sont
greffées pendant les débats et encensent la mise en place de mécanismes de
censure immédiate sans jugement au tribunal et le soutien de dispositifs de
riposte graduée pouvant aboutir sur la déconnexion de l'abonné final, tel
qu'HADOPI en France.

Les points suivant synthétisent les enjeux juridiques de la neutralité des
réseaux dans le contexte actuel et à venir :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item \textbf{Respect de l'utilisateur et de son trafic réseau}\\ Certaines
  mesures légales autorisent des mesures techniques explorant le trafic de
  l'utilisateur, en compromettant ainsi la confidentialité de son contenu
  consulté et produit ainsi que sa vie privée.

  \item \textbf{Application d'une censure légale, appliquée juridiquement}\\ Si
  les lois existantes en France permettent actuellement de rendre inaccessibles
  des sites pédophiles, violents ou racistes grâce à la loi \emph{LOPPSI}
  %
  \footnote{Loi d'orientation et de programmation pour la performance de la
  sécurité intérieure.},
  %
  le procédé est une atteinte grave à la neutralité des réseaux, tout
  particulièrement depuis l'existence de plusieurs précédents où le site fermé
  ne relevait pas des thèmes traités par cette loi. De plus \textbf{aucune des
  solutions actuelles n'est réellement efficace contre une certaine compétence
  en informatique}, seuls les utilisateurs les moins avertis subissent donc le
  filtrage. D'après le collectif citoyen «\emph{La Quadrature du Net}», de
  telles lois ne résolvent rien :

  \begin{center}\emph{
  «Au final, des sommes astronomiques vont être
  dépensées sans apporter l'ombre d'une solution au problème de la pédophilie.»
  }\end{center}

\end{itemize}

\subsection{Enjeux politiques et sociaux}

Internet et l'utilisation massive des réseaux sociaux ont joué un rôle
primordial en 2011, notamment pendant le printemps arabe et les révolutions
impliquées. Les régimes ayant pris soin de bâillonner les médias dit
«classiques», les activistes ont très vite compris l'intérêt d'utiliser
Internet qui, décentralisé et global, est difficile à contrôler par un unique
état. De la préparation des émeutes jusqu'à une couverture médiatique
alternative témoignant de la violence et des répressions, il est probable qu'en
2011 le réseau Internet démontra son importance comme canal d'information
populaire et accessible.

Pourtant, les régimes autoritaires menacés par ces actions ont pris la menace
d'Internet très au sérieux et tous tentèrent de bâillonner les communications
informatiques. Certains se limitèrent à un filtrage inefficace des sites web des
réseaux sociaux, destinés à stopper les utilisateurs les moins avancés
techniquement, tandis que d'autres prirent des décisions folles : l'Égypte,
coupant tous les liens réseaux
%
\footnote{Il s'agit ici de couper les sessions du protocole \textbf{BGP} qui
permet aux différents operateurs (\textbf{Autonomous Systems}) d'échanger les
routes qui composent le réseau Internet.}
%
existants avec les opérateurs, a ainsi disparu entièrement d'Internet pendant
presque une semaine, avec plus de 91\% du réseau égyptien indisponible !

Ces faits constituent ici un précédent grave et unique en son genre, violant non
seulement les principes de la neutralité des réseaux, mais plus gravement encore
la liberté d'expression et de communication des peuples.

\clearpage

Concernant le rôle d'Internet dans les démocraties, Benjamin Bayart
%
\footnote{Benjamin Bayart, né le 24 octobre 1973, est expert en
télécommunications et président de \emph{French Data Network}, le plus ancien
fournisseur d’accès à Internet en France encore en exercice. Militant pour les
libertés fondamentales dans la société de l'information par la neutralité du net
et le logiciel libre, ses prises de positions en ont fait une personnalité
remarquée de l'Internet français.}
%
déclara en 2009 dans l'ouvrage collectif «La Bataille HADOPI» :

\begin{center}\emph{
«L'imprimerie a permis au peuple de lire, Internet va lui permettre d'écrire»
}\end{center}

Ces quelques exemples récents permettent d'exposer les enjeux sociaux et
politiques suivants :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item \textbf{Promouvoir l'accès universel à la communication}\\
  Grâce à une offre proposant des conditions et frais d'accès au réseau
  Internet, dont la nature et les montants sont au choix du FAI dans un contexte
  de marché ouvert et concurrentiel pour le bien du consommateur. Bien
  que reposant sur des indices essentiellement économiques (frais) et techniques
  (déploiement de haut débit, couverture maximale), c'est surtout un enjeu
  social car il implique un \textbf{accès égalitaire à la sphère
  communicationnelle.}

  \item \textbf{Respecter la liberté d'expression}\\
  La libre communication des pensées et des opinions est un droit fondamental du
  citoyen. Bien entendu, le cadre de la loi s'applique ici et les contenus ou
  usages doivent respecter ce cadre. Chaque nation dispose de sa propre
  législation concernant la liberté d'expression, mais il n'existe aucune raison
  de différencier la liberté de la presse à la liberté d'expression sur Internet
  : le réseau mondial n'est pas un sous-produit d'un des médias de masse
  traditionnel, mais au contraire un espace public ouvert que nous devons
  protéger.

  \item \textbf{Soutenir Internet comme progrès démocratique}\\
  Internet représente un bon en avant considérable quantitativement et
  qualitativement sur le plan des sources d'informations par rapport aux médias
  traditionnels où trop souvent le lecteur (utilisateur du support) et citoyen
  est souvent passif. En proposant une information plus ouverte provenant de
  sources multiples et en valorisant l'espace public virtuel en permettant au
  plus grand nombre d'accéder à Internet, \emph{«la société en réseau rend les
  individus plus autonomes et potentiellement plus proactifs»}.
\end{itemize}

\section{Positions défendues en France}

Le cas français est unique dans le monde : c'est un pays libre et républicain,
dont la tradition démocratique et l'image de Liberté associée est diffusée comme
une aura politique mondiale. Pourtant, c'est également le pays qui a vu se
mettre en place depuis 2008 le plus de lois et de mesures cherchant à contrôler,
réguler et censurer Internet. Pourtant la plupart de ces mesures sont
techniquement inefficaces et comportent de dangereux effets de bords, allant à
l'encontre de la traditionnelle image de liberté.

\emph{Lobbying} des majors ? Incompréhension et crainte de la nouveauté ? Réelle
volonté de contrôle des citoyens ? Ces causes possibles sont hors contexte dans
cette analyse de la neutralité des réseaux, mais au final, que cela soit par
effet indirect ou direct, c'est bien l'utilisateur final qui expérimente un
accès biaisé ou dégradé sur Internet.

Parmi les opposants à la neutralité des réseaux, on retrouve bien évidemment
les grands FAI français, qui veulent proposer des offres basées sur une
segmentation de la qualité technique de la connexion, mais également assurer
l'investissement dans les infrastructures réseaux à la fois par leurs clients
(les utilisateurs terminaux, considérés comme des consommateurs «purs» sans
tenir compte de l'évolution des usages) et les grands fournisseurs de contenu
comme Google, Microsoft, Dailymotion, etc.

On retrouve également certains membres du gouvernement ou politiciens qui voient
en Internet soit une bête noire, soit un \emph{far-west} à «nettoyer», soit un
moyen de contrôle et de renseignement sur les citoyens. D'autres acteurs
économiques, dont l'intérêt est de créer ou de conserver une position de
monopole, s'investissent contre la neutralité des réseaux.

Le clan en faveur de la neutralité des réseaux se compose essentiellement
d'intervenants techniques, pour qui la nature même de la notion de neutralité
est logique et qui respecte les origines historiques d'Internet. On y retrouve
également des associations de protection des consommateurs, des
universitaires… Leur point commun est de défendre un Internet libre, ouvert,
dynamique grâce à sa nature acentrée, respectueux des utilisateurs et de leurs
contenus, qu'il soient consultés ou produits.

\section{Exemples concrets}

Ci dessous sont présentés quelques exemples concrets et avérés selon la nature
de l'atteinte portée à la neutralité des réseaux. Dans tous les exemples ci
dessous, la nomenclature suivante est employée :

\begin{itemize}
  \item \textbf{\#A} désigne un premier FAI.
  \item \textbf{\#B} désigne un second FAI.
  \item \textbf{\$U1} désigne un premier utilisateur d'un des deux FAI.
  \item \textbf{\$U2} désigne un second utilisateur d'un des deux FAI.
\end{itemize}

\subsection{Discrimination à l'égard de la destination}

\textbf{\$U1}, client d'une offre du FAI \textbf{\#A}, constate que l'accès à sa
plateforme de vidéos légales est indisponible ou que le débit est très lent en
comparaison d'autres sites sponsorisés par le FAI \textbf{\#A}. La plateforme de
vidéo à la demande du FAI est quand à elle très rapide !

\subsection{Discrimination à l'égard de la source}

\textbf{\$U1}, client du FAI \textbf{\#A}, constate que la vitesse de transfert
d'un fichier hébergé au sein de l'infrastructure de \textbf{\#B} est nettement
moins élevée qu'avec le même fichier transféré depuis un serveur de
\textbf{\#A}. Son ami \textbf{\$U2}, client du FAI \textbf{\#B}, ne constate pas
ce problème et profite d'excellents débits quel que soit le serveur source.

\subsection{Discrimination à l'égard du contenu}

Un beau jour, le FAI grand public \textbf{\#A} annonce publiquement qu'entre 18h
et 4h du matin, certains protocoles verraient leur débit maximal divisé par
deux, afin que son propre service de télévision payante ne soit pas saturé, même
si l'abonné n'a pas souscrit l'option TV.
