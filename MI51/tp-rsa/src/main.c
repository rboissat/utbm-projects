/*****************************************************************************
 *  Course:   MI51 - TP RSA
 *  Semester: P10
 *  Author:   Romain Boissat
 *  File:     main.c
 *
 *  Main program. Uses code from tool.h as a RSA algorithm proof of concept.
 *
 ****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "tools.h"

#define NORMAL_OUTPUT stdout
#define ERROR_OUTPUT stderr

/* Prototypes */
int dispatcher      (int, char**);
void print_error    (void);
void print_help     (void);

void test_primality (BigInt);
void test_gcd (BigInt, BigInt);
void test_prime_factorization (BigInt);
void test_eeuclid (BigInt, BigInt);
void test_modexp (BigInt, BigInt, BigInt);

void tp(void);

/****************************************************************************/
int main (int argc, char *argv[]) {
  if (argc > 1) {
    int ret_val = dispatcher(argc, argv);
    return ret_val;
  } else {
    print_error();
    return 1;
  }

  return 0;
}

/****************************************************************************/
/* Dispatcher: according to runtime arguments, execute the right sub-function */
int dispatcher (int argc, char** argv) {

  if (argc == 3 && !strcmp(argv[1],"--is-prime") 
                && is_int(argv[2]))
  {
    BigInt nb = strtoll(argv[2], (char **) NULL, 10);
    test_primality(nb);
    return 0;

  } else if (argc == 4  && !strcmp(argv[1],"--gcd")
                        && is_int(argv[2])
                        && is_int(argv[3]))
  {
    BigInt a = strtoll(argv[2], (char **) NULL, 10);
    BigInt b = strtoll(argv[3], (char **) NULL, 10);
    test_gcd(a,b);
    return 0;

  } else if (argc == 3  && !strcmp(argv[1],"--pfactor")
                        && is_int(argv[2]))
  {
    BigInt nb = strtoll(argv[2], (char **) NULL, 10);
    test_prime_factorization(nb);
    return 0;

  } else if (argc == 4  && !strcmp(argv[1],"--eeuclid")
                        && is_int(argv[2])
                        && is_int(argv[3]))
  {
    BigInt a = strtoll(argv[2], (char **) NULL, 10);
    BigInt b = strtoll(argv[3], (char **) NULL, 10);
    test_eeuclid(a,b);
    return 0;

  } else if (argc == 5  && !strcmp(argv[1],"--modexp")
                        && is_int(argv[2])
                        && is_int(argv[3]))
  {
    BigInt a = strtoll(argv[2], (char **) NULL, 10);
    BigInt b = strtoll(argv[3], (char **) NULL, 10);
    BigInt m = strtoll(argv[4], (char **) NULL, 10);
    test_modexp(a,b,m);
    return 0;

  } else if (argc == 2 && !strcmp(argv[1],"--tp"))
  {
    tp();
    return 0;

  } else if (getopt(argc, argv, "-h")) {
    print_help();
    return 0;

  } else {
    print_error();
    return 1;
  }
}

void print_error (void) {
  fprintf(ERROR_OUTPUT,"Usage: ./tp-rsa ACTION\n");
  fprintf(ERROR_OUTPUT,"Use './tp-rsa -h' to list available actions.\n");
}
void print_help (void) {
  fprintf(NORMAL_OUTPUT,"Usage: ./tp-rsa ACTION\n\n");
  fprintf(NORMAL_OUTPUT,"Available actions:\n\
    * --is-prime INTEGER \t\t tests if INTEGER is a prime integer.\n\
    * --gcd INTEGER_1 INTEGER_2 \t prints gcd(INTEGER_1,INTEGER_2).\n\
    * --pfactor INTEGER \t\t prints prime factorization of INTEGER.\n\
    * --eeuclid INTEGER_1 INTEGER_2 \t prints (u,v) | a*u+b*v = gcd(a,b)\n\
    * --modexp BASE EXP MOD \t\t prints BASE^EXP mod MOD\n\
    * --tp \t\t\t\t execute the TP.\n\
    * -h \t\t\t\t prints this help.\n\
    \n");
}

/*********************** TESTS HELPER FUNCTIONS ******************************/
void test_primality (BigInt n) {
  if (is_prime(n))
    printf("%llu is a prime integer.\n\n", n);
  else
    printf("%llu is not a prime integer.\n\n", n);
}

void test_gcd (BigInt a, BigInt b) {
  printf("gcd(%llu,%llu) = %llu\n\n", a, b, gcd(a,b));
}

void test_prime_factorization (BigInt n) {
  if (n > 1)
  {
    int i;
    int len;
    BigInt *factors = prime_factorization(n, &len);

    printf("Prime factorization: %llu = ", n);
    for (i=0; i < len - 1; i++) {
      printf("%llu * ", factors[i]);
    }
    printf("%llu\n\n", factors[len - 1]);
    free(factors);
  }
}

void test_eeuclid (BigInt a, BigInt b) {
  BigInt u,v;

  printf("gcd(%llu,%llu) = %llu\n\n", a, b, gcd(a,b));
  extended_euclidean(a, b, &u, &v);
  printf("(u,v) = (%llu,%llu)\n\n", u, v);
}

void test_modexp (BigInt b, BigInt e, BigInt m) {
  printf("%llu^%llu mod [%llu] = %llu\n\n", b, e, m, mod_expo(b,e,m));
}

/****************************************************************************/
void tp(void) {
  BigInt n = 1370477;
  BigInt b = 377;
  BigInt p, q;
  BigInt* factors;
  int len, i;

  printf("\t\t-- TP RSA --\nn=%llu b=%llu\n\n", n, b);

  /* Q1 */
  printf("------ Q1 ------\n");
  factors = prime_factorization(n, &len);
  p = factors[0];
  q = factors[1];

  printf("Prime factorization: %llu = ", n);
  for (i=0; i < len - 1; i++) {
    printf("%llu * ", factors[i]);
  }
  printf("%llu\n\n", factors[len - 1]);

  test_primality(p);
  test_primality(q);
  free(factors);

  /* Q2 */
  printf("------ Q2 ------\n");
  BigInt phi = (p - 1) * (q - 1);
  printf("phi = (p - 1) * (q - 1) = %llu\n", phi);

  /* Q3 */
  printf("\n------ Q3 ------\n");
  BigInt a;
  BigInt tmp;
  extended_euclidean(b, phi, &a, &tmp);
  printf("Private Key a = %llu\n", a);

  /* Q4 */
  printf("\n------ Q4 ------\n");
  BigInt crypted_message[] = {959334, 1330268, 1344347, 752301, 737637, 62455,\
      295329, 691484, 167338, 277609, 963487, 1, 167338, 1056579, 213179,\
      570345, 963487, 1330268, 363080, 329789};

  char* raw = decipher(crypted_message, 20, a, n);
  printf("Raw deciphered message:      %s", raw);
  char *cleartext = split_decode(raw, strlen(raw));
  printf("\nDecoded deciphered message:  %s\n", cleartext);
}

