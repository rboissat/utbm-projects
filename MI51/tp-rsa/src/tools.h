/*****************************************************************************
 *  Course:   MI51 - TP RSA
 *  Semester: P10
 *  Author:   Romain Boissat
 *  File:     tools.h
 *
 *  Header file of tools.c.
 *
 ****************************************************************************/

#ifndef _RSA_TOOLS
#define _RSA_TOOLS
#define BigInt unsigned long long int

/* PROTOTYPES */
int is_int(char*);
int is_prime(BigInt);
BigInt* prime_factorization(BigInt, int*);
BigInt gcd (BigInt, BigInt);
void extended_euclidean (BigInt, BigInt, BigInt*, BigInt*);
BigInt mod_expo (BigInt, BigInt, BigInt);
char* split_decode (char*, int);
char* decipher (BigInt*, int, BigInt, BigInt);

#endif
