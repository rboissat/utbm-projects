/*****************************************************************************
 *  Course:   MI51 - TP RSA
 *  Semester: P10
 *  Author:   Romain Boissat
 *  File:     tools.c
 *
 *  Small library.
 *
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools.h"

int is_int (char *string) {
  int pos;
  for (pos = (string[0] == '-') ? 1 : 0; string[pos] != '\0'; pos++)
    if (string[pos] < '0' || string[pos] > '9')
      return 0;
  return 1;
}

int is_prime (BigInt n) {
  BigInt d;
  if (n <= 2 && n > 0)
    return 1;

  if (n%2 == 0)
    return 0;

  for (d = 3; (d*d) <= n; d+=2)
    if (n % d == 0)
      return 0;

  return 1;
}

BigInt* prime_factorization (BigInt n, int *length) {
  BigInt *factors = NULL;
  int l = 0;

  while (n > 1) {
    /* If n is already a prime number, n = n * 1 ... */
    if (is_prime(n)) {
      l++; /* we have 1 prime factor */
      factors = realloc (factors, l * sizeof(BigInt));
      factors[l-1] = n;
      break;
    }
    /* If n is even */
    if (n%2 == 0) {
      l++;
      factors = realloc (factors, l * sizeof(BigInt));
      factors[l-1] = 2;
      n /= 2;
    } else {
      BigInt d;
      for (d = 3; (d*d) < n; d += 2) {
        if (n%d == 0) {
          l++;
          factors = realloc (factors, l * sizeof(BigInt));
          factors[l-1] = d;
          n /= d;
          break;
        }
      }
    }
  }
  *length = l;

  return factors;
}

BigInt gcd (BigInt a, BigInt b) {
  while (b) {
    BigInt c = a % b;  /* remainder of a/b */
    a = b;          /* a becomes b */
    b = c;          /* b becomes the former remainder */
    /* At last iteration, a yields the gcd, since b = former remainder = 0 */
  }

  return a;
}

void extended_euclidean (BigInt a, BigInt b, BigInt *u, BigInt *v) {

 BigInt p=1, q=0, r=0, s=1;
 BigInt c, quotient, new_r, new_s;

  while (b != 0) {
    c = a % b;
    quotient = a / b;
    a = b;
    b = c;

    new_r = p - quotient * r;
    new_s = q - quotient * s;

    p = r;
    q = s;

    r = new_r;
    s = new_s;
  }
  *u = p;
  *v = q;
}

BigInt mod_expo (BigInt base, BigInt exp, BigInt mod) {
  BigInt res = 1;

  while (exp) {
    if ((exp & 1) > 0)
      /* multiply in this bit's contribution while using modulus */
      res = (res * base) % mod;
    /* move on the next bit of the exponent, square and mod the base accordingly*/
    exp >>= 1;
    base = (base * base) % mod;
  }

  return res;
}

char* decipher (BigInt *input, int len, BigInt a, BigInt n) {
  char output[len];
  char tmpbuff[len/2];
  BigInt m = 0;
  int i;

  output[0] = '\0';
  tmpbuff[0] = '\0';

  for (i = 0; i < len/2; i++) {
    m = mod_expo(input[i], a, n);
    sprintf(strchr(output,0), "%03llu", m);
  }

  for (i = len/2; i < len; i++) {
    m = mod_expo(input[i], a, n);
    sprintf(strchr(tmpbuff,0), "%03llu", m);
  }
  strcat(output, tmpbuff);

  return strdup(output);
}

char* split_decode (char* input, int len) {
  char output[len];
  unsigned int i;

  output[0] = '\0';

  for (i = 0; i < strlen(input); i+=2) {
    sprintf(strchr(output,0),"%c", (((input[i]-48)*10 + (input[i+1]-48)) +96));
  }

  return strdup(output);
}

