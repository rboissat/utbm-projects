\subsection{Analyse du code}
Cette section présente une analyse du code des fonctions importantes du TP
RSA. Dans les sources, une macro définit le type \emph{BigInt} comme
\emph{unsigned long long}.

\subsubsection{Test de primalité}
La première fonction importante est la fonction de test de la primalité d'un
nombre entier. La fonction \textbf{is\_prime} prend en entrée un \emph{BigInt}
et retourne 0 si cet entier n'est pas premier, ou 1 s'il est premier.
\vspace*{8pt}

\begin{small}
\lstinputlisting[language=C,firstline=24,lastline=37,numbers=left]{../src/tools.c}
\end{small}

\begin{itemize}
  \item{\textbf{L. 3-4 :} un nombre premier est positif, et 1 ainsi que 2 sont
  considérés comme premiers.}
  \item{\textbf{L. 6-7 :} un nombre pair n'est pas premier : on élimine donc la
  moitié des cas.}
  \item{\textbf{L. 9-11 :} test de tous les entiers impairs dans l'intervalle
  $[3;\sqrt{n}]$.}
  \item{\textbf{L. 13 :} aucun diviseur n'a été trouvé, l'entier est premier.}
\end{itemize}

\subsubsection{Décomposition en facteurs premiers}
La seconde fonction requise permet la décomposition d'un nombre entier en une
suite de facteurs premiers. Cette méthode est extrèmement basique, et donc très
peu optimisée en terme de temps de calcul. Cet algorithme est donc applicable
sur des entiers « relativement » petits.

La fonction prend en paramètre un grand entier \emph{BigInt} à décomposer en
facteurs premiers, ainsi qu'un pointeur vers un entier dans lequel le nombre de
facteurs premiers sera retourné. La fonction retourne le tableau des facteurs
premiers. Ce tableau étant alloué dans le corps de la fonction, il conviendra de
le libérer de la mémoire via un appel à \textbf{free()}:
\vspace*{8pt}

\begin{small}
\lstinputlisting[language=C,firstline=39,lastline=73,numbers=left]{../src/tools.c}
\end{small}

\begin{itemize}
  \item{\textbf{L 2 :} On initialise un tableau nul. Ce tableau contiendra les
  facteurs premiers de la décomposition.}
  \item{\textbf{L. 5 :} tant que la décomposition n'est pas terminée, on
  continue.}
  \item{\textbf{L. 6-12 :} si le nombre est premier, il n'y a pas d'autres
  facteurs dans la décomposition que le nombre lui même. On augmente alors la
  taille du tableau à 1 élément de type \emph{BigInt}, on assigne l'entier à
  l'unique case du tableau et on sort de la boucle.}
  \item{\textbf{L. 13-18 :} si le nombre est pair, 2 est donc un des facteurs
  premiers. On agrandit alors le tableau, on y ajoute 2, et on divise le BigInt
  par deux pour la suite de la boucle.}
  \item{\textbf{L. 13-28 :} dans le dernier cas, on teste la division du
  \emph{BigInt} par les entiers impairs à partir de 3 jusqu'à $\sqrt{n}$. Si on
  trouve un diviseur, on augmente la taille du tableau, on le réalloue, et on
  ajoute de la même manière que les autres cas l'entier à la liste des facteurs
  premiers.}
  \item{\textbf{L. 32 :} on retourne la taille du tableau dans l'entier pointé
  par \emph{length}.}
  \item{\textbf{L. 34 :} on retourne l'adresse du tableau contenant les facteurs
  premiers.}
\end{itemize}

\subsubsection{Calcul du PGCD}
La fonction \textbf{gcd} permet de calculer le PGCD des deux \emph{BigInt}
passés en argument. La fonction retourne alors un \emph{BigInt} qui vaut le
PCGD. L'algorithme est simple : il s'agit des divisions successives du diviseur
de la division précédente par le reste de la division précédente, jusqu'à un
reste nul. le dernier quotient est alors le PGCD.
\vspace*{8pt}

\begin{small}
\lstinputlisting[language=C,firstline=75,lastline=84,numbers=left]{../src/tools.c}
\end{small}

\clearpage
\subsubsection{Algorithme d'Euclide étendu}
L'algorithme d'Euclide étendu permet le calcul de l'exposant de déchiffrement
\textbf{a}, car il permet de calculer les entiers \emph{(u,v)} de l'identité de
Bézout $a*u + b*v = pcgd(a,b)$. La fonction prend 4 arguments, deux \emph{BigInt},
correspondant à \emph{a} et \emph{b}, et deux pointeurs vers BigInt,
correspondant à \emph{u} et \emph{v}. Dans le cadre du TP, on a l'identité $b*x
+ \phi{(n)}*y = 1$ car $b$ et $\phi{(n)}$ sont premiers entre eux. En appliquant
$\bmod{(\phi{(n)})}$, on obtient la nouvelle identité $b*x = 1
\pmod{\phi{(n)}}$. Le $x$ retourné est donc l'inverse modulaire de $b$ (clé
publique), c'est-à-dire $a$ (clé privée).
\vspace*{8pt}

\begin{small}
\lstinputlisting[language=C,firstline=86,lastline=108,numbers=left]{../src/tools.c}
\end{small}

\subsubsection{Exponentiation modulaire}
L'exponentiation modulaire offre un avantage important pour l’algorithme de
chiffrement RSA. En effet, RSA repose sur des calculs du type
$a\exp{x}\pmod(m)$, avec $x$ pouvant être grand. Au lieu de brutalement
calculer $a^x$ puis de calculer le modulo, le modulo est appliqué à
chaque calcul intermédiaire. L'algorithme de cette fonction est fourni dans le
livre de Bruce Schneier \emph{Applied Cryptography}. La fonction prend 3
arguments : le \emph{BigInt} de base ($a$), le \emph{BigInt} d'exposant ($x$) et
le \emph{BigInt} modulo ($m$).

\begin{small}
\lstinputlisting[language=C,firstline=110,lastline=123,numbers=left]{../src/tools.c}
\end{small}

\begin{itemize}
  \item{\textbf{L. 3 :} tant qu'on a pas parcouru tous les bits de l'exposant,
  on continue.}
  \item{\textbf{L. 4 :} si le bit courant est à 1, il est pertinent de calculer,
  sinon on continue la boucle sans calcul.}
  \item{\textbf{L. 7 :} on calcule le modulo du produit de la base courante avec
  la base précédente.}
  \item{\textbf{L. 9-10 :} on décale d'un bit vers la droite l'exposant, d'où
  le passage la base au carré, calculée modulo $mod$.}
  \item{\textbf{L. 13 :} on retourne le résultat.}
\end{itemize}

\subsubsection{Déchiffrement}
La fonction de déchiffrement permet d'obtenir une chaîne de caractères composée
de chiffres. Cette chaîne sera ensuite découpée et décodée depuis les valeurs
ASCII, afin d'obtenir alors une chaîne de caractères compréhensible par un être
humain. Ce traitement est effectué par une autre fonction
\textbf{split\_decode}. Ici, la fonction \textbf{decipher} effectue uniquement
le déchiffrement. Pour une raison inconnue, le traitement de déchiffrement doit
se faire en au moins deux temps, sur une fraction de la chaîne chiffrée, puis
sur l'autre. Sans cela, la chaîne déchiffrée est imcomplète, et ce comportement
n'est pas le même suivant les architectures 32 ou 64bits, ni encore selon la
version de la libc. C'est spécifiquement l'appel à \textbf{sprintf} qui
semblerait avoir un souci lors d'appels dans une boucle.
La fonction prend 4 arguments : un tableau de \emph{BigInt} contenant les
données chiffrées, sa longueur, l'exposant de déchiffrement et le modulo.

\begin{small}
\lstinputlisting[language=C,firstline=125,lastline=146,numbers=left]{../src/tools.c}
\end{small}

\begin{itemize}
  \item{\textbf{L. 1-8 :} initialisation de la chaîne de caractères qui sera
  retournée et des variables intermédiaires.}
  \item{\textbf{L. 10-13 :} on boucle sur la moitié du \emph{BigInt} chiffrée.
  On applique une exponentiation modulaire sur chaque chiffre du \emph{BigInt},
  que l'on ajoute en fin de la chaîne de caractère qui sera retournée. L'ajout
  se fait avec un formattage qui facilitera le découpage et le décodage en
  caractères ACSII de la chaîne déchiffrée..}
  \item{\textbf{L. 15-18 :} même boucle que précédemment sur la deuxième moitiée
  du \emph{BigInt}.}
  \item{\textbf{L. 19 :} on concatène les deux fractions de la chaîne
  déchiffrée.}
  \item{\textbf{L. 21 :} on retourne la chaîne qui contient la chaîne
  déchiffrée.}
\end{itemize}

\clearpage
\subsubsection{Découpage et décodage}
Cette fonction permet de traduire la chaîne déchiffrée, composée de chiffres, en
une chaîne de caractères ASCII, compréhensible par un humain. 

\begin{small}
\lstinputlisting[language=C,firstline=148,lastline=159,numbers=left]{../src/tools.c}
\end{small}

\begin{itemize}
  \item{\textbf{L. 2-5 :} initialisation de la chaine de charactère à
  retourner et de la variable de bouclage.}
  \item{\textbf{L. 7-9 :} on boucle de deux caractères en deux caractères sur la
  chaîne déchiffrée. On effectue alors la traduction d'un nombre composé de deux
  chiffres en caractère ASCII minuscule.}
  \item{\textbf{L. 11 :} on retourne la chaine décodée.}
\end{itemize}

