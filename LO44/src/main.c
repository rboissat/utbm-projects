/*****************************************************************************
 * PROJET LO44
 * Calculatrice 'Big Int'
 *
 * Fichier: 'main.c'
 *
 * Copyright (c) 2008 Romain BOISSAT, Antoine GAVOILLE
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *****************************************************************************/

/*
 *                          PROGRAMME PRINCIPAL
 *                                                                          */

/* Includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Include Lib BigInteger */
#include "lib/biginteger.h"

/* On definit les sorties standard
 * A adapter si on veut ecrire dans un fichier */
#define NORMAL_OUTPUT stdout
#define ERROR_OUTPUT stderr

/* Prototypes */
void print_error(void);
void print_help(void);
Boolean is_bigint(char *string);
void dispatch_op(char *op, char *nb1, char *nb2);

/* Programme principal */
int main(int argc, char *argv[]) {
  /* On verifie la bonne utilisation du programme:
   *  1 - On verifie si l'utilisateur veut afficher l'aide
   *  2 - On verifie le nombre d'arguments, on gere le cas des operateurs a
   *  entree unique */
  if (argc == 2 && strcmp(argv[1], "-h") == 0)
    print_help();
  else if (argc == 1 ||
          (argc != 4 && (strstr("! null sign",argv[1]) == NULL)) ||
          (argc != 3 && (strstr("! null sign",argv[1]) != NULL)))
    print_error();

  /* Verification des donnees d'entree, a savoir si l'utilisateur a bien saisi
   * deux entiers relatifs, et non pas des chaines de caractere arbitraires. */
  if (is_bigint(argv[2]) == FALSE ||
      ((argc == 4 && is_bigint(argv[3]) == FALSE)))
    print_error();

  /*  Si on a choisi un operateur a une entree, on a un seul nombre. */
  if (strstr("! null sign",argv[1]) != NULL)
    dispatch_op(argv[1], argv[2], NULL);
  else
    dispatch_op(argv[1], argv[2], argv[3]);

  return 0;
}

void print_error(void) {
  fprintf(ERROR_OUTPUT,"Usage: ./CalcBigInt [operator] [number1] \
[nullnumber2]\n");
  fprintf(ERROR_OUTPUT,"Use './CalcBigInt -h' to list available operators\n\n");

  exit(1);
}

void print_help(void) {
  fprintf(NORMAL_OUTPUT,"Usage: ./CalcBigInt [operator] [number_1] \
[number_2]\n");
  fprintf(NORMAL_OUTPUT,"Available operators:\n\
    * x   (multiply)\n\
    * +   (add)\n\
    * -   (substract)\n\
    * /   (divide and print the rest)\n\
    * gcd (print the Greatest Common Divisor)\n\
    * lcm (print the Least Common Multiple)\n\
    * !   (factorial, require only one *positive* int < 4 294 967 295)\n\
    * cnp (Combination: cnp k n = n!/(k!*(n-k)!), require two unsigned int)\n\
    WARNING: cnp and ! can be very memory hungry with big numbers.\n\
    You should use them cautiously!\n\
    ---------------------------------------------\n\
    Complementary operators: operators for testing internal functions\n\
    * null    (isNull(BigInteger): require only one bigint)\n\
    * sign    (signBigInt(BigInteger): require only one bigint)\n\
    * =       (equalsBigInt(BigInteger): require only one bigint)\n\
    * compare (compareBigint(BigInteger): require only one bigint)\n\n\
    Released under the General Public Licence V.2 - http://www.gnu.org\n\
    Copyright(c) 2008 - Romain BOISSAT, Antoine GAVOILLE\n\n");

  exit(0);
}

Boolean is_bigint(char *string) {
  int pos;
  for (pos = (string[0] == '-') ? 1 : 0; string[pos] != '\0'; pos++)
    if (string[pos] < '0' || string[pos] > '9')
      return FALSE;
  return TRUE;
}

void dispatch_op(char *op, char *nb1, char *nb2) {
  /* variable pour addition ou soustraction */
  int ope;

  /* On effectue une copie des donnees d'entrees reservee pour l'affichage */
  char *nb_tmp1 = malloc(strlen(nb1) + 1);
  strcpy(nb_tmp1, nb1);
  char *nb_tmp2 = "";
  /* On gere la factorielle */
  if (nb2 != NULL) {
    nb_tmp2 = malloc(strlen(nb2) + 1);
    strcpy(nb_tmp2, nb2);
  }

  /*  On convertit les donnees d'entree en BigInteger */
  BigInteger bi1 = newBigInteger(nb1);
  BigInteger bi2 = createBigInt();
  /* On gere les operateurs a une seule entree */
  if (nb2 != NULL)
    bi2 = newBigInteger(nb2);

  BigInteger result = createBigInt();

  /*  En fonction de l'operateur saisi, on effectue le calcul voulu */
  if (strcmp(op,"x") == 0) {
    fprintf(NORMAL_OUTPUT,"%s %s %s = ", nb_tmp1, op, nb_tmp2);
    result =  mulBigInt(bi1, bi2);
    printBigInteger(result);

  } else if (strcmp(op,"+") == 0) {
    ope = 1;
    fprintf(NORMAL_OUTPUT,"%s %s %s = ", nb_tmp1, op, nb_tmp2);
    result = sum_or_diff(ope, bi1, bi2);
    printBigInteger(result);

  } else if (strcmp(op,"-") == 0) {
    ope = -1;
    fprintf(NORMAL_OUTPUT,"%s %s %s = ", nb_tmp1, op, nb_tmp2);
    result = sum_or_diff(ope, bi1, bi2);
    printBigInteger(result);

  } else if (strcmp(op,"/") == 0) {
    result = quotientBigInt(bi1, bi2);
    BigInteger leftover = restBigInt(bi1, bi2);
    fprintf(NORMAL_OUTPUT,"%s %s %s = ", nb_tmp1, op, nb_tmp2);
    printBigInteger(result);
    fprintf(NORMAL_OUTPUT,"leftover = ");
    printBigInteger(leftover);

  } else if (strcmp(op,"gcd") == 0) {
    /* met le bigint le plus grand en premier pour le calcul */
    if (compareBigInt (bi1, bi2) == 1)
      result = gcdBigInt(bi1, bi2);
    else
      result = gcdBigInt(bi2, bi1);

    fprintf(NORMAL_OUTPUT,"%s(%s,%s) = ", op, nb_tmp1, nb_tmp2);
    printBigInteger(result);

  } else if (strcmp(op,"lcm") == 0) {
    /* met le bigint le plus grand en premier pour le calcul */
    if ( compareBigInt (bi1, bi2) == 1)
      result = lcmBigInt(bi1, bi2);
    else
      result = lcmBigInt(bi2, bi1);

    fprintf(NORMAL_OUTPUT,"%s(%s,%s) = ", op, nb_tmp1, nb_tmp2);
    printBigInteger(result);

  } else if (strcmp(op,"!") == 0) {
    if (bi1->sign == -1) {
      fprintf(ERROR_OUTPUT,"Error: negative int is forbidden\n");
      exit(1);
    }
    unsigned int ui_nb1 = (unsigned int) strtoul(nb1, NULL, 10);
    result = factorial(ui_nb1);
    fprintf(NORMAL_OUTPUT,"%s%u = ", op, ui_nb1);
    printBigInteger(result);

  } else if (strcmp(op,"cnp") == 0) {
    unsigned int ui_nb1 = (unsigned int) strtoul(nb1, NULL, 10);
    unsigned int ui_nb2 = (unsigned int) strtoul(nb2, NULL, 10);
    result = cnp(ui_nb1, ui_nb2);
    fprintf(NORMAL_OUTPUT,"%s(%u,%u) = ", op, ui_nb1, ui_nb2);
    printBigInteger(result);

  } else if (strcmp(op,"null") == 0) {
    if (isNull(bi1))
      fprintf(NORMAL_OUTPUT,"Is null? TRUE\n");
    else
      fprintf(NORMAL_OUTPUT,"Is null? FALSE\n");

  } else if (strcmp(op,"sign") == 0) {
    switch (signBigInt(bi1)) {
      case -1:
        fprintf(NORMAL_OUTPUT,"Sign: Negative\n");
        break;
      case 1:
        fprintf(NORMAL_OUTPUT,"Sign: Positive\n");
        break;
      default:
        fprintf(ERROR_OUTPUT,"Error!\n");
        exit(1);
    }

  } else if (strcmp(op,"=") == 0) {
    switch (equalsBigInt(bi1,bi2)) {
      case TRUE:
        fprintf(NORMAL_OUTPUT,"Are equals? TRUE\n");
        break;
      case FALSE:
        fprintf(NORMAL_OUTPUT,"Are equals? FALSE\n");
        break;
      default:
        fprintf(ERROR_OUTPUT,"Error!\n");
        exit(1);
    }

  } else if (strcmp(op,"compare") ==0) {
    switch (compareBigInt(bi1,bi2)) {
      case 0:
        fprintf(NORMAL_OUTPUT,"The bigints are equals\n");
        break;
      case 1:
        fprintf(NORMAL_OUTPUT,"bi1 is bigger than bi2\n");
        break;
      case -1:
        fprintf(NORMAL_OUTPUT,"bi1 is smaller than bi2\n");
        break;
      default:
        fprintf(ERROR_OUTPUT,"Error!\n");
        exit(1);
    }

  } else {
    print_error();
  }
  /* On libere la memoire allouee a la copie d'affichage et aux BigInt */
  free(bi1);
  free(bi2);
  free(result);
  free(nb_tmp1);
  /* On gere les operateurs a une seule entree */
  if (nb2 != NULL)
    free(nb_tmp2);
}
/*  EOF */
