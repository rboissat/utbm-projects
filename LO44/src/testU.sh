#!/bin/bash

PROG=./CalcBigInt
ARGS=('+' '-' 'x' '/' 'gcd' 'lcm' 'compare' '=')
ARGS2=('null' 'sign')

if [[ $1 = "-h" || $1 = ""
                || $2 = ""
                || $1 =~ ^.*[a-zA-Z]{1,}.*$
                || $2 =~ ^.*[a-zA-Z]{1,}.*$
                || $# -gt 2 ]]
then
  printf "Usage: ./testU.sh [BigInt_1] [BigInt_2]\n"
  exit
fi

for items in ${ARGS[*]}; do $PROG $items $1 $2; done

printf "\nConcerning '$1' only:\n"
echo "------------------------------"

for items in ${ARGS2[*]}; do $PROG $items $1; done

if [[ $1 -gt 2000 ]]
then
  printf "'!' operator is very memory hungry with numbers > 2000\n"
else
  $PROG '!' $1
fi

printf "\n"
echo "------------------------------"
printf "cnp' operator is very memory hungry with numbers > 100\n"
printf "Please use with precaution\n"
printf "If this script seems to freeze, please interrupt it with ^C\n"

$PROG 'cnp' $1 $2

#EOF
