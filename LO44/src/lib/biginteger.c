/*****************************************************************************
 * PROJET LO44
 * Calculatrice 'Big Int'
 *
 * Fichier: 'lib/biginteger.c'
 *
 * Copyright (c) 2008 Romain BOISSAT, Antoine GAVOILLE
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *****************************************************************************/

/*
 *                          BIBLIOTHEQUE BigInt
 *                                                                          */

/* INCLUDES */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "./biginteger.h"

/* On définit les sorties standard
 * A adapter si on veut écrire dans un fichier */
#define NORMAL_OUTPUT stdout
#define ERROR_OUTPUT stderr

/*====================== fonction créer un BigInt =======================*/

BigInteger createBigInt() {
  BigInteger bi;

  bi = (BigInteger) malloc (sizeof(BigInt));

  bi->absValHead = NULL;
  bi->absValTail = NULL;
  bi->sign = 1;

  return bi;
}

/*====================== fonction ajouter en tete =======================*/

void add_head(BigInteger bi, unsigned int n) {
  list temp;

  temp = (element*) malloc (sizeof(element));
  temp->val = n;
  temp->prev = NULL;
  temp->next = bi->absValHead;
  if (bi->absValHead != NULL) {
    temp->next->prev = temp;
  }
  else {
    bi->absValTail = temp;
  }
  bi->absValHead = temp;

}

/*======================= fonction ajouter en queue =====================*/

void add_tail(BigInteger bi, unsigned int n) {

  list temp,head,tail;

  head = bi->absValHead;
  tail = bi->absValTail;
  temp = (element*) malloc (sizeof(element));
  temp->val = n;
  temp->next = NULL;
  bi->absValTail = temp;
  if (head == NULL) {
    bi->absValHead = temp;
    temp->prev = NULL;
  }
  else {
    tail->next = temp;
    temp->prev = tail;
  }

}

/*======================= fonction supprimer queue =====================*/

void del_tail(BigInteger bi) {

  if (bi->absValTail != NULL) {
    if (bi->absValTail->prev != NULL) {
      bi->absValTail = bi->absValTail->prev;
      free(bi->absValTail->next);
      bi->absValTail->next = NULL;
    }
    else {
      free(bi->absValTail);
      bi->absValTail = NULL;
      bi->absValHead = NULL;
    }
  }

}

/*======================= fonction valeur de tete ======================*/

int head_value(BigInteger bi) {
  int n;

  if (bi->absValHead != NULL) {
    n = bi->absValHead->val;
  }
  else {
    n = 0;
  }
  return n;
}

/*=========================== fonction reste ===========================*/

BigInteger rest(BigInteger bi) {
  if (bi->absValHead != NULL) {
    bi->absValHead = bi->absValHead->next;
    if (bi->absValHead != NULL) {
      bi->absValHead->prev = NULL;
    }
  }
  return bi;
}

/*========================= fonction copyBigInt =========================*/

BigInteger copyBigInt(BigInteger bi) {

  BigInteger new = createBigInt();
  list elem = bi->absValHead;
  new->sign = bi->sign;

  while (elem != NULL) {
    add_tail(new, elem->val);
    elem = elem->next;
  }

  return new;
}

/*========================= fonction mise a zero =======================*/

void zeroBigInt(BigInteger bi) {

  list elem = bi->absValTail;

  if (elem != NULL) {
    while (elem->prev != NULL) {
      elem = elem->prev;
      free(elem->next);
    }
  }
  elem->val = 0;
}

/*==================== fonction stripBigInteger =========================*/
/* Permet de "striper" un BigInt, c'est à dire transformer 000101 en 101 */
void stripBigInteger(BigInteger bi) {
  list elem = bi->absValTail;

  while (elem != NULL) {
    if (elem->val == 0)
      del_tail(bi);
    elem = elem->prev;
  }
}


/*==================== fonction newBIgInteger ===========================*/

BigInteger newBigInteger(char* nb_char) {
  int i = 0;
  int long_nbr;
  int nbr_elem;
  int nb_digit_restant;
  int temp;
  BigInteger bi = createBigInt();
  char buffer[5];
  buffer[4] = '\0';

  strcpy(buffer, "0000");
  if (nb_char[0] == '-') {
    bi->sign = -1;
    while ( nb_char[i] != '\0' ) {
      nb_char[i] = nb_char[i+1];
      i++;
    }
    nb_char[i] = ' ';
  }
  else {
    bi->sign=1;
  }

  long_nbr=strlen(nb_char);
  nbr_elem = long_nbr/4;
  for (i = 0; i < nbr_elem; i++) {
    buffer[3] = nb_char[((long_nbr) - 1) - (4 * i)];
    buffer[2] = nb_char[((long_nbr) - 1) - (4 * i) - 1];
    buffer[1] = nb_char[((long_nbr) - 1) - (4 * i) - 2];
    buffer[0] = nb_char[((long_nbr) - 1) - (4 * i) - 3];
    temp = atoi(buffer);
    add_tail(bi, temp);
  }

  strcpy(buffer, "0000");
  nb_digit_restant = long_nbr - 4 * nbr_elem;
  if (nb_digit_restant !=0) {
    for (i = 0; i < nb_digit_restant; i++) {
      buffer[3-i] = nb_char[long_nbr - 1 - 4*nbr_elem -i];
    }
    temp = atoi(buffer);
    add_tail(bi, temp);
  }

  return bi;
}

/*===================== fonction printBigInteger ========================*/

void printBigInteger(BigInteger bi) {
  list elem = bi->absValTail;

  if (isNull(bi)) {
    printf("0");
  }
  else {
    if (bi->sign == -1) {
      printf("-");
    }
    do {
        fprintf(NORMAL_OUTPUT,"%d",elem->val);
    } while ((elem = elem->prev) != NULL);
  }
  printf("\n");
}

/*======================= fonction isNull ================================*/

Boolean isNull(BigInteger bi) {
  Boolean test = TRUE;
  list elem = bi->absValHead;

  if (elem != NULL) {
    if (elem->val != 0) {
      test = FALSE;
    }
    while (elem->next != NULL) {
      if (elem->next->val != 0) {
        test = FALSE;
      }
      elem = elem->next;
    }
  }
  return test;
}

/*==================== fonction signBigInt ===========================*/

int signBigInt(BigInteger bi) {
  int signe = -2;

  if (bi->sign == -1) {
    signe = -1;
  }
  else if (bi->sign == 1) {
    signe = 1;
  }
  else if (isNull(bi) == TRUE) {
    signe = 0;
  }
  return signe;
}

/*==================== fonction equalsBigInt ============================*/

Boolean equalsBigInt(BigInteger bi1, BigInteger bi2) {
  Boolean test = TRUE;
  list elem1 = bi1->absValHead;
  list elem2 = bi2->absValHead;

  if (bi1->sign != bi2->sign) {
    test = FALSE;
  }
  else {
    while (elem1->next != NULL && elem2->next != NULL) {
      if (elem1->val != elem2->val) {
        test = FALSE;
      }
      elem1 = elem1->next;
      elem2 = elem2->next;
    }
    if (elem1->next != elem2->next) {
      test = FALSE;
    }
    else if (elem1->val != elem2->val) {
      test = FALSE;
    }
  }
  return test;
}

/*=================== fonctin compareBigInt =============================*/

int compareBigInt(BigInteger bi1, BigInteger bi2) {
  int test = 2;
  list elem1 = bi1->absValHead;
  list elem2 = bi2->absValHead;

  if (equalsBigInt(bi1,bi2) == TRUE) {
    test = 0;

  } else {

    while (elem1->next != NULL && elem2->next != NULL) {
      elem1 = elem1->next;
      elem2 = elem2->next;
    }


    if (elem1->next == NULL && elem2->next != NULL) {

      if (bi1->sign < bi2->sign || (bi1->sign == 1 && bi2->sign == 1))
        test = -1;
      else if (bi1->sign > bi2->sign || (bi1->sign == -1 && bi2->sign == -1))
        test = 1;

    } else if (elem1->next != NULL && elem2->next == NULL) {

      if (bi1->sign > bi2->sign || (bi1->sign == 1 && bi2->sign == 1))
        test = 1;
      else if (bi1->sign < bi2->sign || (bi1->sign == -1 && bi2->sign == -1))
        test = -1;

    } else {
      do {
        if (elem1->val < elem2->val) {
          if (bi1->sign < bi2->sign || (bi1->sign == 1 && bi2->sign == 1))
            test = -1;
          else if (bi1->sign > bi2->sign ||(bi1->sign == -1 && bi2->sign == -1))
            test = 1;

        } else if (elem1->val > elem2->val) {
          if (bi1->sign > bi2->sign || (bi1->sign == 1 && bi2->sign == 1))
            test = 1;
          else if (bi1->sign < bi2->sign ||(bi1->sign == -1 && bi2->sign == -1))
            test = -1;

        } else if (elem1->val == elem2->val &&
                   elem1->prev == NULL && elem2->prev == NULL) {
          if (bi1->sign > bi2->sign)
            test = 1;
          else if (bi1->sign < bi2->sign)
            test = -1;

        } else {
          elem1 = elem1->prev;
          elem2 = elem2->prev;
        }
      } while (test == 2 && elem1 != NULL && elem2 != NULL);
    }
  }
  return test;
}

/* ================== fonction orientation somme soustraction ============*/

BigInteger sum_or_diff(int op, BigInteger bi1, BigInteger bi2) {

  BigInteger result;
  int signeabs;
  BigInteger absbi1;
  BigInteger absbi2;

  absbi1 = copyBigInt(bi1);
  absbi2 = copyBigInt(bi2);
  absbi1->sign = 1;
  absbi2->sign = 1;
  signeabs = compareBigInt(absbi1,absbi2);

  /* si l'un des deux bigint est nul, on retourne l'autre directement*/
  if (isNull(bi1)) {
    result = copyBigInt(bi2);
    if (op == 1) {
      result->sign = bi2->sign;
    }
    else {
      result->sign = -(bi2->sign);
    }
  }
  else if (isNull(bi2)) {
    result = copyBigInt(bi1);
  }
  else {
    result = createBigInt();
    /* cas ou on oriente sur l'addition */
    if (op * bi2->sign == 1 && bi1->sign == 1) {
      result = sumBigInt(bi1, bi2);
      result->sign = 1;
    }
    else if (op == 1 && bi1->sign == -1 && bi2->sign == -1) {
      result = sumBigInt(bi1, bi2);
      result->sign = -1;
    }
    /* sinon on oriente sur la soustraction */
    else if (signeabs == 1 && op * bi2->sign == -1) {
      result = diffBigInt(bi1, bi2);
      result->sign = 1;
    }
    else if (signeabs == -1 && op * bi2->sign == -1) {
      result = diffBigInt(bi2, bi1);
      result->sign = -1;
    }
    else if (signeabs == -1 && op * bi2->sign == 1 && bi1->sign == -1) {
      result = diffBigInt(bi2, bi1);
      result->sign = 1;
    }
    else if (signeabs == 1 && op * bi2->sign == 1 && bi1->sign == -1) {
      result = diffBigInt(bi1, bi2);
      result->sign = -1;
    }
  }
  free(absbi1);
  free(absbi2);
  return result;
}

/* ====================== fonction somme ============================*/

BigInteger sumBigInt(BigInteger bi1, BigInteger bi2) {
  BigInteger sum;
  list elem1=bi1->absValHead;
  list elem2=bi2->absValHead;
  int retenue = 0;
  unsigned int somme = 0;

    sum = createBigInt();
    /* on additionne les blocs entre eux tant que les 2 bigint sont non nuls et
     * on les ajoute en queue du bigint créé */
    while (elem1 != NULL && elem2 != NULL) {
      somme = elem1->val + elem2->val + retenue;
      retenue = 0;
      elem1 = elem1->next;
      elem2 = elem2->next;
      if (somme > 9999) {
        retenue = 1;
        somme = somme - 10000;
      }
      add_tail(sum, somme);
    }
    /* on ajoute le reste du big int non nul en queue du bigint créé */
    /* si c'est le bigint1 qui est non nul */
    if (elem1 != NULL) {
      /* si la derniere somme etait supérieure a 9999, on ajoute 1 au premier
       * element du reste */
      while (retenue == 1 && elem1 != NULL) {
        somme = elem1->val + 1;
        retenue = 0;
        if (somme > 9999) {
          retenue = 1;
          somme = somme - 10000;
        }
        add_tail(sum, somme);
        elem1 = elem1->next;
      }
      while (elem1 != NULL) {
        add_tail(sum, elem1->val);
        elem1 = elem1->next;
      }
    }
    /* sinon si c'est le bigint2 qui est non nul */
    if (elem2 != NULL) {
      /* si la derniere somme etait supérieur a 9999, on ajoute 1 au premier
       * element du reste */
      while (retenue == 1 && elem2 != NULL) {
        somme = elem2->val + 1;
        retenue = 0;
        if (somme > 9999) {
          retenue = 1;
          somme = somme - 10000;
        }
        add_tail(sum, somme);
        elem2 = elem2->next;
      }
      while (elem2 != NULL) {
        add_tail(sum, elem2->val);
        elem2 = elem2->next;
      }
    }
    /* sinon si les 2 bigint sont nuls (donc de même taille) et que
     * la retenue = 1, il faut ajouter 1 en tête */
    if (retenue == 1) {
      add_tail(sum, 1);
    }
  return sum;
}

/* ====================== fonction difference ============================*/

BigInteger diffBigInt(BigInteger bi1, BigInteger bi2) {
  BigInteger diff;
  list elem1=bi1->absValHead;
  list elem2=bi2->absValHead;
  int retenue = 0;
  int difference = 0;

    diff = createBigInt();
    /* on soustrait les blocs entre eux tant que les 2 bigint sont non nuls et
     * on les ajoute en queue du bigint créé */
    while (elem1 != NULL && elem2 != NULL) {
      difference = elem1->val - elem2->val + retenue;
      retenue = 0;
      elem1 = elem1->next;
      elem2 = elem2->next;
      if (difference < 0) {
        difference = difference + 10000;
        retenue = -1;
      }
      add_tail(diff, difference);
    }
    /* on ajoute le reste du big int non nul en queue du bigint créé */
    /* si c'est le bigint1 qui est non nul */
    if (elem1 != NULL) {
      /* si la derniere somme etait supérieure a 9999, on ajoute 1 au premier
       * element du reste */
      while (retenue == -1 && elem1 != NULL) {
        difference = elem1->val + retenue;
        retenue = 0;
        if (difference < 0) {
          retenue = -1;
          difference = difference + 10000;
        }
        add_tail(diff, difference);
        elem1 = elem1->next;
      }
      while (elem1 != NULL) {
        add_tail(diff, elem1->val);
        elem1 = elem1->next;
      }
    }
    /* sinon si c'est le bigint2 qui est non nul */
    if (elem2 != NULL) {
      /* si la derniere somme etait supérieure a 9999, on ajoute 1 au premier
       * element du reste */
      while (retenue == 1 && elem2 != NULL) {
        difference = elem2->val + retenue;
        retenue = 0;
        if (difference < 0) {
          retenue = -1;
          difference = difference + 10000;
        }
        add_tail(diff, difference);
        elem2 = elem2->next;
      }
      while (elem2 != NULL) {
        add_tail(diff, elem2->val);
        elem2 = elem2->next;
      }
    }
    /* sinon si les 2 bigint sont nuls (donc de même taille) et que
     * la retenue = -1, il faut retirer 1 en tête */
    if (retenue == -1) {
      diff->absValTail->val = diff->absValTail->val - 1;
    }
  stripBigInteger(diff);
  return diff;
}

/*========================= fonction int x BigInt =======================*/

BigInteger intxBigInt(BigInteger bi, unsigned int n) {
  BigInteger prod = createBigInt();
  list elem = bi->absValHead;
  unsigned int produit = 0;
  unsigned int retenue = 0;

  while (elem != NULL) {
    produit = elem->val * n + retenue;
    retenue = produit / 10000;
    produit = produit % 10000;
    add_tail(prod, produit);
    elem = elem->next;
  }
  if (retenue != 0) {
    add_tail(prod, retenue);
  }
  return prod;
}

/*========================== fonction mulBigInt ========================= */

BigInteger mulBigInt(BigInteger bi1, BigInteger bi2) {
  BigInteger prod_inter = createBigInt();
  BigInteger prod_final = createBigInt();
  list elem = bi2->absValHead;
  int decalage = 0;
  int i;

  while (elem != NULL) {
    prod_inter = intxBigInt(bi1, elem->val);
    for (i = 0; i < decalage; i++) {
      add_head(prod_inter, 0);
    }
    prod_final = sumBigInt(prod_final, prod_inter);
    decalage++;
    elem = elem->next;
    zeroBigInt(prod_inter);
  }
  prod_final->sign = bi1->sign * bi2->sign;

  free(prod_inter);
  return prod_final;
}

/*===================== fonction quotientBigInt =========================*/

BigInteger quotientBigInt( BigInteger bi1, BigInteger bi2 ) {
  BigInteger temp1 = createBigInt();
  BigInteger quot = createBigInt();
  BigInteger abs_bi1 = createBigInt();
  BigInteger abs_bi2 = createBigInt();
  list elem;
  unsigned int quotient;

  abs_bi1 = copyBigInt(bi1);
  abs_bi2 = copyBigInt(bi2);
  abs_bi1->sign = 1;
  abs_bi2->sign = 1;

  if (isNull(bi2) == TRUE) {
   fprintf(ERROR_OUTPUT,"Dividing by 0 is forbidden\n");
   exit(1);

  } else if (compareBigInt(abs_bi1,abs_bi2) == -1 ) {
    add_tail(quot,0);

  } else {

    elem = abs_bi1->absValTail;
    do {
      do {
        add_head(temp1, elem->val);
        elem = elem->prev;
      } while (compareBigInt(temp1, abs_bi2) == -1 && elem != NULL);
      quotient = 1;
      while (compareBigInt(intxBigInt(abs_bi2, quotient), temp1) == -1) {
        quotient++;
      }
      if (compareBigInt(intxBigInt(abs_bi2, quotient), temp1) == 1) {
        quotient--;
      }
      add_head(quot, quotient);
      temp1 = diffBigInt(temp1, intxBigInt(abs_bi2, quotient));
    } while (elem != NULL);

    quot->sign = bi1->sign * bi2->sign;

    /* Si les bigint sont de signes différents et que la différence de leur
     * dernier bloc en base 10000 a un reste < 0, on rajoute 1 à la valeur
     * absolue du quotient                                                  */
    if (abs_bi1->absValHead->val != abs_bi2->absValHead->val) {
      if (bi1->sign != bi2->sign)
        quot->absValHead->val++;
    }
  }
  free(temp1);
  free(abs_bi1);
  free(abs_bi2);
  return quot;
}

/*====================== fonction restBigInt =========================*/

BigInteger restBigInt(BigInteger bi1, BigInteger bi2) {
  BigInteger rest = createBigInt();

  /* On force la soustraction */
  rest = sum_or_diff(-1, bi1, mulBigInt(quotientBigInt(bi1,bi2),bi2));

  return rest;
}

/*====================== fonction gcdBigInt ==========================*/

BigInteger gcdBigInt(BigInteger bi1, BigInteger bi2) {
  BigInteger rest = createBigInt();
  BigInteger temp1;
  BigInteger temp2;
  /* Si bi1 = 0, gcd(bi1,bi2) = bi2, et réciproquement */
  if (isNull(bi1) == TRUE) {
    free(rest);
    return bi2;

  } else if (isNull(bi2) == TRUE) {
    free(rest);
    return bi1;

  } else {
    temp1 = copyBigInt(bi1);
    temp2 = copyBigInt(bi2);
    temp1->sign = 1;
    temp2->sign = 1;
    rest = restBigInt(temp1,temp2);
    while(isNull(rest) == FALSE) {
      temp1 = temp2;
      temp2 = rest;
      rest = restBigInt(temp1,temp2);
    }
    free(rest);
    free(temp1);
    temp2->sign = bi1->sign * bi2->sign;
    return temp2;
  }
}

/*======================= fonction lcmBigInt ========================*/

BigInteger lcmBigInt(BigInteger bi1, BigInteger bi2) {
  BigInteger lcm = createBigInt();
  BigInteger null = createBigInt();
  /* Si bi1 = 0 ou bi2 = 0, lcm(bi1,bi2) = 0 */
  if (isNull(bi1) || isNull(bi2)) {
    free(lcm);
    add_tail(null,0);
    return null;

  } else {
    free(null);
    lcm = quotientBigInt(mulBigInt(bi1,bi2), gcdBigInt(bi1,bi2));
    lcm->sign = bi1->sign * bi2->sign;
    return lcm;
  }
}

/*======================= fonction factorielle =========================*/

BigInteger factorial(unsigned int x1) {
  BigInteger fact = createBigInt();

  add_head(fact, 1);
  while (x1 > 1) {
    fact = intxBigInt(fact,x1);
    x1 = x1 - 1;
  }
  return fact;
}

/*======================= fonction combinatoire ========================*/

BigInteger cnp(unsigned int k, unsigned int n) {
  BigInteger comb;
  BigInteger null;
  /* Si k > n, la combinaison vaut 0 */
  if (k > n) {
    null = createBigInt();
    add_tail(null,0);
    return null;
  } else {
    comb = quotientBigInt(factorial(n),
                          mulBigInt(factorial(k),factorial(n-k)));
    return comb;
  }
}
/* EOF */
