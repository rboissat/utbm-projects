/*****************************************************************************
 * PROJET LO44
 * Calculatrice 'Big Int'
 *
 * Fichier: 'lib/biginteger.h'
 *
 * Copyright (c) 2008 Romain BOISSAT, Antoine GAVOILLE
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 *****************************************************************************/

/*
 *                          BIBLIOTHEQUE BigInt
 *                                                                          */
#ifndef _BIG_INT
#define _BIG_INT
/* STRUCTURES ET TYPES */
typedef enum {
  FALSE = 0,
  TRUE
} Boolean;

typedef struct elem {
  struct elem *prev;
  unsigned int val;
  struct elem *next;
} element;

typedef element *list;

typedef struct {
  int sign;
  list absValHead; /* Tête */
  list absValTail; /* Queue */
} BigInt;

typedef BigInt *BigInteger;

/* PROTOTYPES PRIVÉS */
BigInteger createBigInt();
void add_head(BigInteger bi, unsigned int n);
void add_tail(BigInteger bi, unsigned int n);
int head_value(BigInteger bi);
BigInteger rest(BigInteger bi);
BigInteger copyBigInt (BigInteger bi);
void zeroBigInt(BigInteger bi);
void stripBigInteger(BigInteger bi);


/* PROTOTYPES PUBLICS */

BigInteger newBigInteger( char* string );

void printBigInteger( BigInteger bi );

Boolean isNull( BigInteger bi );

int signBigInt( BigInteger bi );

Boolean equalsBigInt( BigInteger bi1, BigInteger bi2 );

int compareBigInt( BigInteger bi1, BigInteger bi2 );

BigInteger sum_or_diff( int op, BigInteger bi1, BigInteger bi2 );

BigInteger sumBigInt( BigInteger bi1, BigInteger bi2 );

BigInteger diffBigInt( BigInteger bi1, BigInteger bi2 );

BigInteger intxBigInt (BigInteger bi, unsigned int n);

BigInteger mulBigInt( BigInteger bi1, BigInteger bi2 );

BigInteger quotientBigInt( BigInteger bi1, BigInteger bi2 );

BigInteger restBigInt(BigInteger bi1, BigInteger bi2);

BigInteger gcdBigInt( BigInteger bi1, BigInteger bi2 );

BigInteger lcmBigInt( BigInteger bi1, BigInteger bi2 );

BigInteger factorial( unsigned int x1 );

BigInteger cnp( unsigned int n, unsigned int p );

#endif
/* EOF */
