\documentclass[A4,11pt]{article}
\usepackage{ae}
\usepackage[francais]{babel}
%\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}   % Apple utf8 encoding
%\usepackage[utf8]{inputenc}   % utf8 encoding
% -> USE xelatex
\usepackage{graphicx}         % graphic files
\usepackage{alltt}            % filtered verbatim
\usepackage[margin=4cm,top=25mm,bottom=25mm]{geometry} % page layout
\usepackage[colorlinks,urlcolor=blue,
            pdfauthor={Romain BOISSAT},
            pdftitle={ LO44: Calculatrice BigInteger },
            pdfcreator={pdftex},
            pdfsubject={Rapport de projet LO44},
            pdfkeywords={utbm,lo44,biginteger}]{hyperref} % URLs an hrefs
%\usepackage{fontspec}
\usepackage{lmodern}
%
\setlength{\parindent}{0pt}
\setlength{\parskip}{3pt}
\pagestyle{headings}
%
\newenvironment{algo}[2]
{\begin{list}{}{
\setlength{\listparindent}{\parindent}
\setlength{\itemindent}{\parindent}
\setlength{\leftmargin}{#1}
\setlength{\rightmargin}{#2}
}\item }
{\end{list}}


%
\begin{document}

\title{LO44: Projet - Calculatrice BigInteger}
\author{Romain BOISSAT - Antoine GAVOILLE}
\maketitle
\begin{center}
  \small{Enseignants: M. Abderrafiaa Koukam, M. David Meignan}

  \vspace{100pt}

  \includegraphics[scale=0.5]{logo}
\end{center}

\newpage
\tableofcontents

\newpage
\section{Rappel du projet}
\label{Rappel du projet}

Dans le but d'évaluer et de valider les acquis des étudiants de l'unité de
valeur LO44, la réalisation d'un projet est demandée.

Le projet du semestre \textbf{A08} est la réalisation en \textbf{langage C}
d'une bibliothèque et d'une interface employant cette bibliothèque pour
manipuler des \emph{Big Integer} ou grands entiers. Il permettra d'effectuer
un ensemble d'opérations mathématiques sur un ou deux \emph{Big Integer} qui
seront passés au programme via l'interface.

Ce document est le compte-rendu de ce projet. Il présentera les choix de
conception, d'abord au niveau des structures de données et des
algorithmes, qui seront détaillés, puis le fonctionnement des autres
composants, comme par exemple l'interface utilisateur et le \emph{Makefile}.

Enfin les difficultés rencontrées et les limites du programme seront
explicitées à la fin de ce document.

\newpage
\section{Représentation des données}
\label{data}

La représentation des données consiste dans ce projet à concevoir une
structure de données pratique à manipuler. Cette représentation est donnée
dans le sujet du projet, elle implique l'utilisation de \emph{listes doublement
chaînées}. Le nombre est alors découpé en base 10 000, et stocké en
\emph{Litte Endian}, c'est-à-dire de droite (les puissances les plus faibles)
à gauche (les puissances les plus fortes).

Nous verrons par la suite que ce choix de représentation facilite certaines
opérations, mais en grèvent d'autres... Nous avons apporté une modification
sur la structure pour palier à cette limitation.

\bigskip

Telle que donnée dans le sujet, la structure de données présente les
caractéristiques suivantes :

\begin{itemize}
  \item Une liste doublement chaînée contenant la valeur absolue du nombre en
  codage \emph{Little Endian}.

  \item Un structure \emph{BigInteger} contenant le signe codé sous la forme
  d'un \texttt{int} et un pointeur vers la liste doublement chainée.
\end{itemize}

Les modifications que nous avons apportées sont les suivantes :

\begin{itemize}
  \item Le type \emph{BigInteger} n'est plus une structure, mais un pointeur
  vers la structure, nous avons pensé nos fonctions par passage d'adresse
  et non passage par copie, pour la simplicité conceptuelle et la souplesse
  des pointeurs. Nous effectuons une copie des BigIntegers quand cela est
  nécessaire.

  \item Le pointeur \emph{absval} sur la liste doublement chainée est
  remplacé par deux pointeurs \emph{absValHead} et \emph{absValTail}
  représentant respectivement la tête de la liste
  (ex: pour un parcours \emph{Little Endian}) et la queue de la liste 
  (ex: pour parcours \emph{Big Endian}).
\end{itemize}

Un aperçu du code C associé :
\begin{small}
\begin{verbatim}
typedef struct elem {
  struct elem *prev;
  unsigned int val;
  struct elem *next;
} element;

typedef element *list;

typedef struct {
  int sign;
  list absValHead; /* Tête */
  list absValTail; /* Queue */
} BigInt;

typedef BigInt *BigInteger;

\end{verbatim}
\end{small}

\newpage
\section{Algorithmes de la bibliothèque}
\label{algos}

Cette section rassemble tous les algorithmes développés pour le projet.
Dans un premier temps, nous aborderons les algorithmes des fonctions
exposées dans le sujet, puis les algorithmes des fonctions \emph{privées},
nécessaires aux fonctions principales.

Ces algorithmes utilisent des fonctions de base sur les listes doublement
chaînées telles que \emph{ajouter\_tete},\emph{ajouter\_queue},
\emph{valeur\_tete}, \emph{valeur\_queue} ainsi que les fonctions
\emph{reste\_gauche} et \emph{reste\_droit}
correspondant respectivement à un déplacement vers les éléments
précédents ou suivants de la liste.

  \subsection{Algorithmes principaux}
  \label{algos_main}
    \begin{footnotesize}
    \input{algos1}
    \end{footnotesize}

  \subsection{Algorithmes complémentaires}
  \label{algos_comp}
    \begin{footnotesize}
    \input{algos2}
    \end{footnotesize}

\newpage
\section[Programme principal et UI] {Programme principal et UI
\footnote{\emph{User Interface}: Interface Utilisateur}}
\label{main_and_ui}

  \subsection{Conception de l'interface}
  \label{ui}
  L'interface est la partie du projet permettant à l'utilisateur du programme
  de tester sa fonction principale. Pour ce projet, il s'agit de développer
  une interface simple permettant à l'utilisateur de manipuler un opérateur
  et un ou deux nombres et d'obtenir le résultat de l'opération
  correspondante. Le choix a été porté sur une interface en ligne de
  commande, à saisir dans un émulateur de terminal sous un système Unix
  ou Unix-like, à la manière d'autres commandes
  \label{footnote2}\footnote{La portabilité du projet sur Microsoft Windows
  ne fut pas contrôlée}.

  Ainsi il suffit de se placer dans le dossier de l'executable et de saisir
  la commande suivante pour effectuer l'addition de 3 et de 2 et obtenir le
  résultat dans le terminal :

  \medskip
  \verb_user@host:./CalcBigInt + 3 2_ \qquad\emph{Return}
  \medskip

  Le code de l'interface est contenu dans le fichier \texttt{main.c}.

  Le nombre d'arguments ainsi que les données d'entrée sont testées avant
  de lancer le calcul afin d'éviter les plantages dûs à une erreur de
  saisie. En l'occurrence, le programme principal appelle une routine
  \emph{is\_bigint} pour vérifier que les nombres passés en argument soient
  des \textbf{entiers relatifs}.
  Pour l'opérateur, un appel à la routine \emph{dispatch\_op} effectue
  une simple disjonction de cas en testant
  si la chaîne correspondant à l'opérateur saisi correspond à un opérateur
  existant, auquel cas l'opération est effectuée et le résultat affiché.
  Le cas contraire appelle une routine d'erreur \emph{print\_error}.

  L'utilisation de l'argument \emph{-h} appelle la routine
  \emph{print\_help}, dont le détail sera donné dans la sous-section
  \textbf{Manuel} (\label{reftoman}\ref{man}).

  \medskip

  Voici les prototypes associés aux routines du programme principal :

  \begin{verbatim}
  void print_error(void);
  void print_help(void);
  Boolean is_bigint(char *string);
  void dispatch_op(char *op, char *nb1, char *nb2);
  \end{verbatim}

\newpage

  \subsection{Compilation du projet}
  \label{compilation}

  Pour faciliter la compilation du projet, ainsi que sa fragmentation
  en plusieurs fichiers, nous avons décidé d'écrire un \emph{makefile}.
  Ce fichier contient les relations entre les différents objets ainsi que
  les instructions de compilation. De ce fait, l'utilisateur n'a plus qu'à
  taper la commande \texttt{make} pour lancer la compilation de la
  bibliothèque en objet partagé \textbf{libBigInteger.so}, la compilation
  du \textbf{main.c} en objet \textbf{main.o} et le \emph{linkage} des deux
  objets en un seul exécutable \textbf{CalcBigInt}.

  Des actions personnalisées sont également disponibles, à utiliser après
  la compilation du projet avec la commande \texttt{make} :

  \begin{itemize}
  \item \texttt{make clean} : Nettoie le répertoire de travail en supprimant
  les objets (\verb+.o+) et les objets partagés (\verb+.so+).

  \item \texttt{make mrproper} : Comme \texttt{make clean}, mais supprime
  également l'exécutable en vue d'une recompilation complète ou archivage du
  dossier.
  \end{itemize}

  Nous avons également utilisé le \emph{flag} \texttt{-O2} dans les options de
  compilation. Ce marqueur indique au compilateur qu'il peut optimiser le code
  machine pour une performance accrue, au détriment de la taille finale de
  l'exécutable, ici minime. Le niveau 2 est suffisant, d'autant qu'il
  n'implique aucune optimisation poussée qui pourrait générer des effets de
  bord.

  \subsection{Manuel}
  \label{man}

  Plutôt que d'écrire une explication inutilement complexe, voici la sortie
  terminal obtenue par \verb+./CalcBigInt -h+ :

  \begin{verbatim}
Usage: ./CalcBigInt [operator] [number_1] [number_2]
Available operators:
    * x   (multiply)
    * +   (add)
    * -   (substract)
    * /   (divide and print the rest)
    * gcd (print the Greatest Common Divisor)
    * lcm (print the Least Common Multiple)
    * !   (factorial, require only one *positive* int < 4 294 967 295)
    * cnp (Combination: cnp k n = n!/(k!*(n-k)!), require two unsigned int)
    WARNING: cnp and ! can be very memory hungry with big numbers.
    You should use them cautiously!
    ---------------------------------------------
    Complementary operators: operators for testing internal functions
    * null    (isNull(BigInteger): require only one bigint)
    * sign    (signBigInt(BigInteger): require only one bigint)
    * =       (equalsBigInt(BigInteger): require only one bigint)
    * compare (compareBigint(BigInteger): require only one bigint)

    Released under the General Public Licence V.2 - http://www.gnu.org
    Copyright(c) 2008 - Romain BOISSAT, Antoine GAVOILLE
  \end{verbatim}

\newpage

  \subsection{Test Unitaire}
  \label{testU}
  Un script shell de test unitaire est fourni dans les sources du programme
  (\texttt{testU.sh}). Ce script, développé dès la finition de l'interface
  et sa validation fonctionnelle, a permis de tester au fur et à mesure
  du développement des opérateurs :
  \begin{itemize}
    \item le bon fonctionnement de l'opérateur nouvellement développé
    \item l'absence de régressions dûes à un une modification de code qui
          affecte un autre fonction.
  \end{itemize}

  Nous avons gagné beaucoup de temps grâce à ces méthodes de développement.

  Nous conseillons d'ailleurs à l'évaluateur du projet d'utiliser ce script
  de test unitaire, lui permettant d'obtenir \textbf{pour un cas donné} un
  résultat synthéthique du fonctionnement de la calculatrice.

  Usage : \texttt{./testU.sh [BigInti\_1] [BigInt\_2]}

  Exemple de sortie écran :
  \begin{verbatim}
./testU.sh 45 2

45 + 2 = 47
45 - 2 = 43
45 x 2 = 90
45 / 2 = 22
leftover = 1
gcd(45,2) = 1
lcm(45,2) = 90
bi1 is bigger than bi2
Are equals? FALSE

Concerning '45' only:
------------------------------
Is null? FALSE
Sign: Positive
!45 = 119622220865480194561963161495657715064383733760000000000

------------------------------
cnp' operator is very memory hungry with numbers > 100
Please use with precaution
If this script seems to freeze, please interrupt it with ^C
cnp(45,2) = 0

  \end{verbatim}

\newpage

\section{Bilan}
\label{conclusion}

  \subsection{Difficultés rencontrées}
  \label{difficulties}

  Clairement, les difficultés étaient d'ordre technique, à savoir que
  ce projet nous a demandé beaucoup de connaissances sur le langage C
  et la programmation en général, souvent apprises en dehors des
  cours et TPs. Les difficultés les plus présentes se
  sont posées sur la résolution des plantages dûs à des fuites de mémoires
  ou autres erreurs de bus. Heureusement l'utilitaire \texttt{gdb} nous
  a permis de sauver un temps précieux grâce à son efficacité et sa
  précision. Ensuite, nous avons dû gérer le développement en binôme,
  notamment en apprenant l'utilisation d'un système de contrôle de version,
  ici \textbf{Subversion}, qui permet de travailler aisément à plusieurs,
  tout en gardant un suivi et une traçabilité exemplaire.

  Enfin la gestion des cas particuliers nous a coûté quelques nuits
  de débogage.

  \subsection{Limites du programme}
  \label{limits}

  Les limites du programmes apparaissent sur la gestion de très gros
  nombres sur certains opérateurs. Ainsi, sur une machine moderne,
  calculer la factorielle d'un nombre supérieur à 2000 devient
  \textbf{très coûteux} en temps CPU et mémoire, allant même jusqu'au
  blocage de la machine. Enfin, la gestion de certains cas particuliers
  n'est pas forcément optimale, comme dans les operateurs "/" ou "-",
  rendant le code plus complexe. Néanmoins, dans tous les tests menés
  jusqu'à présent, le résultat affiché (si le calcul ne plante pas)
  est juste.

  \subsection{Remarques complémentaires}
  \label{remarks}
  Nous tenions tout particulièrement à remercier Messieurs Abderrafiaa
  Koukam, David Meignan et Pawel Kmiotek pour leur aide précieuse.

  L'url du dépôt SVN est la suivante :\\
  \url{https://ssl.chrooted-universe.org/svn/PROJECTS/LO44/}

  Les sources \LaTeX{} de ce document y sont également disponibles.

  Liste des programmes utilisés pour le développement :
  \begin{itemize}
    \item \textbf{Systèmes d'exploitation :} Ubuntu, Debian, Mac OS X.
    \item \textbf{Éditeurs :} Vim, Gedit.
    \item \textbf{Débogueur :} GNU Debug.
    \item \textbf{Compilateur :} GCC, GNU make.
    \item \textbf{Gestion de version :} Subversion,.Apache, WebDAV.
  \end{itemize}

\end{document}


