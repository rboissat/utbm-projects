# UTBM Projects

This repository contains old code from school projects when I was an
engineering student at the Université de Technologie de Belfort Montbéliard.

The directory layout refers to the codename of the teaching module/unit ("Unité de Valeur").
