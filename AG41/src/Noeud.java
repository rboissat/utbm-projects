
public class Noeud {
	public int numero;	// ID du point (O pour le depot)
	public int x, y;		// coordonees
	public int demande;	// 0 pour le depot, demande pour les autres
	public int ouverture, fermeture;	//horraire
	public int duree_livr;	// duree livraison (10 sauf depot)
	
	public Noeud(int numero, int x, int y, int demande, int ouverture, int fermeture, int duree_livr) {
		this.numero = numero;
		this.x = x;
		this.y = y;
		this.demande = demande;
		this.ouverture = ouverture;
		this.fermeture = fermeture;
		this.duree_livr = duree_livr;
	}	
}
