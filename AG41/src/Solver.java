/* Solver : regroupe les information relative a l'algorithme travaillant sur la solution.
 * 
 * TODO: Commenter le code
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
public class Solver {
	public Solution bestSolutionEver;
	
	public Solution[] pop;
	int taillePop; //doit etre PAIRE!
	public Graphe g = null;
	int typeSelection;
	double[]probCumuler;
	static int penalite ;
	double[]entropie;
	static SecureRandom s = new SecureRandom();
	
	public Solver (Graphe g,int taillePop, int penalite, int typeSelection,int typeGeneration){
		this.typeSelection = typeSelection;
		this.g=g;
		this.taillePop = taillePop;
		pop = new Solution[taillePop];
		
		Solver.penalite = penalite;

		
		
		
		//lireClavier();
		probCumuler = new double[taillePop];


		//this.afficherTouteSol();
	}
	
	public void trierSol(){
		ArrayList<Solution> aS = new ArrayList<Solution>();
		
		Solution s = null;
		for(int i = 0; i < taillePop; i++) {
			s = pop[i];
			if(aS.size()==0){
				aS.add(s);
			}else{
				int j;
				for(j = 0; j < aS.size()-1 && s.fitness > aS.get(j).fitness; j++){}
				aS.add(j,s);
			}
		}
		pop = new Solution[taillePop];
		for(int i = 0; i < aS.size();i++){
			pop[i] = aS.get(i);
		}
	}
	
	/**************************SELECTION**********************************************/
	//RANKING
	public void calculerProbaRanking(){
		double somme=(taillePop + 1)*taillePop / 2;
		for(int i = 0; i < taillePop; i++){
			pop[i].probSelect=(taillePop - i)/somme;
		}
		calculProbCumuler();
	}
		
	//ROUE BIAISEE
	public void calculerProbaRoueBiaise(){
		//calcul de la somme des fit
		double sommeFit = 0;
		for(int i = 0; i < taillePop; i++){
			sommeFit+=pop[i].fitness;
		}
		
		//calcule les proba!
		for(int i = 0; i < taillePop; i++){
			pop[i].probSelect = 1-(pop[i].fitness / sommeFit);
		}
		calculProbCumuler();
	}
	
	//UTILS SELECT
	public void calculProbCumuler(){
		double prob = 0;
		for(int i = 0; i < taillePop; i++){
			//AG41.debugPrintln("probselect : "+pop[i].probSelect);
			prob+=pop[i].probSelect;
			probCumuler[i] = prob;
		}
	}
	
	public int selectIndiv(){
		double r = s.nextFloat()*probCumuler[taillePop - 1];
		//AG41.debugPrintln("prob cumulee : "+probCumuler[taillePop - 1]);
		for(int i = 0; i < taillePop; i++){
			if(r<=probCumuler[i]){
				return i;
			}
		}
		AG41.debugPrintln("Solver.selectIndivByRanking() qui deconne: r="+r+" probCumuler="+probCumuler[taillePop - 1]);
		System.exit(0);
		return -1;
	}
	/******************************MAJ*******************************************/
	
	public void MAJbestSolution(int generation, long l){
		if(bestSolutionEver==null){bestSolutionEver = pop[0];}
		else {
			if(bestSolutionEver.fitness > pop[0].fitness){
				cpt = 0;
				bestSolutionEver = pop[0];
				AG41.debugPrintln("bestSolEver : "+bestSolutionEver.fitness+" d="+bestSolutionEver.distance+" r="+bestSolutionEver.retard + " a la generation : "+generation + " , en : "+l / 1000+"sec");
			} else {
				cpt++;
			}
		}
	}
	
	public double getFitnessMeilleurSol(int generation, long l){
		MAJbestSolution(generation, l);
		return bestSolutionEver.fitness;
	}
	
	//utile pour marquer des temps d'attente
	public static void lireClavier(){
		AG41.debugPrintln("appuyer sur entree pour continuer...");
		InputStreamReader isr = new InputStreamReader(System.in); 
		BufferedReader br = new BufferedReader(isr); 
		try {
			br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	int cpt = 0;
	public void algoGenetiqueGuillaume(int nbGeneration){		

		entropie = new double[nbGeneration];
		//lireClavier();

		int numGen = 0;
		getFitnessMeilleurSol(numGen,0);
		int nbPopConserver = taillePop / 10;
		AG41.debugPrintln("appuyer sur entree pour continuer...");

		while(numGen < nbGeneration){
			//afficherTouteSol();
			
			Solution []individuCroise = new Solution[taillePop];
			int p1,p2;
			Solution []fils = new Solution[2];
			if(typeSelection==1 && cpt < 1000)
				calculerProbaRoueBiaise();
			for(int i = 0; i < taillePop - nbPopConserver; i+=2){
				//selection
				if(typeSelection==2 || cpt >= 1000) {
					
					p1=(int)(s.nextFloat()*taillePop);
					p2=(int)(s.nextFloat()*taillePop);
				} else {
					p1 = selectIndiv();
					p2 = selectIndiv();
				}

				fils[0] = new Solution(pop[p1]);
				fils[1] = new Solution(pop[p2]);
				
				fils[0].p1 = p1;;
				fils[0].p2 = p2;
				fils[1].p1 = p2;;
				fils[1].p2 = p1;
				
				//mutation

				//fils[0].mutOrdonnerCamion();
				//fils[1].mutOrdonnerCamion();
				double probMut = s.nextFloat();
				if(probMut < 5){
					fils[0].mutEchange2Villes();
					fils[1].mutEchange2Villes();
				}
				
				
				
				/* else if(probMut >= 0.1 && probMut < 0.2 && numGen > nbGeneration / 2){
					fils[0].mutEchangerOrdreCamion();
					fils[1].mutEchangerOrdreCamion();
				} else if(probMut >= 0.2 && probMut < 0.3 && numGen > nbGeneration / 2){
					fils[0].mutPermuterOrdreCamion();
					fils[1].mutPermuterOrdreCamion();
				}*/
				/*if(numGen < 20){
					fils[0].rechercheTabou(10);
					fils[1].rechercheTabou(10);
					fils[0].genererCoupureDescente();
					fils[1].genererCoupureDescente();
				}*/
				
				/*if (cpt > 1500) {
					fils[0].genererCoupuresMinimum();
					fils[1].genererCoupuresMinimum();
				}*/
				
				fils[0].evaluer();
				fils[1].evaluer();
				individuCroise[i] = fils[0];
				individuCroise[i+1] = fils[1];
			}
			//remplacement
			for(int i = 0; i < taillePop - nbPopConserver; i++){
				pop[i+nbPopConserver] = individuCroise[i];
			}
			trierSol();
			getFitnessMeilleurSol(numGen,0);
			numGen++;
			
			AG41.debugPrintln("*****"+numGen+"***** best : "+this.bestSolutionEver.fitness+","+" meilleurActuel : "+ pop[0].fitness+"   admissible : "+this.bestSolutionEver.admissible());

		}
		this.bestSolutionEver.evaluer();
	}
	
	public void afficherTouteSol(){
		for(int i = 0; i < taillePop; i++){
			Solution.printNombre(i,taillePop);
			AG41.debugPrint(")");
			pop[i].afficher();
		}
	}
	
	public void calculEntropie(int g){
		double eMoy = 0;
		double sommeEntropieMoy = 0;
		for(int i = 0; i < taillePop; i++){
			eMoy = 0;
			for(int j = 0; j < taillePop; j++){
				if(j!=i){
					int eTemp = pop[i].entropieAvec(pop[j]);
					eMoy+=eTemp;
				}
			}
			eMoy = eMoy/(taillePop - 1);
			sommeEntropieMoy+=eMoy;
		}
		sommeEntropieMoy = sommeEntropieMoy / taillePop;
		this.entropie[g] = sommeEntropieMoy;
	}
	
	/*
	 * renvoie le nombre d'individu ayant la mm fitness dans la population
	 */
	public int compareFitnessPop(Solution s) {
		int test = 0;
		for (int i = 0; i < taillePop; i++) {
			if (s.fitness==pop[i].fitness) {
				test++;
			}
		}
		return test;
	}
	
	public void algoGenetiqueAntoine(int nbGeneration){

		//entropie = new double[nbGeneration];
		//lireClavier();

		int numGen = 0;
		getFitnessMeilleurSol(numGen,0);
		int nbPopConserver = taillePop / 10;
		AG41.debugPrintln("appuyer sur entree pour continuer...");

		while(numGen < nbGeneration){
			//afficherTouteSol();
			
			Solution []individuCroise = new Solution[taillePop];
			int p1,p2;
			Solution []fils = new Solution[2];
			if(typeSelection==1)
				calculerProbaRoueBiaise();
			for(int i = 0; i < taillePop - nbPopConserver; i+=2){
				//selection
				if(typeSelection==2 || cpt >= 1000) {
					p1=(int)(s.nextFloat()*taillePop);
					p2=(int)(s.nextFloat()*taillePop);
					while (p2==p1)
						p2=(int)(s.nextFloat()*taillePop);
				} else {
					p1 = selectIndiv();
					p2 = selectIndiv();
				}
				
				//croisement
				/*if (nbGeneration%3==0) {
					fils = pop[p1].croisementSequenceClient(pop[p2]);
				} else if (nbGeneration%3==1) {
					fils = pop[p1].croisement1X(pop[p2]);
				} else {
					fils = pop[p1].croisementContenuCamion(pop[p2]);
				}*/
				//fils = pop[p1].croisementContenuCamion(pop[p2]);
				//fils = pop[p1].croisement1X(pop[p2]);
				//fils = pop[p1].croisementGarderMeilleurCamion(pop[p2]);
				do {
					p1 = selectIndiv();
					p2 = selectIndiv();
					fils = pop[p1].croisementLOX(pop[p2]);
				}while(compareFitnessPop(fils[0]) > 10 &&compareFitnessPop(fils[1]) > 10);
				/*
				if (compareFitnessPop(fils[0]) > 5) {
					fils[0] = new Solution(g,1);
				}
				if (compareFitnessPop(fils[1]) > 5) {
					fils[1] = new Solution(g,0);
				}*/
				
				fils[0].p1 = p1;;
				fils[0].p2 = p2;
				fils[1].p1 = p2;;
				fils[1].p2 = p1;
				
				//mutation

				//fils[0].mutOrdonnerCamion();
				//fils[1].mutOrdonnerCamion();
				double probMut = s.nextFloat();
				if(probMut < 0.2){
					fils[0].mutEchange2Villes();
					fils[1].mutEchange2Villes();
					//fils[0].mutOrdonnerCamion();
					//fils[1].mutOrdonnerCamion();
					//fils[1].mutClientSuivantPlusProche();
				}/* else if(probMut < 0.5 && cpt >= 1500){
					//fils[0].mutEchange2Villes();
					//fils[1].mutEchange2Villes();
				}*/
				
				
				
				/* else if(probMut >= 0.1 && probMut < 0.2 && numGen > nbGeneration / 2){
					fils[0].mutEchangerOrdreCamion();
					fils[1].mutEchangerOrdreCamion();
				} else if(probMut >= 0.2 && probMut < 0.3 && numGen > nbGeneration / 2){
					fils[0].mutPermuterOrdreCamion();
					fils[1].mutPermuterOrdreCamion();
				}*/
				/*if(numGen < 20){
					fils[0].rechercheTabou(10);
					fils[1].rechercheTabou(10);
					fils[0].genererCoupureDescente();
					fils[1].genererCoupureDescente();
				}*/
				
				/*if (cpt > 1500) {
					fils[0].genererCoupuresMinimum();
					fils[1].genererCoupuresMinimum();
				}*/
				
				fils[0].evaluer();
				fils[1].evaluer();
				individuCroise[i] = fils[0];
				individuCroise[i+1] = fils[1];
			}
			//remplacement
			for(int i = 0; i < taillePop - nbPopConserver; i++){
				pop[i+nbPopConserver] = individuCroise[i];
			}
			//calculEntropie(numGen);
			trierSol();
			/*if (numGen%100==1)
				pop[0].rechercheTabou(100);*/
			getFitnessMeilleurSol(numGen,0);
			numGen++;
			
			//remise d'al�atoire si tro stable
			/*
			if (cpt==3000) {
				for (int i = pop.length - 1; i >= 9 * pop.length / 10; i--) {
					pop[i] = new Solution(g,0);
					pop[i].rechercheTabou(100);
				}
				trierSol();
				AG41.debugPrintln("remise d'al�atoire");
				afficherTouteSol();
				//lireClavier();
			} else if (cpt==6000) {
				for (int i = pop.length - 1; i >= pop.length / 2; i--) {
					pop[i] = new Solution(g,0);
					pop[i].rechercheTabou(100);
				}
				trierSol();
				AG41.debugPrintln("remise d'al�atoire");
				//lireClavier();
			}else if (cpt==10000) {
				for (int i = pop.length - 1; i >= 0; i--) {
					pop[i] = new Solution(g,0);
					pop[i].rechercheTabou(100);
				}
				trierSol();
				AG41.debugPrintln("remise d'al�atoire");
				//lireClavier();
				cpt = 0;
			}*/

			
			
			if(numGen==4000) {
				afficherTouteSol();
				lireClavier();
			}
			
			AG41.debugPrintln("*****"+numGen+"***** best : "+this.bestSolutionEver.fitness+","+" meilleurActuel : "+ pop[0].fitness+"   admissible : "+this.bestSolutionEver.admissible());

		}
		this.bestSolutionEver.evaluer();
	}
	
	
	public void algoGenetiqueAntoineRapide(int duration, double part){

		int numGen = 0;
		getFitnessMeilleurSol(numGen,0);
		int nbPopConserver=(int)(taillePop / 10);
		long dateDebut = new Date().getTime() ;
		long dateCourante = new Date().getTime() ;
		
		if(typeSelection==0) {
		calculerProbaRanking();
		AG41.debugPrintln("selection par ranking");
		} else {
		AG41.debugPrintln("selection par roue biaisee");
		}
		
		do {
			Solution []individuCroise = new Solution[taillePop - nbPopConserver];
			int p1,p2;
			Solution []fils = new Solution[2];
			if(typeSelection==1)
				calculerProbaRoueBiaise();
			for(int i = 0; i < taillePop - nbPopConserver - 1; i+=2){
				//selection
				p1 = selectIndiv();
				p2 = selectIndiv();
				double probCrois = s.nextFloat();
				if (probCrois < 0.25) {
					fils = pop[p1].croisementLOX(pop[p2]);
				} else if (probCrois >= 0.25 && probCrois < 0.5) {
					fils = pop[p1].croisementContenuCamion(pop[p2]);
				} else if (probCrois >= 0.5 && probCrois < 0.75) {
					fils = pop[p1].croisement1X(pop[p2]);
				} else {
					fils = pop[p1].croisementContenuCamion(pop[p2]);
					//fils = pop[p1].croisementGarderMeilleurCamion(pop[p2]);
				}
				
				fils[0].p1 = p1;;
				fils[0].p2 = p2;
				fils[1].p1 = p2;;
				fils[1].p2 = p1;
				
				//mutation
				double probMut = s.nextFloat();
				//if (cpt <3000) {
					if(probMut < 0.3){
						fils[0].mutEchange2Villes();
						fils[1].mutEchange2Villes();
					} else if(probMut >= 0.1 && probMut < 0.2){
						//fils[0].mutClientSuivantPlusProche();
						//fils[1].mutClientSuivantPlusProche();
					} else if(probMut >= 0.2 && probMut < 0.3){
						//fils[0].mutPermuterOrdreCamion();
						//fils[1].mutPermuterOrdreCamion();
					} else if(probMut >= 0.3 && probMut < 0.4){
						//fils[0].mutOrdonnerCamion();
						//fils[1].mutOrdonnerCamion();
						//fils[0].mutAjouterCamion();
						//fils[1].mutAjouterCamion();
					}	else if(probMut >= 0.4 && probMut < 0.5){
						//fils[0].genererCoupuresMinimum();
						//fils[1].genererCoupuresMinimum();
					}
				/*}else{
					if(probMut < 0.4){
						fils[0].mutEchange2Villes();
						fils[1].mutEchange2Villes();
					} else if(probMut >= 0.4 && probMut < 0.6){
						//fils[0].mutClientSuivantPlusProche();
						//fils[1].mutClientSuivantPlusProche();
					} else if(probMut >= 0.6 && probMut < 0.8){
						fils[0].mutPermuterOrdreCamion();
						fils[0].mutPermuterOrdreCamion();
					} else if(probMut >= 0.8 && probMut < 1){
						fils[0].mutOrdonnerCamion();
						fils[1].mutOrdonnerCamion();
						//fils[0].mutAjouterCamion();
						//fils[1].mutAjouterCamion();
					}	else if(probMut >= 0.8 && probMut < 1){
						//fils[0].genererCoupuresMinimum();
						//fils[1].genererCoupuresMinimum();
					}
				}*/
					
				fils[0].evaluer();
				fils[1].evaluer();
				individuCroise[i] = fils[0];
				//AG41.debugPrintln("tableau.length : "+individuCroise.length+" i : "+i);
				individuCroise[i+1] = fils[1];
			}
			//remplacement
			for(int i = 0; i < taillePop - nbPopConserver; i++){
				pop[i+nbPopConserver] = individuCroise[i];
			}
			
			if (((dateCourante - dateDebut) > duration * 0.25) && numGen%100==0) {
				int p3 = (int)(s.nextFloat()*taillePop);
				int p4 = selectIndiv();
				pop[p3] = new Solution(g,0);
				fils = pop[p3].croisementContenuCamion(pop[p4]);
				pop[(int)(s.nextFloat()*taillePop)] = fils[0];
				pop[(int)(s.nextFloat()*taillePop)] = fils[1];
			}
			
			trierSol();
			
			getFitnessMeilleurSol(numGen,dateCourante - dateDebut);
			numGen++;
			dateCourante = new Date().getTime();
		}while((dateCourante - dateDebut) < duration * part);
		AG41.debugPrintln(numGen+" generation en : "+((dateCourante - dateDebut)/1000)+" sec");
		this.bestSolutionEver.evaluer();
	}
	
	public void algorithmeHybride(int duration, double geneticPart, double tabouPart) {
		System.out.println("****** Debut Algo Genetique ******");
		algoGenetiqueAntoineRapide(duration, geneticPart);
		System.out.println("****** Fin Algo Genetique ******");
		System.out.println("****** Debut de la recherche tabou ******");
		pop[0].rechercheTabou(duration, tabouPart);
		System.out.println("****** Fin de la recherche tabou ******");
		pop[0].evaluer();
		getFitnessMeilleurSol(0,0);
	}
	
	
	//}while(fils[0].hentropieMaxAvec(pop,nbPopConserver)>taillePop * 0.95 || fils[1].hentropieMaxAvec(pop,nbPopConserver)>taillePop * 0.95);
	public void algoGenetiqueEntropie(int nbGeneration){
		
		//lireClavier();

		int numGen = 0;
		getFitnessMeilleurSol(numGen,0);
		int nbPopConserver = taillePop / 10;
		AG41.debugPrintln("appuyer sur entree pour continuer...");

		while(numGen < nbGeneration){
			//afficherTouteSol();
			
			Solution []individuCroise = new Solution[taillePop];
			int p1,p2;
			Solution []fils = new Solution[2];
			
			
			if(typeSelection==1 && cpt < 1000)
				calculerProbaRoueBiaise();
			
			for(int i = 0; i < taillePop - nbPopConserver; i+=2){
				//selection
				
				//do{
				if(typeSelection==2 || cpt >= 1000) {
					p1=(int)Solver.s.nextFloat()*taillePop;
					p2=(int)Solver.s.nextFloat()*taillePop;
				} else {
					p1 = selectIndiv();
					p2 = selectIndiv();
				}
				
				//croisement
				if (nbGeneration%3==0) {
					//fils = pop[p1].croisementSequenceClient(pop[p2]);
					fils = pop[p1].croisement1X(pop[p2]);
				} else if (nbGeneration%3==1) {
					fils = pop[p1].croisement1X(pop[p2]);
				} else {
					fils = pop[p1].croisementContenuCamion(pop[p2]);
				}
				//test
				
				fils[0].p1 = p1;;
				fils[0].p2 = p2;
				fils[1].p1 = p2;;
				fils[1].p2 = p1;
				
				double probMut = Solver.s.nextFloat();
				if(probMut < 0.2 && cpt < 1500){
					fils[0].mutEchange2Villes();
					fils[1].mutEchange2Villes();
				} else if(probMut < 0.5 && cpt >= 1500){
					fils[0].mutEchange2Villes();
					fils[1].mutEchange2Villes();
				}
				AG41.debugPrintln(fils[0].entropieMaxAvec(pop,nbPopConserver));
				AG41.debugPrintln(fils[1].entropieMaxAvec(pop,nbPopConserver));
			//}while(fils[0].hentropieMaxAvec(pop,nbPopConserver)>taillePop * 0.95 || fils[1].hentropieMaxAvec(pop,nbPopConserver)>taillePop * 0.95);
			
			
				if (cpt > 1500) {
					fils[0].genererCoupuresMinimum();
					fils[1].genererCoupuresMinimum();
				}
				
				fils[0].evaluer();
				fils[1].evaluer();
				individuCroise[i] = fils[0];
				individuCroise[i+1] = fils[1];
			}
			//remplacement
			for(int i = 0; i < taillePop - nbPopConserver; i++){
				pop[i+nbPopConserver] = individuCroise[i];
			}
			trierSol();
			/*if (numGen%100==1)
				pop[0].rechercheTabou(100);*/
			getFitnessMeilleurSol(numGen,0);
			numGen++;
			
			//remise d'al�atoire si tro stable
			
			if (cpt==3000) {
				for (int i = pop.length - 1; i >= 9 * pop.length / 10; i--) {
					pop[i] = new Solution(g,0);
					//pop[i].rechercheTabou(100);
				}
				trierSol();
				AG41.debugPrintln("remise d'aleatoire");
				afficherTouteSol();
				lireClavier();
			} else if (cpt==6000) {
				for (int i = pop.length - 1; i >= pop.length / 2; i--) {
					pop[i] = new Solution(g,0);
					//pop[i].rechercheTabou(100);
				}
				trierSol();
				AG41.debugPrintln("remise d'aleatoire");
				//lireClavier();
			}else if (cpt==10000) {
				for (int i = pop.length - 1; i >= 0; i--) {
					pop[i] = new Solution(g,0);
					//pop[i].rechercheTabou(100);
				}
				trierSol();
				AG41.debugPrintln("remise d'aleatoire");
				//lireClavier();
				cpt = 0;
			}

			
			
			/*if(numGen < 20)
				lireClavier();*/
			
			AG41.debugPrintln("*****"+numGen+"***** best : "+this.bestSolutionEver.fitness+","+" meilleurActuel : "+ pop[0].fitness+"   admissible : "+this.bestSolutionEver.admissible());
			if (!this.bestSolutionEver.admissible()) {
				lireClavier();
				this.bestSolutionEver.afficher();
			}
		}
		this.bestSolutionEver.evaluer();
	}
	
	
	public void genererPopDepart(int popMax,int typeGeneration) {
		ArrayList<Solution> popDepart = new ArrayList<Solution>();
		
		for (int i = 0; i < popMax; i++) {
			Solution s = new Solution(g,typeGeneration);
			if(popDepart.size()==0){
				popDepart.add(s);
			}else{
				int j;
				for(j = 0; j < popDepart.size()&&s.fitness > popDepart.get(j).fitness; j++){}
				popDepart.add(j,s);
			}
			
		}
		for (int i = 0; i < taillePop; i++) {
			pop[i] = popDepart.get(i);
			pop[i].afficher();
		}
		getFitnessMeilleurSol(0,0);
	}

	public void test() {
		AG41.debugPrintln("avant croisement : ");
		pop[0].afficher();
		pop[1].afficher();
		
		Solution[]ensembleFils = pop[0].croisement1X(pop[1]);
		

		AG41.debugPrintln("apres croisement : ");
		ensembleFils[0].afficher();
		ensembleFils[1].afficher();
		
		//afficherpop();
	}
	
	/*************************CROISEMENT******************************************/
	
	//croisement sequence : on prend une serie de 2 ville dans la premiere solution
	//et on la reporte dans la deuxieme solution
	public void croisement_sequence(Solution sol1, Solution sol2, int iteration ) {
		AG41.debugPrintNewLine();
		int[] clientReference = new int[2];
		boolean echange = false;
		int j =0;
		int clientTemp = 0;
		int indexTemp = 0;
		boolean suivantSemblable = false;
		for (int i = 0; i < iteration; i++) {
			int indexClient = (int)(Solver.s.nextFloat()*(Solution.nbClient - 2));	
			while (sol2.Clients[Solution.nbClient - 1] == sol1.Clients[indexClient])
				indexClient = (int)(Solver.s.nextFloat()*(Solution.nbClient - 2));
			clientReference[0] = sol1.Clients[indexClient];
			clientReference[1] = sol1.Clients[indexClient + 1];
			echange = false;
			j = 0;
			while (echange == false) {
				if (sol2.Clients[j] == clientReference[0]) {
					if (clientReference[1] == sol2.Clients[j+1])
						suivantSemblable = true;
					clientTemp = sol2.Clients[j+1];
					indexTemp = j + 1;
					sol2.Clients[j+1] = clientReference[1];
					echange = true;
				}
				j++;
			}
			echange = false;
			j = 0;
			while (echange == false && suivantSemblable != true) {
				if (sol2.Clients[j] == clientReference[1] && j != indexTemp) {
					sol2.Clients[j] = clientTemp;
					echange = true;
				}
				j++;
			}
		}
	}
	/*
	//on croise les coupure par lintermediaire des contenu de camion :
	//on recupere le contenu du premier camion de sol1 et on met une coupure sur
	//sol2 juste avant que le premier camion depasse cette capacit� et ainsi de suite
	public void croisement_contenu_camion(Solution sol1, Solution sol2) {
		int[] contenuCamion1 = sol1.getContenuCamion();
		int demandeTemp = 0;
		//on supprime les coupure de sol2
		for (int i = 0; i < sol2.Coupures.length; i++) {
			sol2.Coupures[i] = false;
		}
		//indice de parcours de contenuCamion
		int j = 0;
		//boucle de calcul des contenu
		for (int i = 0; i < sol2.nbClient - 1; i++) {
			//si la somme des contenu des client deja parcouru est inferieur 
			//au contenu du camion de sol1, on continu a ajouter
			if (demandeTemp + g.tabNoeud[sol2.Clients[i+1]].demande < contenuCamion1[j]) {
				demandeTemp += g.tabNoeud[sol2.Clients[i]].demande;
			//sinon, on ajoute une coupure, on passe au camion suivant et on remet a 0 
			} else {
				sol2.Coupures[i] =  true ;
				demandeTemp = 0;
				j++;
			}
		}
		//si le dernier camion depasse 200, on rajoute une coupure au milieu
		boolean test = false;
		int i = sol2.Coupures.length - 1;
		int contenuDernierCamion = 0;
		while (test == false) {
			test = sol2.Coupures[i];
			i--;
		}
		for (int k = i; k< sol2.Coupures.length; k++) {
			contenuDernierCamion += g.tabNoeud[sol2.Clients[k]].demande;
		}
		if (contenuDernierCamion > 200) {
			int r = (int)((Solver.s.nextFloat()*((sol2.Coupures.length)-i)));
			sol2.Coupures[r+i] = true;
		}
	}*/

	/*************************MUTATION************************************************/
	/*
	//invers� les client entre 2 bornes (attention coupure a reg�n�rer derriere)
	public void mut_InversionSequence (Solution sol1, int seq) {
		if (seq > sol1.nbClient) {
			seq = sol1.nbClient - 1;
		}
		int borneBasse = (int)(Solver.s.nextFloat()*((sol1.nbClient)-seq - 1));
		int borneHaute = borneBasse + seq;
		for (int i = borneBasse; i< (int)((borneHaute + borneBasse)/2) ; i++) {
			sol1.permuterClient(i, borneHaute - i);
		}
	}
	
	//ajoute un nb de camion pass� en argument (avec test pour ne pas depasser 25
	public void g�n�rerCoupureAl�atoire(Solution sol1, int nbCamions) {
		if (nbCamions + sol1.getContenuCamion().length >= 25) {
			nbCamions = 25 - sol1.getContenuCamion().length;
		}
		for (int i = 0; i < nbCamions; i++) {
			int r = (int)(Solver.s.nextFloat()*sol1.Clients.length - 1);
			while(sol1.Coupures[r] = =true) {
				r = (int)(Solver.s.nextFloat()*sol1.Clients.length - 1);
			}
			sol1.Coupures[r] = true;
		}
	}
	
	//echange un client entre deux camions
	//code tre bancale a tester, bocou de probleme de limite de tableau
	//sa metonnerai que sa passe du premier coup
	//a faire : tester si c'est le seul client de ce camion (si i a true et i - 1 a tru, => i a false)
	public void mut_ChangerClientdeCamion(Solution sol1, int nbChangement) {
		//boucle pour le nombre de changement
		for (int iter = 0; iter < nbChangement; iter++) {
			//on selectionne un client
			int r = (int)(Solver.s.nextFloat()*sol1.Clients.length - 2)+1;
			boolean test = false;
			//i est egal au nombre de camion - 1 (donc au nombre de coupure)
			int i = sol1.getContenuCamion().length - 1;
			//tant que il y a des coupure et qu'on ne trouve pa un camion avec assez
			//de place on boucle
			while (!test && i > 0) {
				if (sol1.getContenuCamion()[i] + g.tabNoeud[sol1.Clients[r]].demande <= 200) {
					test = true;
				}
				i--;
			}
			//si on en a trouver un qui peut deservir ce client
			if (test) {
				int camionR = sol1.Clients[r]; //client qui va changer de camion
				int numCamion = sol1.getContenuCamion().length - 1 - i; // numero du nouveau camion choisi
				int j = sol1.Coupures.length - 1;
				//boucle pour trouver l'index de fin du camion choisi
				while(numCamion > 0) {
					if (sol1.Coupures[j] == true) {
						numCamion--;
					}
					j--;
				}
				//boucle qui va decaler les client tout en conservant les coupure
				//elle itere entre le client choisi r et l'index de la fin du camion choisi j
				for (int k = r; k < j; k ++) {
					//si on trouve une coupure et qu'elle n'est pas a la fin, on la decale
					//un cran vers la gauche
					if (sol1.Coupures[k] == true && k < sol1.Coupures.length) {
						sol1.Coupures[k] = false;
						sol1.Coupures[k-1] = true;
					}
					//on decale tous les client vers la gauche
					sol1.Clients[k] = sol1.Clients[k+1];
				}
				//on ajoute notre client au camion choisi
				sol1.Clients[j] = camionR;
			}
		}
	}
	
	//invers� l'ordre des clients d'un camion selectionn� al�atoirement
	public void mut_EchangerOrdreCamion (Solution sol1) {
		//on selection un des camions
		int r = (int)(Solver.s.nextFloat()*(sol1.getContenuCamion().length - 1));
		int i = 0;
		//on cherche la borne basse
		while(r > 0) {
			//si on trouve un true on arrete
			if (sol1.Coupures[i] = =true)
				r--;
			i++;
		}
		//on reaprt de i
		int j = i;
		//on cherche la deuxieme coupure ou la fin du tableu
		while (!sol1.Coupures[j] && j < sol1.Coupures.length) {
			j++;
		}
		//on inverse entre i et j
		for (int k = i; k < j; k++) {
			sol1.permuterClient(k, j - k);
		}
	}
	
	//decale les client en gardant les coupures et verifie a la fin si les
	//coupure sont toujours valable et les redefini si necessaire
	public void mut_DecalerSequence (Solution sol1) {
		//boucle ki decale les client un cran a droite
		int temp = sol1.Clients[sol1.nbClient - 1];
		for(int i = sol1.nbClient - 1; i > 0; i++) {
			sol1.Clients[i] = sol1.Clients[i-1];
		}
		sol1.Clients[0] = temp;
		//boucle qui verifie les coupures
		//pour chaque camion
		for (int i = 0; i < sol1.getContenuCamion().length; i++) {
			int numCamion = i + 1; //numero du camion tester a cet iteration
			int j = 0; //index de parcours du tableau de coupure
			//tant que son contenu est > 200
			while (sol1.getContenuCamion()[i] > 200) {
				// tant que numCamion n'est pas a 0 et qu'on ne depasse pas du tableau
				while (numCamion > 0 && j < sol1.nbClient - 1) {
					if (sol1.Coupures[j]) // si on arrive a une coupure
						numCamion--; //on diminue numCamion
					j++;
				}
				// si on est pas a 0, on recule la coupure de 1 pour diminuer
				// la contenance du camion
				if (j > 0) {
					sol1.Coupures[j] = false;
					sol1.Coupures[j-1] = true;
				}
			}
		}
		// si le dernier camion est tro plein, on rajoute un camion pour diviser 
		// sa tourn�e par deux
		if (sol1.getContenuCamion()[sol1.getContenuCamion().length - 1] > 200) {
			int k = sol1.nbClient - 2;
			while (!sol1.Coupures[k]) {
				k--;
			}
			sol1.Coupures[(int)((k + sol1.nbClient - 2)/2)] = true;
		}
	}
	
	//red�fini les coupure pour que les camion soit le plus plein possible
	public void G�n�rerCoupureMax(Solution sol1) {
		int demandeTotal = 0;
		int nbDemande = 0;
		for(int i = 0; i < sol1.nbClient - 1; i++){
		demandeTotal+=g.tabNoeud[sol1.Clients[i]].demande;
		if(demandeTotal + g.tabNoeud[sol1.Clients[i+1]].demande > 200){
			sol1.Coupures[i] = true;
			demandeTotal = 0;
			nbDemande = 0;
		}
		nbDemande++;
		}
	}
	*/
	/*************************AFFICHAGE DE GRAPHE DE SOL******************************/
	public void afficherpop(){
		double[][]t = new double[taillePop][2];
		for(int i = 0; i < taillePop; i++){
			t[i][0] = i;
			t[i][1] = pop[i].fitness;
		}
		new FrmChart(t).setVisible( true );
	}
	
	public void afficherEntropie(){
		double[][]t = new double[entropie.length][2];
		for(int i = 0; i < entropie.length; i++){
			t[i][0] = i;
			t[i][1] = this.entropie[i];
		}
		new FrmChart(t).setVisible( true );
	}
	
	public void afficherProb(){
		double[][]t = new double[taillePop][2];
		for(int i = 0; i < taillePop; i++){
			t[i][0] = i;
			t[i][1] = (pop[i].probSelect);
		}
		new FrmChart(t).setVisible( true );
	}

	/************************ UTILES ******************************/

 	public int sommeTableau(int[] tab) {
 		int somme = 0;
 		for (int i = 0; i < tab.length; i++) {
 			somme += tab[i];
 		}
 		return somme;
 	}

}