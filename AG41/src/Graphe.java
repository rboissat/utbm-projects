/* Graphe.java: Dessine un graphe
 * 
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Graphe {
	public Noeud []tabNoeud;
	int nbNoeud;
	public double [][]matriceDistance;
	/*
	 * Graphe(String url) genere un graphe a partir du fichier specifier
	 * */
	public Graphe(File inputFile) {
		ArrayList<String> l = new ArrayList<String>();
		String line = "";
		
		try {
	    	  BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
	    	  //System.out.println("debut");
	    	  while (line != null) {
	    		  line = reader.readLine();
	    		  if (line != null && line.matches("^(?:\\s*\\d{1,}){7}.*")) {l.add(line);}
	    	  }
	    	  nbNoeud = l.size();
	    	  tabNoeud = new Noeud[nbNoeud];
	    	  for(int i = 0; i < l.size(); i++){
	    		  String s = l.get(i);
	    		  // DEBUG: a comparer avec la ligne 89
	    		  // System.out.println("String "+s);
	    		  tabNoeud[i] = convStringToNode(s);
	    	  }
	    	  matriceDistance = new double[nbNoeud][nbNoeud];
	    	  calculerDistance();//calculerMatriceRetard();
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	}

	/**
	 * calcul pour chaque couple de clients (1,2) (0,1), le retard minimum.
	 * si celui - ci est �gale � 0
	 */
	public double [][]mRetard;
	public int calculerMatriceRetard(){
		int nbBonCouple = 0;
		mRetard = new double[nbNoeud][nbNoeud];
		for(int i = 0;i < nbNoeud; i++){
			for(int j = 0;j < nbNoeud; j++){
				double hArriveMinimumEnJ = tabNoeud[i].ouverture + tabNoeud[i].duree_livr + calculerDureeTrajet(i,j);
				
				if(hArriveMinimumEnJ > tabNoeud[j].fermeture){
					mRetard[i][j]=hArriveMinimumEnJ - tabNoeud[j].fermeture;
				}else{
					mRetard[i][j]=hArriveMinimumEnJ - tabNoeud[j].fermeture;
					nbBonCouple++;
				}
			}
			
		}
		
		return nbBonCouple;
	}
	
	
	public int getRetardColonneClientRestant(int depart,ArrayList<Integer> clientRestant){
		double min = Double.MAX_VALUE;
		int indMin=-1;
		for(int i = 0;i < clientRestant.size();i++){
				if(mRetard[depart][clientRestant.get(i)]<min){
					indMin = i;
					min = mRetard[depart][clientRestant.get(i)];
				}
			}
		return indMin;
	}
	
	public static void printNombre(double n,int nMax){
		int nbArrondi=(int)n;
		String s="";
		if(n < 1 && n > 0){nbArrondi = 1;}
		while(nbArrondi < nMax && nMax > 10){
			s+=" ";
			nMax = nMax / 10;
			
		}

		System.out.print(s + nbArrondi);
	}
	
	public void afficherMatriceRetard(){
		for(int i = 0;i < nbNoeud; i++){
			for(int j = 0;j < nbNoeud; j++){
					Graphe.printNombre(mRetard[i][j],10000);
			}
			System.out.println();
		}
	}
	
	public void calculerRetardPotentiel(int i, int j){
		
	}
	
	void calculerDistance(){
  	  for(int i = 0; i < nbNoeud; i++){
  		  for(int j = i + 1; j < nbNoeud; j++){
  			  matriceDistance[i][j]=Math.sqrt((tabNoeud[i].x - tabNoeud[j].x)*(tabNoeud[i].x - tabNoeud[j].x)+(tabNoeud[i].y - tabNoeud[j].y)*(tabNoeud[i].y - tabNoeud[j].y));
  		  }
  	  }
	}
	
	/**
	 * @param s est une ligne du tableau du fichier (ex : R112.txt)
	 * @return le noeud qui correspond.
	 */
	public Noeud convStringToNode(String s){

		int []int_tab = new int[7];
		if(s!=null){
			String buf="";
			char c;
			int numParam = 0;
			for (int i = 0; i < s.length(); i++){
				c = s.charAt(i);
				
				if (c!=' ' && c>='0' && c<='9' && i < s.length()-1) {
				// Si on est avant ou a l'avant - dernier chiffre	
					buf+=c;
				
				} else if (c!=' ' && c>='0' && c<='9') {
				// Si on est au dernier chiffre	
					buf+=c;
					int_tab[numParam]=Integer.valueOf(buf);
					
				} else if(buf.isEmpty() == false){
				// Sinon si on est dans la ligne
					int_tab[numParam]=Integer.valueOf(buf);
					buf="";
					numParam++;
				
				}
			}
			
			/*for (int i = 0; i < 7; i++) {
				//System.out.print(int_tab[i] + " ");
			}*/
		}			
		//System.out.println(int_tab[0]+","+int_tab[1]+","+int_tab[2]+","+int_tab[3]+","+int_tab[4]+","+int_tab[5]+","+int_tab[6]);
		Noeud p = new Noeud(int_tab[0],int_tab[1],int_tab[2],int_tab[3],int_tab[4],int_tab[5],int_tab[6]);
		return p;
	}
	
	/*
	 * calcul la duree necessaire pour aller du noeud d'indice i1 vers i2 (= distance)
	 */

	public double calculerDureeTrajet(int i, int j){
		return (i < j) ? matriceDistance[i][j] : matriceDistance[j][i];
		//return Math.sqrt((tabNoeud[i].x - tabNoeud[j].x)*(tabNoeud[i].x - tabNoeud[j].x)+(tabNoeud[i].y - tabNoeud[j].y)*(tabNoeud[i].y - tabNoeud[j].y));
	}
	
	public double retard(int c, double h){
		double r = h-tabNoeud[c].fermeture;
		return (r > 0) ? r : 0;
	}
	
	/*
	 * permet de recuperer la nouvelle heure en cas d'attente ou non
	 */
	public double attente(int c,double h){
		double a=tabNoeud[c].ouverture-h;
		return (a > 0) ? a : 0;		
	}
}
