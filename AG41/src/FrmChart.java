import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * @author ludo
 * @since  11 avril 2004
 * 
 * class d exemple permettant de montrer l utilisation de la class PnlChart
 * ca fait rien d autre ;o) 
 * mais on peu y voir qd mem aussi ;o)
 * un exemple de JTable 
 * un exemple d utilisation de JChooserColor
 * un exemple d utilisation d ActionListener (2) :p)
 * 
 */
public class FrmChart extends JFrame
{
	private static final long serialVersionUID = -8009130248716117010L;
	/* private static final double[][] defaultValues =  {
                            { 1., 1. },
                            { 2., 2. },
                            { 3., 3. },
                            { 4., 3.5 },
                            { 5.2, 4. },
                            { 6.3, 4. },
                            { 7.4, 4. },
                            { 8., 4. },
                            { 9., 5. },
                            { 10., 6. },
                            { 11., 7. },
                            { 12., 8. },
                            { 13., 9. },
                            { 14., 10. },
                            { 15., 12. },
                            { 16., 14. },
                            { 17., 14.6 },
                            { 18., 15. },
                            { 19., 20. },
                            { 20., 10. },
                            { 21., 3. },
                                                    };*/
    // on a besoin de la reference sur le model de la table juste pour lui ajouter des ligne
    DefaultTableModel tmn = null;
    // on conserve une ref sur le graph pour pouvoir en modifier les proprietes a l aide du menu
    PnlChart pnlChart = null;    

    public FrmChart(double[][] defaultValues)
    {
        super();
        setSize( 500, 500 );
        setTitle( "Dessine un graph" );
        getContentPane().setLayout( new BorderLayout() );
        // la table
        JScrollPane sclp = new JScrollPane();
        sclp.setPreferredSize( new Dimension( 100, 500 ) );
        JTable table = new JTable();

        // le panelchart demande un defaultmodel dc on s assure de son existence ... ;o)
        tmn = new DefaultTableModel();
        table.setModel( tmn );
        // ici on init le tableau avec des valeur par default 
        // juste pour l exemple 
        {
            tmn.addColumn( "X" ); tmn.addColumn( "Y" );
            for (int i = 0; i < defaultValues.length; i++)
            {   // la j utilise des Double mais on peut faire des string c pareil
                Object[] vals = { new Double( defaultValues[ i ][0] ), new Double( defaultValues[ i ][ 1 ] ) };
                tmn.addRow( vals );
            }
        }
        
        sclp.getViewport().add( table );
        JPanel pnl = new JPanel();
        getContentPane().add( pnl, BorderLayout.WEST );
        pnl.setLayout( new BorderLayout() );
        pnl.add( sclp, BorderLayout.CENTER );
        // le bouton
        JButton btn = new JButton( "Add coords" );
        pnl.add( btn, BorderLayout.SOUTH );
        // determine l action du bouton (ajoute une ligne)
        btn.addActionListener( new ActionListener() { public void actionPerformed(ActionEvent e) { if ( tmn != null ) tmn.addRow( new Object[] { "0","0" } ); } } );
        // le graph 
        pnlChart = new  PnlChart();
        pnlChart.setTable( table ); 
        pnlChart.setPreferredSize( new Dimension( 400, 500 ) );
        pnlChart.setSize( new Dimension( 400, 500 ) );
        getContentPane().add( pnlChart, BorderLayout.CENTER );

        // pour quitter proprement la fenetre en une ligne
        addWindowListener( new WindowAdapter() { public void windowClosing(WindowEvent e) { System.exit( 0 ); } } );
        // alors elle est pas belle cette ligne ;o) h�h�

        // creation du menu 
        createMenu(); // on fait le menu une foi que on a crer le pnlChart
    }
    
    private void createMenu()
    {
        JMenuBar mBar = new JMenuBar();
        setJMenuBar( mBar );
        // creation des chex box
        ActionListener actions = new ActionsFlags();
        JMenu jm = new JMenu( "Options" );
        mBar.add( jm );
        JCheckBoxMenuItem chkItm = new JCheckBoxMenuItem( "Coords sur la souris" );
        chkItm.setActionCommand( "onMouse" );
        chkItm.addActionListener( actions );
        chkItm.setSelected( pnlChart.isBubleOnMouse() );
        jm.add( chkItm );
        chkItm = new JCheckBoxMenuItem( "Affiche les coords" );
        chkItm.setActionCommand( "coords" );
        chkItm.addActionListener( actions );
        chkItm.setSelected( pnlChart.isUseBuble() );
        jm.add( chkItm );
        chkItm = new JCheckBoxMenuItem( "Affiche les Points" );
        chkItm.setActionCommand( "node" );
        chkItm.addActionListener( actions );
        chkItm.setSelected( pnlChart.isShowNode() );
        jm.add( chkItm );
        chkItm = new JCheckBoxMenuItem( "Affiche la croix" );
        chkItm.setActionCommand( "cross" );
        chkItm.addActionListener( actions );
        chkItm.setSelected( pnlChart.isUseMouseCross() );
        jm.add( chkItm );
        chkItm = new JCheckBoxMenuItem( "Points sur courbe" );
        chkItm.setActionCommand( "onLine" );
        chkItm.addActionListener( actions );
        chkItm.setSelected( pnlChart.isNodeOnLine() );
        jm.add( chkItm );
        // creation des menu items couleur
        actions = new ActionsColors();
        jm = new JMenu( "Couleurs" );
        mBar.add( jm );
        JMenuItem mit = new JMenuItem( "Courbe" );
        mit.setActionCommand( "courbe" );
        mit.addActionListener( actions );
        jm.add( mit );
        mit = new JMenuItem( "Coords" );
        mit.setActionCommand( "coords" );
        mit.addActionListener( actions );
        jm.add( mit );
        mit = new JMenuItem( "Points" );
        mit.setActionCommand( "node" );
        mit.addActionListener( actions );
        jm.add( mit );
        mit = new JMenuItem( "Axes" );
        mit.setActionCommand( "axes" );
        mit.addActionListener( actions );
        jm.add( mit );
        mit = new JMenuItem( "Croix" );
        mit.setActionCommand( "cross" );
        mit.addActionListener( actions );
        jm.add( mit );
        
    }
    
    class ActionsFlags implements ActionListener
    {
        public void actionPerformed( ActionEvent e )
        {
            String action = e.getActionCommand();
            JCheckBoxMenuItem source = (JCheckBoxMenuItem)e.getSource();
            if ( action.equals( "node" ) )
                pnlChart.setShowNode( source.isSelected() );
            else if ( action.equals( "onMouse" ) )
                pnlChart.setBubleOnMouse( source.isSelected() );
            else if ( action.equals( "coords" ) )
                pnlChart.setUseBuble( source.isSelected() );
            else if ( action.equals( "cross" ) )
                pnlChart.setUseMouseCross( source.isSelected() );
            else if ( action.equals( "onLine" ) )
                pnlChart.setNodeOnLine( source.isSelected() );
            pnlChart.fireTriggerDraw();
            pnlChart.repaint();
        }
    }

    class ActionsColors implements ActionListener
    {
        public void actionPerformed( ActionEvent e )
        {
            String action = e.getActionCommand();
            if ( action.equals( "courbe" ) )
                pnlChart.setColorLine( getColor( "de la courbe. ", pnlChart.getColorLine() ) );
            else if ( action.equals( "coords" ) )
                pnlChart.setColorText( getColor( "du text. ", pnlChart.getColorText() ) );
            else if ( action.equals( "node" ) )
                pnlChart.setColorNode( getColor( "des points. ", pnlChart.getColorNode() ) );
            else if ( action.equals( "axes" ) )
                pnlChart.setColorAxes( getColor( "des axes. ", pnlChart.getColorAxes() ) );
            else if ( action.equals( "cross" ) )
                pnlChart.setColorCross( getColor( "de la croix. ", pnlChart.getColorCross() ) );

            pnlChart.fireTriggerDraw();
            pnlChart.repaint();
        }
    }
    
    private Color getColor( String titre, Color col )
    {
        return JColorChooser.showDialog( this, "Choix de la couleur " + titre, col );
    }
    
    public static void main(String[] args)
    {
    	
        //new FrmChart("").setVisible( true );
    }

    
    
}
