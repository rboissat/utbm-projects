/* Solution: Classe definissant la structure de donnees (deux vecteurs) solution
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;


public class Solution {
	static ArrayList<Integer> lnn = new ArrayList<Integer>(); //util pour la generation aleatoire
	
	public static int nbIter = 0;
	
	//***********************Forme de la solution*************************/
	public int []Clients;//{1,2,3,4,5,6,7,8,9};
	public boolean []Coupures;
	public static int nbClient=-1;
	/*ATTENTION, une coupure d'indice N => indique que apres l'element N, on rentre au depot*/
	/* ex: Clients=[32,34,22,31] et coupure=[0,2] => sol = 32|34,22|31 */
	
	//Fitness
	public double fitness=-1;
	public double distance=-1;
	public Graphe g = null;
	public double probSelect = 0;
	private int cpt = 0;

	public double []h;//heure cumule
	public double []r;//retard non cumule
	public double []a;//attente non cumule
	public double retard=-1;
	
	public int p1=-1,p2=-1;//pere p1 et p2
	//******************************CONSTRUCTEUR**************************/

	@SuppressWarnings("static-access")
	public Solution(Solution s){
		this.a=new double[nbClient];
		this.h=new double[nbClient];
		this.r=new double[nbClient];
		this.nbClient = s.nbClient;
		this.Clients = s.Clients.clone();
		this.Coupures = s.Coupures.clone();
		this.fitness = s.fitness;
		this.distance = s.distance;
		this.retard=s.retard;
		this.r = s.r.clone();
		this.h = s.h.clone();
		this.g = s.g;
		this.probSelect = s.probSelect;
	}
	
	@SuppressWarnings("static-access")
	public Solution(Graphe g, int typeGeneration){
		this.g = g;
		if(this.nbClient==-1){
			nbClient = g.nbNoeud - 1;
			AG41.debugPrintln("NBCLIENT="+nbClient);
		}		
		this.a=new double[nbClient];
		this.h=new double[nbClient];
		this.r=new double[nbClient];

		Coupures = new boolean[nbClient];
		for(int i = 0;i < nbClient; i++){
			Coupures[i]=false;
		}
		Coupures[nbClient - 1]=true;
		switch(typeGeneration){
		case 0:genererOrdreClient();genererCoupuresAdmissible();break;
		case 1:genererOrdreSansRetard();break;
		case 2:genererOrdreOriente();genererCoupuresAdmissible();break;
		case 3:genererOrdreClient();genererCoupuresMinimum();break;
		case 4:genererOrdreClientSimple();genererCoupuresMinimum();break;
		default:AG41.debugPrintln("t'es bien niquer!");System.exit(0);
		}

		//evaluer();genererCoupureDescente();  // NE PAS SUPPRIMER

		evaluer();
		
	}
	


	//**********************METHODE D'EVALUATION**************************/
	/**
	 * evalue la solution, et stoque sa valeur dans this.fitness (this.distance et this.retard)
	 * l'evaluation est appeler dans le constructeur, apres generation de la solution
	 */
	//evaluateur_windows.exe R112.txt 2 sol.txt
	public  void evaluer(){
		h[0]=0;
		distance = 0;
		evaluerDistanceUniquement(0,nbClient);
		retard = 0;
		evaluerRetardUniquement(0,nbClient);
		this.fitness = retard * Solver.penalite + distance;	
	}
	
	public  void evaluerDistanceUniquement(int dep,int fin){
		double temp=-1;
		for(int i = dep;i < fin; i++){
			if(i==0){
				temp = g.calculerDureeTrajet(0, Clients[i]);
			} else {
				if(!Coupures[i-1]){		//client suivant
					temp = g.calculerDureeTrajet(Clients[i-1], Clients[i]);
				} else {				//rentre au depot
					distance+=g.calculerDureeTrajet(Clients[i-1], 0);
					temp = g.calculerDureeTrajet(0, Clients[i]);
				}
			}
			distance+=temp;
		}
		distance+=g.calculerDureeTrajet(Clients[nbClient - 1],0);
		//AG41.debugPrintln(retard);
		this.fitness = retard * Solver.penalite + distance;		
	}
	
	public  void evaluerRetardUniquement(int dep,int fin){
		double h=this.h[dep];
		
		double temp=-1;
		double attente=0; double retardTemp=0;
		for(int i = dep;i < fin; i++){
			this.r[i]=0;
			this.a[i]=0;
			this.h[i]=h;
			if(i==0){
				temp = g.calculerDureeTrajet(0, Clients[i]);
				h = temp;
			} else {
				if(!Coupures[i-1]){		//client suivant
					temp = g.calculerDureeTrajet(Clients[i-1], Clients[i]);
					h+=temp;
					retardTemp=g.retard(Clients[i], h);
					retard+=retardTemp;
					this.r[i]=retardTemp;
				} else {				//rentre au depot
					temp = g.calculerDureeTrajet(0, Clients[i]);
					h = temp;
				}
			}

			attente=g.attente(Clients[i], h);
			h =h+ attente;
			this.a[i]=attente;
			h+=g.tabNoeud[Clients[i]].duree_livr;
		}
		//AG41.debugPrintln(retard);
		this.fitness = retard * Solver.penalite + distance;		
	}
	
	public  void supprimerRetard(int dep,int fin){
		for(int i = dep;i < fin; i++){
			retard-=this.r[i];
		}
	}
	
	public int precedent(int i){
		if(i!=0&&Coupures[i-1]==false){
			return Clients[i-1];
		}else{
			return 0;
		}
	}
	
	public int suivant(int i){
		if(Coupures[i]==false){
			return Clients[i+1];
		}else{
			return 0;
		}
	}
	
	public void supprimerCoutDistance(int i,int j){
		int avantI=precedent(i);
		int apresI=suivant(i);
		int avantJ=precedent(j);
		int apresJ=suivant(j);
		distance-=g.calculerDureeTrajet(avantI, Clients[i]);
		distance-=g.calculerDureeTrajet(Clients[i], apresI);
		distance-=g.calculerDureeTrajet(Clients[j], apresJ);
		if(j!=i+1){
			distance-=g.calculerDureeTrajet(avantJ, Clients[j]);
		}
	}
	
	public void ajouterCoutDistance(int i, int j){
		int avantI=precedent(i);
		int apresI=suivant(i);
		int avantJ=precedent(j);
		int apresJ=suivant(j);
		distance+=g.calculerDureeTrajet(avantI, Clients[i]);
		distance+=g.calculerDureeTrajet(Clients[i], apresI);
		distance+=g.calculerDureeTrajet(Clients[j], apresJ);
		if(j!=i+1){
			distance+=g.calculerDureeTrajet(avantJ, Clients[j]);
		}
	}
	

	
	public static int test=0;
	public void refreshCoutRetard(int i, int j){
		if(j<this.getNextCamion(i)){
			this.supprimerRetard(i, getNextCamion(i));
			this.evaluerRetardUniquement(i, getNextCamion(i));
		}else{
			this.supprimerRetard(i, getNextCamion(i));
			this.supprimerRetard(j, getNextCamion(j));
			this.evaluerRetardUniquement(i, getNextCamion(i));
			this.evaluerRetardUniquement(j, getNextCamion(j));
		}
		
	}
	
	public int getNextCamion(int i){//retourne la prochaine coupure+1;
		for(int j=i;j<nbClient;j++){
			if(Coupures[j]){
				return j+1;
			}
		}
		return nbClient;
	}
	
	//on supprime les couts de distance de liaison de i et j
	public void majFitnessAvantPermutation(int i, int j){
		if(i>j){int temp=i;i=j;j=temp;}
		this.supprimerCoutDistance(i,j);
	}
	
	public void majFitnessApresPermutation(int i, int j){
		if(i>j){int temp=i;i=j;j=temp;}
		this.ajouterCoutDistance(i,j);
		this.refreshCoutRetard(i,j);
		this.fitness = retard * Solver.penalite + distance;		
	}
	//************************GENERER***********************************************/
	public void genererOrdreClientSimple(){
		nbClient = g.nbNoeud - 1;
		Clients= new int[nbClient];
		for(int i=0;i<nbClient;i++){
			Clients[i]=nbClient-i;
		}
	}
	
	@SuppressWarnings("unchecked")
	public void genererOrdreOriente(){
		nbClient = g.nbNoeud - 1;
		if(Solution.lnn.size()==0){ //on initialise lnn si besoin
			for(int i = 1;i < nbClient + 1;i++){
				Solution.lnn.add(i);
			}
		}
		
		ArrayList<Integer> lnn= (ArrayList<Integer>) Solution.lnn.clone(); //on clone lnn
	
		//ArrayList<Integer> sol = new ArrayList<Integer>();
		
		//on genere le premier client
		Clients= new int[nbClient];
		int pind = Solver.s.nextInt(lnn.size());
		Clients[0]=lnn.get(pind);
		lnn.remove(pind);
		
		//on genere les autres.
		for(int i = 1;i < nbClient; i++){
			int indMin = g.getRetardColonneClientRestant(Clients[i-1],lnn);
			Clients[i]=lnn.get(indMin);
			lnn.remove(indMin);
			//if(g.mRetard[i][indMin]>0){
		}
		
	}

	@SuppressWarnings("unchecked")
	public void genererOrdreSansRetard(){
		do{
		nbClient = g.nbNoeud - 1;
		if(Solution.lnn.size()==0){ //on initialise lnn si besoin
			for(int i = 1;i < nbClient + 1;i++){
				Solution.lnn.add(i);
			}
		}
		
		for(int i = 0;i < Coupures.length; i++){
			Coupures[i]=false;
		}
		
		ArrayList<Integer> lnn=(ArrayList<Integer>)Solution.lnn.clone(); //on clone lnn
	
		//ArrayList<Integer> sol = new ArrayList<Integer>();
		
		//on genere le premier client
		double h = 0;
		Clients= new int[nbClient];
		int pind = Solver.s.nextInt(lnn.size());
		Clients[0]=lnn.get(pind);
		lnn.remove(pind);
		//MAJ de la nouvelle heure au 1er Client
		h = g.calculerDureeTrajet(0,Clients[0]);  //l'heur represente toujour l'heur de depart du client : i
		h =h+ g.attente(Clients[0], h)+g.tabNoeud[Clients[0]].duree_livr;
		double hmin = g.tabNoeud[Clients[0]].ouverture;
		double delta = h - hmin;
		//on genere les autres.
		for(int i = 1;i < nbClient; i++){
			int r = Solver.s.nextInt(lnn.size());
			//AG41.debugPrintln("FOCK"+(delta + g.mRetard[Clients[i-1]][lnn.get(r)]));
			
			if(delta + g.mRetard[Clients[i-1]][lnn.get(r)] <= 0){
				Clients[i]=lnn.get(r);
				h = h+ g.attente(Clients[i], h)+g.tabNoeud[Clients[i]].duree_livr;
				lnn.remove(r);
				h+=g.calculerDureeTrajet(Clients[i-1], Clients[i]);
				hmin = g.tabNoeud[Clients[i]].ouverture;
				delta = h - hmin;
			} else {
				Coupures[i-1]=true;
				
				pind = Solver.s.nextInt(lnn.size());
				Clients[i]=lnn.get(pind);
				lnn.remove(pind);
				h = g.calculerDureeTrajet(0, Clients[i])+g.tabNoeud[Clients[i]].duree_livr;
				hmin = g.tabNoeud[Clients[i]].ouverture + g.tabNoeud[Clients[i]].duree_livr;
				delta = h - hmin;
			}
		}
		}while(this.getNombreCamion()>25);
		//evaluer();
		//Solver.lireClavier();
		
	}

	
	@SuppressWarnings("unchecked")
	//**********************METHODE DE GENERATION ALEATOIRE**************************/
	private void genererOrdreClient(){
		nbClient = g.nbNoeud - 1;
		if(Solution.lnn.size()==0){ //on initialise lnn si besoin
			for(int i = 1;i < nbClient + 1;i++){
				Solution.lnn.add(i);
			}
		}
		
		ArrayList<Integer> lnn=(ArrayList<Integer>)Solution.lnn.clone(); //on clone lnn
		Clients= new int[nbClient];
		int r=-1;
		for(int i = 0;i < nbClient; i++){
			r=(int)((Solver.s.nextFloat()*(lnn.size()))); 
			Clients[i]=lnn.get(r);
			lnn.remove(r); //on se sert de lnn pour eviter de retirer plusieurs fois le meme client.
		}
	}
	
	public void genererCoupuresMinimum(){
		int demandeTotal = 0;
		int nbDemande = 0;

		for(int i = 0;i < nbClient - 1;i++){
		demandeTotal+=g.tabNoeud[Clients[i]].demande;
		
		if(demandeTotal + g.tabNoeud[Clients[i+1]].demande > 200){
			Coupures[i]=true;
			demandeTotal = 0;
			nbDemande = 0;
		}
		nbDemande++;
		}
	}
	
	public void genererCoupuresAdmissible(){
		
		do {
			int demandeTotal = 0;
			int nbDemande = 0;
			Coupures = new boolean[nbClient];
			for(int i = 0;i < nbClient - 1;i++){
				demandeTotal+=g.tabNoeud[Clients[i]].demande;
				
				if(demandeTotal + g.tabNoeud[Clients[i+1]].demande > 200){
					i = i-nbDemande+(int)(Solver.s.nextFloat()*(nbDemande))+1;
					Coupures[i]=true;
					
					demandeTotal = 0;
					nbDemande = 0;
				}
				nbDemande++;
			}
			Coupures[nbClient - 1]=true;
		}while(getContenuCamion().length > 25);
	}
	
	public void genererCoupureDescente(){
		//AG41.debugPrintln("descente");
		try {
			ArrayList<Solution> voisinage = new ArrayList<Solution>();
			//genererCoupuresAdmissible();
			genererCoupuresMinimum();
			do{
				voisinage = new ArrayList<Solution>();
				//AG41.debugPrintln("exec");
				for(int i = 0;i < nbClient; i++){	//on genere le voisinage
					Solution s = new Solution((Solution)this);
					s.Coupures[i]=!s.Coupures[i];
					s.evaluer();
					if(true){
						int j;
						for(j = 0;j < voisinage.size()&&s.fitness > voisinage.get(j).fitness; j++){}
						voisinage.add(j,s);
					}
				}

			}while(MAJSolReel(voisinage.get(0))&&voisinage.get(0).getNombreCamion()<25);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//this.afficher();
	}
	
	public int[] getMinAspiration() {
		int []indexMin = {0,0};
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < nbClient - 1; i++) {
			for (int j = i+1; j < nbClient; j++) {
				if (aspiration[i][j]<min && aspiration[i][j]>=0) {
					min = aspiration[i][j];
					indexMin[0]=i;
					indexMin[1]=j;
				}
			}
		}
		return indexMin;
	}
	
	public void afficherMatriceAspiration() {
		for(int i = 0;i < nbClient; i++){
			for(int j = 0;j < nbClient; j++){
					Graphe.printNombre(aspiration[i][j],100000);
			}
			AG41.debugPrintNewLine();
		}
	}
	
	public void afficherMatriceListeTabou() {
		for(int i = 0;i < nbClient; i++){
			for(int j = 0;j < nbClient; j++){
					Graphe.printNombre(listeTabou[i][j],100000);
			}
			AG41.debugPrintNewLine();
		}
	}
	
	public int[][]listeTabou;
	public int[][]aspiration;
	public Solution solTemp;
	
	double nbRetardjuste=0;
    double nbRetardfaux=0;
	//sauvegarde de l'ancienne recherche tabou, avec critere d'aspiration
	@SuppressWarnings("static-access")
	public void rechercheTabou(int duration, double part){
		int iter = 0;
		Solution sCourante = new Solution(this);
		sCourante.evaluer();
		Solution solTemp = new Solution(this);
		listeTabou = new int[this.nbClient][this.nbClient];
		aspiration = new int[this.nbClient][this.nbClient];
		//int[] indexMin;
		int iM = 0;
		int rM = 0;
		long dateDebut = new Date().getTime() ;
		//boolean dansOptiLocal = false;
		long dateCourante = new Date().getTime();
		do{
			double meilleurFitnessVoisinage=Double.MAX_VALUE;
				for(int r = 0;r < nbClient; r++){
					for(int i = r+1;i < nbClient; i++){	
						Solution s = new Solution(sCourante);
					 	s.majFitnessAvantPermutation(i, r);
						s.permuterClient(i,r);
					 	s.majFitnessApresPermutation(i, r);
						if (((s.fitness+1)<solTemp.fitness||(s.fitness<meilleurFitnessVoisinage&&listeTabou[r][i]<=iter))&&s.admissible()) {
							meilleurFitnessVoisinage=s.fitness;
							rM = r;
							iM = i;
						}
				}
				}
				
				sCourante.permuterClient(iM, rM);
				sCourante.evaluer();
					//2 * nbClient correspond e la valeur variable de la duree tabou... parametre optimum pour sortir de l'optimum local
					listeTabou[rM][iM]=iter + nbClient * 2;

				
			if(solTemp.fitness > sCourante.fitness){
				solTemp = new Solution(sCourante);
			}
			iter++;
			if (iter%200 == 0){
				AG41.debugPrintln("                                               Evaluation partiel="+meilleurFitnessVoisinage);
				AG41.debugPrintln("***** TABOU "+iter+"***** best : "+solTemp.fitness+","+" meilleurActuel : "+ sCourante.fitness+"   admissible : "+this.admissible()+" en "+(dateCourante - dateDebut)/1000+" sec");
			}
			dateCourante = new Date().getTime();
		}while((dateCourante - dateDebut) < duration * part);
		
		Solution.nbIter = iter;
		
		MAJSolReel(solTemp);
		
		Solution.nbIter = iter;
		
	}
	
	@SuppressWarnings("static-access")
	private boolean MAJSolReel(Solution s) {
		if(this.fitness<=s.fitness){cpt++;return false;}
		this.Clients = s.Clients.clone();
		this.Coupures = s.Coupures.clone();
		this.nbClient = s.nbClient;
		this.fitness = s.fitness;
		AG41.debugPrintln("amelioration fitness="+(int)this.fitness);
		this.distance = s.distance;
		this.retard = s.retard;
		this.g = s.g;
		cpt = 0;
		return true;
	}
	
	//********************UTILS*********************************************/
	
	//deplace un element au pif
	public void permuterClient(int i, int j){
	 	if(i==j){return;}
		int temp = Clients[i];
		Clients[i]=Clients[j];
		Clients[j]=temp;
}	
	

	
	public boolean permutationVerifiee(int i, int j) {
		 if(i==j){return true;}
		 int c1 = getVilleCamion(i);
		 int c2 = getVilleCamion(j);
		 //AG41.debugPrintln("permut de "+i+" "+j);
		 if(c1==c2){permuterClient(i,j);//AG41.debugPrintln("camion egal"); 
		 return true;}
		 if(g.tabNoeud[Clients[i]].demande==g.tabNoeud[Clients[j]].demande){
			 permuterClient(i,j); //AG41.debugPrintln("contenu equivalent"); 
			 return true;
		 }
		 //int []tContenue = getContenuCamion();
		 //afficher();
		 //AG41.debugPrintln("c1 : "+c1+", c2 : "+c2+", tContenu.length : "+getContenuCamion().length+", i : "+i+", j :"+j+", nbclient :"+nbClient);
		 int newContenu1 = getContenuCamion()[c1]-g.tabNoeud[Clients[i]].demande + g.tabNoeud[Clients[j]].demande;
		 int newContenu2 = getContenuCamion()[c2]+g.tabNoeud[Clients[i]].demande - g.tabNoeud[Clients[j]].demande;
		 if(newContenu1<=200 && newContenu2<=200){
			 permuterClient(i,j);//AG41.debugPrintln("permute normal"); 
			 return true;
		 }
		 
		 return false;
	}
	
	private int getVilleCamion(int v) {
		int c = 0;
		for(int i = 0; i<=v; i++){
			if(Coupures[i]){
				c++;
			}
		}
		if (c!=0) {
			c--;
		}
		return c;
	}

	public void permuterDepot(int i, int j){
		boolean temp = Coupures[i];
		Coupures[i]=Coupures[j];
		Coupures[j]=temp;
	}


	/*
	 * fournit l'ordre d'apparition des clients dans la solution
	 */
	public int[] getOrdreAparition(int []iv){
		int[] ordre = new int [iv.length];
		int nbTrouve = 0;
		
		for(int i = 0;i < nbClient; i++){
			for(int j = 0; j < iv.length; j++){
			if(iv[j]==Clients[i]){ //A optimiser
				ordre[nbTrouve]=Clients[i];
				nbTrouve++;
			}
			}
		} 
		//AG41.debugPrintln("nbtrouv "+nbTrouve);
		return ordre;
	}
	
	public boolean member(int e,int[] t){
		for(int i = 0;i < t.length; i++){
			if(e==t[i]){return true;}
		}
		return false;
	}
	
	/*
	 * fournit l'ordre d'apparition des clients n'etant pas membre de iv dans la solution
	 */
	public int[] getOrdreAparitionDesAutres(int []iv){
		//AG41.debugPrintln("nbClient="+nbClient+" iv.length="+iv.length);
		int[] ordre = new int [nbClient - iv.length];
		int nbTrouve = 0;
		
		for(int i = 0;i < nbClient; i++){
			if(!member(Clients[i],iv)){ //A optimiser
				//AG41.debugPrintln("ok"+i);
				ordre[nbTrouve]=Clients[i];
				nbTrouve++;
			
			}
		} 
		//AG41.debugPrintln("nbtrouv "+nbTrouve);
		return ordre;
	}
	
	public int[] getSequence(int debutSequence){
		return getSequence(debutSequence,nbClient);
	}
	
	public int[] getSequence(int debutSequence, int finSequence){
		if(debutSequence > finSequence){
			int temp = debutSequence;
			debutSequence = finSequence;
			finSequence = temp;
		}
		
		int []seq = new int[finSequence - debutSequence];
		//AG41.debugPrint("getseq=[");
		for(int i = debutSequence; i<finSequence; i++){
			//AG41.debugPrint(Clients[i]+",");
			seq[i - debutSequence]=Clients[i];
		}
		//AG41.debugPrintln("]");
		return seq;
	}
	

	public void setSequence(int debutCopie, int[] seq){
		setSequence(debutCopie,0,seq.length,seq);
	}
	
	
	private int CalculerPlusProche(int client) {
		int proche = 0;
		double min = Double.MAX_VALUE;

	  	for(int i = 0; i < client; i++){
	  		if (g.matriceDistance[i][client]<min) {
	  			min = g.matriceDistance[i][client];
	  			proche = i;
	  		}
		}
	  	for(int i = client; i < nbClient; i++){
	  		if (g.matriceDistance[client][i]<min) {
	  			min = g.matriceDistance[i][client];
	  			proche = i;
	  		}
		}
		
		return proche;
	}
	
	/**
	 * recopie la liste seq fournit jusqu'e l'element finCopie de cette liste.
	 * @param debutCopie
	 * @param finCopie
	 * @param seq
	 */
	public void setSequence(int debutCopie, int dejaCopier, int finCopie,int[] seq){
		//AG41.debugPrintln("debutCopie dans Clients:"+debutCopie+" dejaCopier="+dejaCopier+" finCopie="+finCopie);
		//AG41.debugPrint("setseq=[");
		for(int i = dejaCopier; i<finCopie; i++){
			Clients[i + debutCopie]=seq[i];
		}
		//AG41.debugPrintln("]");
	}
	
	//***********************CROISEMENT***********************************************//	
	public Solution[] croisement1X(Solution p2){
		Solution fils1 = new Solution(this);
		Solution fils2 = new Solution(p2);
		int indexCroisement=(int)(Solver.s.nextFloat()*nbClient);
		//AG41.debugPrintln("indexCroisement="+indexCroisement);
		fils1.setSequence(indexCroisement, p2.getOrdreAparition(this.getSequence(indexCroisement)));
		fils2.setSequence(indexCroisement, this.getOrdreAparition(p2.getSequence(indexCroisement)));
		Solution[] s={fils1,fils2};
		
		if(!fils1.admissible()){
			fils1.genererCoupuresAdmissible();
		}
		if(!fils2.admissible()){
			fils2.genererCoupuresAdmissible();
		}
		return s;
	}
	
	public Solution[] croisement2X(Solution p2){
		Solution fils1 = new Solution(this);
		Solution fils2 = new Solution(p2);
		int indexCroisement1=(int)(Solver.s.nextFloat()*(nbClient - 1));
		int indexCroisement2=(int)(Solver.s.nextFloat()*(nbClient - 1));
		int i1 = Math.min(indexCroisement1, indexCroisement2);
		int i2 = Math.max(indexCroisement1, indexCroisement2);
		//AG41.debugPrintln("indexCroisement1="+i1+" indexCroisement2="+i2);
		fils1.setSequence(0, p2.getOrdreAparition(this.getSequence(0,i1)));
		fils1.setSequence(i2, p2.getOrdreAparition(this.getSequence(i2)));
		
		fils2.setSequence(0, this.getOrdreAparition(p2.getSequence(0,i1)));
		fils2.setSequence(i2, this.getOrdreAparition(p2.getSequence(i2)));
		Solution[] s={fils1,fils2};
		
		if(!fils1.admissible()){
			fils1.genererCoupuresAdmissible();
		}
		if(!fils2.admissible()){
			fils2.genererCoupuresAdmissible();
		}
		return s;
	}

	public int entropieMaxAvec(Solution[]s){
		return entropieMaxAvec(s,s.length);
	}
	
	public int entropieAvec(Solution s){
		int h = 0;
		for(int i = 0;i < nbClient; i++){
			if(s.Clients[i]==this.Clients[i]){
				h++;
			}
		}
		return h;
	}
	
	public int entropieMaxAvec(Solution[]s,int nbSol){
		int hmax = 0;
		int h;
		
		for(int nums = 0;nums < nbSol; nums++){
			h = entropieAvec(s[nums]);
			if(h > hmax){
				hmax = h;
			}
		}
		
		
		return hmax;
	}
	
	public void recopieSequence (int finBoucle, int debutClient, int debutSequence, int[] seq) {
		for (int i = 0; i < finBoucle; i++) {
			this.Clients[i + debutClient] = seq[i + debutSequence];
		}
	}
	
	public Solution[] croisementLOX(Solution p2){
		//AG41.debugPrintln("LOX");
		Solution fils1 = new Solution(this);
		Solution fils2 = new Solution(p2);
		int indexCroisement1=(int)(Math.random()*(nbClient - 1));
		int indexCroisement2=(int)(Math.random()*(nbClient - 1));
		int i1 = Math.min(indexCroisement1, indexCroisement2);
		int i2 = Math.max(indexCroisement1, indexCroisement2);
		
		int[] seqInterne1 = this.getSequence(i1, i2);
		int[] seqInterne2 = p2.getSequence(i1, i2);
		int[] seqExterne1 = p2.getOrdreAparitionDesAutres(seqInterne1);
		int[] seqExterne2 = this.getOrdreAparitionDesAutres(seqInterne2);
		fils1.recopieSequence(i1, 0, 0, seqExterne1);
		fils1.recopieSequence(seqExterne1.length - i1, i2, i1, seqExterne1);
		fils2.recopieSequence(i1, 0, 0, seqExterne2);
		fils2.recopieSequence(seqExterne2.length - i1, i2, i1, seqExterne2);
		
		if(!fils1.admissible()){
			fils1.genererCoupuresAdmissible();
		}
		if(!fils2.admissible()){
			fils2.genererCoupuresAdmissible();
		}
		
		Solution[] s={fils1,fils2};
		return s;
	}
	
	public int[] getIndiceCoupure() {
		int[] indexCoupure = new int[getNombreCamion()];
		int j = 0;
		for (int i = 0; i < nbClient; i++) {
			if (Coupures[i]) {
				indexCoupure[j] = i;
				j++;
			}
		}
		return indexCoupure;
	}
	
	public int getMeilleurCamion() {
		int indexMax = 0;
		int contenuMax = 0;
		for (int i = 0; i < getContenuCamion().length; i++) {
			if (getContenuCamion()[i] > contenuMax) {
				indexMax = i;
				contenuMax = getContenuCamion()[i];
			}
		}
		return indexMax;
	}
	
	public Solution[] croisementGarderMeilleurCamion(Solution p2){
		//AG41.debugPrintln("LOX");
		Solution fils1 = new Solution(this);
		Solution fils2 = new Solution(p2);
		int[] indexCoupure = getIndiceCoupure();
		//int c1=(int)(Math.random()*(indexCoupure.length - 1));
		int c1 = getMeilleurCamion();
		c1--;
		int i1;
		int i2;
		if (c1 == -1) {
			i1 = 0;
			i2 = indexCoupure[0];
		} else {
			i1 = indexCoupure[c1];
			i2 = indexCoupure[c1 + 1];
		}
		
		int[] seqInterne1 = this.getSequence(i1, i2);
		int[] seqInterne2 = p2.getSequence(i1, i2);
		int[] seqExterne1 = p2.getOrdreAparitionDesAutres(seqInterne1);
		int[] seqExterne2 = this.getOrdreAparitionDesAutres(seqInterne2);
		fils1.recopieSequence(i1, 0, 0, seqExterne1);
		fils1.recopieSequence(seqExterne1.length - i1, i2, i1, seqExterne1);
		fils2.recopieSequence(i1, 0, 0, seqExterne2);
		fils2.recopieSequence(seqExterne2.length - i1, i2, i1, seqExterne2);
		
		if(!fils1.admissible()){
			fils1.genererCoupuresAdmissible();
		}
		if(!fils2.admissible()){
			fils2.genererCoupuresAdmissible();
		}
		
		Solution[] s={fils1,fils2};
		return s;
	}
	
	public Solution[] croisementSequenceClient(Solution p2) {
		Solution fils1 = new Solution(this);
		Solution fils2 = new Solution(p2);
		int nbIteration = 1;
		for (int i = 0; i < nbIteration; i++) {
			int indexClient1Sol2;
			int indexClient2Sol2;
			do {
				int r;
				do {
					r = (int)(Solver.s.nextFloat()*(nbClient - 1));
					//AG41.debugPrintln("client[r] : "+Clients[r]+", index : "+p2.rechercherClient(this.Clients[r]));
				}while(p2.rechercherClient(this.Clients[r])==nbClient - 1);
				indexClient1Sol2 = p2.rechercherClient(this.Clients[r]);
				indexClient2Sol2 = p2.rechercherClient(this.Clients[r+1]);
				//AG41.debugPrintln("index1 : "+indexClient1Sol2+", index2 : "+indexClient2Sol2);
			}while(!p2.permutationVerifiee(indexClient1Sol2 + 1, indexClient2Sol2));
		}
		
		
		Solution[] s={fils1,fils2};
		return s;
	}
	
	@SuppressWarnings("static-access")
	public Solution[] croisementContenuCamion(Solution p2) {
		Solution fils1 = new Solution(this);
		Solution fils2 = new Solution(p2);
		int[] contenuCamion1 = this.getContenuCamion();
		//int[] contenuCamion2 = p2.getContenuCamion();
		int demandeTemp = 0;
		
		//pour fils2
		
		//on supprime les coupure de sol2
		for (int i = 0; i < fils2.Coupures.length; i++) {
			fils2.Coupures[i] = false;
		}
		fils2.Coupures[nbClient - 1]=true;
		//indice de parcours de contenuCamion
		int j = 0;
		//boucle de calcul des contenu
		for (int i = 0; i < fils2.nbClient - 1 && j < contenuCamion1.length; i++) {
			//si la somme des contenu des client deja parcouru est inferieur 
			//au contenu du camion de sol1, on continu a ajouter
			//AG41.debugPrintln(" i : "+i+", j : "+j+", nbCamion.length"+contenuCamion1.length);
			if (demandeTemp + g.tabNoeud[fils2.Clients[i+1]].demande < contenuCamion1[j]) {
				demandeTemp += g.tabNoeud[fils2.Clients[i]].demande;
			//sinon, on ajoute une coupure, on passe au camion suivant et on remet a 0 
			} else {
				fils2.Coupures[i]= true ;
				demandeTemp = 0;
				j++;
			}
		}
		//pour fils1
		/*
		demandeTemp = 0;
		for (int i = 0; i < fils1.Coupures.length; i++) {
			fils1.Coupures[i] = false;
		}
		fils1.Coupures[nbClient - 1]=true;
		j = 0;
		for (int i = 0; i < fils1.nbClient - 1 && j < contenuCamion2.length; i++) {
			if (demandeTemp + g.tabNoeud[fils1.Clients[i+1]].demande < contenuCamion2[j]) {
				demandeTemp += g.tabNoeud[fils1.Clients[i]].demande;
			} else {
				fils1.Coupures[i]= true ;
				demandeTemp = 0;
				j++;
			}
		}*/
		
		Solution[] s={fils1,fils2};
		return s;
	}
	

	//**********************MUTATION**************************************************//
	
	/*
	 * Mutation par echange de 2 ville quelconques
	 * @return retourne vrai si la mutation est effectuer.
	 * @comment la mutation est effectuer, lorsqu'elle est possible
	 */
	public boolean mutEchange2Villes() {
		return permutationVerifiee((int)(Solver.s.nextFloat()*nbClient), (int)(Solver.s.nextFloat()*nbClient));
	}
	
	public void mutEchangerOrdreCamion () {
		//on selection un des camions
		int r = (int)(Solver.s.nextFloat()*(getContenuCamion().length - 1));
		int i = 0;
		//on cherche la borne basse
		while(r > 0) {
			//si on trouve un true on arrete
			if (Coupures[i]==true)
				r--;
			i++;
		}
		//on reaprt de i
		int j = i;
		//on cherche la deuxieme coupure ou la fin du tableu
		while (!Coupures[j]) {
			j++;
		}
		//on inverse entre i et j
		for (int k = i; k < (j + i)/2; k++) {
			permuterClient(k, j - k);
		}
	}
	
	public void mutPermuterOrdreCamion () {
		//on selection un des camions
		int r = (int)(Solver.s.nextFloat()*(getContenuCamion().length - 1));
		int i = 0;
		//on cherche la borne basse
		while(r > 0) {
			//si on trouve un true on arrete
			if (Coupures[i]==true)
				r--;
			i++;
		}
		//on reaprt de i
		int j = i;
		//on cherche la deuxieme coupure ou la fin du tableu
		while (!Coupures[j]) {
			j++;
		}
		//on inverse entre i et j
		for (int k = i; k < (j + i)/2; k++) {
			r = (int)(Solver.s.nextFloat()*(j - i))+i;
			permuterClient(k, r);
		}
	}
	
	public void mutDecalerSequence () {
		//boucle ki decale les client un cran a droite tant que la sol n'est pa admissible
		int iter = 0;
		do {
			if(iter > 10000) {
				AG41.debugPrintln("boucleinfini dans mutDEcalerSequence : "+iter);
				genererCoupuresAdmissible();
			}
			int temp = Clients[nbClient - 1];
			for(int i = nbClient - 1; i > 0; i--) {
				Clients[i] = Clients[i-1];
			}
			Clients[0] = temp;
			iter++;
		}while(!admissible());
	}
	
	public void mutOrdonnerCamion() {
		int r = (int)(Solver.s.nextFloat()*(getContenuCamion().length - 1));
		int i = 0;
		//on cherche la borne basse
		while(r > 0) {
			//si on trouve un true on arrete
			if (Coupures[i]==true)
				r--;
			i++;
		}
		//on reaprt de i
		int j = i;
		//on cherche la deuxieme coupure ou la fin du tableu
		while (!Coupures[j]) {
			j++;
		}
		//AG41.debugPrintln("i : "+i+" j : "+j);
		int[] camion = new int[j-i];
		for (int k = 0; k < camion.length; k++) {
			camion[k] = Clients[k+i];
		}
		ordonnerCamion(camion);
		for (int k = 0; k < camion.length; k++) {
			Clients[k+i] = camion[k];
		}
		
		
	}
	
	private void ordonnerCamion(int[] camion) {
		double max = 0;
		int indexMax = 0;
		int j;
		int i;
		for (i =camion.length - 1; i >= 0; i--) {
			for (j = 0 ;j < i; j++) {
				double moyenneHeure = (g.tabNoeud[camion[j]].ouverture + g.tabNoeud[camion[j]].fermeture)/2;
				if (moyenneHeure > max) {
					indexMax = j;
					max = moyenneHeure;
				}
			}
			int temp = camion[j];
			camion[i] = camion[indexMax];
			camion[indexMax] = temp;
			indexMax = 0;
			max = 0;
		}
	}
	
	public void mutClientSuivantPlusProche() {
		int r = (int)(Solver.s.nextFloat()*(nbClient - 2));
		int ClientProche = CalculerPlusProche(Clients[r]);
		int i = 0;
		while(Clients[i]!=ClientProche){i++;}
		permuterClient(r + 1, i);
	}
	
	public void mutAjouterCamion() {
		if (getNombreCamion()<25) {
			int r = (int)(Solver.s.nextFloat()*nbClient);
			while (Coupures[r]) {
				r = (int)(Solver.s.nextFloat()*nbClient);
			}
			Coupures[r]=true;
		}
	}
	
	
	//**********************METHODE d'AFFICHAGE et de SORTIE**************************//
	public void ecrireSolDansFichier(String output){
		AG41.debugPrintln("ecriture de la solution de fitness: "+this.fitness+"="+this.distance+"+2*"+this.retard);
		try{
		    PrintWriter ecrivain =  new PrintWriter(new BufferedWriter(new FileWriter(output)));
		    
		    int n = 0;
		    ecrivain.print(n+"	"+0+"	");
		    for(int c = 0;c < nbClient; c++){
		    	ecrivain.print(Clients[c]+"	");
		    	if(Coupures[c]&&c < nbClient - 1){ecrivain.println(0);n++;ecrivain.print(n+"	"+0+"	");}
		    }
		    ecrivain.println(0);
		    ecrivain.close();	
			}
			
			catch(Exception e){
				AG41.debugPrintln(e.getMessage());
			}
	}
	
	public static void printNombre(double n,int nMax){
		int nbArrondi=(int)n;
		String s="";
		while(nbArrondi < nMax && nMax > 10){
			s+=" ";
			nMax = nMax / 10;
		}
		AG41.debugPrint(s + nbArrondi);
	}
	
	public void afficher(){
		if(p1==-1){AG41.debugPrint("fils de SolutionInitiale         ");}
		else{
		AG41.debugPrint("fils de p1=");
		printNombre(p1,100);
		AG41.debugPrint(" ,p2=");
		printNombre(p2,100);
		}
		printNombre(this.fitness,100000);
		AG41.debugPrint("=");
		printNombre(this.retard,100000);
		AG41.debugPrint("*2+");
		printNombre(this.distance,100000);
		AG41.debugPrint("[");
		
		
		for(int i = 0;i < nbClient; i++){
			printNombre(Clients[i],100);
				if(Coupures[i]){
					AG41.debugPrint("|");
				}else{
					if(i!=nbClient - 1) {AG41.debugPrint(",");}//n'affiche pas la derniere virgule
				}
		}
		AG41.debugPrintln("]");
		//afficherTableauCoupure();
	}
	
	void afficherTableauCoupure(){
		for(int i = 0;i < Coupures.length; i++){
			AG41.debugPrint(Coupures[i]+",");
		}
		AG41.debugPrintNewLine();
	}
	
	void afficherTableauHeure(){
		for(int i = 0;i < h.length; i++){
			AG41.debugPrint(h[i]+",");
		}
		AG41.debugPrintNewLine();
	}
	
	void afficherTableauRetard(){
		for(int i = 0;i < r.length; i++){
			AG41.debugPrint(r[i]+",");
		}
		AG41.debugPrintNewLine();
	}
	
	//renvoi un tablo de int avec le contenu de chaque camion
	
	public int getNombreCamion(){
		int c = 0;
		for (int i =0; i < Coupures.length; i++) {
			if (Coupures[i] == true) {
				c++;
			}
		}
		return c;
	}
	
	public boolean admissible(){
		int []contenuCam = getContenuCamion();
		
		if (contenuCam.length > 25)
			return false;
		for(int i = 0;i < contenuCam.length; i++){
			if(contenuCam[i]>200){
				//AG41.debugPrintln("le camion: "+i+" est plein: "+contenuCam[i]);
				//this.ecrireSolDansFichier(AG41.projectDir + AG41.dirSeparator+"src"+AG41.dirSeparator+"solPouri.txt");
				return false;
			}
		}
		return true;
	}
	
	public int[] getContenuCamion() {
		int[] contenu = new int[getNombreCamion()]; 
		
		int contenucamion = 0; //int temporaire pour compte contenu de chaque camion
		int c =0; //indice de parcour du tablo a return
		
		for (int i = 0; i < Coupures.length; i++) { //on parcour sur la taille du tablo de coupure donc taille de sol - 1

			contenucamion += g.tabNoeud[Clients[i]].demande;
			if (Coupures[i]){ //si on a un false on ajoute a la variable temp
				contenu[c] = contenucamion;
				contenucamion = 0;
				c++;
			}
			
		}
		return contenu;
	}
	
	public int rechercherClient(int client) {
		int index = 0;
		while (this.Clients[index]!=client) {
			index++;
		}
		return index;
	}
	

	/*//garder pour l'instant
	private void evaluer2(){
		double[]r = new double[50];
		double[]H = new double[50];
		
		double h = 0;
		distance = 0;
		retard = 0;
		int j = 0;//indice coupure
		double temp=-1;
		
		for(int i = 0;i < nbClient; i++){
			if(i==0){
				temp = g.calculerDureeTrajet(0, Clients[i]);
				h = temp;
			}
			else{
				if(Coupures[j]!=i - 1){//client suivant
					temp = g.calculerDureeTrajet(Clients[i-1], Clients[i]);
					h+=temp;
					H[i]=h;
					r[i]=g.retard(Clients[i], h);
					retard+=r[i];
				}else{//rentre au depot
					j++;
					distance+=g.calculerDureeTrajet(Clients[i-1], 0);
					temp = g.calculerDureeTrajet(0, Clients[i]);
					h = temp;
				}
			}
			h = g.attente(Clients[i], h);
			distance+=temp;
		}
		distance+=g.calculerDureeTrajet(Clients[nbClient - 1],0);
		AG41.debugPrintln("************");
		double test = 0;
		AG41.debugPrintln(getStringFormat("r")+getStringFormat("h")+getStringFormat("gainH")+getStringFormat("gainD")+getStringFormat("gainRImmediat"));
		for(int i = 0;i < 50; i++){
			test+=r[i];
			AG41.debugPrint(getStringFormat((int)r[i])+getStringFormat((int)H[i])+getStringFormat((int)(H[i]-Math.min(g.calculerDureeTrajet(Clients[i], Clients[0]),g.tabNoeud[Clients[i]].ouverture))));
			//gainD
			if(i!=0){AG41.debugPrint(getStringFormat((int)(g.calculerDureeTrajet(Clients[i], Clients[i-1])-(g.calculerDureeTrajet(Clients[i], Clients[0])+g.calculerDureeTrajet(Clients[0], Clients[i-1])))));}
			else{AG41.debugPrint(getStringFormat("0"));}
			AG41.debugPrintln(getStringFormat((int)(Math.max(r[i]-(H[i]-Math.min(g.calculerDureeTrajet(Clients[i], Clients[0]),g.tabNoeud[Clients[i]].ouverture)),0))));
			
		}
		AG41.debugPrintln("}");
		AG41.debugPrintln("t="+test);
		this.fitness = retard * 2+distance;		
		this.evaluer();
		AG41.debugPrintln("r="+this.retard);
		//ecrireSolDansFichier();
	}
		public String getStringFormat(int s){
		return getStringFormat(Integer.toString(s));
	}
	public String getStringFormat(String s){
		int d = s.length()-10;
		if(d < 0){
			String ajout="|";
			for(int i = 0;i<-d; i++){
				ajout+=" ";
			}
			return ajout + s;
		}else{
			return s;
		}
		
	}

*/
}