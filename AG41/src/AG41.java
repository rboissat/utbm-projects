/* Programme principal
 * 
 */


import java.io.File;
import java.util.Date;

public class AG41 {
	 //static String dirSeparator="";
	 static String projectDir="";
	 // Si true, on doit preciser les arguments en ligne de commande
	 static final boolean argParsing = true;
	 static boolean debug = false;
	 
	 public static void debugPrint(String s) {
		 if (AG41.debug)
			 System.out.print(s);
	 }
	 
	 public static void debugPrintln(String s) {
		 if (AG41.debug)
			 System.out.println(s);
	 }
	 
	 public static void debugPrintln(int i) {
		 if (AG41.debug)
			 System.out.println(i);
	 }
	 
	 public static void debugPrintNewLine() {
		 if (AG41.debug)
			 System.out.println();
	 }
	 
	 public static String getDuree(long dateDebutResolution) { 
			long dateCourante = new Date().getTime() ;

			long ms = dateCourante - dateDebutResolution ;

			long sec  = ms / 1000 ;
			ms -= sec * 1000 ;
				
			if (sec==0) return new String (ms+"ms") ;
				
			long min =  sec / 60 ;
			sec -= min * 60 ;
			if (min==0) return new String (sec + "sec " + ms+"ms") ;
				
			long heure = min / 60 ;
			min -= heure * 60 ;
			if (heure==0) return new String (min + "min " + sec + "sec") ;

			long jour = heure / 24 ;
			heure -= jour * 24 ;
			if (jour==0) return new String (heure + "hr " + min + "min") ;
			return new String (jour + "jr " + heure + "hr") ;
		    }
	 
	 public static void listerRepertoire(File repertoire){ 

		 String [] listefichiers; 

		 int i; 
		 listefichiers = repertoire.list(); 
		 for(i = 0;i < listefichiers.length; i++){ 
			 if(listefichiers[i].endsWith(".txt")==true){ 
				 System.out.println(listefichiers[i].substring(0,listefichiers[i].length()));
			 } 
		 } 
	}
	 
	 public static void main(String[] args) throws InterruptedException {
		File inputFile = new File(projectDir);
		int penalty = 2;
		String outputFile = new String();
		int execDuration = 120000;	
		 		
		AG41.projectDir = System.getProperty("user.dir");
		//AG41.dirSeparator = (AG41.projectDir.indexOf('/') == 0) ? "/" : "\\";
		
		// Arguments
		//  challenge2009_ < n° du groupe> fichier_entree.txt penalite solution.txt temps (--debug)
		//if (argParsing == true) {
			if (args.length == 4 || args.length == 5) {
				
				inputFile = new File(args[0]);
				inputFile = inputFile.getAbsoluteFile();
				
				if (inputFile.exists() == false) {
					System.out.println("Fichier manquant ou inexistant !");
					System.out.println("Usage: java -jar challenge2009_17.jar fichier_entree.txt penalite solution.txt temps  (--debug)");
					System.exit(1);
				}
				
				
				try {
					penalty = Integer.parseInt(args[1]);
					if (penalty < 0)
						throw new IllegalArgumentException("le parametre de penalite doit etre positif");
				
				} catch (NumberFormatException e) {
					System.out.println("Erreur: la penalite saisie n'est pas un nombre");
					System.out.println("Usage: java -jar challenge2009_17.jar fichier_entree.txt penalite solution.txt temps  (--debug)");
					System.exit(1);
				}
				
				outputFile = AG41.projectDir + File.separator + args[2];
				
				try {
					execDuration = Integer.parseInt(args[3]) * 1000;
					if (execDuration < 0)
						throw new IllegalArgumentException("le parametre de temps doit etre positif");
				
				} catch(NumberFormatException e) {
					System.out.println("Erreur: le temps d'execution saisi n'est pas un nombre");
					System.out.println("Usage: java -jar challenge2009_17.jar fichier_entree.txt penalite solution.txt temps  (--debug)");;
					System.exit(1);
				}
				
				AG41.debug = (args.length == 5 && args[4].equals("--debug")) ? true : false;
				String debugStr = (args.length == 5 && args[4].equals("--debug")) ? "--debug" : "";
				
				System.out.println("\nInput args: "+inputFile.getAbsolutePath()+" "+penalty+" "+outputFile+" "+execDuration/1000+" "+debugStr+"\n");
				
			} else {
				
				System.out.println("Usage: java -jar challenge2009_17.jar fichier_entree.txt penalite solution.txt temps  (--debug)");
				System.exit(1);
			}
			
		/*} else {
			
			// Variables en dur
			inputFile = "RC106_25.txt";
			inputFile = AG41.projectDir + AG41.dirSeparator+"src"+AG41.dirSeparator + inputFile;
			penalty = 2;
			outputFile = AG41.projectDir + AG41.dirSeparator+"src"+AG41.dirSeparator+"sol.txt";
			execDuration = 120000;		
		}
		*/
		
			long dateDebutResolution= new Date().getTime();
		    
			System.out.println("Entree Algorithme");
			int nbExec                  = -1;
			int taillePop 				= 80;
			int typeSelection;
			
			Graphe g 					= new Graphe(inputFile);
			int nbClient 				= g.nbNoeud - 1;
			double tabouExecDuration;
			double geneticExecDuration;
			
			if 	(0 < nbClient && nbClient <= 25) {
				nbExec 				= 1;
				typeSelection		= 1;
				tabouExecDuration	= 0.3;
				geneticExecDuration = 0.7;
		
			} else if (25 < nbClient && nbClient <= 50) {
				nbExec 				= 1;
				typeSelection		= 0;
				tabouExecDuration	= 0.3;
				geneticExecDuration = 0.7;
			
			} else if (50 < nbClient && nbClient <= 100) {
				nbExec 				= 1;
				typeSelection		= 0;
				tabouExecDuration	= 1;
				geneticExecDuration = 0;
			
			} else {
				nbExec 				= 1;
				typeSelection		= 0;
				tabouExecDuration	= 0.3;
				geneticExecDuration = 0.7;
			}
			
			debugPrintln("tabou: "+Double.toString(tabouExecDuration));
			debugPrintln("genetic: "+Double.toString(geneticExecDuration));
			
			Solver solv = new Solver(g,taillePop,penalty,typeSelection,0);
			
			
			
			solv.genererPopDepart(taillePop, 0);

			for(int i=0; i<nbExec; i++){
				solv.algorithmeHybride(execDuration,geneticExecDuration/nbExec,tabouExecDuration/nbExec);
				solv.bestSolutionEver.afficher();
			}
			
			System.out.println("******************BILAN***************************");
			System.out.println("Solution finale est admissible : "+solv.bestSolutionEver.admissible());
			System.out.println("Duree de l'execution : "+getDuree(dateDebutResolution));
			System.out.println("Fitness Solution Finale = "+solv.getFitnessMeilleurSol(-1,120));
			solv.bestSolutionEver.ecrireSolDansFichier(outputFile);
			if (debug)
				solv.bestSolutionEver.afficher();
			else
				System.out.println("Solution Finale : "+solv.bestSolutionEver.fitness+" d="+solv.bestSolutionEver.distance+" r="+solv.bestSolutionEver.retard);
			System.out.println(inputFile+" => "+solv.bestSolutionEver.fitness+" => "+outputFile);

			System.exit(0);
	}
}
