#!/bin/bash

# On quitte les programmes si on appelle "--halt"
if [ "$1" == "--halt" ]
then
  echo "killing MTA and MDA..."
  ps -au $USER | egrep "M(D|T)A"| cut -d " " -f 0-3 | xargs kill
  exit 0
fi

# On lance en arrière-plan le MDA
if [[ -a MTA && -x MTA ]]
then
  ./MTA&
  PID_MTA=$!
  echo "MTA: succès (PID: $PID_MTA)"
else
  echo "MTA: échec"
  exit 1
fi

# On lance en arrière plan le MDA
if [[ -a MDA && -x MDA ]]
then
  ./MDA&
  PID_MDA=$!
  echo "MDA: succès (PID: $PID_MDA)"
else
  echo "MDA: échec"
  kill $PID_MTA
  exit 2
fi

# On lance le MUA en premier plan
#if [[ -a MUA && -x MUA ]]
#then
#  ./MUA
#else
#  echo "MUA: échec"
#  kill $PID_MTA
#  kill $PID_MDA
#  exit 3
#fi

# EOF
