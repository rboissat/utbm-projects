#ifndef __STRUCTURES_H
#define __STRUCTURES_H

struct s_user
{
  char *username;
  int nbMails;
  char *mails[MAX_MAIL_NUMBER];
};
typedef struct s_user user;

struct s_tree
{
  int nbUsers;
  user *users[MAX_USER_NUMBER];
};
typedef struct s_tree tree;

/* Verifie si un user est deja dans l'arbre
   Prend en argument le nom de l'user, renvoie un pointeur vers
   l'user s'il existe, null sinon */
user* hasUser(tree*,char*);

/* Ajoute un mail a un User */
int addMail(user*,char*);

/* Ajoute un nouvel utilisateur dans notre arbre,
   et renvoie un pointeur vers celui ci */
user* newUser(tree*,char*);

/* Supprime un arbre */
void freeTree(tree*);

/* Renvoie le mail le plus recent et le supprime de la pile de reception */
char* popMail(user*);


#endif
