#ifndef __TOOLS_H
#define __TOOLS_H

#include "common.h"

/* On donne le hostname, le port, et le type de socket que l'on
   veut etablir : 0 => serveur, 1 => client */
SOCKET openSocket(const char*,int,int,int);

/* Fonction de saisie, prend en parametre un entier etant la taille
   du buffer, retourne un pointeur vers une chaine */
char* getInput(int);

/* Recupere le destinataire d'un message */
char* getRecipient(char*);

/* Recupere la requete d'un message */
char* getRequest(char*);

#endif
