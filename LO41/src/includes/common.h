#ifndef __COMMON_H
#define __COMMON_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */
#include <pthread.h>
#include <errno.h>

typedef int SOCKET;

#define TRUE             1
#define FALSE            0
#define INVALID_SOCKET   -1
#define SOCKET_ERROR     -1
/* Port d'ecoute du MTA */
#define MTA_PORT         8888
/* Port d'écoute du MDA */
#define MDA_PORT         9999
/* Longueur en caractere des choix pour l'interface */
#define CHOICE_LENGTH    1
/* Longueur en caractere du destinataire du mail */
#define RECIPIENT_SIZE   32
/* Nombre de connexions simultanees du MTA */
#define MTA_USER_LIMIT   25
/* Nombre de connexions simultanees du MTA */
#define MDA_USER_LIMIT   25
/* Longueur d'un mail */
#define MSG_SIZE         1024
/* Longueur de l'objet d'un mail */
#define OBJECT_SIZE      32
/* Longueur Max d'une requete */
#define RQ_SIZE          1024
/* Nombre max de mail par utilisateur */
#define MAX_MAIL_NUMBER  20
/* Nombre max d'utilisateurs */
#define MAX_USER_NUMBER  20
/* Alias de close(s) */
#define closesocket(s) close(s)


/* Definitions des erreurs
 Le mail est mal formatté */
#define WRONG_MAIL_FORMATTING 2

#endif
