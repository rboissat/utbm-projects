#include "common.h"
#include "structures.h"
#include "tools.h"

#define NO_MAIL_MSG "============No mails==========\n"
#define END_OF_MAIL "=========No more Mails========\n"
#define NEW_MAIL    "============New Mail==========\n"

tree *storage;
pthread_mutex_t mutex;

void *handleClient(void *arg)
{
  /* socket client de communication avec le MDA */
  SOCKET socks;

  user *userToHandle;
  
  int n;
  char buffer[MSG_SIZE+1];
  char *tmp;
  char *mail;
  char *request;
  char *recipient;

  socks = *(int*)arg;
  mail = (char *)malloc(sizeof(buffer));

  fprintf(stdout,"Thread : Lancement du thread avec le socket : %d\n",socks);

  if((n = recv(socks, buffer, sizeof(buffer) - 1, 0)) < 0) 
  {
    perror("Thread : Recv() failed"); 
    exit(errno); 
  }

  buffer[n] = '\0';

  tmp = &buffer[0];
  memcpy(mail, &buffer, sizeof(buffer));

  fprintf(stdout,"Thread : Donnees recues : %s\n",buffer);

  request = getRequest(tmp);

  fprintf(stdout, "Thread : Requete : %s\n",request);

  tmp = tmp + strlen(request);

  if(!strcmp(request, "To:\0"))
  {
    recipient = getRecipient(tmp);

    fprintf(stdout, "Thread : Destinataire : %s\n",recipient);

    pthread_mutex_lock(&mutex);
    userToHandle = hasUser(storage,recipient);
    pthread_mutex_unlock(&mutex);
    
    if (!userToHandle)
    {
      pthread_mutex_lock(&mutex);
      userToHandle = newUser(storage,recipient);
      pthread_mutex_unlock(&mutex);
    }

    pthread_mutex_lock(&mutex);
    addMail(userToHandle,mail);
    pthread_mutex_unlock(&mutex);

  }
  else if(!strcmp(request,"Fetch:\0"))
  {
    char *mailToSend;
    recipient = getRecipient(tmp);
    fprintf(stdout, "Thread : Destinataire : %s\n",recipient);

    pthread_mutex_lock(&mutex);
    userToHandle = hasUser(storage,recipient);
    pthread_mutex_unlock(&mutex);

    pthread_mutex_lock(&mutex);
    if(userToHandle)
    {
      while ((mailToSend = popMail(userToHandle))!=NULL)
      {
        if(send(socks,NEW_MAIL,strlen(NEW_MAIL),0) < 0)
          exit(EXIT_FAILURE);
        if(send(socks,mailToSend,strlen(mailToSend),0) < 0)
          exit(EXIT_FAILURE);
        fprintf(stdout,"Thread : Mail pop : %s\n",mailToSend);
      }
      if(send(socks,END_OF_MAIL,strlen(END_OF_MAIL),0) < 0)
        exit(EXIT_FAILURE);
    }
    else
    {
      if(send(socks,NO_MAIL_MSG,strlen(NO_MAIL_MSG),0) < 0)
        exit(EXIT_FAILURE);
    }
    pthread_mutex_unlock(&mutex);
  }
  else
  {
    perror("Thread : Format de protocole non reconnu");
    return ((void*)0);
  }

  fprintf(stdout,"Fin du thread\n");

  closesocket(socks);
  free(request);

  return ((void*)0);
}

int main(void)
{
  /* SOCKET d'ecoute du MDA */
  SOCKET socks;
  /* socket de communication avec le client */
  SOCKET csocks;
  struct sockaddr_in csin = {0};
  socklen_t csin_size = sizeof(csin);
  pthread_t thread;
  
  pthread_mutex_init(&mutex, NULL);
  
  storage = (tree *)malloc(sizeof(tree));
  
  socks = openSocket(NULL,MDA_PORT,0,MDA_USER_LIMIT);
  
  while(1)
      {
        fprintf(stdout,"MDA : Entree dans la boucle, attente d'un client\n");
        
        csocks = accept(socks,(struct sockaddr *)&csin,&csin_size);
        
        fprintf(stdout,"MDA : socket %d ouvert avec le client\n",(int)csocks);
        
        if(csocks == INVALID_SOCKET)
            {
              perror("MDA : accept() failed\n");
              exit(errno);
            }
        
        fprintf(stdout, "MDA : Creation du thread\n");
        pthread_create(&thread, NULL,handleClient,(void*)&csocks);
      }
  closesocket(socks);
  freeTree(storage);
  return 0;
}
