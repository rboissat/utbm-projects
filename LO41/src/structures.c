#include "common.h"
#include "structures.h"

user* hasUser(tree* storage,char* toSearch)
{
  int i;
  for(i=0;i<storage->nbUsers;i++)
  {
    if(!strcmp(storage->users[i]->username,toSearch))
      return storage->users[i];
  }
  return NULL;
}

int addMail(user* toStore,char* mail)
{

  if(toStore->nbMails > MAX_MAIL_NUMBER)
  {
    fprintf(stdout,"Structure: MAX_MAIL_NUMBER reached");
    return FALSE;
  }
  
  toStore->mails[toStore->nbMails] = (void *)mail;
  
  toStore->nbMails++;
  
  return TRUE;
}

user* newUser(tree* storage,char* name)
{
  user *toAdd;
  
  if(storage->nbUsers >= MAX_USER_NUMBER)
  {
    fprintf(stdout,"Structure: MAX_USER_NUMBER reached");
    return NULL;
  }
  
  toAdd = (user *)malloc(sizeof(user));
  toAdd->username=name;
  
  storage->users[storage->nbUsers] = toAdd;
  
  storage->nbUsers++;

  return toAdd;
}

void freeTree(tree* storage)
{
  int i,j;
  
  for(i=0;i<storage->nbUsers;i++)
  {
    user *tmp;
    tmp = storage->users[i];
    free(tmp->username);
    for(j=0;j<tmp->nbMails;j++)
      free(tmp->mails[j]);
    free(tmp);
  }
  free(storage);
}

char* popMail(user *toPop)
{
  char *tmp;
  if (toPop->nbMails > 0)
  {
    toPop->nbMails--;
    tmp = toPop->mails[toPop->nbMails];
    toPop->mails[toPop->nbMails] = NULL;
  }
  else
  {
    return NULL;
  }
  
  return tmp;
}
