#include "common.h"
#include "tools.h"

int fetchMail(const char* hostname)
{
  SOCKET sock;
  char *toSend;
  char *user;
  char buffer[MSG_SIZE+1];
  int n;
  
  toSend = (char *) malloc((MSG_SIZE+1)*sizeof(char));

  user = (char *)malloc(RECIPIENT_SIZE*sizeof(char));
  
  printf("Please enter your account name : ");
  scanf("%s",user);
  
  toSend = strcat(toSend,"Fetch: ");
  toSend = strcat(toSend,user);
  
  sock = openSocket(hostname,MDA_PORT,1,0);
  
  if(send(sock,toSend,strlen(toSend),0) < 0)
  {
    perror("MUA : Send() error");
    exit(errno);
  }
  
  while(1)
  {
    n = recv(sock, buffer, sizeof(buffer) - 1, 0);
    
    if(n < 0) 
    {
      perror("Thread : Recv() failed"); 
      exit(errno); 
    }
    else if(n==0)
    {
      break;
    }
    fprintf(stdout, "%s",buffer);
  }
  /* Nettoyage */
  closesocket(sock);
  free(toSend);
  
  return TRUE;
}


/* Methode d'envoi de Mail vers un MTA specifie */
int sendMail(const char* hostname)
{
  SOCKET sock;
  char *recipient;
  char *object;
  char *message;
  char *toSend;

  toSend = (char *) malloc((MSG_SIZE+1)*sizeof(char));

  sock = openSocket(hostname,MTA_PORT,1,0);

  printf("Please enter the recipient : ");
/*  recipient = getInput(RECIPIENT_SIZE); */
  recipient = (char *)malloc(RECIPIENT_SIZE*sizeof(char));
  scanf("%s",recipient);

  toSend = strcat(toSend,"To: ");
  toSend = strcat(toSend,recipient);

  printf("Please enter the object of the mail : ");
/*  object = getInput(OBJECT_SIZE); */
  object = (char *)malloc(OBJECT_SIZE*sizeof(char));
  scanf("%s",object);

  toSend = strcat(toSend,"\nSubject: ");
  toSend = strcat(toSend,object);

  printf("Please enter your message (End the Input by Ctrl-D) :\n");

  message = getInput(MSG_SIZE-RECIPIENT_SIZE-OBJECT_SIZE);
  toSend = strcat(toSend, "\n");
  toSend = strcat(toSend, message);

  if(send(sock,toSend,strlen(toSend),0) < 0)
    exit(EXIT_FAILURE);

  /* Nettoyage */
  closesocket(sock);
  free(recipient);
  free(object);
  free(message);
  free(toSend);

  return TRUE;
}

int main (void) {
  char *usr_choice;
  usr_choice = (char *) malloc((CHOICE_LENGTH+1)*sizeof(char));

  printf("Hello, What do you want to do ?\n");
  printf("-->(S)end a Mail, (F)etch your mails, (Q)uit : ");
  scanf("%s",usr_choice);

  if (!(strcmp(usr_choice,"S\0") && strcmp(usr_choice,"s\0")))
  {
    printf("We're sending a mail\n");
    sendMail("localhost");
  }

  if (!(strcmp(usr_choice,"F\0") && strcmp(usr_choice,"f\0")))
  {
    printf("We're getting your mails\n");
    fetchMail("localhost");
  }

  if (!(strcmp(usr_choice,"Q\0") && strcmp(usr_choice,"q\0")))
  {
    printf("Thanks for using our MUA\n");
  }

  free(usr_choice);
  return 0;
}
