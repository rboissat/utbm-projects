#include "common.h"
#include "tools.h"

struct thread_data{
  int  thread_id;
  SOCKET toHandle;
};

void *handleClient(void *arg)
{
  /* socket client de communication avec le MDA */
  SOCKET sockc;
  SOCKET *socks;

  int n;
  char buffer[MSG_SIZE+1];

  socks = (int*)arg;

  fprintf(stdout,"Thread : Lancement du thread avec le socket : %d\n",*socks);

  if((n = recv(*socks, buffer, sizeof(buffer) - 1, 0)) < 0) 
  {
    perror("Thread : Recv() failed"); 
    exit(errno); 
  }

  buffer[n] = '\0'; 
  
  fprintf(stdout,"Thread : Donnees recues : %s\n",buffer);

  sockc = openSocket("localhost", MDA_PORT, 1,0);

  if (send(sockc,buffer,strlen(buffer),0) < 0)
  {
    perror("Threads : Send() Error");
    exit(errno);
  }

  fprintf(stdout,"Fin du thread\n");

  closesocket(sockc);
  closesocket(*socks);

  return ((void*)0);
}

int main(void)
{
  /* SOCKET d'ecoute du MTA */
  SOCKET socks;
  /* socket de communication avec le client */
  SOCKET csocks;
  struct sockaddr_in csin = {0};
  socklen_t csin_size = sizeof(csin);
  pthread_t thread;

  socks = openSocket(NULL,MTA_PORT,0,MTA_USER_LIMIT);

  while(1)
  {
    fprintf(stdout,"MTA : Entree dans la boucle, attente d'un client\n");

    csocks = accept(socks,(struct sockaddr *)&csin,&csin_size);

    fprintf(stdout,"MTA : socket %d ouvert avec le client\n",(int)csocks);

    if(csocks == INVALID_SOCKET)
    {
      perror("MTA : accept() failed\n");
      exit(errno);
    }

    fprintf(stdout, "MTA : Creation du thread\n");
    pthread_create(&thread, NULL,handleClient,(void*)&csocks);
  }
  closesocket(socks);
  return 0;
}
