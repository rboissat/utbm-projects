#include "tools.h"


SOCKET openSocket(const char* hostname,int port,int socket_type,int user_limit)
{
  /* initialise la structure avec des 0 */
  struct sockaddr_in sin = { 0 };

  /* on cree un nouveau socket
  AF_INET => IPv4, SOCK_STREAM => TCP */
  SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);

  if (sock==INVALID_SOCKET)
  {
    perror("openSocket : Socket() failed");
    exit(errno);
  }

  if (socket_type == 1) /* Oooooh c'est un client, il est si mignon */
  {
    /* On cree la structure de connexion */
    struct hostent *hostinfo = NULL;

    if (!hostname)
    {
      perror("openSocket : Client -> Null hostname");
      exit(errno);
    }

    /* on récupère les informations de l'hôte auquel on veut se connecter */
    hostinfo = gethostbyname(hostname);

    if (!hostinfo)
    {
      perror("openSocket : Client -> Null hostinfo");
      exit(errno);
    }
    
    /* l'adresse se trouve dans le champ h_addr de la structure hostinfo */
    memcpy(hostinfo->h_addr, &sin.sin_addr, hostinfo->h_length);
    /* on utilise htons pour le port */
    sin.sin_port = htons(port);
    sin.sin_family = AF_INET;

    if(connect(sock,(struct sockaddr *) &sin, sizeof(struct sockaddr_in)) == SOCKET_ERROR)
    {
      perror("openSocket : Client -> Connect() failed");
      exit(errno);
    }
  }
  else if (socket_type == 0)
  {
    /* On est le serveur, on accepte toutes les IP */
    sin.sin_addr.s_addr = htonl(INADDR_ANY);

    sin.sin_family = AF_INET;

    sin.sin_port = htons(port);

    if(bind (sock, (struct sockaddr *) &sin, sizeof(sin)) == SOCKET_ERROR)
    {
      perror("openSocket : Serveur -> Bind() failed");
      exit(errno);
    }
    
    if(listen(sock,user_limit) == SOCKET_ERROR)
    {
      perror("openSocket : Serveur -> Listen() failed");
      exit(errno);
    }
  }
  else
  {
    perror("OpenSocket : Argument inconnu");
    exit(EXIT_FAILURE);
  }

  return sock;
}

char* getInput(int bufsize)
{
  char *buffer;
  int tmp;
  char ctmp;

  buffer = (char *)malloc(bufsize*sizeof(char));
  do
  {
    fflush(stdin);
    setbuf(stdin,NULL);
    rewind(stdin);
    tmp = getchar();
    ctmp = tmp;
    if(tmp!=EOF)
      strcat(buffer,&ctmp);
  } while(tmp!=EOF);

  return buffer;
}

char* getRequest(char* string)
{
  char *request;

  request = (char *)malloc((RQ_SIZE+1)*sizeof(char));

  sscanf(string, "%s",request);

  return request;
}


char* getRecipient(char* string)
{
  char *recipient;

  recipient = (char *)malloc((RECIPIENT_SIZE+1)*sizeof(char));

  sscanf(string, "%s",recipient);

  return recipient;
}
