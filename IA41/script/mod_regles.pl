%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          ______                                             %
%                         /___ ___  __  __   __   ___                         %
%                        /    /  / /_/ /    /_    ___\                        %
%                       /    /__/ / \  \__ /__    ___/                        %
%                                                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Projet de force3 pour l'UV IA41
% Implantation de l'algorithme MinMax
%
% Copyright (c) 2008  Romain BOISSAT, Frederic RECHTENSTEIN, Maxime RIPARD
%
% This program is free software ; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation ; either version 2
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY ; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program ; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:- module(mod_regles, [gagne/2, taquin_pour/2, taquin2_pour/3, voisin_de/2,
                      oppose_de/2, row/3, column/3, diagonal/3, nbp/3,
                      coup/3]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         Base de connaissance et Règles                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Vocabulaire:
%  JR : Jeu Joueur -> entier (1 ou 2)
%  PL : Représentation du plateau
%  CA : Case actuelle
%  CS : Case suivante
%  C  : Case arbitraire

%%%% Conditions de victoire pour le joueur :
% gagne/2 (+JR, +PL)

% Il place ses 3 pions en ligne
gagne(JR, [JR,JR,JR,_,_,_,_,_,_]) :- JR \= 0.
gagne(JR, [_,_,_,JR,JR,JR,_,_,_]) :- JR \= 0.
gagne(JR, [_,_,_,_,_,_,JR,JR,JR]) :- JR \= 0.

% Il place ses 3 pions en colonnes
gagne(JR, [JR,_,_,JR,_,_,JR,_,_]) :- JR \= 0.
gagne(JR, [_,JR,_,_,JR,_,_,JR,_]) :- JR \= 0.
gagne(JR, [_,_,JR,_,_,JR,_,_,JR]) :- JR \= 0.

% Il place ses trois pions en diagonale
gagne(JR, [JR,_,_,_,JR,_,_,_,JR]) :- JR \= 0.
gagne(JR, [_,_,JR,_,JR,_,JR,_,_]) :- JR \= 0.

%% taquin_pour/2 (?Taquin, ?Case)
% | 0 | 1 | 2 |
% | 3 | 4 | 5 |
% | 6 | 7 | 8 |
% si le taquin est en 0, 1 et 3 sont déplaçables
% Ex: ?- taquin_pour(0,1).
%       Yes.
taquin_pour(0,C):- C = 1 ; C = 3.
taquin_pour(1,C):- C = 0 ; C = 2 ; C = 4.
taquin_pour(2,C):- C = 1 ; C = 5.
taquin_pour(3,C):- C = 0 ; C = 4 ; C = 6.
taquin_pour(4,C):- C = 1 ; C = 3 ; C = 5 ; C = 7.
taquin_pour(5,C):- C = 2 ; C = 4 ; C = 8.
taquin_pour(6,C):- C = 3 ; C = 7.
taquin_pour(7,C):- C = 4 ; C = 6 ; C = 8.
taquin_pour(8,C):- C = 5 ; C = 7.

%% taquin2_pour/2 (?Taquin, ?Case1, ?Case2)
% | 0 | 1 | 2 |
% | 3 | 4 | 5 |
% | 6 | 7 | 8 |
% si le taquin est en 0, alors (1,2) et (3,6) sont déplaçables 2 par 2
taquin2_pour(0,C1,C2):- C1 = 1, C2 = 2 ; C1 = 3, C2 = 6.
taquin2_pour(1,4,7).
taquin2_pour(2,C1,C2):- C1 = 1, C2 = 0 ; C1 = 5, C2 = 8.
taquin2_pour(3,4,5).
taquin2_pour(5,4,3).
taquin2_pour(6,C1,C2):- C1 = 7, C2 = 8 ; C1 = 3, C2 = 0.
taquin2_pour(7,4,1).
taquin2_pour(8,C1,C2):- C1 = 7, C2 = 6 ; C1 = 5, C2 = 2.

%% voisin_de/2 (?Case1, ?Case2)
% Case1 est voisine (adjacente) de Case2
% Tous les sens sont corrects
% ex: voisin_de(0,1).
voisin_de(0,C):- C = 1 ; C = 4 ; C = 3.
voisin_de(1,C):- C = 2 ; C = 5 ; C = 4 ; C = 3 ; C = 0.
voisin_de(2,C):- C = 5 ; C = 4 ; C = 1.
voisin_de(3,C):- C = 0 ; C = 1 ; C = 4 ; C = 7 ; C = 6.
voisin_de(4,C):- C = 0 ; C = 1 ; C = 2 ; C = 3 ; C = 5 ; C = 6 ; C = 7 ; C = 8.
voisin_de(5,C):- C = 2 ; C = 8 ; C = 7 ; C = 4 ; C = 1.
voisin_de(6,C):- C = 3 ; C = 4 ; C = 7.
voisin_de(7,C):- C = 4 ; C = 5 ; C = 8 ; C = 6 ; C = 3.
voisin_de(8,C):- C = 5 ; C = 7 ; C = 4.

%% oppose_de/2 (?Case1,?Case2)
oppose_de(C1,C2) :- C1 = 0 , C2 = 6 ;
                    C1 = 1 , C2 = 7 ;
                    C1 = 2 , C2 = 8 ;
                    C1 = 3 , C2 = 5 ;
                    C1 = 5 , C2 = 3 ;
                    C1 = 6 , C2 = 0 ;
                    C1 = 7 , C2 = 1 ;
                    C1 = 8 , C2 = 2 .


%% row/3 (?PL, ?I, ?Seq)
% Unifie Seq avec la Ieme ligne du plateau PL
row(PL, I, [E1, E2, E3]) :-
    I1 is (I-1)*3, nth0(I1, PL, E1),
    I2 is 3*I-2, nth0(I2, PL, E2),
    I3 is 3*I-1, nth0(I3, PL, E3).

%% colums/3 (?PL, ?IJ, ?Seq)
% Unifie Seq avec la Jeme colone du plateau PL
column(PL, J, [E1, E2, E3]) :-
    nth1(J, PL, E1),
    I2 is J+3, nth1(I2, PL, E2),
    I3 is J+6, nth1(I3, PL, E3).

%% diagonal/3 (?PL, ?N, ?Seq)
% Unifie Seq avec la Neme colone du plateau PL
diagonal(PL, 1, [E1,E2,E3]) :-
    nth1(1, PL, E1),
    nth1(5, PL, E2),
    nth1(9, PL, E3).

diagonal(PL, 2, [E1,E2,E3]) :-
    nth1(3, PL, E1),
    nth1(5, PL, E2),
    nth1(7, PL, E3).


%% nbp/3 (+JR, +PL, ?NB)
% Calcul le nombre de pions du joueur JR placés sur le plateau
% sublist/3(P1,L1,L2). : unifie L2 avec tous les éléments de L1 auxquels
% s'applique P1.
% NB : entier, nombre de pions
% =(JR) : unification avec le n° de joueur.
nbp(JR,PL,NB):- sublist(=(JR), PL, L), length(L,NB).

%% les coups
% Les différents coups sont codés par un ID unique:
% - pose d'un pion            : 0
% - déplacement d'un pion:    : 1
% - déplacement d'une case:   : 2
% - déplacement de deux cases : 3
%
% coup/3 (+JR, +PL, -[CA,CS,id_mouvement]).

% Pose d'un pion, limité à N = 3 coups
% CA = -1 => Pion hors plateau
coup(JR, [0|R], [-1,0,0])                         :-
  nbp(JR,[0|R],N), N < 3.

coup(JR, [C0,0|R], [-1,1,0])                      :-
  nbp(JR,[C0,0|R],N), N < 3.

coup(JR, [C0,C1,0|R], [-1,2,0])                   :-
  nbp(JR,[C0,C1,0|R],N), N < 3.

coup(JR, [C0,C1,C2,0|R], [-1,3,0])                :-
  nbp(JR,[C0,C1,C2,0|R],N), N < 3.

coup(JR, [C0,C1,C2,C3,0|R], [-1,4,0])             :-
  nbp(JR,[C0,C1,C2,C3,0|R],N), N < 3.

coup(JR, [C0,C1,C2,C3,C4,0|R], [-1,5,0])          :-
  nbp(JR,[C0,C1,C2,C3,C4,0|R],N), N < 3.

coup(JR, [C0,C1,C2,C3,C4,C5,0|R], [-1,6,0])       :-
  nbp(JR,[C0,C1,C2,C3,C4,C5,0|R],N), N < 3.

coup(JR, [C0,C1,C2,C3,C4,C5,C6,0|R], [-1,7,0])    :-
  nbp(JR,[C0,C1,C2,C3,C4,C5,C6,0|R],N), N < 3.

coup(JR, [C0,C1,C2,C3,C4,C5,C6,C7,0|R], [-1,8,0]) :-
  nbp(JR,[C0,C1,C2,C3,C4,C5,C6,C7,0|R],N), N < 3.

% Déplacement d'un pion déjà posé sur le plateau sur l'une des 9 cases.
% Pour cela, il faut :
%   * que la case CA et CS soient voisines
%   * que sur la case CA il y ait un pion du Joueur JR => nth0(CA,P,JR)
%   * que sur la case CS il n'y ait pas de pion, qqst le JR => nth0(CS,P,0)
coup(JR, PL, [CA,CS,1]) :- voisin_de(CA,CS), nth0(CA,PL,JR), nth0(CS,PL,0).

% Déplacement d'une case CA vers CS
% Pour cela, il faut :
%   * que CA soit le taquin pour CS
coup(_, PL, [CA,CS,2]) :- taquin_pour(CA,CS), nth0(CA,PL,-1).

% Déplacement de deux cases
% Pour cela il faut :
%   * que C1 soit le taquin et C2 soit alignée avec le taquin
coup(_, PL, [C1,CI,C2,3]) :- taquin2_pour(C1,CI,C2), nth0(C1,PL,-1).

% EOF
