/** Inteface utilisateur (UI)

Ce module fournit une interface utilisateur en mode texte interactif.
Le jeu se déroule suivant l'apel des deux prédicats de base : jouer/0 et jouer_ia/0.

@author Romain Boissat, Frédéric Rechtenstein, Maxime Ripard
@license GPL
*/

:- module(mod_ui, [init_ui/0, partie_ia/0, jouer_ia/0, jouer/0, welcome/0]).
:- use_module('mod_jeu.pl').
:- use_module('mod_regles.pl').
:- use_module('mod_eval.pl').

:- dynamic plateau/1.       % Le prédicat dynamique plateau est utilisé pour
                            % stocker l'état du plateau de jeu.
/**
*   init_ui is det
*
*   Initialise le module
*/
init_ui:-
    retractall(plateau(_)), % suppression des prédicats plateau/1 de la base
    plateau_vide(P),
    assert(plateau(P)).     % et on en redéfini un avec une plateau vide

/**
*   demande_coup(+PL, ?Coup) is nondet
*
*   Demande un coup à jouer au joueur.
*/
demander_coup(PL, [C1,C2,ID]) :-
  afficher_plateau_coords(PL),
  writeln('Mouvement ?  0: Pose Pion            '),
  writeln('             1: Deplacement Pion     '),
  writeln('             2: Deplacement Taquin   '),
  ask_id(ID),
  ID == 0 -> ID is 0, C1 is -1, ask_dest(C2);
  ID == 2 -> ask_orig(C1), ask_dest(C2), id_move(C2,C1,ID);
  ask_orig(C1), ask_dest(C2).

ask_id(ID) :-
  read(ID), integer(ID), between(0, 3, ID),!;
  writeln('Erreur, veuillez Saisir un mouvement valide, avec . a la fin'),
  ask_id(ID).

ask_orig(C) :-
  write('Coordonnee Origine : '), read(C), integer(C), between(0, 8, C),!;
  writeln('Erreur, veuillez Saisir une coordonnee valide, avec . a la fin'),
  ask_orig(C).

ask_dest(C) :-
  write('Coordonnee Destination : '), read(C), integer(C), between(0, 8, C), !;
  writeln('Erreur, veuillez Saisir une coordonnee valide, avec . a la fin'),
  ask_dest(C).

% id_move/3 (+Case1, +Case2, ?ID)
% Détermine l'Id de mouvement à renvoyer au prédicat de mouvement
id_move(C1,C2, 2) :-
  C1 = 0, member(C2, [1,3]);
  C1 = 1, member(C2, [0,2,4]);
  C1 = 2, member(C2, [1,5]);
  C1 = 3, member(C2, [0,4,6]);
  C1 = 4, member(C2, [1,5,7,3]);
  C1 = 5, member(C2, [2,8,4]);
  C1 = 6, member(C2, [3,7]);
  C1 = 7, member(C2, [4,8,6]);
  C1 = 8, member(C2, [5,7]).

id_move(C1,C2, 3) :-
  C1 = 0, member(C2, [6,2]);
  C1 = 1, C2 = 7;
  C1 = 2, member(C2, [0,8]);
  C1 = 3, C2 = 5;
  C1 = 5, C2 = 3;
  C1 = 6, member(C2, [0,8]);
  C1 = 7, C2 = 1;
  C1 = 8, member(C2, [2,6]).


/**
*   enregistre_coup(+Joueur, +Coup) is det
*
*   Enregistre le coup Coup joué par le joueur Joueur sur le plateau de jeu.
*
*   @see plateau/1.
*/
enregistre_coup(Joueur,Coup) :-
    retract(plateau(B)),
    mouv(Joueur,B,Coup,B1),
    assert(plateau(B1)).

/**
*   partie_ia is det
*
*   Fait jouer l'ordinateur contre lui-même.
*/
partie_ia :-
    plateau(PL),
    alpha_beta(1,6,PL,-200,200,Coup,_Valeur),!,
    enregistre_coup(1,Coup),
    plateau(NPL),
    afficher_plateau(NPL),
    not(agagne),
    alpha_beta(2,5,NPL,-200,200,Coup2,_Valeur2),!,
    enregistre_coup(2,Coup2),
    plateau(NPL2),
    afficher_plateau(NPL2),
    not(agagne),
    partie_ia.

/**
*   jouer_ia is det
*
*   Fait jouer l'ordinateur.
*/
jouer_ia :-
    plateau(PL),
    alpha_beta(1,4,PL,-200,200,Coup,_Valeur),!,
    enregistre_coup(1,Coup),
    plateau(NPL),
    afficher_plateau(NPL),
    agagne.

/**
*   jouer is det
*
*   Demander au joueur humain de jouer un coup et enregistre son coup.
*
*   @see jouer_ia/0
*/
jouer :-
    plateau(PL),
    demander_coup(PL,Coup),
    enregistre_coup(2,Coup),
    plateau(NPL),
    afficher_plateau(NPL),
    agagne.

/**
*   agagne is det
*
*    Verifit si un joueur Joueur a gagné la partie.
*    Si c'est le cas, stope le jeux et en commence un nouveau.
*/

agagne :-
    plateau(PL),
    gagne(JR, PL),
    writef("Le joueur %w gagne !", [JR]),
    init_ui,!.

/**
*   afficher_plateau(+PL) is det
*
*   Affiche le plateau spécifié. Le symbole de chaque case est donné par afc/1.
*
*   @see afc/1
*/
afficher_plateau([C1,C2,C3,C4,C5,C6,C7,C8,C9]):-
    write('    |'),afc(C1),write(' '),afc(C2),write(' '),afc(C3),write('|'),nl,
    write('    |'),afc(C4),write(' '),afc(C5),write(' '),afc(C6),write('|'),nl,
    write('    |'),afc(C7),write(' '),afc(C8),write(' '),afc(C9),write('|'),nl.

/**
*   afficher_plateau_coords(+PL) is det
*
*   De meme que afficher_plateau, mais affiche les coordonées des cases.
*
*   @see afficher_plateau/1
*/
afficher_plateau_coords([C1,C2,C3,C4,C5,C6,C7,C8,C9]):-
    write('    |'),afc(C1),write(' '),afc(C2),write(' '),afc(C3),write('|'),
    write('                         |0 1 2|'),nl,
    write('    |'),afc(C4),write(' '),afc(C5),write(' '),afc(C6),write('|'),
    write('   Coordonnees Cases --> |3 4 5|'),nl,
    write('    |'),afc(C7),write(' '),afc(C8),write(' '),afc(C9),write('|'),
    write('                         |6 7 8|'),nl,
    !.

/**
*   afc(+Case) is det
*
*   Affiche une case suivant son contenu :
*   * espace    si la case est vide
*   * #         pour le taquin
*   * o         pour un pion du joueur 1
*   * x         pour un pion du joueur 2
*
*   @see afficher_plateau/1
*/
afc(X)   :- var(X), write('-').
afc(0)   :- write(' ').
afc(-1)  :- write('#').
afc(1)   :- write('o').
afc(2)   :- write('x').


/**
*     welcome is det
*
*     Affiche l'ecran d'accueil.
*/
welcome :-
  nl,
  writeln('     ______                       '),
  writeln('   /___ ___  __  __   __   ___    '),
  writeln('  /    /  / /_/ /    /_    ___\\  '),
  writeln(' /    /__/ / \\  \\__ /__    ___/ '),
  writeln('----------------------------------'),
  writeln('(c) 2008 Romain BOISSAT, Frederic RECHTENSTEIN, Maxime RIPARD'),
  writeln('jouer. pour jouer un coup, jouer_ia. pour un coup de l\'IA.'),
  writeln('partie_ia. pour voir une partie CPU vs CPU'),
  nl,!.

% EOF
