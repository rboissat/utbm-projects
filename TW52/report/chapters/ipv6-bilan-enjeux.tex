Ce chapitre expose les résultats et les progrès obtenus tout au long du semestre
et tout particulièrement à partir de mi-décembre. En toute honnêteté, les
progrès réalisés sont surtout d'ordre organisationnel et théorique : les progrès
techniques concrets sont surtout attendus pour la fin du premier trimestre 2012.

Dans un second temps, j'exposerai quelques détails concernant la suite du projet
de déploiement complet d'IPv6 de la bordure de l'opérateur jusqu'au client, sous
la forme d'étapes mais dont le calendrier précis reste encore à définir. Enfin,
quelques enjeux spécifiques à Trinaps seront présentés dans une dernière
section.

\section{Bilan}

Finalement, les progrès techniques réalisés pendant ce semestre sont
malheureusement limités, reposant essentiellement sur des schémas de cas clients
et un plan d'adressage plus détaillé. Néanmoins, ces schémas ont une importance
capitale concernant la suite du déploiement :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item Ils permettent de qualifier ou non les équipements existants.

  \item Ils permettent de valider ou de repenser la topologie niveau physique ou
  niveau lien (L1/L2 du modèle OSI) qui lie les équipements entre eux.

  \item Ils offrent une représentation très concrète du plan d'adressage IPv6 et
  participent à son amélioration continue.
\end{itemize}

Sur le plan technique, nous disposons donc d'un plan d'adressage plus précis et
certainement dans la forme qui sera concrètement déployée. Des schémas
d'infrastructure et d'autres détaillant les différents cas clients viennent
compléter la documentation IPv6 interne, qui sera enrichie dans les mois à venir
de procédures visant à faciliter voire quasiment industrialiser l'activation
IPv6 jusqu'à un client donné.

Il reste également quelques zones d'ombres concernant la gestion des
informations RIPE concernant les préfixes alloués aux clients. Il n'y a pas eu
de progrès particuliers sur les plans économique ou humain.

\clearpage

\section{Étapes suivantes}

Alors que le semestre se termine, le mois de février devrait offrir suffisamment
de temps consacré à l'avancée sereine du déploiement. Voici  un aperçu des
tâches restantes dans l'ordre chronologique :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item \textbf{Plan d'adressage}\\
  Il s'agit d'appliquer le nouveau plan d'adressage sur les sections critiques
  de l'infrastructure : routeurs, passerelles des réseaux locaux, services
  importants (DNS, mail, etc) et autres équipements réseaux compatibles. À noter
  que \textbf{cette phase ne concerne que l'adressage et non l'activation et la
  publication des services en IPv4 + IPv6}.

  \item \textbf{Déploiement initial}\\
  Les locaux de Trinaps sont connectés au réseau FAI via un CPE, un routeur
  indépendant posé chez le client, à l'instar d'une box ADSL par exemple. Cela
  nous permet à la fois de nous mettre dans la situation d'un certain type de
  clients (cf chapitre \ref{modeles-client} page \pageref{modeles-client}) et de
  séparer le réseau opérateur du réseau \emph{bureautique} interne. L'objectif
  de déploiement initial dans ce réseau interne et d'établir des procédures et
  des configurations applicables à d'autres clients et de calibrer la charge de
  travail nécessaire.

  \item \textbf{Déploiement sur les services FAI}\\
  Une fois les locaux de Trinaps connectés en double pile et le bon
  fonctionnement d'IPv6 validé, il est nécessaire d'activer les services
  essentiels au bon fonctionnement d'un réseau informatique moderne (DNS, mails,
  NTP, etc). Ces services seront dans un premier temps utilisés directement via
  leur adresse IPv6 puis une fois validés, ils seront publiés par les serveurs
  DNS de Trinaps. Seuls la configuration des serveurs DNS récursifs restera
  manuelle car les CPE Cisco ne supportent pas encore les annonces RDNSS
  permettant au routeur IPv6 d'annoncer sur le réseau local les serveurs DNS
  récursifs joignables en IPv6, grâce aux messages ICMPv6 de \emph{Router
  Advertisement}.

  \item \textbf{Déploiement sur les services hébergement}\\
  La phase précédente permettra de valider un pan entier de l'infrastructure
  opérateur, mais Trinaps offre également des prestations de services sur IP et
  notamment de l'hébergement mail, web et de données. Si la connectivité IPv6
  du client à l'Internet via les services FAI fonctionne bien, nous pourrons
  alors procéder à la mise à jour de cette plateforme d'hébergement et la
  valider depuis le réseau interne et depuis d'autres réseaux IPv6 externes.

  \item \textbf{Déploiement sur les cas clients compatibles}\\
  À la fin de la phase précédente, l'intégration et l'activation du protocole
  IPv6 sur la totalité de l'infrastructure opérateur (FAI et hébergeur) seront
  validées. De même, les différentes procédures de déploiement correspondant à
  chaque cas client directement compatible seront standardisées. Nous pourrons
  alors procéder à l'activation d'IPv6 jusqu'au client. Selon le cas client
  déployé, Trinaps fournira également l'accès IPv6 sur le LAN, sinon le client
  devra l'effectuer depuis son propre équipement de routage s'il est compatible
  IPv6. Dans tous les cas, le client sera \emph{approvisionné} \textbf{en
  disposant d'un préfixe IPv6 alloué}.

\clearpage

  \item \textbf{Réflexion de plan de migration des clients incompatibles}\\
  Enfin il faut réfléchir quand le client se trouve dans une situation qui ne
  lui permet pas de profiter de connectivité IPv6 native : collecte xDSL/Fibre
  limitée à IPv4, CPE ou matériel client incompatible, etc. Certains problèmes
  peuvent être réglés grâce aux techniques de transition telles que les tunnels
  \emph{IPv6 over IPv4} (6in4). D'autres nécessiteront le rachat de nouveaux
  équipements et surtout de convaincre le client que l'investissement est
  nécessaire.

\end{itemize}

\section{Enjeux}

A priori, le déploiement du protocole IPv6 au sein de Trinaps ne fut pas trop
retardée malgré un semestre tumultueux. Néanmoins, nous devons toutefois veiller
à respecter et répondre au mieux à certains enjeux qui caractérisent la fin de
la période de déploiement :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item \textbf{Assurer le service}\\
  Un réseau de production composé de serveurs d'hébergement professionnels et un
  réseau d'accès à Internet destinés à des professionnels ne sont pas des
  maquettes de laboratoire qui peuvent être démantelées et reconstruites à
  volonté. Des engagements contractuels contraignent à la meilleure
  disponibilité du service possible. De plus, le protocole IPv6 est trop
  transparent pour l'utilisateur final, pour qui il sera difficile de justifier
  l'indisponibilité temporaire de son service ou de son accès à Internet.
  Le \emph{déploiement d'IPv6 à grande échelle} doit absolument se dérouler dans
  \textbf{la stabilité du service existant}.

  \item \textbf{Assurer la transparence}\\
  Pour conserver la même qualité de service, l'utilisateur ne doit pas avoir à
  effectuer de manipulation sur son poste. S'il veut profiter pleinement de la
  connectivité IPv6 et que certaines modifications de configuration sont
  nécessaires sur ses postes, une prestation peut être mise en place.

  \item \textbf{Agir avec pragmatisme}\\
  Le protocole IPv6 est un élément primordial à la croissance et l'évolution du
  réseau mondial Internet. Malheureusement, les répercussions économiques pour
  un opérateur comme Trinaps ne sont pas évidentes : \emph{Stub AS} ne vendant
  pas (encore) de transit IP à d'autres AS et fournissant des clients qui dans
  l'écrasante majorité des cas n'ont aucune conscience du protocole IP, quelle
  que soit la version qu'ils utilisent de manière transparente.

  \item \textbf{Documenter et former}\\
  Peut-être un des enjeux les plus importants au court terme dans une vision au
  long terme : être capable dès à présent de documenter l'existant et le futur
  d'IPv6 dans l'infrastructure opérateur, allant de la conception jusqu'aux
  notes de production concernant des aspects techniques très pointus, comme la
  politique de filtrage des messages ICMPv6. La transmission de connaissances et
  de compétences est un processus très important lorsque l'on traite d'un des
  piliers de l'Internet de demain, alors que beaucoup (trop) de gens dans le
  métier y sont encore insensibles.

\end{itemize}
