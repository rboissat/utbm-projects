#!/bin/bash
if [ "$UID" -ne 0 ]
then
    echo "scsi-scan.sh: please run as root or with sudo"
else
    for i in /sys/class/scsi_host/*; do echo "- - -" > $i/scan; done
fi
