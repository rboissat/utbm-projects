# coding: utf8
import os
import sys
from gluon.tools import *

path = os.path.join(request.folder, 'modules')
if not path in sys.path:
    sys.path.append(path)

import bwtools as bt

db = DAL('sqlite://storage.sqlite')          # database file used for settings
auth=Auth(globals(),db)                      # authentication/authorization
auth.settings.hmac_key='2fd29c74-4eec-4a68-9536-21c3cfa38f54'
auth.define_tables()                         # creates all needed tables
crud=Crud(globals(),db)                      # for CRUD helpers using auth
service=Service(globals())                   # for json, xml, jsonrpc, xmlrpc, amfrpc

## Data in separate db
db = DAL("sqlite://data.sqlite")

#########################################################################
## Localization
#########################################################################
T.force('fr-fr' or 'en')

#########################################################################
## Tables definition
#########################################################################

## Client table
db.define_table("client",
        Field("cli_name", "string",
                length=512, notnull=True, default=None, unique=True,
                label=T('Name of the client'),
                comment=T('eg: eNovance | M. Doe')
            ),
        Field("cli_email", "string",
                length=512, notnull=True, default=None, unique=True,
                label=T('Contact email'),
                comment=T('must be a valid email!')
            ),
        Field("timstp", "datetime",
                default=request.now, readable=True, writable=False,
                update=request.now),
        migrate='client.table'
        )

## Project table
db.define_table("project",
        Field("pro_name", "string",
                length=512, notnull=True, default=None, unique=True,
                label=T('Name of the project'),
                comment=T('eg. "Association BLEH"')
            ),
        Field("timstp", "datetime",
                default=request.now, readable=True, writable=False,
                update=request.now),
        migrate='project.table'
        )

## Project (1,n) ---- (1,n) sub-project
db.define_table("rel_pro_pro",
        Field("id_parent", db.project,
                label=T('Parent project'),
                comment=T('The parent project must be already  recorded in db!')
            ),
        Field("id_child", db.project,
                label=T('Child project'),
                comment=T('The child project must be already recorded in db!')
            ),
        Field("timstp", "datetime",
                default=request.now, readable=False, writable=False,
                update=request.now),
        migrate='rel_pro_pro.table'
        )

## Client (1,n) ---- (1,n) Project
db.define_table("rel_cli_pro",
        Field("id_cli", db.client,
                label=T('Owner'),
                comment=T('The client must be already recorded in db!')
            ),
        Field("id_pro", db.project,
                label=T('Project'),
                comment=T('The project must be already recorded in db!')
            ),
        Field("timstp", "datetime",
                default=request.now, writable=False, readable=False,
                update=request.now),
        migrate='rel_cli_pro.table'
        )

## Zone table
db.define_table("zone",
        Field("zon_name", "string",
                length=512, notnull=True, default=None, unique=True,
                label=T('Zone'),
                comment=T('eg. chrooted-universe.org')
            ),
        Field("zon_alias", "integer",
                 default=7200, unique=False,
                label=T('Is an alias to'),
                comment=T('if the zone is just an alias')
            ),
        Field("id_project", db.project,
                label=T('Linked project')
            ),
        Field("timstp", "datetime",
                default=request.now, writable=False, readable=False,
                update=request.now),
        migrate='zone.table'
        )

## attribute table
# This model must cover *ALL* types of RR
# eg.   "_sip._tcp.example.com. 86400 IN SRV 0 5 5060 serveursip.example.com."
db.define_table("attribute",
        Field("att_owner", "string",
                length=512, notnull=True, default=None, unique=False,
                label=T('Owner'),
                comment=T('eg. @, chrooted-universe.org, _sip._tcp.example.com.')
            ),
        Field("att_type", "string",
                length=512, notnull=True, default=None, unique=False,
                label=T('Type'),
                comment=T('eg. A, SRV, PTR, MX')
            ),
        Field("att_ttl", "integer",
                default=7200, unique=False,
                label=T('Time To Live'),
                comment=T('eg. 3600 (sec)')
            ),
        Field("att_class", "string",
                length=512, notnull=True, default=None, unique=False,
                label=T('Class'),
                comment=T('by default IN (Internet)')
            ),
        Field("att_prio", "integer",
                unique=False,
                label=T('MX/SRV Priority'),
                comment=T('Integer. Only for MX or SRV Resource Records')
            ),
        Field("att_weight", "integer",
                unique=False,
                label=T('SRV weight'),
                comment=T('Integer. Only for SRV Resource Records')
            ),
        Field("att_port", "integer",
                unique=False,
                label=T('SRV port'),
                comment=T('Service port. Only for SRV Resource Records')
            ),
        Field("att_rdata", "string",
                length=512, notnull=True, default=None, unique=False,
                label=T('Data / Value'),
                comment=T('a valid IP, domain name, or any valid value \
                           according to the type')
            ),
        Field("att_email", "string",
                length=512, notnull=False, default=None, unique=False,
                label=T('SOA e-mail'),
                comment=T('valid e-mail address for the SOA record ONLY!')
            ),
        Field("att_sn", "integer",
                unique=False, writable=False, readable=True,
                label=T('SOA Serial Number'),
                comment=T('generally YYYYMMDDXX with 0 < XX < 99. Must be \
                           incremented')
            ),
        Field("att_ref", "integer",
                unique=False,
                label=T('SOA Refresh'),
                comment=T('Indicates the time when the slave will try to \
                           refresh the zone from the master ')
            ),
        Field("att_retry", "integer",
                unique=False,
                label=T('SOA Retry'),
                comment=T('Defines the time between retries if the slave fails \
                           to contact the master when  refresh has expired')
            ),
        Field("att_exp", "integer",
                unique=False,
                label=T('SOA Expire'),
                comment=T('Indicates when the zone data is no longer \
                           authoritative')
            ),
        Field("att_min", "integer",
                unique=False,
                label=T('SOA minimum'),
                comment=T('The number of seconds that the records in the zone \
                           are valid for')
            ),
        Field("timstp", "datetime",
                default=request.now, writable=False, readable=True,
                update=request.now
            ),

        migrate="attribute.table"
        )

## Zone (1,n) ---- (1,n) attribute
db.define_table("rel_zon_att",
        Field("id_zon", db.zone),
        Field("id_att", db.attribute),
        Field("timstp", "datetime",
                default=request.now, writable=False, readable=False,
                update=request.now),
        migrate="rel_zon_att.table"
        )


## Atomic zone-attributes relationship
db.define_table("atom_zon_att",
        Field("id_zon" , db.zone),
        Field("id_oldatt", db.attribute),
        Field("id_newatt", db.attribute),
        Field("timstp", "datetime",
                default=request.now, writable=False, readable=False,
                update=request.now),

        migrate="atom_zon_att.table"
        )

## Resource Record type storage (eg. A, PTR, MX, AAAA)
db.define_table("types",
        Field("typ_title", "string",
                length=8, default=None, unique=True),
        Field("typ_weight", "integer",
                default=None),
        migrate="types.table"
        )

#########################################################################
## Requirements
#########################################################################

db.client.cli_name.requires = (     IS_LENGTH(maxsize=512,
                                        error_message=str(T('too long! 512 chars max'))),
                                    IS_NOT_EMPTY(error_message=str(T('cannot be empty!'))),
                                    IS_NOT_IN_DB(db, 'client.cli_name',
                                        error_message=str(T('value already in db!'))))

db.client.cli_email.requires = (    IS_LENGTH(maxsize=512,
                                        error_message=str(T('too long! 512 chars max'))),
                                    IS_NOT_EMPTY(error_message=str(T('cannot be empty!'))),
                                    IS_NOT_IN_DB(db, 'client.cli_email',
                                        error_message=str(T('value already in db!'))),
                                    IS_EMAIL(error_message=str(T('invalid e-mail!'))))

db.project.pro_name.requires = (    IS_LENGTH(maxsize=512,
                                        error_message=str(T('too long! 512 chars max'))),
                                    IS_NOT_EMPTY(error_message=str(T('cannot be empty!'))),
                                    IS_NOT_IN_DB(db, 'project.pro_name',
                                        error_message=str(T('value already in db!'))))

## Zone specific requirements
db.zone.zon_name.requires = (   IS_LENGTH(maxsize=512,
                                        error_message=str(T('too long! 512 chars max'))),
                                IS_NOT_EMPTY(error_message=str(T('cannot be empty!'))),
                                IS_NOT_IN_DB(db, 'zone.zon_name',
                                        error_message=str(T('value already in db!'))),
                                IS_MATCH('^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$',
                                    error_message=str(T('invalid domain name!'))))

######## TODO: IMPLEMENT IS_NOT_ALIAS()
db.zone.zon_alias.requires =        IS_NULL_OR(
                                    IS_IN_DB(db, 'zone.id', '%(zon_name)s',
                                        error_message=str(T('unknown main domain!'))))

## Zone attributes specific requirements
db.attribute.att_owner.requires = ( IS_NOT_EMPTY(
                                        error_message=str(T('cannot be empty!'))),
                                    IS_LENGTH(maxsize=512,
                                        error_message=str(T('too long! 512 chars max'))))

db.attribute.att_type.requires =    IS_IN_DB(db, 'types.typ_title')

db.attribute.att_ttl.requires =     IS_NULL_OR(
                                    IS_INT_IN_RANGE(600, 172800,
                                        error_message=str(T('please compute a value between 600 and 36000'))))

db.attribute.att_class.requires =   IS_IN_SET(['IN','CH','HS'])

db.attribute.att_rdata.requires =   IS_LENGTH(maxsize=512,
                                        error_message=str(T('too long! 512 chars max')))

## For scaffolding compatibility
db.attribute.att_prio.requires =    IS_NULL_OR(
                                    IS_INT_IN_RANGE(0, 100,
                                        error_message=str(T('please compute a value between 0 and 100'))))

db.attribute.att_weight.requires =  IS_NULL_OR(
                                    IS_INT_IN_RANGE(0, 100,
                                        error_message=str(T('please compute a value between 0 and 100'))))

db.attribute.att_port.requires =    IS_NULL_OR(
                                    IS_INT_IN_RANGE(0, 100,
                                        error_message=str(T('please compute a value between 0 and 100'))))

db.attribute.att_email.requires = ( IS_LENGTH(maxsize=512,
                                        error_message=str(T('too long! 512  chars max'))),
                                    IS_NULL_OR(
                                    IS_EMAIL(error_message=str(T('invalid e-mail!')))))

db.attribute.att_sn.requires =      IS_NULL_OR(
                                    IS_INT_IN_RANGE(1000000000, 4294967295,
                                        error_message="YYYYMMDDXX with 01 < XX < 99"))

db.attribute.att_ref.requires = IS_NULL_OR(
                                IS_INT_IN_RANGE(0, 4294967295,
                                    error_message=str(T("please compute a value between 0 and 4294967295"))))

db.attribute.att_retry.requires=IS_NULL_OR(
                                IS_INT_IN_RANGE(0, 4294967295,
                                    error_message=str(T("please compute a value between 0 and 4294967295"))))

db.attribute.att_exp.requires = IS_NULL_OR(
                                IS_INT_IN_RANGE(0, 4294967295,
                                    error_message=str(T("please compute a value between 0 and 4294967295"))))

db.attribute.att_min.requires = IS_NULL_OR(
                                IS_INT_IN_RANGE(0, 4294967295,
                                    error_message=str(T("please compute a value between 0 and 4294967295"))))

## Atom zon att relationship requirements
db.atom_zon_att.id_zon.requires =   IS_IN_DB(db, 'zone.id', '%(zon_name)s')

db.atom_zon_att.id_oldatt.requires =IS_NULL_OR(
                                    IS_IN_DB(db, 'attribute.id',
                                                 '%(att_owner)s ...  %(att_type)s ...  %(att_rdata)s'))

db.atom_zon_att.id_newatt.requires =IS_NULL_OR(
                                    IS_IN_DB(db, 'attribute.id',
                                                 '%(att_owner)s ...  %(att_type)s ...  %(att_rdata)s'))

## RR Types requirements
db.types.typ_title.requires = (     IS_LENGTH(maxsize=8,
                                        error_message=str(T('too long! 8 chars max'))),
                                    IS_ALPHANUMERIC(
                                        error_message=str(T('must be alphanumeric!'))),
                                    IS_UPPER())

db.types.typ_weight.requires =      IS_INT_IN_RANGE(0, 100,
                                        error_message=str(T('please compute a value between 0 and 100')))

#########################################################################
## Relations between tables (remove fields you don't need from requires)
#########################################################################

db.zone.id_project.requires =       IS_IN_DB(db, 'project.id', '%(pro_name)s')

db.rel_cli_pro.id_cli.requires =    IS_IN_DB(db, 'client.id', '%(cli_name)s')
db.rel_cli_pro.id_pro.requires =    IS_IN_DB(db, 'project.id', '%(pro_name)s')

db.rel_pro_pro.id_parent.requires = IS_IN_DB(db, 'project.id', '%(pro_name)s')
db.rel_pro_pro.id_child.requires =  IS_IN_DB(db, 'project.id', '%(pro_name)s')

db.rel_zon_att.id_zon.requires =    IS_IN_DB(db, 'zone.id', '%(zon_name)s')
db.rel_zon_att.id_att.requires =    IS_IN_DB(db, 'attribute.id',
                                                 '%(att_owner)s ... %(att_type)s ... %(att_rdata)s')

#########################################################################
## Many to Many Joins
#########################################################################
db.join_zon_att = db((db.zone.id==db.rel_zon_att.id_zon) & (db.attribute.id==db.rel_zon_att.id_att))

db.join_cli_pro = db((db.client.id==db.rel_cli_pro.id_cli) & (db.project.id==db.rel_cli_pro.id_pro))

db.join_pro_pro = db((db.project.id==db.rel_pro_pro.id_parent) & (db.project.id==db.rel_pro_pro.id_child))
