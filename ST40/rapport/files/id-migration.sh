#!/bin/bash
## This script migrates:
##  * the jive's unix group to a new GID
##  * the jive's unix user to a new UID

NEW_ID=10000

## Some warning first
echo "
###############################################################
#  THIS SCRIPT AFFECTS USERS AND GROUPS -- USE WITH CAUTION!  #
###############################################################

* This script must be run with root privileges via the sudo command

* This script should be used *once* right after the jive sbs installation

* By default, this script does nothing, run it with the \"--effective\" flag
  for real operation.
"

## If with root privileges then continue, else exit.
if [ "$UID" -ne 0 ]
then
    echo "ER_USER: REQUIRES ROOT PRIVILEGES!" && exit 42
fi

## If run with the --effective flag, then continue, else exit.
if [ "x$1" == "x--effective" ]
then
    echo -e "Begining effective operation in 5 seconds\nHIT CTRL+C TO CANCEL\n" && sleep 6
else
    echo "ER_FLAG: REQUIRES --effective FOR REAL OPERATION" && exit 42
fi

## If jive UID is already $NEW_ID, assume that the script was already executed beforehand.
if [[ `id -u jive` -eq $NEW_ID && `id -g jive` -eq $NEW_ID && $2 != "--force" ]]
then
    echo "ER_SCRIPT: this script seems to have been already executed. Exiting now..." && exit 42
fi

## Begin the real stuff
# Stop the jive services
echo -e "STOPPING JIVE SERVICES\n----------------------\n"
for i in /etc/init.d/jive-*; do echo "## $i stop" && $i stop; done

# Changing matching files' UID from old value to $NEW_ID
echo -e "\n\nCHOWNING FILES FROM UID:`id -u jive` TO UID:$NEW_ID"
/usr/bin/find / -uid `id -u jive` -exec chown $NEW_ID "{}"  \; > /dev/null 2>&1

# Changing matching files' GID from old value to $NEW_ID
echo -e "\nCHOWNING FILES FROM GID:`id -g jive` TO GID:$NEW_ID"
/usr/bin/find / -gid `id -g jive` -exec chown :$NEW_ID "{}" \; > /dev/null 2>&1

# Changing jive unix group GID
echo -e "\nUSERMOD: UPDATING USER jive"
/usr/sbin/groupmod -g $NEW_ID jive > /dev/null 2>&1

# Changing jive unix group GID
/usr/sbin/usermod -u $NEW_ID -g $NEW_ID jive > /dev/null 2>&1

# Start the jive services
echo -e "\nSTARTING JIVE SERVICES\n----------------------\n"
for i in /etc/init.d/jive-*; do echo "## $i start" && $i start; done

# Print out a summary
echo -e "\n#############\nSUMMARY: The user jive is now: `id jive`\n" && exit 0
