#!/usr/bin/env python 
# _*_ encoding: utf8 _*_
import sys
import os
from time import strftime
from time import gmtime
from re import match
from bwtools import log_except
#from gluon.html import *
#from gluon.http import *
#from gluon.validators import *
from gluon.sqlhtml import *
# request, response, session, cache, T, db(s) 
# must be passed and cannot be imported!

FILE = os.path.basename(globals()['__file__'])

###############################################################################
## COMMON TOOLS
###############################################################################
def get_all_zones(db):
    """ Return existing zones
    """
    zones = None
    try:
        zones = db().select(db.zone.ALL, orderby=db.zone.zon_name)
    except:
        print log_except(FILE, sys.exc_info()[0])

    return zones


def get_zone(db, id_zo):
    """ Fetch a zone
    """
    zone = None
    try:
        zone = db(db.zone.id==id_zo).select()[0]
    except:
        print log_except(FILE, sys.exc_info()[0])

    return zone


def get_attributes(db, id_zo):
    """ Fetch a zone's attributes (attributes)
    """
    attributes = None
    query = (db.zone.id==id_zo) & (db.attribute.att_type==db.types.typ_title)
    try:
        attributes = db.join_zon_att(query).select( db.attribute.ALL,
                                                    db.types.typ_weight,
                                                    orderby = db.types.typ_weight )
    except:
        print log_except(FILE, sys.exc_info()[0])

    return attributes


def get_soa(db, id_zo):
    """ Fetch a zone's soa
    """
    soa = None
    query = (db.zone.id==id_zo) & (db.attribute.att_type=="SOA")
    try:
        soa = db.join_zon_att(query).select(db.attribute.ALL)[0]
    except:
        print log_except(FILE, sys.exc_info()[0])

    return soa

def get_record_type(record):
    """ Get Resource Record type (A, MX, SOA, SRV, ...)
    """
    return record.attribute.get('att_type')


def fields_by_RRtype(db, type):
    """ Return a list of fields to display according to the RR type
        Redefine which validators to be used according to the type
    """
    fields = []
    fields.extend(db.attribute.fields)

    if type not in ('MX','SRV','SOA'):
        fields.remove('att_prio')
        fields.remove('att_weight')
        fields.remove('att_port')
        fields.remove('att_email')
        fields.remove('att_sn')
        fields.remove('att_ref')
        fields.remove('att_retry')
        fields.remove('att_exp')
        fields.remove('att_min')

    elif type == 'MX':
        fields.remove('att_weight')
        fields.remove('att_port')
        fields.remove('att_email')
        fields.remove('att_sn')
        fields.remove('att_ref')
        fields.remove('att_retry')
        fields.remove('att_exp')
        fields.remove('att_min')

    elif type == 'SRV':
        fields.remove('att_email')
        fields.remove('att_sn')
        fields.remove('att_ref')
        fields.remove('att_retry')
        fields.remove('att_exp')
        fields.remove('att_min')

    elif type == 'SOA':
        fields.remove('att_prio')
        fields.remove('att_weight')
        fields.remove('att_port')

    else:
        pass

    return fields


###############################################################################
## UPDATE TOOLS: zones and attributes
###############################################################################
def soa_update_serial(db, zone_id):
    """ Update zone SOA Serial Number on zone/attribute update or attribute
        creation/deletion
    """
    soa = get_soa(db, zone_id)
    sn = soa.get('att_sn')

    now = int(strftime("%Y%m%d00", gmtime()))
    sn = (now if sn < now else sn+1)

    try:
        db(db.attribute.id == soa.get('id')).update(att_sn=sn)
    except:
        print log_except(FILE, sys.exc_info()[0])


def frm_zon_update(db, record):
    """ Return an update form for a DNS Zone
    """
    return SQLFORM( db.zone,
                    record.id,
                    showid=False,
                    deletable=False)


def frm_att_update(db, record):
    """ Return an update form for a Resource Record (attribute)
    """
    form_fields = []
    form_fields = fields_by_RRtype(db, get_record_type(record))

    return SQLFORM( db.attribute,
                    record.attribute.id,
                    showid=False,
                    fields=form_fields,
                    deletable=True)


def att_clean_fields(db, record_id):
    """ Cleanse a Ressource Record (attribute) of deprecated fields
        eg: An RR update from MX type to A -> set att_prio to None since
        it is nonsense for A type.
    """
    record = db(db.attribute.id == record_id).select()[0]
    fields = fields_by_RRtype(db, record.get('att_type'))
    keys = record.keys()
    to_delete = {}

    for k in keys:
        if k not in fields and record.get(k) and match("att_*", k):
            to_delete[k] = None

    try:
        # The **{} allows us to dynamically substitute parameters
        record.update_record(**to_delete)
    except:
        print log_except(FILE, sys.exc_info()[0])

