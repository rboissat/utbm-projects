#!/usr/bin/env python
# Mon Jan 11 16:59:37 CET 2010
# Simple Low level Network Heartbeat
#
# Requirements for running this script:
#######################################
# Comment out (prefix with #) the line "Defaults requiretty" in /etc/sudoers
# and append at the end of the file the following line without the quotes:
# "sysadmin ALL = NOPASSWD: /usr/sbin/arping -c 1 -I seth0 *"
#
# For autostart of this script, append the following line without the double quotes:
# "su sysadmin -c 'python /path/to/script/network_heartbeat_daemon.py'"
#
# Replace "sysadmin" with whatever non-root user you want

import sys
import os
import time

###########################################################################
WORKDIR         = os.path.expanduser('~')
PIDFILE         = os.sep.join([WORKDIR, '.net_heartbeat_daemon.pid'])
IP_HEARTBEAT    = '10.101.21.65' # Setup this variable and goto line 61
SLEEP           = 10 # in seconds
###########################################################################

def touch(filename=PIDFILE):
    try:
        if not os.path.exists(filename):
            open(filename,'w').close()

    except IOError:
        print "I/O error"
        sys.exit(1)

def network_heartbeat(ip='8.8.8.8'):
    ping = ['/usr/bin/sudo','arping','-c 1','-I seth0','-f','-q',ip]
    while(1):
        try:
            retcode = os.popen(' '.join(ping)).close()
            if retcode != None and retcode < 0:
                print >>sys.stderr, "Child was terminated by signal", retcode
            elif retcode == None :
                status = "OK"
            else:
                status = "NOK"

            print >>sys.stderr, "ping", status

        except OSError, e:
            print >>sys.stdout, "Execution failed:", e
        time.sleep(SLEEP)

def main():
    os.chdir(WORKDIR)
    os.setegid(os.getgid())
    os.seteuid(os.getuid())
    touch(PIDFILE)
    # Replace the next line with: "network_heatbeat(IP_HEARTBEAT)"
    # After proper setup line 23
    network_heartbeat()

if __name__ == "__main__":
    # do the UNIX double-fork magic, see Stevens' "Advanced
    # Programming in the UNIX Environment" for details (ISBN 0201563177)
    try:
        pid = os.fork()
        if pid > 0:
            # exit first parent
            sys.exit(0)
    except OSError, e:
        print >>sys.stderr, "fork #1 failed: %d (%s)" % (e.errno, e.strerror)
        sys.exit(1)

    # decouple from parent environment
    os.chdir("/")
    os.setsid()
    os.umask(0)

    # do second fork
    try:
        pid = os.fork()
        if pid > 0:
            # exit from second parent, print eventual PID before
            #print "Daemon PID %d" % pid
            open(PIDFILE,'w').write("%d"%pid)
            sys.exit(0)
    except OSError, e:
        print >>sys.stderr, "fork #2 failed: %d (%s)" % (e.errno, e.strerror)
        sys.exit(1)

    # start the daemon main loop
    main()
