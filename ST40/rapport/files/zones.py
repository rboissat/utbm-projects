# coding: utf8

#########################################################################
## This is the ZONE controller
## - Lists Current Zones
#########################################################################  
import sys, os

path = os.path.join(request.folder, 'modules')
if not path in sys.path:
    sys.path.append(path)

import zonelib as zl
import prolib as pl
import bwtools as bt


def user():
    """ Render the authentication form
    """
    return dict(form=auth())


@bt.no_auth_in_admin(auth, request)
def index():
    """ Return a dictionary rendered by the view containing all
        available zones, if any.

    >>> 'zo' in index()
    True

    >>> 'nb' in index()
    True

    >>> 'al' in index()
    True
    """

    zones = zl.get_all_zones(db)

    if not zones or not len(zones):
        session.flash = T('There is no zones available!')
        redirect(URL(a='biswi', c='default', f='index'))
    else:
        aliases = dict()
        nb_zones = len(zones)
        for zo in zones:
            if zo.zon_alias:
                aliases[zo.id] = zl.get_zone(db, zo.zon_alias).zon_name
            else:
                aliases[zo.id] = None

        return dict(zo=zones, nb=nb_zones, al=aliases)


@bt.no_auth_in_admin(auth, request)
def read():
    """ Exposes a specific zone and its attributes
    """

    if request.args:
        zone = zl.get_zone(db, request.args(0))
    else:
        session.flash = T('Wrong request!')
        redirect(URL(a='biswi', c='default', f='index'))

    if not zone:
        session.flash = T('There is no such zone available!')
        redirect(URL(r=request, f='index'))

    else:
        project = pl.get_project(db, zone.id_project)
        if not project:
            session.flash = "Project error"
            redirect(URL(r=request, f='index'))

        alias = dict()

        if zone.zon_alias:
            alias =  zl.get_zone(db, zone.zon_alias)
            attributes = zl.get_attributes(db, zone.zon_alias)
        else:
            attributes = zl.get_attributes(db, zone.id)

        return dict(zo=zone, att=attributes, pr=project, al=alias)


@bt.no_auth_in_admin(auth, request)
def edit():
    """ Allow edition of a zone file and its linked attributes
    """

    if request.args:
        zone = zl.get_zone(db, request.args(0))
    else:
        session.flash = T('Wrong request!')
        redirect(URL(a='biswi', c='default', f='index'))

    if not zone:
        session.flash = T('There is no such zone available!')
        redirect(URL(a='biswi', c='default', f='index'))
    else:
        attributes = zl.get_attributes(db, request.args(0))
        form = zl.frm_zon_update(db, zone)

        if form.accepts(request.vars, onvalidation=bt.check_input):
            session.flash = T('Record is updated!')
            redirect(request.url)

        if zone.zon_alias:
            z =  zl.get_zone(db, zone.zon_alias)
            alias = (z.id, z.zon_name)
            return dict(zo=form, ed=zone.zon_name, al=alias)

        else:
            if not attributes:
                return dict(zo=form, ed=zone.zon_name, att=None, al=None)
            else:
                att_forms = []
                for a in attributes:
                    form_fields = zl.fields_by_RRtype(db, zl.get_record_type(a))
                    att_forms.append(zl.frm_att_update(db, a))

                for f in att_forms:
                    if f.accepts(request.vars, onvalidation=bt.check_input):
                        zl.soa_update_serial(db, request.args(0))

                        if f.vars.delete_this_record != 'on':
                            zl.att_clean_fields(db, f.vars.get('id'))
                            #zl.rel_zon_att_delete

                        session.flash = T('Record is updated!')
                        redirect(request.url)

                return dict(zo=form, ed=zone.zon_name, att=att_forms, al=None)


@bt.no_auth_in_admin(auth, request)
def add():
    pass


@bt.no_auth_in_admin(auth, request)
def delete():
   session.flash='To be implemented soon'
   redirect(URL(r=request, f='index'))

