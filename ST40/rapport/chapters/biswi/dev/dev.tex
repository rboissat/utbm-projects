Un fois l'environnement de développement mis en place, j'ai travaillé sur la clé
de voute du paradigme \emph{MVC} : le modèle de données. En effet,
le ou les contrôleurs ainsi que les vues sont intimement liés au modèle de
données, et il est très important d'éprouver ce modèle de données (ou \emph{Data
Model}) avant tout développement \emph{significatif} des autres éléments. Cela
constitua mon travail principal lors du premier mois, puis un travail de fond au
fur et à mesure de l'évolution des besoins et/ou de l'approche technique
employée. J'y reviendrai dans la section \ref{bi:bilan} page \pageref{bi:bilan}.

La complexité de la vision de \emph{zones DNS héritables et surchageables},
concepts propres à la Programmation Orientée Objet ici appliqués à une base de
données non-objet, fut la principale difficulté lors de la conception de ce
\emph{Data Model}. Pour une représentation en Modèle Merise du \emph{Data Model}
final, voir l'annexe \ref{an:pic:MLD} page \pageref{an:pic:MLD}.

J'ai ensuite développé un premier contrôleur et des vues basiques pour
travailler un prototype sur le modèle de données qui évoluait. Le cadriciel
web2py repose sur une arborescence au niveau du système de fichiers, permettant
un développement simplifié et fractionné pour une plus grande souplesse et peu
ou pas de répétition de code. Ainsi, les applications développées ne sont pas
contenues n'importe où, mais dans un sous-dossier, lui même contenu dans le
dossier \verb+applications+ du dossier principal de \textbf{web2py}. Dans le
dossier de l'application \verb+biswi+ sont présents les dossiers suivants :

\clearpage

\begin{verbatim}
web2py/applications/biswi : 
  ./models
  ./controllers
  ./views
  ./databases
  ./modules
  ./tests
\end{verbatim}

Le dossier \verb+models+ contient les fichiers de définition du menu principal
de l'application, et du \emph{Data Model}, dont un extrait est fourni dans
l'annexe \ref{an:files:db.py} page \pageref{an:files:db.py}. De même avec les autres
dossiers qui contiennent les différents éléments de l'application. Ce découpage
est simple et efficace, mais la dérive est facile, car j'ai eu tendance à
répéter du code source pour chaque contrôleur. Suite aux conseils de M. Jonathan
Clarke, j'ai alors utilisé la souplesse du langage Python à travailler avec des
modules (bibliothèques de code Python réutilisables). De cette manière, j'ai pu
\emph{factoriser} le code source. Exemple avec un extrait du fichier
\verb+web2py/applications/biswi/controllers/zones.py+ qui est le contrôleur en
charge du cœur de l'application métier \textbf{BiSWI} : la gestion de zones
\emph{DNS}.

\begin{verbatim}
# coding: utf8
#########################################################################
## This is the ZONE controller
#########################################################################

import sys, os

# If the local module directory is not within PYTHONPATH variable, we add it.
path = os.path.join(request.folder, 'modules')
if not path in sys.path:
      sys.path.append(path)

import zonelib as zl
import prolib as pl
import bwtools as bt
\end{verbatim}

Le mot clé \verb+import+ a permis dans ce cas d'importer le code source de trois
fichiers Python contenus dans \verb+web2py/applications/biswi/modules/+. Ainsi,
pour utiliser la fonction \verb+get_all_zones()+ du module \verb+zonelib.py+, il
suffit de déclarer \verb+zl.get_all_zones()+ dans \verb+zones.py+, le mot clé
\verb+as+ permettant alors de donner un alias au nom du module, ici plus court
donc moins lourd dans le code source.
Cette modularité m'a amené à développer plusieurs \emph{API}, rendant ainsi le
plus de code source possible réutilisable à plusieurs endroits, tout en
proposant une abstraction supplémentaire au développeur qui ainsi gagne du temps
en utilisant un ensemble réduit de fonctions pour plusieurs contrôleurs.

Pour le développement des vues, \textbf{web2py} met à disposition du développeur
un langage spécifique à la conception de \emph{templates}, des pages web
proposant un contenu dynamique mais avec une présentation commune dans le but de
rendre homogène l'IHM de l'application.

Quant aux modalités de réalisation, elles étaient simples, mais d'une
influence capitale sur le choix des outils de travail. En effet, afin de
développer au rythme soutenu et dynamique d'eNovance, la décision d'utiliser un
\emph{cadriciel} s'imposa d'elle même en lieu et place d'un développement de
zéro. De même pour l'utilisation systématique d'un \emph{VCS} ou
logiciel de gestion de version, permettant d'obtenir un développement beaucoup
plus souple et dynamique, en conservant une trace systématique du développement,
et ceux pour un ou plusieurs développeurs. Au sein d'eNovance, c'est le
\emph{VCS} \textbf{Subversion}\footnote{Système de gestion de version libre qui
a connu un grand succès dans les années 2000 grâce à sa simplicité, et ses
nombreux apports par rapport à \emph{CVS}. Cf.
\url{http://fr.wikipedia.org/wiki/Subversion_(logiciel)}} qui est utilisé.

J'ai donc utilisé Subversion au quotidien, en l'intégrant à part entière dans
mon processus de développement. En effet, l'utilisation d'un \emph{VCS} tel que
Subversion permet de conserver l'historique de développement, même si plusieurs
développeurs travaillent simultanément sur le projet. Ainsi, les modifications
de chacun peuvent être fusionnées, et il est possible de revenir à un état
(\emph{révision} dans le jargon des \emph{VCS}) précédent. De plus cela permet
de simplifier la distribution du code source entre développeurs, puisqu'il est
possible de récupérer l'ensemble du code source versionné, et cela pour
n'importe quelle révision du projet.De plus je tenais au plus possible à
respecter un adage anglais de développeur qui dit
\emph{«~Commit~early,~Commit~often~»}, c'est-à-dire faire en sorte que
chaque révision (version) que je téléverse dans le dépôt situé sur le serveur
Subversion comporte un ensemble de modifications minimal mais logique.
Je testais également le code écrit avant de le téléverser sur le serveur
Subversion, afin qu'aucune révision n'aboutisse sur un projet non fonctionnel.
De plus, pour assurer une traçabilité maximale, j'ai tenu à écrire des résumés
de modifications (\emph{commit logs}) synthétiques.

Quant aux autres outils, j'apprécie beaucoup la souplesse offerte par les
environnements en ligne de commande. J'ai donc utilisé exclusivement l'éditeur
de code Vim, configuré spécialement pour le langage Python (coloration
syntaxique, respect des consignes de style de code\ldots).
