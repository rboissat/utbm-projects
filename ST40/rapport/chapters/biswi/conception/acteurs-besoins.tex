Le projet \textbf{BiSWI} (\textbf{Bi}nd9 \textbf{S}imple \textbf{W}eb
\textbf{I}nterface) a pour principale vocation d'offrir une interface
d'administration interne. Les acteurs concernés sont les employés et les gérants
de l'entreprise eNovance. Plus spécifiquement, c'est M. Nicolas Marchal, gérant
et directeur technique, qui a rempli la fonction de maître d'ouvrage, en
participant activement à la mise en avant des besoins utilisateurs à traîter,
ainsi que les modalités de réalisation du projet. M. Jonathan Clarke, ingénieur
de l'équipe technique, est venu m'assister au bout de quelques semaines dans
certains aspects techniques que je ne maîtrisais pas, ainsi que sur la refonte
du modèle de données du projet.

Malgré le faible nombre d'acteurs impliqués, les enjeux de ce projet sont de
taille pour deux raisons inhérentes au système \emph{DNS} : sa nature
\emph{distribuée} et le fait qu'il soit le principal service global de
traduction de noms de domaines alphanumériques en adresses IP v4 ou v6 utilisé
par les ordinateurs du monde entier. En effet, la propagation des informations
\emph{DNS} n'est pas immédiate, les serveurs de noms des
\emph{FAI}\footnote{Fournisseur d'Accès à Internet. Organisation proposant via
ses services et produits à la fois le rattachement à l'infrastructure de
télécommunications et le matériel terminal nécessaire pour founir l'accès au
réseau Internet à un client.} ont la plupart du temps un cache mémoire afin
d'alléger le trafic dû aux requêtes vers les serveurs de noms, etc. Ces
contraintes sont transparentes pour la plupart des utilisateurs, mais en cas
d'erreur lors de la saisie des informations dans le fichier de zone, les
serveurs de noms de domaine vont alors servir une mauvaise information, voire
aucune information viable concernant la zone \emph{DNS} concernée... et ce pour
une durée pouvant excéder les trois heures, rendant le service
\emph{indisponible aux yeux du monde entier !} Impensable pour une application
critique d'un client. Il est donc très important d'avoir un contrôle syntaxique
des données, pour au moins pallier les erreurs de frappe et d'inattention.

Ces enjeux sont à l'origine des besoins d'eNovance pour la conception d'un tel
logiciel. En effet, l'expérience des différents directeurs techniques leur a
permis de pointer du doigt les travers d'une administration manuelle et non
vérifiée d'un tel service. La réflexion autour des besoins découla de cet
situation de fait.

Voici le besoin initial exprimé par le maître d'ouvrage et les futurs
utilisateurs :
\begin{center}
\emph{«~Assurer l'ajout, la consultation, l'édition et la suppression de zones
DNS, ainsi que leur propagation sur les serveurs de noms de domaine.~»}
\end{center}

Ce besoin initial a connu par la suite un développement en plusieurs besoins
fonctionnels élémentaires, dont la formulation est simplifiée, en utilisant un
vocabulaire réduit, proche du quotidien des utilisateurs, afin d'en
faciliter la compréhension. L'absence de termes techniques spécifiques au
développement est un impératif pour éviter de perdre l'utilisateur non-initié.
Ces besoins fonctionnels élémentaires sont alors regroupés dans une liste
d'\emph{User Stories}\footnote{Histoires Utilisateur. Les méthodes de
développement dites agiles favorisent ce genre de formulation afin de clarifier
au mieux possible le dialogue entre l'utilisateur et le développeur, en levant
ainsi les ambiguïtés sur les fonctions principales du projet.} :

\begin{itemize}
  \item{Afficher la liste des clients.}
  \item{Créer un nouveau client.}
  \item{Modifier un client.}
  \item{Supprimer un client.}
  \item{Afficher la liste des projets.}
  \item{Créer un nouveau projet.}
  \item{Modifier un projet.}
  \item{Supprimer un projet.}
  \item{Afficher la liste de toutes les zones DNS gérées.}
  \item{Créer une nouvelle zone DNS.}
  \item{Modifier une zone DNS.}
  \item{Supprimer une zone DNS.}
  \item{Afficher le détail d'une zone DNS, pour en lire facilement les
        attributs.}
  \item{Ajouter et supprimer un enregistrement à une zone DNS existante.}
  \item{Modifier un enregistrement d'une zone DNS existante.}
  \item{Créer une nouvelle zone DNS qui hérite tous les enregistrements d'une
        zone existante.}
  \item{Ajouter un enregistrement à une zone DNS héritée sans l'ajouter à la
        zone père.}
  \item{Supprimer un enregistrement d'une zone DNS héritée sans la supprimer de
        la zone père.}
  \item{Modifier un enregistrement dans une zone DNS héritée sans modifier la
        zone père.}
  \item{Consulter la liste des zones rattachées à un projet.}
  \item{Consulter la liste des zones appartenant à un client.}
  \item{Consulter la liste des sous-projets d'un projets.}
  \item{Consulter la liste des projets appartenant à un client.}
  \item{Générer les fichiers de zones par zone, projet, client\ldots}
  \item{Obtenir le fichier de zone de n'importe quelle zone pour le charger
        directement dans BIND.}
  \item{Historiser au niveau fichier.}
  \item{Consulter les différences avant/après, avant la génération des fichiers
  de zone.}
  \item{Propager les fichiers générés sur les serveurs de noms.}
  \item{Controler le rechargement des zones sur les serveurs distants.}
  \item{Traitement par lot.}
  \item{Importer des fichiers de zone existants.}
\end{itemize}

Une fois établie cette liste de besoins élémentaires, j'ai dressé une liste
de contraintes et de caractéristiques techniques. Ainsi, j'ai conservé
l'utilisation du langage \textbf{Python} étant donné que j'avais mis en avant
ce langage dans le cadre de ce stage. Ensuite, étant donné l'orientation
serveur/services de ce projet, j'ai conservé une \textbf{approche «~web~»}
selon le désir du maître d'ouvrage. C'est à dire que l'interface de
l'application sera présentée sous la forme d'une application web, utilisant des
technologies éprouvées par d'autres sites web. Cela permettrait un déploiement
simplifié sur un parc de serveurs disposant déjà d'un service d'hébergement web.

Concernant le stockage et le traitement des données, le compromis semblait être
une base de données. Cela reste beaucoup plus souple par rapport à un stockage
simple dans des fichiers texte, car on dispose du langage
\emph{SQL}\footnote{\emph{Structured Query Language}. Pseudo-langage
informatique (de type requête) standard et normalisé, destiné à interroger ou à
manipuler une base de données relationnelle.}
pour exécuter des requêtes avancées sur les données en fonction de leurs
relations. Ainsi, l'utilisateur pourra alors utiliser de puissantes
fonctions pour effectuer des opérations complexes sur les données via une
interface simplifiée au maximum.

Les grandes lignes techniques dégagées, j'ai alors commencé à analyser quelles
étaient les différentes solutions existantes pour intégrer le langage de
programmation \emph{Python} au sein d'une application web.
