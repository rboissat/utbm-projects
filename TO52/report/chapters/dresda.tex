\section{Configuration réseau du routeur}

Ce chapitre expose le détail de la configuration présente sur \texttt{Dresda}, le
routeur principal assurant le fonctionnement d'IPv6. Nous allons dans un premier
temps présenter comment monter l'interconnexion avec le \emph{tunnel broker},
puis comment interconnecter les autres routeurs avec le routeur principal. Puis
nous détaillerons les directives nécessaires au routage sécurisé (filtré) d'IPv6
dans une infrastructure. Enfin la configuration des services principaux de
\texttt{Dresda} pour les rendre accessibles en IPv6 sera détailée.

\subsection{Interconnexion avec \emph{Hurricane Electric}}

Voici la configuration nécessaire à l'établissement du tunnel \emph{6in4} avec
le point de présence (PoP) d'\emph{Hurricane Electric} avec le système
d'exploitation Debian GNU/Linux (fichier \texttt{/etc/network/interfaces}) :

{\small
\begin{lstlisting}
# HOST: Dresda
# FILE: /etc/network/interfaces
# HE 6in4 tunnel
auto hev6                               # L'interface sera up au boot
iface hev6 inet6 v4tunnel               # L'interface est un tunnel 6 in 4
  address 2001:470:1f12:40d::2          # @ IPv6 d'interconnexion avec HE
  netmask 64                            # /64
  local 91.121.93.194                   # @ IPv4 de l'extremite locale du 6in4
  endpoint 216.66.84.42                 # @ IPv4 de l'extremite distante du 6in4
  gateway 2001:470:1f12:40d::1          # Passerelle IPv6
  ttl 64                                # Hop-limit de 64
  up ip link set mtu 1280 dev hev6      # On passe la MTU a 1280 octets
\end{lstlisting}
}

Une fois l'interface réseau \texttt{hev6} opérationnelle avec la commande
\texttt{ifup hev6}, nous sommes en mesure d'effectuer un ping6 vers
\url{IPv6.google.com}. Voici alors la route IPv6 par défaut telle que donnée
par la commande \texttt{ip -6 route} :

{\small
\begin{lstlisting}
default via 2001:470:1f12:40d::1 dev hev6  metric 1024  mtu 1280 advmss 1220 hoplimit 0
\end{lstlisting}}

et le \texttt{ping6} :

{\small
\begin{lstlisting}
$ ping6 -c 1 IPv6.google.com
PING IPv6.google.com(2a00:1450:8006::68) 56 data bytes
64 bytes from 2a00:1450:8006::68: icmp_seq=1 ttl=56 time=94.4 ms

--- IPv6.google.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 94.492/94.492/94.492/0.000 ms
\end{lstlisting}}

On peut alors configurer les interfaces restantes. L'extrait suivant ne contient
que les directives pertinentes pour IPv6 :

{\small
\begin{lstlisting}
# HOST: Dresda
# FILE: /etc/network/interfaces
auto iface eth0
iface eth0 inet6 static
  address 2001:470:c8be::
  netmask 64

auto iface brVPN
iface brVPN inet6 static
  address 2001:470:c8be:1::
    netmask 64
\end{lstlisting}}

Passons désormais au détail de l'interconnexion avec l'infrastructure
\emph{LVO}.

\subsection{Interconnexion avec LVO}

C'est de loin l'interconnexion la plus complexe des trois présentes dans
l'infrastructure globale, mais également la plus souple et sécurisée. En effet,
alors que les liaisons \emph{PoP} $\leftrightarrow$ \emph{Dresda} et
\emph{Dresda} $\leftrightarrow$ \emph{HS} sont moins complexes, elles ne sont
pas du tout sécurisées (pas de chiffrement du tunnel) ni souples (tous les
paramètres sont en dur dans des fichiers) et souffrent d'une montée à l'échelle
complexe.

La solution retenue pour la liaison \emph{Dresda} $\leftrightarrow$ \emph{LVO}
apporte une réponse solide à ces défauts :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item{La liaison se déroule via un tunnel construit par deux interfaces
  \texttt{TUN}, une sur chaque machine. Ce tunnel route de l'IP (v4 ou v6) dans
  de l'IP (v4 ici).}
  \item{La connexion est chiffrée : le trafic IP transporté est illisible par un
  attaquant, chiffré en Blowfish-CBC 128 bits. L'identification (au sens de
  l'anglais \emph{authentication}) s'effectue grâce à une chaîne de certificats
  SSL/TLS disposant de clés RSA de 2048 bits.}
  \item{Les paramètres du tunnel et du lien d'encapsulation IP sont définis dans
  des fichiers, mais la configuration du routage est dynamique grâce au
  protocole BGP4 en mode iBGP.}
\end{itemize}

Voici la définition de l'interface \texttt{TUN} et le fichier de configuration
OpenVPN du côté de \texttt{Dresda} (/etc/openvpn/lvo-dsa.conf):

{\small
\begin{lstlisting}
# HOST: Dresda
# FILE: /etc/network/interfaces
# DR<-LVO
auto tunLVO
iface tunLVO inet static
  address 192.168.144.64                # IPv4 d'interco + BGP
  netmask 255.255.255.255               # Masque /32
  pointopoint 192.168.144.208           # Point to Point
  pre-up openvpn --mktun --dev tunLVO
  post-down openvpn --rmtun --dev tunLVO
iface tunLVO inet6 static
          # IPv6 pour BGP (OpenVPN ne supporte pas encore bien IPv6)
  address 2001:470:c8be:f::1
          # Masque /128
  netmask 128
          # pas de pointopoint en IPv6 --> route unicast (/128) manuelle
  up ip -6 route add 2001:470:c8be:1f::1/128 dev tunLVO proto static
\end{lstlisting}}

\clearpage

{\small
\begin{lstlisting}
# HOST: Dresda
# FILE: /etc/openvpn/lvo-dsa.conf
port 1196               # Port UDP d'ecoute
local 178.33.42.224     # IPv4 d'ecoute
proto udp               # Transport UDP
dev tunLVO              # Interface de bind
tun-IPv6                # Activation routage IPv6
fragment 1400           # Reduction de la MTU a cause du surcout
mssfix                  # d'encapsulation
# Parametres TLS/SSL
ca    /etc/openvpn/certs/vpin/lvo-dsa/ca-chain-routers.pem
cert  /etc/openvpn/certs/vpin/lvo-dsa/dsa.crt
key   /etc/openvpn/certs/vpin/lvo-dsa/dsa.key
dh    /etc/openvpn/certs/vpin/lvo-dsa/dh1024.pem
tls-server              # Activation de TLS mode serveur
keepalive 10 60         # Definit un keepalive ICMP Request Echo
comp-lzo                # Active la compression du payload IP
\end{lstlisting}}

Concernant \texttt{Latex}, le routeur de l'infrastructure \emph{LVO}, les
directives de configuration seront fournies dans la partie \ref{la:config} page
\pageref{la:config}.

\section{Routage et Filtrage}

Dans cette section seront exposé en détail la configuration du routage sur
\texttt{Dresda} via les routes statiques insérées par le kernel ou manuellement
par l'administrateur et les routes dynamiques insérées par le démon \emph{zebra}
avec les informations obtenues par le démon \emph{BGP}. Enfin nous présenterons
la sécurité sur un routeur IPv6 grâce à \emph{Netfilter} et \texttt{ip6tables}.

\subsection{Routage statique}
Sur \texttt{Dresda}, les routes statiques sont nécessaires pour deux fonctions :
atteindre l'Internet IPv6 via le réseau d'\emph{Hurricane Electric}, simuler
une liaison point à point avec \emph{LVO} pour le tunnel VPN car le noyau Linux
n'a pas la notion de point-à-point IPv6 et atteindre le sous-réseau IPv6 délégué
à Harold via le tunnel \emph{6in4} reliant \texttt{Dresda} et l'infrastructure
\emph{HS}. Voici ces routes obtenues avec la commande \texttt{ip -6 route} :

{\footnotesize
\begin{lstlisting}
# Routes vers le 6in4 de Hurricane Electric
2001:470:1f12:40d::1 dev hev6  metric 1024  mtu 1280 advmss 1220 hoplimit 0
2001:470:1f12:40d::/64 via :: dev hev6  proto kernel  metric 256  mtu 1280 advmss 1220 hoplimit 0

# Routes vers l'infra HS
2001:470:1f13:40d::1 via :: dev dsa6HS  proto kernel  metric 256  mtu 1280 advmss 1220 hoplimit 0
2001:470:1f13:40d::2 dev dsa6HS  proto static  metric 1024  mtu 1280 advmss 1220 hoplimit 0
2001:470:c8be:e::/64 dev dsa6HS  proto static  metric 1024  mtu 1280 advmss 1220 hoplimit 0

# Routes locales
2001:470:c8be::/64 dev eth0  proto kernel  metric 256  mtu 1500 advmss 1440 hoplimit 0
2001:470:c8be:1::/64 dev brVPN  proto kernel  metric 256  mtu 1500 advmss 1440 hoplimit 0

# Routes "Point-to-Point" vers l'infra LVO
2001:470:c8be:f::1 dev tunLVO  proto kernel  metric 256  mtu 1500 advmss 1440 hoplimit 0
2001:470:c8be:1f::1 dev tunLVO  metric 1024  mtu 1500 advmss 1440 hoplimit 0

# Les routes multicast et link-local sont omises pour la clarte de l'extrait

# Route par defaut
default via 2001:470:1f12:40d::1 dev hev6  metric 1024  mtu 1280 advmss 1220 hoplimit 0
\end{lstlisting}}

\subsection{Routage dynamique}
\label{ch:dresda:rd}

Le routage dynamique prend place entre le routeur \texttt{Dresda} (infrastruture
\emph{OVH}) et le routeur \texttt{Latex} (infrastructure \emph{LVO}). La
configuration d'un routeur BGP n'étant pas un thème au cœur de notre sujet
d'étude, nous allons simplement rappeler les adresses IP d'interconnexion
(\emph{peering}) et fournir la configuration BGP spécifique au \emph{peers}.
Ci-dessous les adresses IPv4 et IPv6 point-à-point d'interconnexion :

\begin{figure}[ht]
\begin{center}
\renewcommand{\arraystretch}{1.4}
\begin{tabular}{ |c|  c|  c|  c| }
\hline
\textbf{Local router} & \textbf{@IP} & \textbf{Remote router} & \textbf{@IP}\\
\hline
LVO & \texttt{192.168.144.208/32} & DSA & \texttt{192.168.144.64/32}\\
\hline
DSA & \texttt{192.168.144.64/32} & LVO & \texttt{192.168.144.208/32}\\
\hline
\hline
LVO & \texttt{2001:470:c8be:1f::1/128}  & DSA & \texttt{2001:470:c8be:f::1/128}\\
\hline
DSA & \texttt{2001:470:c8be:f::1/128} & LVO & \texttt{2001:470:c8be:1f::1/128}\\
\hline
\end{tabular}

\caption{Table d'adressage des \emph{peerings} BGP.}

\end{center}
\end{figure}

Voici la configuration du routeur BGP sur \texttt{Dresda}, seules les directives
indispensables au fonctionnement du démon \emph{bgpd} sont précisées. La
configuration du démon \emph{zébra} qui assure la gestion des routes au niveau
du noyau Linux n'est donc pas précisée ici : 

{\small
\begin{lstlisting}
# HOST: Dresda
# FILE: /etc/bind/named.conf
#
#!/bin/sh
! DEFINITION DU ROUTEUR BGP DE NUMERO D'AS 64544
router bgp 64544
 bgp router-id 192.168.144.64
 no bgp default ipv4-unicast
! ANNONCES DES PREFIXES IPV4
 network 91.121.93.194/32
 network 192.168.144.64/26
 network 192.168.248.53/32
 timers bgp 5 15
 neighbor internal peer-group
 neighbor internal description internal default configuration
 neighbor internal activate
 neighbor internal next-hop-self
 neighbor 192.168.144.208 remote-as 64544
 neighbor 192.168.144.208 peer-group internal
 neighbor 192.168.144.208 description LVO
 neighbor 2001:470:c8be:f::2 remote-as 64544
 neighbor 2001:470:c8be:f::2 description LVO6
!
 address-family IPv6
! ANNONCE DU PREFIXE IPV6
 network 2001:470:c8be::/60
 neighbor 2001:470:c8be:f::2 activate
 neighbor 2001:470:c8be:f::2 next-hop-self
 neighbor 2001:470:c8be:f::2 default-originate
 exit-address-family
!
\end{lstlisting}}

\clearpage

Le détail de cette configuration pourrait être ajouté dans une révision
ultérieure de ce document. Remarque importante, les routes annoncées par le
démon BGP présent sur \texttt{Dresda} apparaitront dans la table de routage sur
\texttt{Latex} après que les sessions BGP entres les deux routeurs soient
actives. Sur \texttt{Latex} nous pourrons alors voir les routes IPv6 suivantes
avec la commande \texttt{ip -6 route | grep zebra} :

{\footnotesize
\begin{lstlisting}
2001:470:c8be::/60 via 2001:470:c8be:f::1 dev tunDR  proto zebra (...) hoplimit 0
default via 2001:470:c8be:f::1 dev tunDR  proto zebra (...) hoplimit 0
\end{lstlisting}}

La commande \texttt{grep zebra} permet de filtrer le résultat de la commande
\texttt{ip -6 route} pour n'afficher que les routes dynamiques. On constate que
le routeur \emph{Latex} dispose d'une route IPv6 par défaut via \texttt{Dresda},
ainsi que le préfixe IPv6 \texttt{/60} annoncé par le démon BGP de
\texttt{Dresda}. On a donc ici la table de routage désirée selon le
schéma \emph{\ref{ch:conception:interco}} page \pageref{ch:conception:interco}.

De plus, nous utilisons ici \emph{iBGP} (\emph{interior BGP}) en configurant le
même numéro d'\emph{AS} sur les deux démons BGP. Cela permet alors d'utiliser
de manière complètement transparente un protocole externe comme BGP en tant que
protocole de routage dynamique interne.

\subsection{Filtrage}

Le filtrage est un aspect important en sécurité réseau et en sécurité des
systèmes d'information. Dans cette sous-section, nous allons vous présenter
quelles sont les règles \texttt{ip6tables} minimales pour assurer le
fonctionnement de l'infrastructure en toute sécurité :

{\small
\begin{lstlisting}
# HOST: Dresda
# FILE: /etc/init.d/firewall6
#
#!/bin/sh
IP6T=`which ip6tables`

## ENABLING IPv6 FORWARDING
echo 1 >| /proc/sys/net/IPv6/conf/all/forwarding

## INTERFACES
MAIN_IF6=hev6

## IPv6 ADDRESSES
MAIN_IP6="2001:470:c8be::"

## ALLOWED LAYER 4 PORTS
ALLOW_IN_MAIN_TCP6="22,53,80,443,63667"
ALLOW_IN_MAIN_UDP6="53"

## DEFAULT POLICIES
$IP6T -P INPUT DROP
$IP6T -P OUTPUT ACCEPT
$IP6T -P FORWARD DROP

## CUSTOM CHAINS
$IP6T -N new_in_main
$IP6T -N INPUT_VPIN
$IP6T -N FORWARD_VPIN

## CUSTOM CHAINS RULES
# accept new traffic and pass it to 'new_in_main' chain
$IP6T -A INPUT_VPIN -m state --state NEW -j new_in_main
# accept 179/tcp for BGP sessions
$IP6T -A INPUT_VPIN -p tcp --dport 179 -m state --state NEW -j ACCEPT
# forward traffic in NEW or INVALID state
$IP6T -A FORWARD_VPIN -m state --state NEW,INVALID -j ACCEPT

## GENERAL INPUT
# loopback if
$IP6T -A INPUT -i lo -j ACCEPT

# allow established and related traffic
$IP6T -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# icmpv6 is really important
$IP6T -A INPUT -p IPv6-icmp -j ACCEPT

# drop bad TCP states
$IP6T -A INPUT -p tcp --tcp-flags SYN,ACK SYN,ACK -m state --state NEW -j DROP
$IP6T -A INPUT -p tcp ! --syn -m state --state NEW -j DROP

# redirect all new traffic to 'new_in_main' chain
$IP6T -A INPUT -i $MAIN_IF6 -m state --state NEW -d $MAIN_IP6 -j new_in_main

# allow INPUT on L4 ports previously defined
$IP6T -A new_in_main -p tcp -m multiport --dports $ALLOW_IN_MAIN_TCP6 -j ACCEPT
$IP6T -A new_in_main -p udp -m multiport --dports $ALLOW_IN_MAIN_UDP6 -j ACCEPT

## GENERAL FORWARD
# allow all related,established traffic, icmpv6 and some bogus situation
$IP6T -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
$IP6T -A FORWARD -p IPv6-icmp -j ACCEPT
$IP6T -A FORWARD -i $MAIN_IF6 -o $MAIN_IF6 -j ACCEPT

## FORWARD from/to HS
$IP6T -A FORWARD -i dsa6+ -j FORWARD_VPIN
$IP6T -A FORWARD -o dsa6+ -j FORWARD_VPIN

## INPUT/FORWARD FOR LVO
$IP6T -A INPUT -i tun+ -j INPUT_VPIN
$IP6T -A FORWARD -i tun+ -j FORWARD_VPIN
$IP6T -A FORWARD -o tun+ -j FORWARD_VPIN
\end{lstlisting}}

Le détail de cette configuration pourrait être ajouté dans une révision
ultérieure de ce document.

\clearpage
\section{Services}

Alors que la configuration réseau du système d'exploitation semble plutôt
volumineuse, la grande majorité des services supportent nativement IPv6 sans
apporter de modification paticulière à leur configuration. Très souvent
d'ailleurs ces services sont déjà servis sur un double adressage IP, puis que
même sans IPv6 globale, il reste la bloucle locale \texttt{::1} et l'adresse de
lien local en \texttt{fe80::}. Seuls quelques services nécessitent de la part de
l'administrateur d'explicitement déclarer dans leur configuration les adresses
IPv6 où ces services se lieront.

\subsection{Bind9}

C'est le cas de \emph{Bind9}, serveur de noms DNS, qui nécessite la
configuration suivante pour écouter et servir en IPv4 et en IPv6 :
{\small
\begin{lstlisting}
# HOST: Dresda
# FILE: /etc/bind/named.conf
#
options {
....
  listen-on     { 127.0.0.1; 91.121.93.194; };  /* ou { any; }; */
  listen-on-v6  { ::1; 2001:470:c8be::; };      /* ou { any; }; */
....
};
\end{lstlisting}}

\subsection{Apache, sshd, ntpd}

Aucun de ces services n'a nécessité de configuration supplémentaire pour être
globalement accessibles en IPv6. Cela montre la grande maturité des logiciels et
des services qui exploitent judicieusement les abstractions offertes par les
\emph{API} du système d'exploitation.
