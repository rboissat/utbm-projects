/**
 * FILE:    TaBuleXJTable.java
 * AUTHORS: Antoine Gavoille
 * ROLE:    Allow JTable initialization and alignment settings
 */
package tabulex_view;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;


public class TaBuLeXJTable {

	private JTable table;
	
	// JTable constructor
	public TaBuLeXJTable(String[][] dataContent, String[] columnNames) {
		table = new JTable(new DefaultTableModel(dataContent, columnNames));
	}
	
	// Update input parameters
	public void buildJTable(String[][] dataContent, String[] columnNames) {
		((DefaultTableModel)table.getModel()).setDataVector(dataContent, columnNames);
		//table = new JTable(new DefaultTableModel(dataContent, columnNames));
		//table.setModel(new DefaultTableModel(dataContent, columnNames));
	}
	
	// Center the content of one column
	public void centerColumnContent(int i) {     
		DefaultTableCellRenderer custom = new DefaultTableCellRenderer(); 
		custom.setHorizontalAlignment(JLabel.CENTER);
		table.getColumnModel().getColumn(i).setCellRenderer(custom);
	}
	
	// Right align the content of one column
	public void alignRightColumnContent(int i) {     
		DefaultTableCellRenderer custom = new DefaultTableCellRenderer(); 
		custom.setHorizontalAlignment(JLabel.RIGHT);  
		table.getColumnModel().getColumn(i).setCellRenderer(custom);
	}
	
	// Left align the content of one column
	public void alignLeftColumnContent(int i) {     
		DefaultTableCellRenderer custom = new DefaultTableCellRenderer(); 
		custom.setHorizontalAlignment(JLabel.LEFT);  
		table.getColumnModel().getColumn(i).setCellRenderer(custom);
	}
	
	public JTable getJTable() {
		return table;
	}
}
