/**
 * FILE:    LaTeXGenerator.java
 * AUTHORS: Romain Boissat
 * ROLE:    Generate LaTeX code from the data hold in the Model
 */

package tabulex_view;

import tabulex_model.ModelCell;

public class LaTeXGenerator {
	// Contains the final LaTeX code
	private String latexCode = new String();
	
	private String tabularIntro = "% Require \\usepackage{tabularx}\n\n";
	private String tabularDeclaration = "\\begin{tabular}";
	private String tabularOptions;
	
	private String manualColumnRight = new String(); 
	private String manualColumnCenter = new String();
	
	private String tabularContent;

	private String hline = "\\hline\n";
	private String tabularEnd = "\\end{tabular}";
	
	
	public LaTeXGenerator(ModelCell[][] model) {
		
		this.manualColumnCenter = (atLeastOneManualWidth(model))
									? 	"% Macro for manual width and centered alignment\n" +
										"% Define only once in the tex file\n" +
										"\\newcolumntype{C}{>{\\centering}p}\n\n" : "";
		
		this.manualColumnRight = (atLeastOneManualWidth(model)) 
									? 	"% Macro for manual width and right alignment\n" +
										"% Define only once in the tex file\n" +
										"\\newcolumntype{R}{>{\\hfill}p}\n\n" : "";
		
		this.tabularOptions = generateTabularOptions(model);
		this.tabularContent = generateTabularContent(model);
		
		this.latexCode += 	this.tabularIntro +
							this.manualColumnRight +
							this.manualColumnCenter +
							this.tabularDeclaration +
							this.tabularOptions +
							this.tabularContent +
							this.tabularEnd;
	}

	// Return true if at least one width is setup by the user
	public boolean atLeastOneManualWidth(ModelCell[][] data) {
		boolean test = false;
		
		for (int i =0; i < data[0].length; i++)
			if (data[0][i].getWidth() != "Automatic")
				test = true;
		
		return test;	
	}
	
	// Generates the tabular content
	public String generateTabularContent(ModelCell[][] data){
		String content = new String();
		String sep = new String();
		String tmp = new String();
		
		for (int i = 0; i < data.length; i++) {
			// Check fo upper Border
			content += (data[i][0].getBorders()[0] == true) ? hline : "" ;

			for (int j = 0; j < data[0].length; j++) {
				// Dynamic separator
				sep  = (j==data[0].length - 1) ? "\\\\\n" : " & ";
				
				// Content
				if (data[i][j].getBold() == true && data[i][j].getItalic() == true) {
					
					content += "\\textbf{\\emph{";
					
					tmp = data[i][j].getContent();
					tmp = toLaTeXSpecialChars(tmp);
					content += tmp;
					content += "}}"+ sep;
					
				} else if (data[i][j].getBold() == true && data[i][j].getItalic() == false) {
					
					content += "\\textbf{";
					tmp = data[i][j].getContent();
					tmp = toLaTeXSpecialChars(tmp);
					content += tmp;
					content += "}"+ sep;
				
				} else if (data[i][j].getBold() == false && data[i][j].getItalic() == true) {
					
					content += "\\emph{";
					tmp = toLaTeXSpecialChars(tmp);
					content += tmp;
					content += "}"+ sep;
				
				} else {
					tmp = data[i][j].getContent();
					tmp = toLaTeXSpecialChars(tmp);
					content += tmp + sep;
				
				}	
			}
			// Check fo lower Border
			content += (data[i][0].getBorders()[2] == true) ? hline : "";
		}
		
		return content;
	}
	
	// Generate the column, alignment and vertical borders settings
	public String generateTabularOptions(ModelCell[][] data) {
		String optString = "{";
		
		for (int i = 0; i < data[0].length; i++) {
			// Check for a left border
			optString += (data[0][i].getBorders()[3] == true) ? " |" : " ";
			
			// Check alignment
			switch (data[0][i].getAlignment()) {
				case 0 : // Left Alignment
					optString += (hasManualWidth(data[0][i])) ? "p" : "l"; break;
				
				case 1: // Centered
					optString += (hasManualWidth(data[0][i])) ? "C" : "c"; break;
				
				case 2: // Right Alignment
					optString += (hasManualWidth(data[0][i])) ? "R" : "r"; break;
			}
			
			// Check for the width
			optString += (hasManualWidth(data[0][i])) ? "{"+data[0][i].getWidth()+"px}" : "" ;
			
			// Check for the right border
			optString += (data[0][i].getBorders()[1] == true) ? "| " : " ";
		}
		optString += "}\n";
		
		return optString;
	}

	public String getLatexCode() {
		return latexCode;
	}
	
	public boolean hasManualWidth(ModelCell c) {
		return (c.getWidth() == "Automatic") ? false : true;
	}
	
	public void setLatexCode(String latexCode) {
		this.latexCode = latexCode;
	}
	
	// LaTeX special characters Handler
	public String toLaTeXSpecialChars(String tmp) {
		tmp = tmp.replaceAll("\\\\", "\\\\verb|\\\\|");
		tmp = tmp.replaceAll("~", "\\\\verb|~|");
		tmp = tmp.replaceAll("\\^", "\\\\verb|^|");
		tmp = tmp.replaceAll("\\$", "\\\\\\$");
		tmp = tmp.replaceAll("_", "\\\\_");
		tmp = tmp.replaceAll("&", "\\\\&");
		tmp = tmp.replaceAll("%", "\\\\%");
		tmp = tmp.replaceAll("#", "\\\\#");
		tmp = tmp.replaceAll("\\{", "\\\\\\{");
		tmp = tmp.replaceAll("\\}", "\\\\\\}");

		return tmp;
	}
}
