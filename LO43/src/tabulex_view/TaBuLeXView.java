/**
 * FILE:    TaBuLeXView.java
 * AUTHORS: Romain Boissat - Antoin Gavoille
 * ROLE:    Display the main window
 */

package tabulex_view;

import java.awt.Checkbox;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToggleButton;
import javax.swing.Timer;
import javax.swing.JToolBar.Separator;
import javax.swing.GroupLayout;
import javax.swing.DefaultComboBoxModel;

import org.jdesktop.application.Action;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.TaskMonitor;

import tabulex_main.TaBuLeXApp;
import tabulex_model.ModelCell;
import tabulex_model.TableData;

/**
 * The application's main frame.
 */
public class TaBuLeXView extends FrameView {

    // Variables declaration
    private JToggleButton buttonAlignCenter;
    private JToggleButton buttonAlignLeft;
	private JToggleButton buttonAlignRight;
	private JToggleButton buttonStyleBold;
	private JToggleButton buttonStyleItalic;
	private Checkbox checkboxUp;
	private Checkbox checkboxRight;
    private Checkbox checkboxLeft;
	private Checkbox checkboxDown;
    private JComboBox comboboxNbCols;
	private JComboBox comboboxNbLines;
    private javax.swing.JPanel infoPanel;
    private JLabel labelBorders;
    private JLabel labelCalculatedCoord;
    private JLabel labelCoord;
    private JLabel labelNbCols;
    private JLabel labelNbLines;
    private JLabel labelCalculatedColWidth;
    private JLabel labelColWidth;
    private javax.swing.JPanel mainPanel;
    private TaBuLeXJTable mainTable;
    private javax.swing.JScrollPane mainTableContainer;
    private JMenuBar menuBar;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JToolBar.Separator separatorAlignBar;
    private javax.swing.JToolBar.Separator separatorColLine;
    private javax.swing.JToolBar.Separator separatorGenerateButton;
    private Separator separatorStyleBar;
	private JLabel statusAnimationLabel;
    private JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JToolBar toolbar;
    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private JDialog aboutBox;
    private TaBuLeXGeneratedLaTeX generatedLaTeX;
	private JMenu fileMenu;
	private JMenuItem exitMenuItem;
    private JMenu helpMenu;
    private JMenuItem aboutMenuItem;
    private JMenu latexMenu;
    private JMenu tableMenu;
    private JMenuItem latexMenuItem;
    private JMenuItem resetColsWidth;

	private javax.swing.JSeparator statusPanelSeparator;
    private ModelCell[][] data;
    
    
	public TaBuLeXView(SingleFrameApplication app, TableData model){
        super(app);
        // This data is used by the TaBuLeXGeneratedLaTeX window
        this.data = model.getModelStruct().data;
        getFrame().setMinimumSize(new java.awt.Dimension(800,600));
        
        initComponents(model);
        initLabels();
        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String)(evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer)(evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
    }
    public JToggleButton getButtonAlignCenter() {
		return buttonAlignCenter;
	}
    public JToggleButton getButtonAlignLeft() {
		return buttonAlignLeft;
	}
    public JToggleButton getButtonAlignRight() {
		return buttonAlignRight;
	}

    public JToggleButton getButtonStyleBold() {
		return buttonStyleBold;
	}
    
    public JToggleButton getButtonStyleItalic() {
		return buttonStyleItalic;
	}
	public Checkbox getCheckboxDown() {
		return checkboxDown;
	}
	public Checkbox getCheckboxLeft() {
		return checkboxLeft;
	}
	
	public Checkbox getCheckboxRight() {
		return checkboxRight;
	}
	public Checkbox getCheckboxUp() {
		return checkboxUp;
	}
	
	public JComboBox getComboboxNbCols() {
		return comboboxNbCols;
	}
	public JComboBox getComboboxNbLines() {
		return comboboxNbLines;
	}
	
    public ModelCell[][] getData() {
		return data;
	}
	public TaBuLeXGeneratedLaTeX getGeneratedLaTeX() {
		return generatedLaTeX;
	}
	public JLabel getLabelCalculatedColWidth() {
		return labelCalculatedColWidth;
	}
	public JLabel getLabelCalculatedCoord() {
		return labelCalculatedCoord;
	}
	public JMenuItem getLatexMenuItem() {
		return latexMenuItem;
	}
	public TaBuLeXJTable getMainTable() {
		return mainTable;
	}
	
    public JMenuItem getResetColsWidth() {
		return resetColsWidth;
	}
	
	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     * @param initTable 
     * @param initTableHeader 
     */
    private void initComponents(TableData model) {

        mainPanel = new javax.swing.JPanel();
        toolbar = new javax.swing.JToolBar();
        labelNbCols = new JLabel();
        comboboxNbCols = new JComboBox();
        separatorColLine = new javax.swing.JToolBar.Separator();
        labelNbLines = new JLabel();
        comboboxNbLines = new JComboBox();
        separatorStyleBar = new javax.swing.JToolBar.Separator();
        buttonStyleBold = new JToggleButton();
        buttonStyleItalic = new JToggleButton();
        separatorAlignBar = new javax.swing.JToolBar.Separator();
        buttonAlignLeft = new JToggleButton();
        buttonAlignCenter = new JToggleButton();
        buttonAlignRight = new JToggleButton();
        separatorGenerateButton = new javax.swing.JToolBar.Separator();
       // buttonGenerator = new javax.swing.JButton();
        infoPanel = new javax.swing.JPanel();
        labelCoord = new JLabel();
        labelBorders = new JLabel();
        labelCalculatedCoord = new JLabel();
        labelColWidth = new JLabel();
        labelCalculatedColWidth = new JLabel();
        checkboxUp = new Checkbox();
        checkboxRight = new Checkbox();
        checkboxLeft = new Checkbox();
        checkboxDown = new Checkbox();
        mainTableContainer = new javax.swing.JScrollPane();
        mainTable = new TaBuLeXJTable(model.getDataContent(), model.getModelStruct().columnNames);
        
        menuBar = new JMenuBar();
        fileMenu = new JMenu();
        exitMenuItem = new JMenuItem();
        helpMenu = new JMenu();
        aboutMenuItem = new JMenuItem();
        latexMenu = new JMenu();
        tableMenu = new JMenu();
        latexMenuItem = new JMenuItem();
        resetColsWidth = new JMenuItem();
        
        statusPanel = new javax.swing.JPanel();
        statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new JLabel();
        statusAnimationLabel = new JLabel();
        progressBar = new javax.swing.JProgressBar();

        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setLayout(new java.awt.BorderLayout());

        toolbar.setFloatable(false);
        toolbar.setRollover(true);
        toolbar.setName("toolbar"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(tabulex_main.TaBuLeXApp.class).getContext().getResourceMap(TaBuLeXView.class);
        labelNbCols.setText(resourceMap.getString("labelNbCols.text")); // NOI18N
        labelNbCols.setName("labelNbCols"); // NOI18N
        toolbar.add(labelNbCols);

        comboboxNbCols.setEditable(true);
        comboboxNbCols.setMaximumRowCount(10);
        comboboxNbCols.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        comboboxNbCols.setName("comboboxNbCols"); // NOI18N
        comboboxNbCols.setPreferredSize(new java.awt.Dimension(60, 25));
        toolbar.add(comboboxNbCols);

        separatorColLine.setName("separatorColLine"); // NOI18N
        toolbar.add(separatorColLine);

        labelNbLines.setText(resourceMap.getString("labelNbLines.text")); // NOI18N
        labelNbLines.setName("labelNbLines"); // NOI18N
        toolbar.add(labelNbLines);

        comboboxNbLines.setEditable(true);
        comboboxNbLines.setMaximumRowCount(10);
        comboboxNbLines.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        comboboxNbLines.setName("comboboxNbLines"); // NOI18N
        
        comboboxNbLines.setPreferredSize(new java.awt.Dimension(60, 25));
        toolbar.add(comboboxNbLines);

        separatorStyleBar.setName("separatorStyleBar"); // NOI18N
        toolbar.add(separatorStyleBar);

        buttonStyleBold.setIcon(resourceMap.getIcon("buttonStyleBold.icon")); // NOI18N
        buttonStyleBold.setText(resourceMap.getString("buttonStyleBold.text")); // NOI18N
        buttonStyleBold.setFocusable(false);
        buttonStyleBold.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonStyleBold.setName("buttonStyleBold"); // NOI18N
        buttonStyleBold.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolbar.add(buttonStyleBold);

        buttonStyleItalic.setIcon(resourceMap.getIcon("buttonStyleItalic.icon")); // NOI18N
        buttonStyleItalic.setText(resourceMap.getString("buttonStyleItalic.text")); // NOI18N
        buttonStyleItalic.setFocusable(false);
        buttonStyleItalic.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonStyleItalic.setName("buttonStyleItalic"); // NOI18N
        buttonStyleItalic.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolbar.add(buttonStyleItalic);

        separatorAlignBar.setName("separatorAlignBar"); // NOI18N
        toolbar.add(separatorAlignBar);

        buttonAlignLeft.setIcon(resourceMap.getIcon("buttonAlignLeft.icon")); // NOI18N
        buttonAlignLeft.setText(resourceMap.getString("buttonAlignLeft.text")); // NOI18N
        buttonAlignLeft.setFocusable(false);
        buttonAlignLeft.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonAlignLeft.setName("buttonAlignLeft"); // NOI18N
        buttonAlignLeft.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolbar.add(buttonAlignLeft);

        buttonAlignCenter.setIcon(resourceMap.getIcon("buttonAlignCenter.icon")); // NOI18N
        buttonAlignCenter.setText(resourceMap.getString("buttonAlignCenter.text")); // NOI18N
        buttonAlignCenter.setFocusable(false);
        buttonAlignCenter.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonAlignCenter.setName("buttonAlignCenter"); // NOI18N
        buttonAlignCenter.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolbar.add(buttonAlignCenter);

        buttonAlignRight.setIcon(resourceMap.getIcon("buttonAlignRight.icon")); // NOI18N
        buttonAlignRight.setText(resourceMap.getString("buttonAlignRight.text")); // NOI18N
        buttonAlignRight.setFocusable(false);
        buttonAlignRight.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonAlignRight.setName("buttonAlignRight"); // NOI18N
        buttonAlignRight.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolbar.add(buttonAlignRight);

        separatorGenerateButton.setName("separatorGenerateButton"); // NOI18N
        toolbar.add(separatorGenerateButton);

        mainPanel.add(toolbar, java.awt.BorderLayout.NORTH);

        infoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("infoPanel.border.title"))); // NOI18N
        infoPanel.setName("infoPanel"); // NOI18N
        infoPanel.setPreferredSize(new java.awt.Dimension(250, 334));

        labelCoord.setText(resourceMap.getString("labelCoord.text")); // NOI18N
        labelCoord.setName("labelCoord"); // NOI18N

        labelBorders.setText(resourceMap.getString("labelBorders.text")); // NOI18N
        labelBorders.setName("labelBorders"); // NOI18N
        
        labelColWidth.setText(resourceMap.getString("labelColWidth.text")); // NOI18N
        labelColWidth.setName("labelColWidth"); // NOI18N

        labelCalculatedColWidth.setText(resourceMap.getString("labelCalculatedColWidth.text")); // NOI18N
        labelCalculatedColWidth.setName("labelCalculatedColWidth"); // NOI18N
        
        
        labelCalculatedCoord.setText(resourceMap.getString("labelCalculatedCoord.text")); // NOI18N
        labelCalculatedCoord.setName("labelCalculatedCoord"); // NOI18N

        checkboxUp.setLabel(resourceMap.getString("checkboxUp.label")); // NOI18N
        checkboxUp.setName("checkboxUp"); // NOI18N

        checkboxRight.setLabel(resourceMap.getString("checkboxRight.label")); // NOI18N
        checkboxRight.setName("checkboxRight"); // NOI18N

        checkboxLeft.setLabel(resourceMap.getString("checkboxLeft.label")); // NOI18N
        checkboxLeft.setName("checkboxLeft"); // NOI18N

        checkboxDown.setLabel(resourceMap.getString("checkboxDown.label")); // NOI18N
        checkboxDown.setName("checkboxDown"); // NOI18N

        GroupLayout infoPanelLayout = new GroupLayout(infoPanel);
        infoPanel.setLayout(infoPanelLayout);
        infoPanelLayout.setHorizontalGroup(
                infoPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(infoPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(infoPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(infoPanelLayout.createSequentialGroup()
                            .addComponent(labelCoord)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(labelCalculatedCoord))
                        .addGroup(infoPanelLayout.createSequentialGroup()
                            .addComponent(labelBorders)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(checkboxLeft, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGroup(infoPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(checkboxUp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(checkboxDown, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGroup(infoPanelLayout.createSequentialGroup()
                                    .addGap(43, 43, 43)
                                    .addComponent(checkboxRight, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                        .addGroup(infoPanelLayout.createSequentialGroup()
                            .addComponent(labelColWidth)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(labelCalculatedColWidth)))
                    .addContainerGap(27, Short.MAX_VALUE))
            );
            infoPanelLayout.setVerticalGroup(
                infoPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(infoPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(infoPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(labelCoord)
                        .addComponent(labelCalculatedCoord))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(infoPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(labelColWidth)
                        .addComponent(labelCalculatedColWidth))
                    .addGap(63, 63, 63)
                    .addGroup(infoPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(labelBorders)
                        .addGroup(infoPanelLayout.createSequentialGroup()
                            .addComponent(checkboxUp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(infoPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addGroup(infoPanelLayout.createSequentialGroup()
                                    .addComponent(checkboxRight, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addGap(5, 5, 5)
                                    .addComponent(checkboxDown, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addComponent(checkboxLeft, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addContainerGap(266, Short.MAX_VALUE))
            );

        mainPanel.add(infoPanel, java.awt.BorderLayout.LINE_END);

        mainTableContainer.setName("mainTableContainer"); // NOI18N
        mainTable.getJTable().setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        mainTable.getJTable().setColumnSelectionAllowed(true);
        // Prevent Column drag'n'drop on the main JTable
        mainTable.getJTable().getTableHeader().setReorderingAllowed(false);
        mainTable.getJTable().setName("mainTable"); // NOI18N

        
        mainTableContainer.setViewportView(mainTable.getJTable());

        mainPanel.add(mainTableContainer, java.awt.BorderLayout.CENTER);

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(tabulex_main.TaBuLeXApp.class).getContext().getActionMap(TaBuLeXView.class, this);
        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        
        
        menuBar.add(fileMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        // Generate Latex Menu
        this.data = model.getModelStruct().data;
        latexMenuItem.setAction(actionMap.get("showGeneratedLaTeX"));
        latexMenuItem.setText(resourceMap.getString("latexMenuItem.text")); // NOI18N
        latexMenuItem.setName("latexMenuItem"); // NOI18N
        latexMenu.add(latexMenuItem);
        latexMenu.setText(resourceMap.getString("latexMenu.text"));
        latexMenu.setName("latexMenu");
        
        resetColsWidth.setText(resourceMap.getString("resetColsWidth.text"));
        resetColsWidth.setName("resetColsWidth");
        tableMenu.add(resetColsWidth);
        tableMenu.setText(resourceMap.getString("tableMenu.text"));
        tableMenu.setName("tableMenu");
        
        menuBar.add(tableMenu);
        menuBar.add(latexMenu);
        
        statusPanel.setName("statusPanel"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        GroupLayout statusPanelLayout = new GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, GroupLayout.DEFAULT_SIZE, 822, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 638, Short.MAX_VALUE)
                .addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, GroupLayout.PREFERRED_SIZE, 2, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
        setItemsDisabled();
    }// </editor-fold>//GEN-END:initComponents
	private void initLabels() {
		// Initialise les labels de l'interface
    	comboboxNbLines.setSelectedIndex(mainTable.getJTable().getRowCount()-1);
    	comboboxNbCols.setSelectedIndex(mainTable.getJTable().getColumnCount()-1);
	}
	public void setButtonAlignCenter(JToggleButton buttonAlignCenter) {
		this.buttonAlignCenter = buttonAlignCenter;
	}
	public void setButtonAlignLeft(JToggleButton buttonAlignLeft) {
		this.buttonAlignLeft = buttonAlignLeft;
	}
	public void setButtonAlignRight(JToggleButton buttonAlignRight) {
		this.buttonAlignRight = buttonAlignRight;
	}
	public void setButtonStyleBold(JToggleButton buttonStyleBold) {
		this.buttonStyleBold = buttonStyleBold;
	}
	public void setButtonStyleItalic(JToggleButton buttonStyleItalic) {
		this.buttonStyleItalic = buttonStyleItalic;
	}
	public void setCheckboxDown(Checkbox checkboxDown) {
		this.checkboxDown = checkboxDown;
	}
	public void setCheckboxLeft(Checkbox checkboxLeft) {
		this.checkboxLeft = checkboxLeft;
	}
    public void setCheckboxRight(Checkbox checkboxRight) {
		this.checkboxRight = checkboxRight;
	}
    public void setCheckboxUp(Checkbox checkboxUp) {
		this.checkboxUp = checkboxUp;
	}
    public void setComboboxNbCols(JComboBox comboboxNbCols) {
		this.comboboxNbCols = comboboxNbCols;
	}
    public void setComboboxNbLines(JComboBox comboboxNbLines) {
		this.comboboxNbLines = comboboxNbLines;
	}
	public void setData(ModelCell[][] data) {
		this.data = data;
	}
	public void setGeneratedLaTeX(TaBuLeXGeneratedLaTeX generatedLaTeX) {
		this.generatedLaTeX = generatedLaTeX;
	}
	public void setItemsDisabled() {
		getButtonAlignCenter().setEnabled(false);
		getButtonAlignLeft().setEnabled(false);
		getButtonAlignRight().setEnabled(false);
		getButtonStyleBold().setEnabled(false);
		getButtonStyleItalic().setEnabled(false);
		getCheckboxDown().setEnabled(false);
		getCheckboxLeft().setEnabled(false);
		getCheckboxRight().setEnabled(false);
		getCheckboxUp().setEnabled(false);
	}
	public void setLabelCalculatedColWidth(
			JLabel labelCalculatedColWidth) {
		this.labelCalculatedColWidth = labelCalculatedColWidth;
	}
	public void setLabelCalculatedCoord(JLabel labelCalculatedCoord) {
		this.labelCalculatedCoord = labelCalculatedCoord;
	}
	public void setLatexMenuItem(JMenuItem latexMenuItem) {
		this.latexMenuItem = latexMenuItem;
	}
	public void setMainTable(TaBuLeXJTable mainTable) {
		this.mainTable = mainTable;
	}
	
    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = TaBuLeXApp.getApplication().getMainFrame();
            aboutBox = new TaBuLeXAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        TaBuLeXApp.getApplication().show(aboutBox);
    }
    
    @Action
    public void showGeneratedLaTeX() {
        JFrame mainFrame = TaBuLeXApp.getApplication().getMainFrame();
        generatedLaTeX = new TaBuLeXGeneratedLaTeX(mainFrame, data);
        generatedLaTeX.setLocationRelativeTo(mainFrame);
        
        TaBuLeXApp.getApplication().show(generatedLaTeX);
    }

}