/**
 * FILE:    TableData.java
 * AUTHORS: Antoine Gavoille
 * ROLE:    Allow use of defaultTableModel for linking the Model and the JTable
 */

package tabulex_model;

public class TableData {
	// Hold text data to display
	private String[][] dataContent;
	// Allow return of functions addColumn and deleteColumn
	private ModelStructTable TableStructure = new ModelStructTable(null, null); 
	
	// Constructor
	public TableData() {
		// Default table of 3 lines, 3 columns
		TableStructure.data = new ModelCell[][] {
                {new ModelCell(), new ModelCell(), new ModelCell()},
                {new ModelCell(), new ModelCell(), new ModelCell()},
                {new ModelCell(), new ModelCell(), new ModelCell()}
            };
		// Initialization of columns headers
		TableStructure.columnNames = new String[] {"Col 1","Col 2","Col 3"};
		// Update the data fron the String[][]
		dataContent = updateTable();
	}
	
	public ModelStructTable getModelStruct() {
		return TableStructure;
	}
	
	public void setModelStruct(ModelStructTable TableStructure) {
		this.TableStructure = TableStructure;
	}
	
	
	public String[][] getDataContent () {
		return dataContent;
	}
	
	public void setDataContent (String[][] dataContent) {
		this.dataContent = dataContent;
	}
	
	// Redefine data table from the cell table
	public String[][] updateTable() {
		String[][] dataContentTemp = new String[TableStructure.data.length][TableStructure.data[0].length];
		for (int i = 0; i< TableStructure.data.length; i++) {
			for (int j = 0; j<TableStructure.data[0].length; j++) {
				if (TableStructure.data[i][j] != null) {
					dataContentTemp[i][j] = TableStructure.data[i][j].getContent();
				}
			}
		}
		return dataContentTemp;
	}
	
	
	// Delete one column from the cell table
	public ModelStructTable deleteColumn(){
		// Create a New cell table and temporary 
		ModelCell[][] dataTemp = new ModelCell[TableStructure.data.length][TableStructure.data[0].length-1];
		String[] columnNamesTemp = new String[TableStructure.data[0].length-1];
		// Copy of current cell content
		for(int i = 0; i < dataTemp.length; i++){
			for(int j = 0; j < dataTemp[i].length; j++){
				dataTemp[i][j] = TableStructure.data[i][j];
			}
		}
		// Copy of current column titles
		for(int i = 0; i < dataTemp[0].length; i++) {
			columnNamesTemp[i] = TableStructure.columnNames[i];
		}
		// Update the ModelStructTable with new data
		ModelStructTable paramColonneTemp = new ModelStructTable(dataTemp, columnNamesTemp);
		return paramColonneTemp;
	}
	
	// Add a column
	public ModelStructTable addColumn(){
		// Temporary tables
		ModelCell[][] dataTemp = new ModelCell[TableStructure.data.length][TableStructure.data[0].length+1];
		String[] columnNamesTemp = new String[TableStructure.data[0].length+1];
		// Copy of current cell content
		for(int i = 0; i < dataTemp.length; i++){
			for(int j = 0; j < dataTemp[i].length; j++){
				// Copy former elements to the new table if the current position is inside the former table
				if(j < dataTemp[i].length-1) {
					dataTemp[i][j] = TableStructure.data[i][j];
				// Else, create new cells
				} else {
					dataTemp[i][j] = new ModelCell();
					if (j>0) {
						boolean[] tempBorder = {dataTemp[i][j-1].getBorders()[0],false,dataTemp[i][j-1].getBorders()[2],false};
						dataTemp[i][j].setBorders(tempBorder);
					}
				}
			}
		}
		// Copy of current column titles
		for(int i = 0; i < dataTemp[0].length-1; i++){
			columnNamesTemp[i] = TableStructure.columnNames[i];
		}
		// New title for created column
		columnNamesTemp[dataTemp[0].length-1] = "Col " + Integer.toString(TableStructure.data[0].length+1);
		ModelStructTable paramColonneTemp = new ModelStructTable(dataTemp, columnNamesTemp);
		return paramColonneTemp;
	}
	
	// Delete a row
	public ModelCell[][] deleteRow(){
		ModelCell[][] dataTemp = new ModelCell[TableStructure.data.length-1][TableStructure.data[0].length];
		for(int i = 0; i < dataTemp.length; i++){
			for(int j = 0; j < dataTemp[i].length; j++){
				dataTemp[i][j] = TableStructure.data[i][j];
			}
		}
		return dataTemp;
	}
	
	// Add a line
	public ModelCell[][] addRow(){
		ModelCell[][] dataTemp = new ModelCell[TableStructure.data.length+1][TableStructure.data[0].length];
		for(int i = 0; i < dataTemp.length-1; i++){
			for(int j = 0; j < dataTemp[i].length; j++){
				dataTemp[i][j] = TableStructure.data[i][j];
			}
		}
		for(int j = 0; j < dataTemp[0].length; j++){
			dataTemp[dataTemp.length-1][j] = new ModelCell();
			if (dataTemp.length>0) {
				boolean[] tempBorder = {false,dataTemp[dataTemp.length-2][j].getBorders()[1],false,dataTemp[dataTemp.length-2][j].getBorders()[3]};
				dataTemp[dataTemp.length-1][j].setBorders(tempBorder);
			}
		}
		return dataTemp;
	}

	// Return a specific cell from the ModelTable
	public ModelCell getCell(int row, int column) {
		return TableStructure.data[row][column];
	}
}
