/**
 * FILE:    ModelStructTable
 * AUTHORS: Antoine Gavoille
 * ROLE:    Compatibility layer between tableData[][] and ModelCell[][]
 */

package tabulex_model;

public class ModelStructTable {
	// Cell array
	public ModelCell[][] data = null;
	// Column titles array
	public String[] columnNames = null;
	
	public ModelStructTable(ModelCell[][] data, String[] columnNames) {
		this.data = data;
		this.columnNames = columnNames;
	}
}
