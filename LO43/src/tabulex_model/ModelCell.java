/**
 * FILE:    ModelCell.java
 * AUTHORS: Antoine Gavoille
 * ROLE:    Base entity of the Model. Hold text content, font style, aligment setting
 *          of a given cell.
 */
package tabulex_model;

public class ModelCell {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String content = null;
	private boolean bold = false;
	private boolean italic = false;
	private int alignment = 0; // 0 = left align, 1 = centered, 2 = right align
	private String width = "Automatic"; // default column width
	private boolean[] borders = {false,false,false,false}; // 0 = no border, 1 = simple, 2 = double, {top, right, bottom, left}

	public ModelCell() {
		content = "";
	}
	
	public ModelCell(String s) {
		content = s;
	}
	
	
	public void setContent(Object value) {
		this.content = (String) value;
	}

	public String getContent() {
		return content;
	}

	
	public void setBold(boolean bold) {
		this.bold = bold;
	}


	public boolean getBold() {
		return bold;
	}
	
	public void setItalic(boolean italic) {
		this.italic = italic;
	}


	public boolean getItalic() {
		return italic;
	}

	public void setBorders(boolean[] borders) {
		this.borders = borders;
	}


	public boolean[] getBorders() {
		return borders;
	}


	public void setWidth(String width) {
		this.width = width;
	}


	public String getWidth() {
		return width;
	}


	public void setAlignment(int alignement) {
		this.alignment = alignement;
	}


	public int getAlignment() {
		return alignment;
	}

	
}
