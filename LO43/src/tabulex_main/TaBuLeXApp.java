/**
 * FILE:    TaBuLeXApp.java
 * AUTHORS: Romain Boissat - Antoine Gavoille
 * ROLE:    Main class: Create several instances (Model, View and finally Controller)
 *          and launch the main Application
 * 
 */

package tabulex_main;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

import tabulex_controller.Controller;
import tabulex_model.TableData;
import tabulex_view.TaBuLeXView;


/**
 * The main class of the application.
 */
public class TaBuLeXApp extends SingleFrameApplication {

    /**
     * At startup create and show the main frame of the application.
     */
    @Override protected void startup() {
    
    	// Instance of the Model
    	TableData model = new TableData();
    	// Initialisation of the model content
    	model.setDataContent(model.updateTable());

    	// Instance of the View
    	TaBuLeXView view = new TaBuLeXView(this, model);

    	// Instance of the Controller
    	@SuppressWarnings("unused")
		Controller controller = new Controller(view, model);
    	
    	// Initialisation of Main application
        show(view);
        
    }
    
    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of TaBuLeXApp
     */
    public static TaBuLeXApp getApplication() {
        return Application.getInstance(TaBuLeXApp.class);
    }

    /**
     * This override allows to *not* save session state, like elements size and position.
     * In fact, restoring the state of the application by updating the model is hardcore.
     * So we decided to bypass the SingleFrameApplication default behavior
     */
    @Override protected void shutdown() {
    	shutdown();
    	this.getContext().getLocalStorage().getDirectory().delete();
    }
    
    /**
     * Main method launching the application.
     */
    public static void main(String[] args) {
        launch(TaBuLeXApp.class, args);
    }
}
