/**
 * FILE:    Controller.java
 * AUTHORS: Romain Boissat - Antoine Gavoille
 * ROLE:    Ensure dynamic updates between View and Model, and vice-versa.
 */

package tabulex_controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JTable;

import tabulex_model.TableData;
import tabulex_view.TaBuLeXJTable;
import tabulex_view.TaBuLeXView;

public class Controller implements MouseListener, MouseMotionListener {

	private TaBuLeXView view;
	private TableData model;
	
	public Controller(TaBuLeXView view, TableData model) {
		this.view = view;
		this.model = model;
		// Listeners
		addListeners(this);
	}
	
	// Declaration of all Listeners
	public void addListeners(Controller controller) {
		
        // mainTable: MouseListener for displaying real-time coordinates
        this.view.getMainTable().getJTable().addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(MouseEvent evt) {
            	// Global View update
				globalViewUpdate();
				updateModelFromView();
            }
        });
        
        // On ENTER Key, update of Model
        this.view.getMainTable().getJTable().addKeyListener(new java.awt.event.KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				
				if (key == KeyEvent.VK_DELETE) {
			    	 resetCellsContent(view.getMainTable().getJTable().getSelectedRows(),
									   view.getMainTable().getJTable().getSelectedColumns());
			    	
			    	 updateModelFromView();
			    	 e.consume();
			    }
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// Global View update
				globalViewUpdate();
				
                // If key = ENTER -> update the Model
				int key = e.getKeyCode();
				
			     if (key == KeyEvent.VK_ENTER)
			    	 updateModelFromView();
			}

			@Override
			public void keyTyped(KeyEvent e) {}
        	
        });
        
        // On header's drag by mouse, update all dynamic labels
        this.view.getMainTable().getJTable().getTableHeader().addMouseMotionListener(new java.awt.event.MouseAdapter() {
        	public void mouseDragged(MouseEvent evt) {
                updateColWidthLabel();
                updateColWidthData();
                updateViewData();
        	}
    	});
        
        // Toolbar: Listener of the left alignment button
        this.view.getButtonAlignLeft().addMouseListener(new java.awt.event.MouseAdapter() {
        	public void mouseReleased(MouseEvent evt) {
        		JTable table = view.getMainTable().getJTable();
        		int colIndex = table.getSelectedColumn();
        		int rowIndex = table.getSelectedRow();
        		if (colIndex != -1 && rowIndex != -1) {
	        		updateModelCellAlign(0);
	        		updateViewButtonsAlign(0);
	        		view.getMainTable().getJTable().repaint();
        		}
                updateViewData();
        	}
        });
        
        // Toolbar: Listener of the center alignment button
        this.view.getButtonAlignCenter().addMouseListener(new java.awt.event.MouseAdapter() {
        	public void mouseReleased(MouseEvent evt) {
        		JTable table = view.getMainTable().getJTable();
        		int colIndex = table.getSelectedColumn();
        		int rowIndex = table.getSelectedRow();
        		if (colIndex != -1 && rowIndex != -1) {
	        		updateModelCellAlign(1);
	        		updateViewButtonsAlign(1);
	        		view.getMainTable().getJTable().repaint();
        		}
                updateViewData();
        	}
        });
        
        // Toolbar: Listener of the right alignment button
        this.view.getButtonAlignRight().addMouseListener(new java.awt.event.MouseAdapter() {
        	public void mouseReleased(MouseEvent evt) {
        		JTable table = view.getMainTable().getJTable();
        		int colIndex = table.getSelectedColumn();
        		int rowIndex = table.getSelectedRow();
        		if (colIndex != -1 && rowIndex != -1) {
	        		updateModelCellAlign(2);
	        		updateViewButtonsAlign(2);
	        		view.getMainTable().getJTable().repaint();
        		}
                updateViewData();
        	}
        });
        
        // Toolbar: Listener of the bold font button
        this.view.getButtonStyleBold().addMouseListener(new java.awt.event.MouseAdapter() {
        	public void mouseReleased(MouseEvent evt) {
        		JTable table = view.getMainTable().getJTable();
        		int colIndex = table.getSelectedColumn();
        		int rowIndex = table.getSelectedRow();
        		if (colIndex != -1 && rowIndex != -1) {
        			updateModelCellBold();
        		}
                updateViewData();
        	}
        });
        
        // Toolbar: Listener of the italic font button
        this.view.getButtonStyleItalic().addMouseListener(new java.awt.event.MouseAdapter() {
        	public void mouseReleased(MouseEvent evt) {
        		JTable table = view.getMainTable().getJTable();
        		int colIndex = table.getSelectedColumn();
        		int rowIndex = table.getSelectedRow();
        		if (colIndex != -1 && rowIndex != -1) {
        			updateModelCellItalic();
        		}
                updateViewData();
        	}
        });
        
        // Toolbar: Listener of the top border checkbox
        this.view.getCheckboxUp().addMouseListener(new java.awt.event.MouseAdapter(){
        	public void mouseReleased(MouseEvent evt) {
        		JTable table = view.getMainTable().getJTable();
        		int colIndex = table.getSelectedColumn();
        		int rowIndex = table.getSelectedRow();
        		if (colIndex != -1 && rowIndex != -1) {
        			updateModelCellHorizontalBorders(0);
        		}
                updateViewData();
        	}
		});
        
        // Toolbar: Listener of the right border checkbox
        this.view.getCheckboxRight().addMouseListener(new java.awt.event.MouseAdapter(){
        	public void mouseReleased(MouseEvent evt) {
        		JTable table = view.getMainTable().getJTable();
        		int colIndex = table.getSelectedColumn();
        		int rowIndex = table.getSelectedRow();
        		if (colIndex != -1 && rowIndex != -1) {
        			updateModelCellVerticalBorders(1);
        		}
                updateViewData();
        	}
		});
        
        // Toolbar: Listener of the bottom border checkbox
        this.view.getCheckboxDown().addMouseListener(new java.awt.event.MouseAdapter(){
        	public void mouseReleased(MouseEvent evt) {
        		JTable table = view.getMainTable().getJTable();
        		int colIndex = table.getSelectedColumn();
        		int rowIndex = table.getSelectedRow();
        		if (colIndex != -1 && rowIndex != -1) {
        			updateModelCellHorizontalBorders(2);
        		}
                updateViewData();
        	}
		});
        
        // Toolbar: Listener of the left border checkbox
        this.view.getCheckboxLeft().addMouseListener(new java.awt.event.MouseAdapter(){
        	public void mouseReleased(MouseEvent evt) {
        		JTable table = view.getMainTable().getJTable();
        		int colIndex = table.getSelectedColumn();
        		int rowIndex = table.getSelectedRow();
        		if (colIndex != -1 && rowIndex != -1) {
        			updateModelCellVerticalBorders(3);
        		}
                updateViewData();
        	}
		});
        
        // Toolbar: Listener of the column combobox
        this.view.getComboboxNbCols().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				//System.out.println((String)view.getComboboxNbCols().getSelectedItem());
				String JComboboxText = view.getComboboxNbCols().getSelectedItem().toString();
				if (testField(JComboboxText)) {
					int combo = Integer.parseInt(JComboboxText);
					updateNbCols(combo);
					updateItemsEnabled();
				} else {
					view.getComboboxNbCols().setSelectedItem(model.getModelStruct().data[0].length);
				}
                updateViewData();
			}
        });
        
        // Toolbar: Listener of the line combobox
        this.view.getComboboxNbLines().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				String JComboboxText = view.getComboboxNbLines().getSelectedItem().toString();
				if (testField(JComboboxText)) {
					int combo = Integer.parseInt(JComboboxText);
					updateNblines(combo);
					updateItemsEnabled();
				} else {
					view.getComboboxNbLines().setSelectedItem(model.getModelStruct().data.length);
				}
                updateViewData();
			}
        });
    
        // LaTeX Menu : Reset Columns width
        this.view.getResetColsWidth().addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		resetColumnsWidth();
        		view.getMainTable().buildJTable(model.getDataContent(), model.getModelStruct().columnNames);
        		globalViewUpdate();
        	}
        });
        
	}
    
// *********************************************** LISTENER SPECIFIC ROUTINES *****************************************************************************
    
	private void updateViewData() {
		view.setData(model.getModelStruct().data);
	}
	
	private void updateModelFromView() {
		JTable table = view.getMainTable().getJTable();
		for (int i = 0; i < model.getModelStruct().data.length; i++) {
			for (int j = 0; j < model.getModelStruct().data[i].length; j++) {
				model.getModelStruct().data[i][j].setContent(table.getValueAt(i, j));
			}
		}
		model.setDataContent(model.updateTable());
	}
	
	private void setItemsEnabled() {
		view.getButtonAlignCenter().setEnabled(true);
		view.getButtonAlignLeft().setEnabled(true);
		view.getButtonAlignRight().setEnabled(true);
		view.getButtonStyleBold().setEnabled(true);
		view.getButtonStyleItalic().setEnabled(true);
		view.getCheckboxDown().setEnabled(true);
		view.getCheckboxLeft().setEnabled(true);
		view.getCheckboxRight().setEnabled(true);
		view.getCheckboxUp().setEnabled(true);
	}
	
	private void setItemsUnselected() {
		view.getButtonAlignCenter().setSelected(false);
		view.getButtonAlignLeft().setSelected(false);
		view.getButtonAlignRight().setSelected(false);
		view.getButtonStyleBold().setSelected(false);
		view.getButtonStyleItalic().setSelected(false);
		view.getCheckboxDown().setState(false);
		view.getCheckboxLeft().setState(false);
		view.getCheckboxRight().setState(false);
		view.getCheckboxUp().setState(false);
	}
	
	private void setCheckboxDisabled() {
		view.getCheckboxDown().setEnabled(false);
		view.getCheckboxLeft().setEnabled(false);
		view.getCheckboxRight().setEnabled(false);
		view.getCheckboxUp().setEnabled(false);
	}
	
	private void updateCoordLabel () {
    	JTable table = view.getMainTable().getJTable();
    	int a = table.getSelectedColumnCount();
    	int b = table.getSelectedRowCount();
    	String newLabel;
    	// Debug
    	//System.out.println("NbCols:"+a+" NbRows:"+b);
    	if (table.getSelectedColumnCount() == 0 || table.getSelectedRowCount() == 0) {
    		newLabel = "";
    	} else{
	    	if (a > 1) {
	    		newLabel = "[ " + Integer.toString(table.getSelectedColumns()[0]+1) + ":" +
	    						Integer.toString(table.getSelectedColumns()[a-1]+1) + " , ";
	    	} else {
	    		newLabel = "[ " + Integer.toString(table.getSelectedColumns()[0]+1) + " , ";
	    	}
	    	
	    	if (b > 1) {
	    		newLabel += Integer.toString(table.getSelectedRows()[0]+1) + ":" +
	    					Integer.toString(table.getSelectedRows()[b-1]+1) + " ]";
	    	} else {
	    		newLabel += Integer.toString(table.getSelectedRows()[0]+1) + " ]";
	    	}
    	}
    	
    	this.view.getLabelCalculatedCoord().setText(newLabel);
    }

    private void updateColWidthLabel () {
    	//Debug
    	//System.out.println("Col. Width:"+table.getTableHeader().getResizingColumn().getWidth());
    	JTable table = view.getMainTable().getJTable();
    	if (table.getTableHeader().getResizingColumn() != null) {
    		int colIndex = table.getTableHeader().getResizingColumn().getModelIndex();
    		//System.out.println(model.getCell(0, colIndex).getWidth());
    		
    		if (model.getCell(0, colIndex).getWidth() != "Automatic") {
    			this.view.getLabelCalculatedColWidth().setText(Integer.toString(table.getTableHeader().getResizingColumn().getWidth())+" px");
    		} else {
    			this.view.getLabelCalculatedColWidth().setText("<html><i>"+model.getCell(0, colIndex).getWidth()+"</i></html>");
    		}
    		
    	} else {
    		
    		int colIndex = table.getColumn(table.getColumnName(table.getSelectedColumn())).getModelIndex();
    		//System.out.println(model.getCell(0, colIndex).getWidth());
    		
    		if (model.getCell(0, colIndex).getWidth() != "Automatic") {
    			this.view.getLabelCalculatedColWidth().setText(Integer.toString(table.getColumn(table.getColumnName(table.getSelectedColumn())).getWidth())+" px");
    		} else {
    			this.view.getLabelCalculatedColWidth().setText("<html><i>"+model.getCell(0, colIndex).getWidth()+"</i></html>");
    		}
    	}
    }
    
    private void setColWidthLabelNull() {
    	this.view.getLabelCalculatedColWidth().setText("  --");
	}
    
    private void updateColWidthData () {
    	JTable table = view.getMainTable().getJTable();
    	int colIndex;
    	int nbRows = model.getModelStruct().data.length;
    	String width;
    	
    	if (table.getTableHeader().getResizingColumn() != null) {
    		colIndex= table.getTableHeader().getResizingColumn().getModelIndex();
    		width = Integer.toString(table.getTableHeader().getResizingColumn().getWidth());
    	} else {
    		colIndex= table.getSelectedColumn();
    		width = Integer.toString(table.getColumn(table.getColumnName(table.getSelectedColumn())).getWidth());
    	}
    	
    	for (int i=0; i < nbRows; i++ ) {
    		model.getCell(i, colIndex).setWidth(width);
    		//System.out.println(model.getCell(i, colIndex).getWidth());
    	}
    	//System.out.println("---------");
    	//System.out.println(model.getCell(1, 0).getWidth());
    	//System.out.println(model.getCell(1, 1).getWidth());
    	//System.out.println(model.getCell(1, 2).getWidth());
    }
    
    private void updateCheckbox() {
    	JTable table = view.getMainTable().getJTable();
		int colIndex = table.getSelectedColumn();
		int rowIndex = table.getSelectedRow();
		boolean[] tempBorders = model.getCell(rowIndex, colIndex).getBorders();
		view.getCheckboxUp().setState(tempBorders[0]);
		view.getCheckboxRight().setState(tempBorders[1]);
		view.getCheckboxDown().setState(tempBorders[2]);
		view.getCheckboxLeft().setState(tempBorders[3]);
	}
    
    private void updateItalicBold() {
    	JTable table = view.getMainTable().getJTable();
		int colIndex = table.getSelectedColumn();
		int rowIndex = table.getSelectedRow();
		boolean tempBold = model.getCell(rowIndex, colIndex).getBold();
		boolean tempItalic = model.getCell(rowIndex, colIndex).getItalic();
		view.getButtonStyleBold().setSelected(tempBold);
		view.getButtonStyleItalic().setSelected(tempItalic);
    }
    
    private void updateViewButtonsAlign(int selectedButton) {
		TaBuLeXJTable table = view.getMainTable();
		int colIndex[] = table.getJTable().getSelectedColumns();
		for (int i = colIndex[0]; i<colIndex[colIndex.length-1]+1; i++) {
	    	if (selectedButton == 0) {
	    		view.getButtonAlignLeft().setSelected(true);
	    		view.getButtonAlignCenter().setSelected(false);
	    		view.getButtonAlignRight().setSelected(false);
	    		table.alignLeftColumnContent(i);
	    	} else if (selectedButton == 1) {
	    		view.getButtonAlignLeft().setSelected(false);
	    		view.getButtonAlignCenter().setSelected(true);
	    		view.getButtonAlignRight().setSelected(false);
	    		table.centerColumnContent(i);
	    	} else if (selectedButton == 2) {
	    		view.getButtonAlignLeft().setSelected(false);
	    		view.getButtonAlignCenter().setSelected(false);
	    		view.getButtonAlignRight().setSelected(true);
	    		table.alignRightColumnContent(i);
	    	}
		}
	}

	private void updateModelCellAlign(int selectedButton) {
		JTable table = view.getMainTable().getJTable();
		int colIndex[] = table.getSelectedColumns();
		for (int i = 0; i < model.getModelStruct().data.length; i++) {
			for (int j = colIndex[0]; j<colIndex[colIndex.length-1]+1; j++) {
				model.getCell(i, j).setAlignment(selectedButton);
			}
		}
		model.setDataContent(model.updateTable());
	}
	
	private void updateModelCellBold() {
		JTable table = view.getMainTable().getJTable();
		int colIndex[] = table.getSelectedColumns();
		int rowIndex[] = table.getSelectedRows();
		for ( int i = rowIndex[0]; i<rowIndex[rowIndex.length-1]+1; i++) {
			for ( int j = colIndex[0]; j<colIndex[colIndex.length-1]+1; j++) {
				if (model.getCell(i , j).getBold()==false) {
					model.getCell(i , j).setBold(true);
				} else {
					model.getCell(i , j).setBold(false);
				}
			}
		}
	}
	
	private void updateModelCellItalic() {
		JTable table = view.getMainTable().getJTable();
		int colIndex[] = table.getSelectedColumns();
		int rowIndex[] = table.getSelectedRows();
		for ( int i = rowIndex[0]; i<rowIndex[rowIndex.length-1]+1; i++) {
			for ( int j = colIndex[0]; j<colIndex[colIndex.length-1]+1; j++) {
				if (model.getCell(i , j).getItalic()==false) {
					model.getCell(i , j).setItalic(true);
				} else {
					model.getCell(i , j).setItalic(false);
				}
			}
		}
	}
	
	private void updateAlignIcon() {
		JTable table = view.getMainTable().getJTable();
		int colIndex = table.getSelectedColumn();
		int rowIndex = table.getSelectedRow();
		if (colIndex != -1 && rowIndex != -1) {
			int tempalign = model.getCell(rowIndex, colIndex).getAlignment();
			if (tempalign == 0) {
				view.getButtonAlignLeft().setSelected(true);
	    		view.getButtonAlignCenter().setSelected(false);
	    		view.getButtonAlignRight().setSelected(false);
			} else if (tempalign == 1) {
				view.getButtonAlignLeft().setSelected(false);
	    		view.getButtonAlignCenter().setSelected(true);
	    		view.getButtonAlignRight().setSelected(false);
			} else if (tempalign == 2) {
				view.getButtonAlignLeft().setSelected(false);
	    		view.getButtonAlignCenter().setSelected(false);
	    		view.getButtonAlignRight().setSelected(true);
			}
		}
	}
	
	private void updateModelCellHorizontalBorders(int border) {
		JTable table = view.getMainTable().getJTable();
		int rowIndex[] = table.getSelectedRows();
		for ( int j = rowIndex[0]; j<rowIndex[rowIndex.length-1]+1; j++) {
			for (int i = 0; i<model.getModelStruct().data[0].length; i++){
				boolean borderState = model.getCell(j, i).getBorders()[border];
				borderState = (borderState == false) ? true : false;
				boolean[] tempBorders = model.getCell(j, i).getBorders();
				tempBorders[border]= borderState;
				model.getCell(j, i).setBorders(tempBorders);
			}
		}
	}
	
	private void updateModelCellVerticalBorders(int border) {
		JTable table = view.getMainTable().getJTable();
		int colIndex[] = table.getSelectedColumns();
		for ( int j = colIndex[0]; j<colIndex[colIndex.length-1]+1; j++) {
			for (int i = 0; i<model.getModelStruct().data.length; i++){
				boolean borderState = model.getCell(i, j).getBorders()[border];
				borderState = (borderState == false) ? true : false;
				boolean[] tempBorders = model.getCell(i, j).getBorders();
				tempBorders[border]= borderState;
				model.getCell(i, j).setBorders(tempBorders);
			}
		}
	}
	
	private void updateNbCols(int nbColNext) {
		int nbColPrev = model.getModelStruct().data[0].length;
		if (nbColPrev<nbColNext && nbColNext > 0) {
			for (int i = nbColPrev; i < nbColNext; i++) {
				model.setModelStruct(model.addColumn());
				model.setDataContent(model.updateTable());
				view.getMainTable().buildJTable(model.getDataContent(), model.getModelStruct().columnNames);
			}
		} else if(nbColPrev>nbColNext && nbColNext > 0){
			for (int i = nbColPrev; i > nbColNext; i--) {
				model.setModelStruct(model.deleteColumn());
				model.setDataContent(model.updateTable());
				view.getMainTable().buildJTable(model.getDataContent(), model.getModelStruct().columnNames);
			}
		}
	}

	private void updateNblines(int nbRowNext) {
		int nbRowPrev = model.getModelStruct().data.length;
		if (nbRowPrev<nbRowNext && nbRowNext > 0) {
			for (int i = nbRowPrev; i < nbRowNext; i++) {
				model.getModelStruct().data = model.addRow();
				model.setDataContent(model.updateTable());
				view.getMainTable().buildJTable(model.getDataContent(), model.getModelStruct().columnNames);
			}
		} else if(nbRowPrev>nbRowNext && nbRowNext > 0) {
			for (int i = nbRowPrev; i > nbRowNext; i--) {
				model.getModelStruct().data = model.deleteRow();
				model.setDataContent(model.updateTable());
				view.getMainTable().buildJTable(model.getDataContent(), model.getModelStruct().columnNames);
			}
		}
	}

	private void resetColumnsWidth() {
		for (int i = 0; i < model.getModelStruct().data.length; i++) {
			for (int j = 0; j < model.getModelStruct().data[0].length; j++) {
				model.getCell(i,j).setWidth("Automatic");
			}
		}
	}
	
	// Test input fields. Must be an Integer > 0, cause a null table is not interesting
	// So test of the first char, then the rest
	private boolean testField(String comboboxText) {
		boolean testHead = true;
		boolean testTail = true;
		
		testHead = (comboboxText.charAt(0)>='1' && comboboxText.charAt(0)<='9') ? true : false;
		
		for(int i = 1; i<comboboxText.length(); i++) {
			if (comboboxText.charAt(i)>='0' && comboboxText.charAt(i)<='9') {
			} else {
				testTail = false;
			}
		}
		return testHead && testTail;
	}
	
	private void updateItemsEnabled() {
		int nbColSelected = view.getMainTable().getJTable().getSelectedColumnCount();
		int nbRowSelected = view.getMainTable().getJTable().getSelectedRowCount();
		if (nbColSelected == 0 && nbRowSelected == 0) {
			view.setItemsDisabled();
		}
	}
	
	
	/**
	 * Call submethods for updating all the elements of the view
	 */
	private void globalViewUpdate() {
		
		JTable table = view.getMainTable().getJTable();
		int colIndex[] = table.getSelectedColumns();
		int rowIndex[] = table.getSelectedRows();
		setItemsEnabled();
        updateCoordLabel();
        updateViewData();
		if (colIndex.length == 1 && rowIndex.length == 1) {
            updateColWidthLabel();
            updateCheckbox();
            updateItalicBold();
            updateAlignIcon();
		} else {
			setItemsUnselected();
			setCheckboxDisabled();
			setColWidthLabelNull();
		}                
	}
	
	private void resetCellsContent(int[] selectedRows, int[] selectedColumns) {
		for (int i = 0; i < selectedRows.length; i++) {
			for (int j = 0; j < selectedColumns.length; j++) {
				view.getMainTable().getJTable().setValueAt("", selectedRows[i], selectedColumns[j]);
			}
		}
	}
	
 // *********************************************** UNUSED SPECIFIC IMPLEMENTATIONS  *****************************************************************************
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
