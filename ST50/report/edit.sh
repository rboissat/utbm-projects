#!/bin/bash

WD=$(dirname $0)

if [ -z $_VIM ]; then _VIM=$(which vim); fi

if [ -z $1 ]
then
  $_VIM -p $WD/chapters/*.tex

elif [ -d $WD/chapters/$1 ]
then
  $_VIM -p $WD/chapters/$1/*.tex

else
  exit 42
fi

exit 0
