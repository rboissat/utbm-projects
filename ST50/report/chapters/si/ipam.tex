Cette section présente les besoins émis par Trinaps concernant la solution
\emph{IPAM} à retenir, la présentation de la solution retenue et quelques
perspectives dans le déploiement d'une telle solution.

\subsubsection{Quels besoins ?}

Sans exposer des besoins qui semblent évidents, je vais surtout présenter quels
étaient les évolutions souhaitables au passage d'une gestion par logiciel de
tableur à un solution logicielle dédiée. Évidemment, l'approche d'un document
tableur dont chacun des multiples feuillets correspond à un aspect précis ou à
une relation entre deux types d'entités est une approche qui est cohérente,
permettant de répondre à la majorité des besoins lorsque le jeu de données n'est
pas de taille conséquente.

Mais dès lors que l'on aborde IPv6, on doit absolument réaliser que même avec
les allocations les plus petites, ce sont des milliards de milliards d'adresses
que l'on peut être amené à gérer. Et ce sans compter sur un nouvel aspect très
important à propos de la gestion de l'espace d'adressage IPv6, présenté dans le
chapitre précédent : \emph{En IPv6, on ne gère plus l'allocation au niveau de
l'adresse, mais au niveau du \textbf{sous-réseau} !} Dès lors, il est préférable
de disposer d'outils adaptés à la manipulation à grande échelle de la notion de
sous-réseau comme unité de base, ce que ne permet pas une application du type
tableur bureautique.

De plus avec le nombre d'adresses à gérer, c'est véritablement un changement de
paradigme à adopter. Dans le tableur, on génère habituellement la liste de
toutes les adresses IPv4 ou tout au moins de tous les sous-réseaux compris dans
un préfixe global. Pour quelques centaines voire quelques milliers d'adresses,
cela reste envisageable, mais il est impossible de générer la liste de toutes
les adresses d'un \emph{simple} \textbf{/64}. Il convient d'avoir une approche
différente, à savoir de déclarer précisément chaque adresse utilisée et en
définir les relations avec d'autres types d'objets, tels qu'un nom de domaine ou
le nom de l'équipement où sera configurée cette adresse. Cette approche
s'inscrit logiquement dans une application reposant sur une base de données
relationnelle, contenant plusieurs tables et des liaisons qui établissent des
relations entre les entités (occurrences) stockées dans chacune de ces tables.

Le passage d'un logiciel tableur à un logiciel dédié doit par conséquent
apporter deux bénéfices :

\begin{itemize}
  \setlength{\itemsep}{8pt}
  \item Apporter la capacité de gestion du format d'adresse IPv6, à l'échelle de
  la taille d'un préfixe opérateur, avec les opérations suivantes : stockage,
  représentation, édition.

  \item Enrichir le Système d'Information existant en permettant d'établir de
  nouvelles relations entre les différentes entités formant l'architecture d'un
  opérateur réseau, sur les plans logique, physique et logistique.
\end{itemize}

\subsubsection{Une solution possible : \emph{Netdot}}

Dans le cadre de recherche d'une solution logicielle adaptée, j'ai évalué les
quelques solutions libres et gratuites à ma disposition. En effet, Trinaps
dispose de l'expertise technique nécessaire pour procéder à la mise en place et
la maintenance de telles solutions, ce qui permet de s'affranchir de coûts
supplémentaires élevés des solutions propriétaires et payantes. Au terme de
quelques jours de recherche et de prise d'information, j'ai trouvé deux
solutions qui paraissaient solides et adaptées à nos besoins : \textbf{NOC
Project}\footnote{Cf. \url{http://redmine.nocproject.org/projects/noc}} et
\textbf{Netdot}\footnote{Cf.
\url{https://osl.uoregon.edu/redmine/projects/netdot}}. Après avoir déployé un
serveur virtuel GNU/Linux pour déployer et tester ces deux solutions, j'ai
rapidement réalisé que NOC Project ne serait pas adapté à l'échelle de Trinaps :
complexe à installer et maintenir et très gourmand en ressources, il propose une
grande quantité de fonctionnalités dont peu sont pertinentes pour Trinaps. En
plus de cela, l'interface est complexe et chargée, demandant des heures
d'exploration à son utilisateur avant d'avoir trouvé ses marques.

\textbf{Netdot} est une solution dédiée à la gestion et l'exploitation
d'infrastructure réseau, offrant en plus de la gestion \emph{IPAM} un service
d'inventaire matériel, un annuaire de contacts ou d'entités, une gestion des
\emph{Points of Presence}, une gestion des \emph{VLANs}, une gestion très
poussée des services \emph{DNS} et \emph{DHCP} et enfin la possibilité
d'extraire des rapports statistiques et d'exporter des fichiers de configuration
pour des logiciels de \emph{monitoring} tels que \emph{Nagios}.

C'est une solution développée par le service informatique de l'université
d'Oregon\footnote{Cf. \url{http://it.uoregon.edu/is/nts}} basée sur le
\emph{SGBD}\footnote{\emph{Système de Gestion de Base de Données}.}
\textbf{MySQL} (ou alternativement \textbf{PostgreSQL}), le serveur web
\textbf{Apache} avec le module \textbf{mod\_perl} permettant d'exécuter au sein
du serveur Apache des pages développées avec le langage \emph{Perl}. Logiciel
\emph{Open-source} sous licence \emph{GNU GPL v2}, Netdot est en réalité
développé majoritairement par le service informatique qui est à son origine,
seuls quelques corrections logicielles ou demandes de fonctionnalités sont
soumises par le reste de la communauté. Actuellement dans sa version
\emph{0.9.9}, Netdot est aujourd'hui une solution mature avec plus de 8 ans de
développement et de multiples versions intermédiaires.

Un des avantages de cette solution est sa souplesse et sa relative facilité
d'utilisation. Un autre avantage pertinent pour Trinaps est la simplicité et la
rapidité du \emph{flux opérationnel} (\emph{workflow}) des tâches d'ajout,
édition, d'affichage ou de suppression d'un objet\footnote{Selon le principe
\emph{CRUD} : \emph{Create}, \emph{Read}, \emph{Update}, \emph{Delete}.} et ce
quelque soit la taille de l'infrastructure à administrer. C'est d'ailleurs ce
point qui m'a convaincu de l'utilité de Netdot pour de petites infrastructures.
J'ai alors déployé une instance sur mon serveur dédié personnel et je l'utilise
depuis pour l'administration de mon infrastructure réseau
personnelle\footnote{IPv6 : préfixe \textbf{/48}, IPv4 : préfixe privé
\textbf{/24}.} et de ses services primordiaux\footnote{\emph{DNS} et
\emph{DHCP}.}. Ainsi, intégré dans une approche consolidée avec les serveurs de
noms \emph{DNS}, Netdot permet de s'affranchir de toute édition manuelle des
fichiers de \emph{zone DNS} au profit d'une édition simplifiée via son interface
web et sa capacité d'export. Je reviendrai en détail sur l'\emph{architecture
DNS} dans une section ultérieure.

De ce déploiement personnel, j'en retire une expérience avancée dans
l'utilisation de cette solution, mais également dans son intégration au sein
d'une infrastructure existante. Cette expérience fut d'une aide précieuse lors
du déploiement sur une infrastructure plus importante comme celle de Trinaps.

Netdot, dont quelques captures d'écran se trouvent en annexe \ref{an:netdot}
page \pageref{an:netdot}, est donc la solution que Trinaps a retenue pour
l'administration et la gestion de son infrastructure. Au delà du besoin de
trouver une solution de gestion \emph{IPAM}, c'est finalement les
\textbf{couches 1, 2 et 3} de la pile OSI qui seront certainement gérées via
Netdot, ce qui dépasserait les objectifs définis en début de mission.
