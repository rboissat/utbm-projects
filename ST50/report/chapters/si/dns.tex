Si \textbf{Netdot} permet de combler un manque dans la gestion de
l'infrastructure opérareur à plusieurs niveaux, il manquait une intégration à
l'un des composants les plus critiques de tout réseau informatique voulant
proposer une qualité de service correcte : les \textbf{serveurs de noms
\emph{DNS}}. On en distingue deux types, généralement consolidés au sein du même
serveur :

\begin{itemize}
  \setlength{\itemsep}{8pt}
  \item \textbf{Les serveurs autoritaires :}  Ils sont dits autoritaires pour
  une zone DNS donnée car le fichier de cette zone les déclare via des
  \emph{enregistrements \textbf{NS}}. Ce sont les seuls serveurs habilités à
  servir les enregistrements du fichier de zone, qu'ils soient consultés par
  d'autres serveurs DNS ou directement par des clients.

  \item \textbf{Les serveurs récursifs :} Ces serveurs permettent la
  \emph{résolution} des noms de domaine afin de répondre aux requêtes des
  clients qui les interrogent. Ces serveurs ne sont ni plus ni moins que des
  relais qui permettent aux clients d'interroger un serveur local plutôt que
  d'interroger directement un serveur racine.
\end{itemize}

Il est courant d'utiliser une même instance de serveur DNS pour assurer les deux
fonctions, permettant ainsi une bonne qualité de service aux clients souhaitant
résoudre des domaines gérés par ce même serveur. C'est l'approche retenue à
Trinaps, où les domaines des clients déclarent les serveurs DNS de Trinaps comme
serveurs autoritaires. Il en est de même pour les nombreux domaines réservés par
Trinaps. Ensuite, ces serveurs DNS sont utilisés sur le réseau opérateur Trinaps
par toutes les machines, clientes ou internes à Trinaps, comme résolveurs
récursifs.

\subsection{Intégration nécessaire avec Netdot}

Avant la refonte de l'infrastructure DNS, Trinaps déployait un modèle simple
mais robuste, basé sur la présence permanente de deux serveurs synchronisés sur
un modèle \emph{maître-esclave}. Avec le serveur DNS utilisé, \textbf{ISC BIND
9}, il est possible de réaliser ce modèle assez simplement. Mais dans le cas de
Trinaps, la synchronisation s'effectuait à un niveau inférieur, via le stockage
des fichiers de zones au sein d'un annuaire \emph{LDAP}, au lieu de simples
fichiers texte.

Cette combinaison \emph{BIND + LDAP} fonctionne bien et permet de faciliter
l'administration des zones DNS en utilisant un explorateur LDAP via un
navigateur web, ce qui est plus simple et plus performant que se connecter en
\emph{SSH} sur le serveur DNS, éditer en \emph{root} les fichiers de zone et
relancer le service DNS manuellement. Pour autant, l'intégration avec Netdot
dans cette configuration n'est pas simple : Netdot exporte les informations de
sa base de données en fichiers de zone au format texte, directement utilisables
par un \emph{BIND 9} classique. Une partie de la solution est d'implémenter un
modèle comprenant un serveur DNS dit \textbf{hidden master} ou maître caché. Ce
serveur n'est pas déclaré comme autoritaire dans les fichiers de zone (d'où le
\emph{hidden}), mais il sera le serveur principal qui distribuera les zones aux
\emph{vrais masters} via le mécanisme \textbf{notify}\footnote{Cf.
\url{www.zytrax.com/books/dns/ch7/xfer.html}} intégré dans \emph{BIND 9}.

Nous disposons donc de la moitié de la solution : \textbf{Netdot} permet de
gérer les zones DNS et de les exporter pour un serveur DNS \textbf{BIND 9}
présent sur la même machine. Ce serveur DNS utilise ensuite le mécanisme de
\textbf{notify} pour avertir les véritables serveurs maîtres de la zone pour
solliciter un transfert de zone. Une fois le transfert effectué, ces serveurs
maîtres diffuseront l'information à jour. Concernant le rechargement
automatique du \emph{hidden master} après export des zones par Netdot, j'ai
développé un script \emph{Bash} qui tourne en tâche de fond sur le serveur
\emph{Netdot \& Hidden master}. Ce script est fourni en annexe
\ref{an:Bind9ZoneMonitor.sh} page \pageref{an:Bind9ZoneMonitor.sh}.

Mais ce modèle tiercé ne peut fonctionner avec les serveurs maîtres actuels qui
reposent sur \emph{LDAP} : il est impossible à ce jour d'importer de nouvelles
données dans cet annuaire via le mécanisme de notification et les transferts de
zones entre serveurs \emph{BIND 9}. La solution la plus évidente est alors de
repasser sur des serveurs DNS classiques, sans l'intégration à l'annuaire
\emph{LDAP}. C'est ce que j'ai réalisé tout d'abord sur une infrastructure de
laboratoire.  Une fois ce modèle validé, nous l'avons passé en production. À ce
jour, il reste encore à mettre en place de la redondance bas niveau pour chacun
des trois serveurs (\emph{Netdot \& hidden master}, \emph{NS1} et \emph{NS2}),
avec des solutions comme \textbf{Corosync} (ex-\emph{Heartbeat}) et
\textbf{DR:BD}.

Voici le schéma final de la nouvelle infrastructure DNS :

\begin{bigcenter}
\begin{figure}[ht]
\centering\fbox{\scalegraphics{infra-dns}}
\caption{Schéma de la nouvelle infrastructure DNS}
\label{pic:infra-dns}
\end{figure}
\end{bigcenter}
