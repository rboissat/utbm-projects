\begin{small}
\begin{description}
\item [Note :] Ce chapitre suppose certaines connaissances techniques de la
part du lecteur, notamment sur la notion de protocole IP et d'adresse réseau.
Dans le cas contraire, un rappel de la pile OSI est présent à l'annexe
\ref{an:osi} page \pageref{an:osi}.

\item [Note :] Ce rapport s'articule autour du travail que j'ai effectué pendant
mon stage selon le \emph{sujet} de celui ci et non les technologies employées.
Si les protocoles IPv4 et IPv6 sont présentés, c'est uniquement de manière
succinte, ce document n'ayant pas la prétention d'être un document technique et
synthétique sur IPv6.
\end{description}
\end{small}

Le succès global et à grande échelle d'Internet est aujourd'hui indéniable. Son
taux de pénétration chez les professionnels comme les particuliers est très
élevé, en parallèle de la démocratisation de l'informatique. Sans compter sur
les nouveaux supports numériques tels que les \emph{smartphones}, les tablettes
et autres liseuses électroniques. La domotique est également connectée, de même
que les premiers véhicules automobiles. La démocratisation d'Internet à tout
endroit et à tout moment est d'autant plus facilitée que les technologies
de connexion évoluent : sans-fil 802.11, GPRS et 3G et très récemment encore de
simples diodes électroluminescentes dont le clignotement en air libre est capté
par une photodiode très sensible sur le récepteur.

Internet est donc une entité massive et massivement exploitée, avec une
multiplication des usages et des moyens de communication sans précédent. Bien
entendu, cette expansion n'est pas restreinte aux utilisateurs, mais s'étend
également aux fournisseurs de services et de contenus. En considérant uniquement
le \emph{World Wide Web}\footnote{Qui n'est représenté que par deux ports (80 et
443) du protocole de transport TCP !}, certains géants comme Google ou Facebook
disposent à ce jours de plusieurs dizaines de datacentres, pour des estimations
de plusieurs centaines de miliers de serveurs voire approchant le million de
serveurs ! Imaginons alors en considérant l'ensemble des services réseau
existants\ldots Pourtant, les protocoles de fondation du réseau Internet n'ont pas
(ou peu) évolués depuis leur période de création.

\subsection{Pourquoi IPv6 ?}

La version du protocole \emph{IP}\footnote{\emph{Internet Protocol} : protocole
de couche 3 de la pile OSI qui permet l'adressage des  ressources qui sont
engagés dans les télécommunications informatiques. Cf. glossaire.} actuellement
la plus déployée (et malheureusement encore la plus connue) est la version
\emph{4} plus connue sous l'acronyme \textbf{IPv4}. Elle fut développée au début
des années 70 pour faciliter la communication et le partage d'informations entre
les chercheurs du gouvernement et les académiciens aux États-Unis. À cette
époque, le système était fermé avec un nombre limité de points d'entrée et de
ressources exposées sur le réseau. Les développeurs n'avait pas envisagé des
prérequis comme la sécurité ou la qualité de service (\emph{QoS}). À son crédit,
IPv4 a survécu et continue de survivre après plus de 30 ans et reste un pilier
fondamental d'Internet. Mais comme toute technologie aussi bien pensée
soit-elle, IPv4 vieillit et son obsolescence devient un enjeu stratégique
important. Limité à $2^{32} = 4 294 967 296$ d'adresses réseau, IPv4 ne fait
plus face aujourd'hui à l'explosion dans la diversité des appareils comme de la
nature des communications.

Le protocole IPv6 a été développé en se basant sur la riche expérience acquise
par le développement et la longue utilisation de la version 4. Les mécanismes
éprouvés furent conservés, les limitations connues furent écartées et la
flexibilité et la capacité d'échelle furent étendues. IPv6 est un protocole
conçu pour absorber la croissance exponentielle d'Internet, tout en assurant les
prérequis sur les services, la mobilité et la sécurité des communications bout
en bout (\emph{end-to-end}).

\subsection{Historique}

L'\emph{IETF}\footnote{\emph{Internet Engineering Task Force} : Groupe
informel, international, ouvert à tout individu, qui participe à l'élaboration
de standards pour Internet. L'IETF produit la plupart des nouveaux standards
d'Internet.} engagea dès le début des années 90 le développement du successeur
du protocole IPv4, à l'époque désigné comme le \emph{Internet Protocol Next
Generation} (IPng). La création du protocole IPv6 fut recommandée en 1994 au 
\emph{meeting} de l'IETF à Toronto. Le cœur des protocoles composant la suite
IPv6 furent regroupés dans un brouillon de standard de l'IETF en août 1998,
incluant la célèbre
%
\textbf{RFC}\footnote{\emph{Request For Comments}:}
%
\textbf{2460}\footnote{\url{http://tools.ietf.org/html/rfc2460}}.

\subsection{Apports principaux}

Voici un aperçu des principaux changements apportés par IPv6 :

\begin{itemize}
  \setlength{\parskip}{8pt}
  \item{\textbf{Extension de l'espace d'adressage} : le format d'adresse passe
  de 32 bits à 128 bits, permettant ainsi un accroissement exponentiel du nombre
  d'adresses disponibles. Ainsi, si on représente l'espace d'adressage IPv4 par
  une surface de 3.2 cm², la surface du système solaire représente alors
  l'espace d'adressage IPv6.

  \begin{bigcenter}
  \begin{figure}[ht]
  \centering\fbox{\scalegraphics{espace}}
  \caption{Comparaison de taille d'espace d'adressage}
  \label{pic:espace}
  \end{figure}
  \end{bigcenter}}

\clearpage

  \item{\textbf{Autoconfiguration des interfaces réseau} (\emph{Stateless
  autoconfiguration}) : Une des fonctionnalités les plus intéressantes d'IPv6.
  Permet au routeur IPv6 d'envoyer automatiquement le préfixe du réseau local
  aux clients. Ceux-ci configurent alors la seconde partie de l'adresse IPv6 via
  un algorithme qui utilise soit un identifiant aléatoire avec les \emph{Privacy
  Extensions}, soit une
  %
  dérivation\footnote{Un algorithme basé la norme EUI-64\footnotemark
  pour la formation de \textbf{l'identifiant d'interface IID}.}
  %
  \footnotetext{\emph{Extended Unique Identifier} : cf.
  \url{http://standards.ieee.org/regauth/oui/tutorials/EUI64.html} et l'annexe
  \ref{an:eui64} page \pageref{an:eui64}.}
  %
  de l'adresse de lien (MAC) de l'interface réseau. Ce mécanisme déporte
  l'intelligence d'un service (DHCP) vers les hôtes clients. De plus, sur un
  même lien Ethernet peuvent coexister plusieurs routeurs et/ou plusieurs
  préfixes IPv6, chose difficile voire impossible avec DHCP en IPv4.}

  \item{\textbf{Simplification du format d'entête} : L'entête d'un paquet IPv6
  est beaucoup plus simple qu'une entête IPv4 et dispose surtout d'une longueur
  fixe de 40 octets contre une longueur variable comprise entre 20 et 60 octets
  en IPv4. La somme de contrôle disparait au profit de la somme de contrôle
  préexistante dans la couche supérieure (TCP, UDP, SCTP\ldots). Cela permet un
  traitement simplifié et accéléré d'un paquet IPv6.}

  \item{\textbf{Emploi d'extensions pour plus d'options disponibles} : IPv4
  intègre les options directement dans le format d'entête standard, ce qui est
  un gaspillage de place lorsque ces options ne sont pas nécessaires à la
  communication en cours. En IPv6, les options sont insérées si besoin sous la
  forme d'extensions. La spécification de base décrit six types d'extensions,
  incluant le routage, la mobilité, la qualité de service et la sécurité.}
\end{itemize}

\subsection{Adoption difficile}

Comme pour IPv4, le protocole IPv6 n'est pas développé du jour au lendemain et
certaines fonctionnalités sont encore à développer tandis que d'autres besoins
font leur apparition. Alors qu'IPv4 a mis plus de dix ans pour s'imposer, cela
fait 16 ans que IPv6 dispose de spécifications fonctionnelles suffisantes à
au développement des premières piles protocolaires fonctionnelles pour les
systèmes d'exploitation majeurs.

Mais le plus gros frein à l'adoption d'IPv6 en masse au début du XXI siècle est
et reste le facteur humain. Cela fait presque 6 ans que IPv6 est considéré comme
un protocole réseau stable, utilisable dans des environnements de production au
niveau de certains systèmes d'exploitation serveurs ou certains systèmes
embarqués sur les équipements réseau. Depuis, le support par les différents
équipementiers et les différents systèmes d'exploitation ne cesse d'augmenter.
En règle générale, la totalité des fonctionnalités minimales et nécessaires à
l'emploi du protocole IPv6 est désormais implémentée. La différence se joue
surtout sur le support de fonctionnalités avancées, voire de fonctionnalités
encore en cours de définition (\emph{Draft RFC} ou \emph{Proposed Standard})
comme par exemple l'option \emph{RDNSS}\footnote{\emph{Recursive DNS
Server} : Option de message \emph{RA} en cours de standardisation qui permet
d'inclure la configuration DNS pour l'autoconfiguration de l'hôte client}.

Le prétexte du support IPv6 matériel ou logiciel tend donc à devenir caduc,
alors que l'inertie d'adoption du protocole IPv6 au sein des entreprises reste
très importante. Au delà des coûts de formation et de migration, c'est également
une certaine réticence à apprendre et maîtriser un nouveau protocole alors que
bien souvent (et heureusement !) le protocole en place fonctionne très bien.
Cette inertie est d'autant plus perceptible que la situation de l'entreprise
laisse penser aux décideurs qu'ils ne sont pas concerné par la gravité de la
situation : allocation IPv4 surdimensionnée, opportunité de racheter une autre
entreprise et ses ressources IPv4, \emph{cash} suffisant pour acheter des blocs
IPv4\ldots Les raisons sont nombreuses et pourtant l'urgence de la situation et la
nécessité d'une phase de transition concernent l'ensemble des acteurs
participant à Internet. Internet ne pourra pas être basculé en un jour comme ce
fut le cas avec la transition \emph{NCP} vers \emph{IPv4} au début des années
80. Il faudra des années avant d'assister à la suprématie d'IPv6 sur IPv4. C'est
pour cela que de nombreux protocoles et mécanismes de transition existent et
permettent depuis quelques années le déploiement effectif et réussi
d'architectures dites \emph{double pile}, où les deux versions du protocole IP
fonctionnent en parallèle (puisque directement incompatibles entre elles).

Mais pour cela, il est nécessaire que les acteurs d'Internet prennent conscience
des enjeux liés à une telle entreprise : Initialiser globalement le protocole
IPv6 tout en conservant la connectivité IPv4 actuelle.

\subsection{Contexte Trinaps}

Chez Trinaps, la problématique de l'activation d'IPv6 était envisagée depuis
quelques mois, mais au delà de la compatibilité du parc matériel et logiciel,
des problématiques spécifiques à un fournisseur d'accès à Internet et de
services sur réseau étaient à prendre en considération.

Mon arrivée en tant qu'ingénieur stagiaire connaissant bien le protocole IPv6 et
les problématiques rattachées ont permis à Trinaps de consacrer du temps et des
moyens sur la question, notamment par une phase de sensibilisation de l'équipe.
La section suivante expose quels étaient les objectifs et les phases de ce
projet majeur.
