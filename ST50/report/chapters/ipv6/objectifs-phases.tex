Mon stage de fin d'étude ST50 se déroulant à Trinaps s'articule autour d'un
objectif principal :

\begin{center}
\emph{« Concevoir et accomplir le déploiement du protocole IPv6 au sein de
l'architecture opérateur Trinaps et pour les clients. »}
\end{center}

Cet objectif est bien entendu formulé de manière globale. Ce n'est pas un
objectif qui conviendrait dans le cadre de la gestion de projet, car il n'est ni
précis, ni mesurable, ni défini temporellement. M. Douchet et moi même avons
formulé cet objectif dès les premiers jours du stage, en même temps qu'un
objectif précis à plus court terme : préparer un support de présentation autour
du protocole IPv6. Cette présentation devait suivre une approche générale et
vulgarisante, avec juste certains éléments techniques majeurs, présenter les
enjeux sur les différents plans qui concernent un FAI et une ébauche de
planning.

Le but de cette présentation au tout début du stage était double. Tout d'abord me
permettre d'acquérir rapidement les informations nécessaires sur la structure de
la plateforme opérateur de Trinaps et mettre en avant mes connaissances sur le
sujet. Puis à Trinaps d'avoir un aperçu personnalisé d'IPv6, qui tente de
s'intégrer au mieux dans le contexte technique et humain de la société.
L'avantage premier est de gagner et de faire gagner beaucoup de temps, tout en
permettant au projet de réellement décoller après une présentation complète et
un premier cadrage des limitations du projet.

La présentation s'articulait autour d'un plan simple mais adapté aux attentes
des dirigeants :

\begin{itemize}
  \item Introduire IPv6.
  \item Comparer IPv4 et IPv6.
  \item Vulgariser les grands principes techniques d'IPv6.
  \item Présenter les enjeux, les objectifs fixés et un planning.
\end{itemize}

À l'issue de la présentation effective à la fin de la deuxième semaine de
stage, alors que ma connaissance de l'infrastructure réseau se limitait presque
au minimum, j'ai proposé dans le cadre de cette présentation une liste
d'objectifs thématiques présentée ci dessous :

\begin{description}
  \item [\textbf{Plan humain}] :\\ Former l'équipe technique, atteindre la
  transparence pour l'utilisateur, minimiser la charge pour l'administrateur\ldots

  \item [\textbf{Plan technique}] :\\ Évaluer l'existant, proposer des
  modifications mesurées et pragmatiques, élaborer et suivre des standards\ldots

  \item [\textbf{Plan gestionnaire}] :\\ Fournir un planning détaillé, effectuer
  un suivi sur l'outil de collaboration interne, initier et maintenir une
  documentation.
\end{description}

De cette liste d'objectifs, je fus amené à produire un découpage en phases
majeures et les détailler au sein d'un diagramme de Gantt présenté en annexe
\ref{an:gantt} page \pageref{an:gantt}.

\clearpage

Ci-dessous le découpage en phase initialement prévu :

\subsection{Phase 1 - Audit, Documentation, Laboratoire}

Cette phase est sans conteste la plus longue et la plus exigeante du stage. En
effet, elle constitue un ensemble de recherches, d'essais et de validations sans
lesquels il est impossible de déterminer si l'infrastructure Trinaps actuelle
supporte ou ne supporte pas IPv6, avec ou sans modifications. Siège de l'enjeu
principal de mon stage, j'y ai accordé une très grande attention, en prenant le
temps suffisant mais nécessaire de peser chaque conclusion.

\subsubsection{Un audit poussé, axé sur l'équipement et les services.}

J'ai abordé cette phase par un audit exhaustif sur l'ensemble des équipements
dont dispose la société Trinaps, que cela soit du matériel mis en production au
niveau de la plateforme opérateur comme du matériel utilisé ponctuellement ou
pour des prestations évènementielles. Trinaps dispose d'un parc varié, composé
de plus de cinq équipementiers différents, de tous types d'appareils réseau
(commutateurs, routeurs, point d'accès sans fil, téléphones IP, serveurs,
imprimantes, etc). L'analyse de chaque modèle et de chaque version de système
embarqué fut consignée au sein d'une section \emph{Étude de l'existant} dans le
cahier des charges qui m'accompagne dans ce projet d'intégration d'IPv6.
L'évolution ne fut pas écartée, j'ai tenté dans la mesure du possible de trouver
des solutions de mise à jour logicielle des systèmes embarqués qui pourraient
apporter ou améliorer le support du protocole IPv6.

J'ai également travaillé sur l'étude des services sur réseau actuellement mis
en production sur l'infrastructure Trinaps. Cela couvre bien entendu les
services essentiels à la fonction d'opérateur réseau, mais également les
services proposés comme option ou comme forfait qui exposent une
vraie plus-value au client Trinaps. Sans rentrer dans le détail, cela couvre du
service \emph{Web} (\emph{LAMP}\footnote{\emph{(GNU/)Linux Apache Mysql PHP} :
Dénote un serveur reposant sur des \emph{briques} logicielles \emph{Open Source}
fondamentales à un serveur Web moderne.}), \emph{Mail}, \emph{DNS}, \emph{DHCP},
\emph{monitoring}, stockage et partage de fichiers et sauvegardes. Cet ensemble
de services qui fait de Trinaps un fournisseur de services Internet moderne et
performant représenta un travail conséquent d'analyse et de recherche.

À l'issue de cette première tâche d'audit de l'existant, certaines conclusions
s'imposèrent, tout d'abord sur le plan matériel :

\begin{itemize}
  \setlength{\itemsep}{8pt}
  \item Il est \textbf{très peu probable} que l'ensemble d'une infrastructure
  réseau, même moderne, supporte IPv6. En effet, certains types d'appareils sont
  tout simplement exclus de l'innovation IPv6, apparemment par choix de leur
  constructeur (ex: téléphones IP).

  \item Les équipements réseau traitant les couches inférieures à la couche du
  protocole IP, c'est-à-dire les couches 2 (liaison de données) et 1 (physique),
  ne supportent pas IPv6, alors qu'ils sont administrables via IPv4. Cela
  concerne plus spécifiquement les commutateurs Ethernet et les point d'accès
  WiFi en mode  pont (ou \emph{bridge}). Seules les dernières générations de ce
  type d'équipement supportent IPv6 et certaines de manière peu fiable.

  \item Les équipements réseau spécifiques à la couche 3 (routeurs, pare-feu)
  offrent un meilleur support d'IPv6, mais qui manque de fraîcheur par rapport
  aux fonctionnalités les plus récentes. Heureusement, ce type d'appareil jouit
  généralement d'une très bonne durée de vie grâce à des mises à jour des
  systèmes embarqués qui corrigent les \emph{bugs} et apportent de nouvelles
  fonctionnalités.

  \item Le point précédent est cependant à nuancer par une remarque qui
  s'applique à la quasi totalité des équipementiers : Si le support IPv6 est
  correct sur quelques modèles, il faut noter que ceux-ci font partie de la
  gamme professionnelle, voire haut de gamme, de chaque marque. Le matériel dit
  \emph{grand public} ou \emph{Small Business} est complètement délaissé sur ce
  point là, avec souvent un unique modèle de chaque marque proposant un support
  minimaliste.
\end{itemize}

L'offre matérielle reste pauvre si le budget ou les besoins sont limités.
D'après les rumeurs planant autour des plus grands équipementiers réseau, cette
décennie verra le support IPv6 s'améliorer sur l'ensemble des modèles. Cela
serait bienvenu et pour le moment le scepticisme domine.

Sur le plan logiciel, à la fois sur les systèmes d'exploitation, serveur ou
bureautique, et les services déployés par dessus, les conclusions sont de
manière générale plus enthousiasmantes :

\begin{itemize}
  \setlength{\itemsep}{8pt}
  \item Les systèmes d'exploitation \emph{serveur} offrent un support d'IPv6
  tout à fait opérationnel, couvrant les fonctionnalités minimales voire
  avancées. Cela comprend les différentes distributions GNU/Linux reposant sur
  un noyau 2.6 et plus récent et Windows Server 2003 et plus récent.

  \item Les systèmes d'exploitation \emph{bureautiques} offrent pour la plupart
  d'entre eux un support de qualité. Seuls les plus anciens ne disposent pas
  d'un support IPv6, ou alors très minimaliste. Par exemple Windows XP, qui
  nécessite une activation \emph{manuelle} de la pile IPv6. Les versions plus
  récentes et les systèmes concurrents sont généralement à la pointe : support
  de   \emph{DHCPv6}, support de \emph{RDNSS}, etc.

  \item La grande majorité des services sur réseau supportent IPv6 depuis des
  années, grâce aux abstractions logicielles offertes par les bibliothèques de
  développement de chaque système d'exploitation ou d'un langage de
  programmation. Seuls quelques services intrinsèques à IPv4 (\emph{DHCP},
  \emph{Monitoring ARP}) sont logiquement incompatibles, ainsi que quelques
  logiciels dont le développement n'a malheureusement pas su tirer parti des
  abstractions logicielles.
\end{itemize}

En conclusion de cet audit, j'ai souligné l'importance de la notion de
transition. Les solutions les plus pragmatiques se situent en effet dans
l'exploitation simultanée du protocole IPv4 et IPv6, alors appelée \emph{double
pile} ou \emph{dual stack} en anglais. Sans cette cohabitation transitoire,
l'intégration et l'activation n'est ni économiquement fondée, ni réaliste
vis-à-vis des besoins des utilisateurs finaux qui veulent qu'un service donné
fonctionne, en étant totalement indifférent du protocole sous-jacent utilisé.

J'ai également réalisé pendant cette période que parler de \emph{migration IPv6}
était une erreur de sémiologie avec des conséquences graves. Cela laisse
entendre que l'on effectue une transition en remplaçant une version par une
autre. C'est donc tout à fait éloigné de l'approche retenue ici, la \emph{double
pile}. C'est un point qui peut paraître futile mais qui en réalité est subtil et
très pertinent : choisir les bons mots face à des décideurs qui n'iront pas
chercher la nuance est d'une importante primordiale. Et même entre (jeunes)
ingénieurs informaticiens le doute subsiste ! Il est à mon sens primordial
d'évoquer une \emph{\textbf{activation} ou une \textbf{intégration} du protocole
IPv6 au sein de l'infrastructure existante}.

\subsubsection{Documentation à tous les instants.}

De mes recherches préliminaires, j'ai rédigé un cahier des charges, plusieurs
pages de remarques ou de liens hypertexte sur le \emph{wiki} interne, etc. Ce
processus de documentation continu a accompagné toutes les étapes ultérieures
des phases de mon stage et les sous-sections suivantes présenteront certains de
ces documents. Cette démarche de documentation passe bien évidemment par la
recherche d'information ou tout simplement de la veille sur certains supports.

Voici mes principales sources d'information :

\begin{itemize}
  \setlength{\itemsep}{8pt}
  \item \textbf{Les RFC}, notamment via le site de l'\emph{IETF}
  (\url{http://www.ietf.org/rfc.html}) et la société australienne \emph{IPv6Now}
  (\url{http://www.ipv6now.com.au/RFC.php}).

  \item \textbf{Les \emph{mailing-lists} opérateurs} telles que \emph{NANOG},
  \emph{FRnOG} ou \emph{FRsAG}\footnote{Adresses web disponibles dans les
  références située à la page \pageref{references}.}.

  \item \textbf{Les sites d'équipementiers} tels que Cisco et Juniper, qui
  offrent souvent une documentation pragmatique concernant des points précis du
  protocole IPv6.

  \item \textbf{Le site du RIPE NCC} qui comprend une base documentaire fournie
  ainsi que de nombreuses informations pratiques et supports de formation.
\end{itemize}

\subsubsection{Laboratoire poussé.}

La conception, la mise en place et l'exploitation d'un laboratoire m'a permis de
valider les équipements et les protocoles mis en jeu dans l'infrastructure
opérateur de Trinaps. En effet, celle-ci repose en bordure de réseau sur des
routeurs \emph{Cisco 7206 Series}. Ces routeurs établissent les sessions BGP
avec les fournisseurs de transit, permettant la propagation des préfixes IP
alloués à Trinaps par le RIPE, ainsi que le trafic IP sortant et entrant de
l'\emph{AS}\footnote{\emph{Autonomous System} : entité indépendante disposant de
ses propres ressources IP allouées par le \emph{Regional Internet Registry}
(RIR) auquel il est rattaché. Il s'interconnecte au reste du réseau Internet via
des sessions BGP établies sur des liens avec d'autres AS.} Trinaps. Leur bon
fonctionnement avec IPv6 ne laissait planer aucun doute. Mais ce n'était pas
le cas des équipements de routage/filtrage principaux, situés à l'échelon
inférieur, derrière les Cisco 7206 : des \emph{Juniper SSG550M}. Ces
routeurs/pare-feu constituant le cœur de réseau de l'infrastructure opérateur
Trinaps et le routeur d'accès pour l'ensemble des clients présents sur le site
du Techn'hom, il était vital de valider des protocoles comme \emph{SLAAC} ou
\emph{DHCPv6} et bien entendu le filtrage IPv6 avec notamment une fine politique
concernant \emph{ICMPv6}\footnote{\emph{Internet Control Message Protocol
version 6} : Protocole d'information et de contrôle qui étend les
fonctionnalités d'ICMPv4 et devient une part essentielle d'IPv6. Définit deux
classes : \emph{error messages} et \emph{information messages}.} qui nécessite
une attention toute particulière par rapport à son homologue en IPv4.

Afin de pouvoir réaliser une maquette réaliste employant des préfixes IPv6
globaux, c'est-à-dire routables sur Internet, j'ai mis à contribution mon
infrastructure personnelle. En effet, Trinaps ne disposant pas encore de son
allocation IPv6 par le RIPE, j'ai utilisé un préfixe IPv6 disponible sur mon
serveur dédié OVH. Grâce à l'utilisation de liens VPN entre mon serveur dédié et
le routeur du laboratoire, j'ai pu déployer dans l'infrastructure de test
plusieurs réseaux de caractéristiques différentes :

\begin{itemize}
  \item \textbf{1} : Adressage manuel des hôtes clients.

  \item \textbf{2} : Adressage des hôtes clients via \emph{SLAAC}.

  \item \textbf{3} : Adressage des clients via \emph{DHCPv6} et \emph{SLAAC} en
  \emph{adressage avec état}.

  \item \textbf{4} : Adressage des clients via \emph{DHCPv6} et \emph{SLAAC} en
  \emph{adressage sans état}.
\end{itemize}

En toute logique, le réseau \textbf{1} et \textbf{2} fonctionnent sans soucis
notables avec des hôtes clients utilisant les systèmes d'exploitation
bureautiques majeurs.

Le réseau \textbf{3} offre la particularité d'utiliser \emph{DHCPv6} pour la
configuration de l'adressage et des paramètres DNS, mais dépend toujours de
\emph{SLAAC} pour annoncer la passerelle (ou route par défaut) aux hôtes
clients. Les résultats sont plutôt médiocres dû à un manque d'implémentations
standards de serveurs DHCPv6 et de support correct côté client. Il existe
certainement des implémentations serveurs fonctionnelles, mais celle intégrée au
système des \emph{Juniper SSG550M} ne l'est pas.

Le réseau \textbf{4} emploie \emph{DHCPv6} pour la configuration DNS, mais
dépend de \emph{SLAAC} pour l'adressage et pour la passerelle. Les résultats
sont identiques à ceux du réseau \textbf{3}.

Après une semaine de préparation et de mise en place et une semaine de tests et
de recherches pratiques, une conclusion allant dans le sens de la \emph{double
pile} s'imposa : \emph{RDNSS} et \emph{DHCPv6} n'étant pas fonctionnels dans
l'infrastructure actuelle, seuls \emph{SLAAC} et l'adressage manuel seront
utilisés. Les informations DNS seront alors récupérées via \emph{DHCPv4}. IPv4
reste clairement indispensable, au delà de l'accès aux ressources disponibles
uniquement en IPv4 sur Internet.

\subsection{Phase 2 - Plan d'adressage, Allocation RIPE, Interconnexion BGP}

Une fois validée la faisabilité du projet d'intégration du protocole IPv6 au
sein de Trinaps, nous avons rapidement déterminé que la rédaction d'un plan
d'adressage et la détermination d'un processus d'allocation de préfixes étaient
critiques. En effet, il était souhaité dès le début du stage d'être prêt pour
l'\emph{IPv6 Day} qui se tenait le 8 juin 2011.

\subsubsection{Plan d'adressage IPv6 : analyse et procédures}

En effet, d'ici ce jour, de nombreuses étapes étaient à franchir et
notamment la problématique de la répartition et du découpage du bloc IPv6 global
qui serait attribué à Trinaps. C'est là même l'essence de la difficulté à
appréhender IPv6 : là où IPv4 est une ressource rare et donc facilement
dénombrable, IPv6 introduit de nouvelles perspectives qui dépassent
l'entendement. En IPv4, on a tendance à découper un bloc de taille arbitraire en
blocs plus petits, dont les tailles respectives reflète les besoins des réseaux
et éventuellement leur possible évolution : c'est une approche
\textbf{conservatrice} (on cherche à économiser un maximum les ressources) et
disposant comme \textbf{unité de base l'adresse IPv4} (les blocs sont réfléchis
en terme de nombre d'adresses contenues). Techniquement, c'est ce que permet
l'utilisation de masques sous-réseau à longueur variable
(\emph{VLSM}\footnote{\emph{Variable Length Subnet Mask} : technique qui permet
l'utilisation de masques de sous-réseau de longueur différentes au sein d'un
même bloc parent. Ex: /24 = /25 + /26 + 2 * /27.}) et la méthode
\emph{CIDR}\footnote{\emph{Classless Inter Domain Routing} : Méthode
d'allocation IP reposant sur l'utilisation de masques de sous-réseau à longueur
variable et non plus fixe comme avec la notion de classe.}.

En IPv6, si l'approche de découpage d'un bloc en sous-blocs persiste, on ne
raisonne plus à l'échelle de l'adresse IP, mais à l'échelle d'un bloc de base
qui contient une quasi infinité d'adresses. Techniquement, on articule ce
découpage autour de la notion de bloc de taille \emph{minimale} pour que
l'autoconfiguration IPv6, ou \emph{StateLess Address AutoConfiguration (SLAAC)}
puisse fonctionner. Pour cela, sur 128 bits composant une adresse IPv6, les 64
derniers bits représentent alors l'identifiant unique d'interface utilisé par le
\emph{SLAAC}. On a donc un bloc réseau de longueur $128 - 64 = 64$ bits, soit
noté \textbf{/64} avec la notation \emph{CIDR}. Ainsi, un \textbf{sous-réseau
IPv6 \textbf{/64} est l'unité de base} du découpage ou \emph{subnetting} IPv6,
ce qui n'empêche pas de découper davantage, en \textbf{/126} par exemple. Mais
c'est véritablement le \textbf{/64} qui s'est imposé, permettant à une
allocation au standard RIPE de longueur \textbf{/32} de contenir $2^{64-32} = 4
294 967 296$ sous-réseaux \textbf{/64} de base, chacun comprenant $2^{64} = 18
446 744 073 709 551 616$ adresses disponibles. On comprend alors naturellement
que dimensionner une architecture avec l'adresse comme unité de base n'a aucun
sens.

C'est donc cette différence \textbf{fondamentale} entre les méthodes de
dimensionnement qu'il fut nécessaire de comprendre et d'intégrer dans toutes les
réflexions ultérieures. L'élaboration ou l'intégration à l'existant d'une
architecture réseau se simplifie alors : au lieu de se préoccuper du nombre de
machines par réseau, c'est à dire par segment
Ethernet/\emph{VLAN}\footnote{\emph{Virtual LAN} : Réseau informatique de niveau
physique indépendant uniquement défini par des directives logiques. Offre de
nombreux avantages de souplesse, segmentation et sécurité. Le protocole repose
sur le standard IEEE 802.1Q.}, il suffit tout simplement de compter le nombre de
réseaux à approvisionner en IPv6. Par exemple, si un client dispose d'une
infrastructure réseau basée sur 5 VLANs internes et un segment externe pour
l'interconnexion avec l'infrastructure Trinaps, il suffira alors d'un
\textbf{/64} d'interconnexion et de router à travers celui ci un bloc IPv6 de
\textbf{/61} afin que le client puisse déployer en interne jusqu'à $2^{64-61} =
8$ sous-réseaux \textbf{/64}, lui offrant alors une marge d'extension de 3
réseaux supplémentaires.

Mais là encore, il n'est pas nécessaire d'allouer et de router à un client
Trinaps donné un préfixe d'une longueur conservatrice. Considérant la
\textbf{taille gigantesque d'un bloc IPv6 /32}, il est plus simple, efficace et
pérenne de déterminer une \textbf{taille de préfixe standard} selon la nature du
client (PME, Grand groupe), de son réseau via un audit et selon la technologie
de raccordement (\emph{xDSL}, fibre optique, Ethernet sur RJ45 cuivre, etc).
Sauf qu'au delà de l'allocation au client final, comment organiser le découpage
d'une telle ampleur pour refléter au mieux à la fois l'architecture Trinaps dans
son état actuel mais également dans des états futurs ? Comment prévoir à la fois
une augmentation du nombre de clients et à la fois du nombre de
sites\footnote{appelés \emph{PoP} comme \emph{Point of Presence}.} sur
lesquels l'infrastructure opérateur Trinaps sera présente ?

En considérant la taille et l'ambition de Trinaps, Gauthier et moi-même avons
déterminé les besoins actuels et à venir en terme de \emph{rôles} ou fonctions.
La définition d'un rôle repose sur la technologie d'interconnexion employée par
l'extrémité client. Cette extrémité, reliée à l'infrastructure opérateur,
correspond concrètement à un appareil situé en bordure de l'infrastructure du
client. Dans le cas des clients présents sur le Techn'hom, il s'agit dans
certains cas d'un routeur appartenant au client ou encore directement les
machines bureautiques du client (Trinaps fournit alors une connectivité de type
LAN RJ45). Pour les clients situés en dehors du Techn'hom , il s'agit d'un
équipement de type routeur/pare-feu connu sous l'appellation générique
\emph{CPE}\footnote{\emph{Customer Premises Equipment} : Équipement qui
se trouve dans le site d'un client (d'une entreprise) et qui est raccordé à
l'infrastructure d'un opérateur, dans un \emph{Point Of Presence (POP)}, via une
boucle locale.}. Il est alors aisé de mettre en avant les différentes
technologies et protocoles de raccordement :

\begin{description}
  \item [Fibre Optique]

  \item [ADSL Professionnels]

  \item [ADSL Particuliers]

  \item [SDSL]

  \item [RJ45]

  \item [WiFiMax]
\end{description}

À cette liste s'ajoute la notion de \textbf{PoP} ou point de présence de
l'opérateur. Ce terme désigne à la fois un emplacement géographique et une
infrastructure opérateur autonome, souvent avec des interconnexions BGP avec
d'autres opérateurs réseau. Actuellement Trinaps dispose d'un unique PoP situé
au cœur du Techn'hom, mais le nombre de PoP peut augmenter d'ici quelques
années. Nous y avons également ajouté la notion de
\textbf{TOIP}\footnote{\emph{Telephony over IP}.} qui concerne les clients
disposant d'un abonnement mettant en œuvre des solutions de téléphonie sur IP.

Nous disposons donc d'un total de \textbf{huit rôles} qui sont utilisés dans
la méthode de répartition de préfixes IPv6. La méthode retenue à Trinaps répond
à plusieurs critères : 

\begin{itemize}
  \setlength{\itemsep}{8pt}
  \item Elle doit être adaptée à la structure de Trinaps. Partir d'une méthode
  générique ou éprouvée par un autre opérateur a peu de chances d'être adaptée.

  \item Elle doit simplifier au plus possible le processus d'allocation d'un
  préfixe IPv6, même si celui si est de longueur (taille) différente selon les
  besoins.

  \item Idéalement, elle doit permettre une représentation graphique la plus
  claire possible tout en exposant un degré de détail suffisant.

  \item Elle doit permettre une segmentation selon les rôles précédemment
  définis tout en laissant une marge de manœuvre pour agrandir un rôle ou
  ajouter un ou plusieurs rôle.
\end{itemize}

Pour comprendre la méthode d'allocation, considérons pour cela le préfixe
\textbf{32} IPv6 \verb+2001:db8::/32+\footnote{Ce préfixe est réservé à
des fins de documentation dans la \emph{RFC 3849}.} Un \textbf{/32} est la
taille de préfixe alloué par le RIPE aux \emph{LIR} qui le demandent. C'est donc
un préfixe équivalent qui fut alloué à Trinaps et qu'il a fallu découper.

J'ai alors développé une méthode d'allocation graphique à partir d'un simple
feuillet de tableur de suite bureautique. Cette méthode s'appuie sur une unité
de base à l'échelle d'un opérateur réseau dont la taille semblait la plus
adaptée dans le cadre de Trinaps. Cette unité de base est le \textbf{/40}, qui
contient $2^{24} = 16777216$ \textbf{/64}. Si l'on considère
\verb+2001:db8::/32+ découpé en préfixes \textbf{/40}, on obtient alors, chaque
symbole représentant 4 bits du préfixe, la forme suivante avec \verb+X+ et
\verb+Y+ variants de 0 à f :

\begin{center} \Huge \verb+2001:db8:XY00::/40+ \end{center}

\clearpage

On définit ensuite un découpage d'un préfixe \textbf{/32} initial en 256
préfixes \textbf{/40}. On groupe alors ces préfixes de quatre en quatre afin de
former la matrice de la page suivante. Celle ci est agencée selon des symétries
axiales successives, afin de passer d'un groupe de quatre \textbf{/40} au groupe
consécutif en passant dans le quartier suivant dans l'ordre des aiguilles d'une
montre. Cette affectation parait complexe, mais la lecture de la matrice en
facilite la compréhension. Cette matrice est donc composée de 64 \emph{cases},
individuellement allouables à un des 8 rôles actuellement définis.

Le compromis entre favoriser la taille de chaque rôle et le nombre de rôles
disponibles fut débattu et se compose actuellement de :

\begin{itemize}
  \item Un ensemble de $4*4$ préfixes de \textbf{/40} par \textbf{rôle}.

  \item Donc avec \textbf{8 rôles}, un total de $4*4*8 = 128$ préfixes
  \textbf{/40} sur 256.

  \item Il reste donc la moitié du potentiel disponible pour expansion d'un ou
  plusieurs rôles ou le développement de nouveaux rôles.
\end{itemize}

En tant que fournisseur d'accès à Internet dédié aux professionnels, une
problématique très importante est celle du dimensionnement client : que doit-on
offrir comme taille de préfixe à un client donné sans que cela constitue un
gaspillage trop important de part et d'autre, tout en couvrant les besoins
actuels et futurs de ce client ? Certains fournisseurs prennent le parti du
\textbf{/48} \emph{pour tous}. Mais pour la grande majorité des clients Trinaps,
cela représenterait un gaspillage immense : \emph{65536} \textbf{/64} à
disposition, alors qu'une majorité des clients n'ont besoin que d'un voire
quatre \textbf{/64}. Mais il n'est pas judicieux de fournir un petit préfixe tel
qu'un \textbf{/62} car les concurrents proposent plus même si le besoin est
inexistant.

Nous avons donc retenu les tailles de préfixes suivantes :

\begin{itemize}
  \item \textbf{/44} : PoP.

  \item \textbf{/48} : Fibre, SDSL, clients RJ45 avec CPE.

  \item \textbf{/56} : ADSL Pro et particuliers, clients RJ45 directs (alloué).

  \item \textbf{/64} : Réseaux internes Trinaps, clients RJ45 directs (routé).
\end{itemize}

Pour les clients RJ45 dit \emph{directs}, un préfixe \textbf{/56} est alloué,
mais seul le premier \textbf{/64} est déployé. Cela permet à une entreprise qui
évolue de pouvoir changer de contrat sans causer une renumérotation obligatoire
en cas de changement de préfixe IPv6.

Le résultat de ce travail de réflexion et de conception se trouve à la page
suivante.

\clearpage

\begin{bigcenter}
\begin{figure}[h!]
\centering{\includegraphics[scale=0.45]{allocation}}
\centering{\includegraphics[scale=0.45]{legende}}
\caption{Méthode d'allocation IPv6 par groupage de quatre \textbf{/40}}
\label{pic:allocation}
\end{figure}
\begin{small}
\begin{description}
  \item [\textbf{Note :}] Les labels sont en anglais car ce document était
  initialement prévu pour le RIPE afin de justicier l'allocation d'un préfixe
  IPv6.
\end{description}
\end{small}
\end{bigcenter}

\clearpage

\subsubsection{Allocation RIPE et Interconnexion BGP}

Une fois la méthode d'allocation de préfixe IPv6 déterminée et validée, nous
avons cherché à produire une version préliminaire de plan d'adressage détaillé
du Techn'hom, en considérant selon la méthode les préfixes suivants :

\begin{itemize}
  \setlength{\itemsep}{8pt}
  \item \textbf{/44}, soit 1048576 \textbf{/64}, pour l'organisation du
  \emph{Point of Presence} : Interconnexions, passerelles, interfaces des
  équipements réseaux, etc.

  \item \textbf{/40}, soit 65536 \textbf{/56} ou 256 \textbf{/48}, pour les
  clients présents sur le Techn'hom.

  \item \textbf{/40}, soit 256 \textbf{/48}, pour les clients SDSL rattachés au
  \emph{PoP} du Techn'hom.
\end{itemize}

Cette ébauche de plan d'adressage est destinée à convaincre le RIPE NCC
d'accorder à l'\emph{AS} Trinaps une allocation de préfixe IPv6 (à ce jour un
\textbf{/32}). Pour cela, il est nécessaire de se connecter et de s'identifier
sur le site du RIPE NCC afin de saisir un formulaire de demande d'allocation. La
procédure est accessible à partir du portail \emph{LIR services}, section IPv6 :
\url{http://www.ripe.net/lir-services/resource-management/ipv6}.

Dans le cas de Trinaps, nous avons obtenu très rapidement l'allocation d'un
préfixe \textbf{/32}, moins d'une heure après soumission du formulaire ! Voici
le préfixe obtenu :

\begin{center} \Huge 2a00:ab80::/32 \end{center}

Une fois obtenu le préfixe, Trinaps, en accord avec son fournisseur de transit,
a préparé et initialisé les sessions \emph{eBGP}\footnote{\emph{exterior BGP} :
Session sur deux routeurs implémentant le protocole \emph{Border Gateway
Protocol} entre deux \emph{AS} différents.} sur ses routeurs de bordure. Une
fois l'interconnexion fonctionnelle au niveau IPv6, le protocole BGP a permis
l'annonce du préfixe alloué à Trinaps au fournisseur de transit qui retransmet
l'annonce à tous ses voisins BGP et ainsi de suite : Trinaps venait d'apparaître
sur l'Internet nouvelle génération !

\subsection{Phase 3 - Déploiement interne, \emph{IPv6 Day}, Tests en ligne}

Un des objectifs secondaires les plus marquants était de parvenir à une
connectivité IPv6 et l'annonce du site \url{www.trinaps.com} en IPv6 pour le 8
juin 2011, soit l'\emph{IPv6 World Day}. Pour cela, nous avons procédé à un
déploiement minimaliste sur le réseau interne de Trinaps, celui utilisé
quotidiennement par son personnel. Le serveur web hébergeant le site de Trinaps
disposant d'une interface réseau au sein de ce même réseau, nous pouvions à la
fois permettre au clients de ce LAN de profiter d'une connectivité IPv6 et de
servir le site de Trinaps en IPv6. Le déploiement en lui même fut simple :

\begin{itemize}
  \setlength{\itemsep}{8pt}
  \item On alloue un \emph{/64} d'interconnexion entre les routeurs de bordure
  et les routeurs/pare-feu de cœur. N'ayant pas besoin d'autoconfiguration, nous
  configurons en réalité les interfaces avec un \textbf{/126} appartenant au
  \textbf{/64} alloué\footnote{Et c'est une pratique très courante pour
  simplifier la gestion IPv6 dans le système d'information. Ainsi, on alloue
  (réserve) un \textbf{/64} comme plus petit préfixe, tout en déployant
  uniquement un ou des sous-préfixes.}.
  Cela permet de réduire la surface d'attaque par épuisement du cache
  NDP\footnote{(\emph{NDP flooding} avec $2^{64}$ entrées possibles par
  \textbf{/64} ce qui dépasse de très loin la taille du \emph{neighbor cache}
  des équipements actuels.)}.

  \item Allocation et déploiement d'un \textbf{/64} avec annonce par le routeur
  de messages d'autoconfiguration pour le LAN Trinaps. Les systèmes compatibles
  IPv6 et supportant l'autoconfiguration récupèrent ainsi le préfixe  réseau et
  la passerelle, puis achèvent l'autoadressage.

  \item Sur l'interface du serveur Trinaps, on déclare manuellement une IPv6
  statique : \\\verb+2a00:ab80:0:2::1337+.

  \item Contrôle des politiques de filtrage sur les routeurs/pare-feu, en
  prenant un soin particulier concernant le filtrage de l'\emph{ICMPv6}.

  \item Enfin, sur les serveurs de noms, dans le fichier de la zone DNS
  \emph{trinaps.com}, on déclare les enregistrements suivants :\\
  \verb+@   IN  AAAA  2a00:ab80:0:2::1337+\\
  \verb+www IN  AAAA  2a00:ab80:0:2::1337+

  \item \url{http://www.trinaps.com} est alors accessible en IPv6 !
\end{itemize}

Pendant l'\emph{IPv6 World Day}, nous avons utilisé principalement deux sites
web pour valider la bonne connectivité IPv6 au reste du monde :

\begin{itemize}
  \setlength{\itemsep}{8pt}
  \item \textbf{ipv6-test.com} qui propose un test basique et très clair,
  ainsi qu'un test comparatif de débit et de latence entre IPv4 et IPv6.

  \item \textbf{test-ipv6.com} qui propose un bilan très poussé et attribue une
  note sur 10 selon les résultats déterminés par l'exécution d'une suite de
  tests.

  \item D'autres sites comme \url{http://netalyzr.icsi.berkeley.edu/index.html}
  proposent des diagnostics de connectivité très poussés et ne se limitant pas à
  IPv6, mais qui offrent des conclusions très intéressantes.
\end{itemize}

Mais j'ai également développé pour Trinaps un petit module de test en \emph{PHP}
déployé sur la page \url{http://www.trinaps.com/IPv6/}. Le code source est
confidentiel, mais repose simplement sur des variables \emph{PHP} comme
\verb+$_SERVER['REMOTE_ADDR']+.

Au bilan de cette journée particulièrement importante pour le déploiement global
d'IPv6 et sa prise de conscience, le site \url{trinaps.com} a répondu à 1713
requêtes HTTP, correspondant à 7 adresses provenant de Free SAS, 33 adresses du
préfixe Trinaps et 5 adresses provenant de mon préfixe IPv6 Hurricane Electric.
C'est un bilan très positif pour la société Trinaps.

\subsection{Phase 4 - Industrialisation, Déploiement client}

Cette phase, bien qu'initialement prévue dans le planning prévisionnel du
projet, n'est actuellement qu'à un stade préliminaire, d'autres problématiques
s'étant imposées au fur et à mesure de mon avancement. C'est d'ailleurs à ces
problématiques secondaires qu'est dédié le chapitre suivant.

