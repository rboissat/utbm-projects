#!/bin/bash
# Watch NetDot bind9 export directory.
# Reload DNS zones if any SOA serial is updated.
#
# Copyright (C) 2011 Romain Boissat <r.boissat@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

ZONEDIR=/opt/netdot/export/bind
MAILADDR=root

get_serial() {
  stats=$(for f in $(ls -x $1); do
            grep $(date +%Y) $1/$f | awk '{print $1}'
          done)

  echo -e $stats
}

reload_zones() {
  $(which rndc) reload
  if [ $? -ne 0 ]; then
    echo "FAILED: 'rndc reload'" | mail -s "$(basename $0)" $MAILADDR
  fi
}

# First, try to reload zones.
reload_zones

# Then monitor zone files.
while true
do
  timestamp0=$(get_serial $ZONEDIR)
  sleep 10
  timestamp1=$(get_serial $ZONEDIR)

  if [ "$timestamp0" != "$timestamp1" ]; then reload_zones; fi
done
