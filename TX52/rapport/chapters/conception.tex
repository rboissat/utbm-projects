Dans ce chapitre sont abordées les différentes techniques et méthodes que j'ai
employées lors de la première phase de travail sur ce projet, à savoir une phase
de conception. L'objectif de cette phase était de parvenir à produire une à
plusieurs solutions possibles pour implémenter mon réseau personnel, au moyen
d'un système de documentation permettant de consigner toutes mes notes de
travail, mais également de constituer une base de connaissances vivante pour la
gestion de l'ensemble du système d'information, au niveau réseau comme services,
ou encore la gestion du matériel déployé ou disponible.

J'ai ainsi utilisé mon wiki personnel comme système de documentation,
car le système de wiki offre d'intéressantes possibilités en matière d'édition
et d'historisation de l'information. Une fois les outils en place, j'ai essayé
d'assurer la phase de conception un cycle composé de plusieurs étapes :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item{Conception à partir des connaissances de départ.}
  \item{Apprentissage de méthodes et techniques dans des ouvrages spécialisés et
  sur Internet.}
  \item{Intégration des connaissances apprises tout au long de la phase de
  conception.}
\end{itemize}

Cette approche m'a permis de consacrer plusieurs semaines assez efficaces,
puisque je suis parvenu à intégrer de nouvelles connaissances au sein de la
conception, tout en remettant régulièrement en question les choix effectués.
Cette période d'environ 4 semaines déboucha sur les résultats exposés dans les
sections suivantes.

\clearpage

\section{Objectifs}

Ce projet de TX est justifié par un déménagement et une acquisition de matériel,
ainsi que de la volonté de perfectionner mes connaissances de conception,
déploiement et sécurisation d'un réseau informatique. Avant d'évoquer les
méthodes employées, cette section va présenter les besoins que devra remplir
la prochaine infrastructure :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item{Permettre d'accueillir une quinzaine de machines.}

  \item{Sécuriser chaque partie du réseau selon sa tâche (LAN, DMZ pour les 
  serveurs, zone d'administration des équipement réseau.)}

  \item{Rendre l'isolation précédente évolutive, afin de s'adapter aux
  changements de topologie ou du nombre de machines et/ou de leur fonction.}

  \item{Exploiter les possibilités du matériel.}
\end{itemize}

Ces besoins semblent peu complexes, et pourtant les solutions
techniques correspondantes sont rarement employées dans des réseaux
informatiques domestiques. En effet, dans la grande majorité des cas, un réseau
informatique domestique n'est constitué que d'une seule entité de couche en
couche :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item{Un unique segment réseau de couche 2 (liaison) avec un voire quelques
  commutateurs Ethernet.}

  \item{Un simple sous-réseau IP de couche 3 (réseau), bien souvent reposant sur
  un adressage depuis une adresse réseau  classe C\footnote{Ou plutôt de
  notation \emph{CIDR}\footnote{\emph{Classless Inter-Domain Routing.}}
  /24, la notion de classe (ou \emph{classful networks})
  étant obsolète depuis longtemps.} comme 192.168.1.0/24.}

  \item{Un simple serveur \emph{DHCP}\footnote{\emph{Dynamic Host Configuration
  Protocol} : protocole permettant gérer l'autoconfiguration réseau de machines
  clientes (réponses aux requêtes, émission de baux, etc).}
  l'autoconfiguration réseau des postes clients.}
\end{itemize}

Il apparait alors évident que des solutions techniques plus poussées sont
requises par la future infrastructure pour répondre correctement à chaque
besoin.

\clearpage

\section{Approche technique}
Cette section va présenter les choix techniques de départ qui vont influencer
le reste du processus de conception. Ces choix ne sont pas très développés dans
le détail, mais permettent plutôt de poser les limites et contraintes dans une
vision plus technique qu'une liste de besoins utilisateur.

Disposant de nombreuses machines aux rôles et à l'exposition bien différentes,
il est nécessaire de trouver une solution permettant une isolation aisée des
ressources sensibles, tout en facilitant l'accès aux ressources publiques.
Ce même principe d'isolation permet également de regrouper les machines selon
leur rôle, et ainsi créer des groupes de machines capables de communiquer entre
elles librement, mais dont les communications avec d'autre groupes sont
contrôlées.

Ainsi, j'ai estimé mes besoins à 4 groupes distincts :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item{\textbf{WAN} : Zone d'interconnexion entre le réseau interne et
  Internet. De cette zone, tout peut sortir, mais l'entrée sera filtrée
  avec soin.}

  \item{\textbf{ADM} : Zone regroupant les équipements réseau principaux, comme
  le ou les routeurs, commutateurs Ethernet, etc. Cette zone aura accès à toutes
  les autres, mais personne n'aura accès à elle.}

  \item{\textbf{DMZ} : Cette zone sera ouverte pour toutes les communications
  entrantes, mais les communications sortantes ne seront ouvertes que vers le
  WAN. Cette zone regroupe les serveurs qui sont exposés sur Internet, il faut
  dont la cloisonner pour éviter les intrusions sur le reste du réseau interne
  en cas de serveur compromis.}

  \item{\textbf{LAN} : Cette zone regroupe les postes bureautiques et les
  ordinateurs portables dans un contexte utilisateur classique. En entrant, seul
  l'ADM sera autorisé. En sortant, le WAN et la DMZ seront autorisés.}
\end{itemize}

Les communications inter-groupes devront suivrent les règles suivantes à
quelques exceptions près :
\begin{verbatim}
    ADM -> WAN  : OK      ADM -> LAN  : OK
    LAN -> WAN  : OK      DMZ -> WAN  : OK

    *   -> DMZ  : OK
    *   -> *    : NOK
\end{verbatim}

Désormais, il est nécessaire de répartir les ressources matérielles et
logicielles.

\clearpage

\section{Matériel à disposition}

Cette section présente l'ensemble du matériel disponible, aussi bien les
équipements réseau qui seront déployés dans l'infrastructure, que les machines
clientes courantes qui utiliseront cette infrastructure.

Au niveau des équipements réseau, les noms de machines sont fournis selon le
format (\emph{long hostname}|\emph{short hostname}), le premier étant le nom de
la machine au niveau du système d'exploitation, l'autre étant le nom DNS.

En voici la liste :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item{\textbf{Commutateurs Ethernet} : Un Cisco Catalyst WS-C2924-XL
  (c2924|c2924)
  de 24 ports Fast Ethernet. Deux Cisco \emph{Small Business} SLM2008
  (slm|slm) de 8 ports Gigabit Ethernet.
  Ils sont administrables via une interface texte ou web.
  Quelques commutateurs Fast Ethernet basiques.}

  \item{\textbf{Routeur/Serveur de services (latex|la)} : Serveur
  \emph{home-made} basé sur une carte-mère au format mini-itx, processeur Via
  C7, 512 MB de RAM. Une carte fille permet de totaliser 4 interfaces réseau,
  une Fast Ethernet et trois Gigabit Ethernet. Le routeur fonctionne sous Debian
  GNU/Linux 5.0 Lenny, la version stable à ce jour.}

  \item{\textbf{Point d'accès WiFi (niluje|ni)} : Un Cisco WAP 4410n proposant
  une connectivité à haut débit, plus la possibilité de créer plusieurs points
  d'accès virtuels. L'interface WiFi du modem/routeur du FAI est inutilisée et
  désactivée.}

  \item{\textbf{Modem} : Fourni par le Fournisseur d'Accès à Internet. En
  l'occurence, il s'agit d'une Freebox HD, configurée en mode modem et non
  modem/routeur.}

  \item{\textbf{Bâti} : Armoire réseau demi-profondeur de 9U permettant de
  \emph{racker} tout le matériel réseau au sein d'un espace compact mais aéré.}
\end{itemize}

Au niveau des machines clientes :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item{\textbf{Serveur multi-rôles (pandora|pa)} : Serveur basé sur une carte
  mère au format mini-itx, processeur Intel Atom, 1GB de RAM. Expose de nombreux
  services comme un serveur web, rsync, nut (onduleur), ntp (synchronisation de
  temps), nfs (montages réseau), samba (montages réseau Windows), serveur de
  streaming audio, distcc (service de compilation C/C++ distribué), etc.}

  \item{\textbf{NAS\footnote{\emph{Network Attached Storage} :
  Appareil qui expose une capacité de stockage sur un réseau, via un ensemble de
  protocoles réseau comme samba, nfs, http, ftp, rsync...} (bender|be)} :
  Stockage réseau destiné à recueillir les backups quotidiens de tous les
  systèmes, ainsi que des données personnelles peu critiques (pas de backup).}

  \item{\textbf{Client léger (nora|no)} : Format compact, basé sur un processeur
  VIA C3. Du même type que les terminaux des salles de TP de l'UTBM, cet
  appareil démarre sur une image système distribuée sur le réseau par le
  serveur multi-rôle. Les protocoles impliqués sont DHCP/BOOTP, PXE et NFS
  version 3. Cette machine est actuellement déployée comme jukebox audio.}

  \item{\textbf{Ordinateurs portables} : Ordinateurs personnels et visiteurs.}
\end{itemize}

\clearpage


\section{Méthodes}
Ayant déjà eu quelques expériences de conception de réseaux informatiques dans
le passé, je désirais intégrer de nouvelles méthodes éprouvées dans le monde
professionnel notamment sur l'allocation d'adresses IP. En effet, si on désire
des réseaux logiques isolés pour des besoins de sécurité, la plus simple des
solutions consiste à utiliser autant d'adresses réseau de classe C, ce qui est
largement suffisant dans une grande majorité des cas d'utilisation, puisque
chaque sous réseau dispose de 254 adresses IP disponibles.

Cela dit, cela engage bien souvent un important gaspillage, ainsi que des
performances moindres (domaine de diffusion important), et une sécurité plus
difficile à assurer, notamment concernant les tempêtes de diffusion. De plus, la
gestion est plus complexe.

Une autre solution consiste à n'utiliser qu'une unique adresse réseau de classe
C par site géographique, et d'utiliser des sous-réseaux CIDR pour obtenir
l'isolation nécessaire. Bien entendu, chaque fois qu'un nouveau sous-réseau est
alloué, cela diminue le nombre d'adresses disponibles au sein de cette adresse
réseau globale. Il convient donc de déterminer une méthode d'allocation
permettant de répondre à des besoins quantitatifs bien définis, tout en
conservant une certaine évolutibilité.

Parmis ces méthodes, l'une des premières abordées est l'allocation séquentielle :

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=16cm]{./images/ip-seq-alloc}
  \caption{Allocation séquentielle.}
  \end{center}
\end{figure}

\clearpage

Les limites de cette méthode apparaissent alors évidentes : que fait-on si le
sous-réseau \textbf{192.168.1.0/28}, qui permet d'allouer 14 adresses IP
utilisables ($2^{32-28} = 16$ IP - 2 IP [réseau et diffusion]), a
besoin d'être agrandi ? Même en passant des heures à évaluer les besoins,
ceux-ci peuvent évoluer et cette méthode est donc inefficace sur le plan de
l'évolutibilité, et ne remplit que la moitié des objectifs de conception.

Les autres méthodes étudiées sont au nombre de deux : la méthode par dichotomie,
celle que j'ai utilisée, et la méthode d'allocation binaire inverse,
conseillée par Cisco. Je vais exposer rapidement la seconde puis détailler la
méthode par dichotomie, que j'ai trouvée plus simple et efficace.

\subsection*{Allocation binaire inverse}
Dans cette méthode, les sous-réseaux sont alloués en comptant en binaire, mais
en inversant le poids des bits (\emph{MSB first to LSB first}). Le passage en
décimal nous fournit alors la valeur du 3\ieme \, octet de l'adresse IP du
sous-réseau à allouer (l'adresse IP est alors exprimée en décimal pointé).
Il suffit alors d'allouer les sous-réseaux dans l'ordre obtenu
(192.168.128.0/28, puis 192.168.64.0/28, etc).

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=16cm]{./images/ip-reversebin-alloc}
  \caption{Calcul de la valeur du 3\ieme \, octet de l'adresse de sous réseau.}
  \end{center}
\end{figure}

On alloue alors les sous-réseaux désirés, tout en conservant une bonne marge
d'évolution, puisque non-adjacents. Pour agrandir l'un des sous-réseaux, il
suffit tout simplement de passer son masque CIDR de /28 à /27. On passe alors de
14 adresses IP utilisables à 30.
La méthode par dichotomie est encore plus simple, tout en conservant une
souplesse très intéressante. L'idée est de conserver à tout moment le plus grand
bloc d'adresses disponibles en divisant par deux le plus petit à chaque fois
qu'un nouveau sous-réseau est alloué. Chaque sous-réseau alloué peut alors être
simplement agrandi, puisqu'il y a le sous-réseau adjacent de taille identique
est libre.

\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=14cm]{./images/ip-reversebin-alloc2}
  \caption{Allocation binaire inverse.}
  \end{center}
\end{figure}
\begin{figure}[ht]
  \begin{center}
  \includegraphics[width=14cm]{./images/ip-divbyhalf-alloc}
  \caption{Allocation par dichotomie.}
  \end{center}
\end{figure}

\clearpage

\section{Résultats}
Rappelons rapidement les besoins initiaux : Il s'agit d'allouer plusieurs
sous-réseaux, chacun au dessus d'un VLAN particulier. Chaque sous-réseau sera
spécialement dimensionné en fonction de son usage final et du nombre actuel de
machines à interconnecter. De plus, il ne faut pas oublier qu'une machine peut
disposer de plusieurs interfaces réseau (portables en particulier, serveurs),
et qu'il y aura par sous-réseau une IP réservée à la passerelle; il faut
également prendre en compte des invités éventuellement nombreux, comme lors de
réunions de projet, ou des parties de jeu en réseau.\\
Voici le plan d'adressage actuellement employé au sein de l'infrastructure :

\begin{figure}[ht]
\hspace{-2cm}
  \includegraphics[width=20cm]{./images/ip-final-alloc}
  \caption{Allocation finale.}
\end{figure}

\begin{figure}[ht]
\hspace{-1cm}
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.25mm}
  \fbox{\includegraphics[width=18cm]{./images/tx52-layer3-general-view}}
  \caption{Vue générale de la couche de réseau (L3).}
\end{figure}

\clearpage

Concernant l'interconnexion au niveau de la couche de liaison (L2), c'est le
Cisco Catalyst qui sera déployé comme commutateur Ethernet de cœur de réseau, et
le ou les Cisco Small Business SLM2008 en commutateurs Ethernet d'accès (ou
distribution). Chaque sous-réseau IP précédemment alloué se voit attribuer un
VLAN particulier. On obtient ainsi des sous-réseaux de couche L3 isolés les un
des autres au niveau de la couche L2. Leur interconnexion se déroulera donc
obligatoirement au niveau de la couche réseau via le routage des paquets IP,
grâce au \emph{packet forwarding} du noyau Linux du routeur et aux règles de
filtrage \emph{IPTables} qui seront exposées dans la section \ref{conf:net:L3}
page \pageref{conf:net:L3}.

La figure suivante présente l'allocation des ports physiques sur le
Catalyst, ainsi que certains détails de configuration qui seront explicités dans
la section \ref{conf:net:L2} page \pageref{conf:net:L3}.

\begin{figure}[ht]
\hspace{-1cm}
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.25mm}
  \fbox{\includegraphics[width=18cm]{./images/tx52-layer2-general-view}}
  \caption{Vue générale de la couche de liaison (L2).}
\end{figure}
