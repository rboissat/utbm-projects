Ce chapitre s'articule autour de la configuration et de la mise en production de
l'ensemble des équipements cités dans le chapitre précédent. Ce projet de TX
s'axant tout particulièrement sur la configuration réseau des équipements, seuls
quelques fichiers spécifiques seront détaillés lorsque l'équipement n'est pas
dédié au réseau, comme le client léger ou le serveur par exemple. Pour les
équipements dédiés, les aspects importants de leur configuration seront
explicités dans ce chapitre, et les fichiers de configuration seront fournis
dans leur intégralité en annexe.\\
\emph{Remarque importante} : seuls les extraits pertinents de la configuration
seront inclus dans ce rapport, et non les commandes à saisir pour parvenir à ce
résultat. En effet, j'ai considéré que l'utilisation d'IOS n'était pas
pertinente pour la TX, mais uniquement la configuration.

\section{Couche de liaison - L2}
\label{conf:net:L2}

Cette section présentera des extraits des configurations du commutateur
Ethernet de cœur, le Cisco Catalyst WS-2924-XL, selon deux composantes :
\textbf{Connectivité} et \textbf{Sécurité}. Dans un troisième temps,
je présenterai quelques tests pratiques afin de démontrer l'efficacité
de certaines directives.

\subsection{Connectivité et Sécurité}

On considère que l'utilisateur a déjà effectué la configuration de base du
commutateur. Celui-ci est alors configuré avec un mot de passe console et
telnet, toutefois sans demande de nom d'utilisateur. De plus le mot de passe est
stocké en clair. Ces paramètres seront corrigés par la suite en activant le
service de chiffrement (directive \texttt{service password-encryption}), et sera
renforcée via l'activation et l'utilisation exclusive du système
AAA\footnote{\emph{Authentication, Authorization Accounting} : acronyme souvent
employé pour qualifier les systèmes de gestion de droits d'accès qui permettent
d'identifier un utilisateur, de lui conférer des droits particuliers, et
d'assurer une traçabilité des actions de cet utilisateur.} intégré à
IOS grâce aux directives suivantes :

\begin{itemize}
  \item{\texttt{aaa new-model}}
  \item{\texttt{aaa authentication login default local}}
  \item{\texttt{aaa authorization exec default local}}
  \item{\texttt{username <USER> privilege <PRIV\_LEVEL> password 7 <PASSWORD>}}
\end{itemize}

Pour configurer le commutateur comme équipement de cœur de réseau, il faut
configurer la base contenant les informations sur les VLANs, puis configurer
l'interface et les services d'administration, et enfin configurer les ports
physiques.

\clearpage

\begin{small}
\begin{lstlisting}
c2924#sh vlan brief
VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active
2    WAN                              active
3    LAN                              active    Fa0/5, Fa0/7, Fa0/8, Fa0/9,
                                                Fa0/10, Fa0/11, Fa0/12, Fa0/13,
                                                Fa0/14, Fa0/15, Fa0/16, Fa0/17,
                                                Fa0/18, Fa0/19, Fa0/20, Fa0/21,
                                                Fa0/22, Fa0/23
4    DMZ                              active    Fa0/3
42   ADM                              active    Fa0/24
100  TV                               active
1002 fddi-default                     active
1003 token-ring-default               active
1004 fddinet-default                  active
1005 trnet-default                    active
\end{lstlisting}
\end{small}

Parmi les VLANs de la table ci-dessus, seuls les VLANs 2 (WAN), 3 (LAN), 4
(DMZ), 42 (ADM) et 100 (TV) furent créés. Les autres sont les VLANs par défaut
que définit Cisco, notamment en fonction du protocole de liaison (Token Ring,
FDDI, etc). Pour Ethernet, c'est le VLAN 1, qui est également par défaut le VLAN
d'administration, c'est-à-dire par lequel l'administrateur se connecte en telnet
sur le commutateur.

Les ports physiques listés par VLAN sont les ports \emph{natifs} à ce VLAN,
c'est à dire qu'un client qui se connecte sur le port Fa0/5 n'a pas besoin
d'insérer les champs spécifiques au VLAN 3 pour établir une connectivité, il n'a
même pas conscience d'être sur un VLAN.

La notion de port natif exclut donc les ports dits \emph{trunk}, qui permettent
d'assurer la transmission de trames Ethernet taguées par différents VLANs.
Cependant, cela ne permet en aucun cas la communication entre VLANs sans
routage situé en couche réseau. Ces \emph{trunks} sont généralement employés
pour des liens d'interconnexion entre commutateurs, voire avec les routeurs du
réseau, comme c'est le cas ici.

\clearpage

La configuration de l'interface d'administration du commutateur est simple.
Sur IOS, chaque VLAN dispose d'une interface Ethernet virtuelle, qui se
configure de la même manière qu'un port physique. Par défaut, l'interface
d'administration est celle du VLAN 1, le VLAN par défaut pour de l'Ethernet. Il
suffit donc de configurer l'interface VLAN1 pour obtenir la configuration
désirée :

\bigskip

\begin{small}
\begin{lstlisting}
!
interface VLAN1
 ip address 192.168.144.229 255.255.255.248
 no ip redirects
 no ip directed-broadcast
 no ip route-cache
!
\end{lstlisting}
\end{small}

Mais souhaitant utiliser le VLAN 42 pour administrer les équipements réseau, il
faut changer le VLAN d'administration, non sans avoir affecté un autre port
physique à ce nouveau VLAN ! Dans le cas contraire, il est alors impossible de
se connecter en telnet, et il est obligatoire de sortir le câble série...
Une fois ces mesures de précautions assurées, on peut effectuer le changement de
VLAN d'administration :

\bigskip

\begin{small}
\begin{lstlisting}
c2924# conf t
c2924(config)# int VLAN42
c2924(config-subif)# management
\end{lstlisting}
\end{small}

On a alors la configuration suivante :

\begin{small}
\begin{lstlisting}
!
interface VLAN42
 ip address 192.168.144.229 255.255.255.248
 no ip redirects
 no ip directed-broadcast
 no ip route-cache
!
\end{lstlisting}
\end{small}

Concernant la politique d'accès à la console IOS du commutateur, les directives
suivantes permettent de sécuriser les lignes d'accès avec certains paramètres
comme le \emph{timeout} de session après 10 minutes d'inactivité, le ou les mots
de passe d'accès et d'obtention de privilèges avec la commande \texttt{enable}.
Enfin, l'accès est limité à maximum 4 sessions telnet simultanées.

\bigskip

\begin{small}
\lstinputlisting[firstline=266,lastline=279]{files/c2924.conf}
\end{small}

\clearpage

Voici la configuration de chaque port du Cisco WS-2924-XL actuellement en
production :

\bigskip

\begin{small}
\lstinputlisting[firstline=47,lastline=57]{files/c2924.conf}
\end{small}

Le premier port est utilisé par le modem fourni par le FAI. C'est la porte
d'entrée et de sortie sur le réseau global Internet. Étant données la nature et
la séparation évidente entre le réseau Internet et un réseau privé, le
\emph{WAN} est donc servi dans un VLAN à part, le VLAN 2. De plus le FAI propose
à ses abonnés de regarder la télévision par Internet. Or dans les scénarios
prévus par le FAI, le boîtier TV est directement branché sur le modem/routeur.
Dans ce cas, il aurait fallu acheminer deux câbles réseau dans le salon, un lien
d'interconnexion avec le commutateur Ethernet du salon, et l'autre spécialement
dédié à la télévision.

Cette solution ne me plaisant guère, j'ai observé après analyse du trafic entre
le boîtier TV et le modem/routeur du FAI que les trames Ethernet étaient taguées
sur le VLAN 100. Il suffit donc de créer un \emph{trunk} qui permet d'acheminer
le VLAN 100 jusqu'au boîtier TV, selon le schéma suivant :

\bigskip

\begin{footnotesize}
\begin{lstlisting}
WAN <> Modem(VLAN100) <> c2924(Trunk VLAN 2/100) <> slm(Trunk VLAN 3/100) <> boitier_TV(VLAN100)

c2924 : Cisco WS-2924-XL              VLAN 2 natif, VLAN 100 tag
slm   : Cisco Small Business SLM2008  VLAN 3 natif, VLAN 100 tag
\end{lstlisting}
\end{footnotesize}

Il y a alors un seul câble partant du Cisco 2924 vers le salon, transportant en
VLAN natif le VLAN 3 (LAN), et en VLAN tagué le VLAN 100 pour la TV.

Quant au reste de la configuration, on peut noter dans l'ordre de lecture la
description de l'interface, qui permet une identification plus rapide dans les
commandes d'affichage d'information. La nomenclature est libre, mais j'ai choisi
une nomenclature qui me paraissait intéressante, puisqu'elle fournit la nature
du port (natif ou \emph{trunk}), les VLANs qui sont impliqués et a fonction
représentée par des acronymes.

Ensuite, la directive de \texttt{port storm-control} permet ici d'activer le
contrôle de flux de diffusion, afin de minimiser les impacts d'une tempête de
broadcast. L'action \texttt{filter} permet de rejeter tout traffic de diffusion
qui comporte plus d'un certain seuil de paquets par seconde. En réalité sur les
équipements Cisco, le seuil est la moyenne du nombre de paquets sur 200 ms. Mal
configuré, le \emph{storm control} peut avoir des conséquences désastreuses sur
les flux de diffusion. Les valeurs que j'ai saisies sont de 500 paquets pour
200 ms pour le seuil haut (seuil de déclenchement), et de 250 paquets pour
200 ms pour le seuil bas (seuil de relâchement). La commande \texttt{show port
storm-control broadcast} fournit une vue générale du statut de cette
technologie.
Cette directive est appliquée sur l'ensemble des 24 ports du commutateur, cf. le
fichier de configuration complet fourni en annexe.

Viennent ensuite les directives spécifiques aux VLANs, comme l'utilisation
explicite du protocole 802.1q en lieu et place du déprécié et propriétaire ISL.
La déclaration explicite d'un  VLAN natif du \emph{trunk} implique que les
trames Ethernet non taguées seront commutées dans ce VLAN. Il est bien entendu
possible de ne laisser passer uniquement des trames Ethernet taguées, sans VLAN
natif. Enfin on déclare les VLANs autorisés au sein du \emph{trunk}.

L'avant dernière directive concerne le \emph{Spanning Tree Protocol}, un
protocole de détection de boucle dans un réseau. Configuré sur l'ensemble des
ports du commutateur, l'argument \texttt{portfast} signifie que ce port est
\emph{censé} être en liaison avec une machine cliente qui n'est pas susceptible
de créer une boucle. Lorsque la machine ou l'équipement raccordé est inconnu, il
est préférable de ne pas passer le port en \texttt{portfast}. Il en est
naturellement de même si l'équipement est un autre commutateur.\\
Au niveau de la configuration générale du STP, voici la section pertinente de la
configuration :

\bigskip

\begin{small}
\lstinputlisting[firstline=31,lastline=39]{files/c2924.conf}
\end{small}

Enfin la dernière directive \texttt{no cdp enable}, commune à tous les ports du
commutateur, sert à désactiver le \emph{Cisco Discovery Protocol}, un protocole
de découverte et de configuration sur réseau. Si ce protocole a une utilité au
sein de grosses structures, c'est également un protocole très risqué s'il n'est
pas maîtrisé, au même titre que le \emph{VLAN Trunk Protocol}. Je l'ai donc
désactivé, ainsi que le VTP.

\bigskip

\begin{small}
\lstinputlisting[firstline=57,lastline=66]{files/c2924.conf}
\end{small}

La seconde interface est la plus importante, car elle expose une méthode peu
utilisée vis-à-vis de la couche réseau (L3). En effet, c'est l'unique port
attribué au routeur, qui pour autant doit disposer d'une passerelle dans
chacun des 4 VLANs 2,3,4 et 42 (resp. WAN, LAN, DMZ, et ADM).
Tout cela sur un unique lien, grâce au module kernel \emph{8021q}
qui permet de faire du \emph{trunking} de VLAN sous GNU/Linux, de manière
similaire à la commande \texttt{switchport trunk} sous Cisco IOS.

\clearpage

Voici en effet un extrait du fichier \texttt{/etc/networks/interfaces} du
routeur GNU/Linux qui contient les paramètres des interfaces réseau sous
Debian GNU/Linux :

\bigskip

\begin{small}
\lstinputlisting[firstline=17,lastline=37]{files/interfaces}
\end{small}

On peut constater que le module \emph{8021q} crée des sous-interfaces avec la
nomenclature \texttt{ethX.V}, V étant le numéro du VLAN ID. Cette configuration
particulière où l'on multiplexe le routage inter VLAN sur un seul lien (et donc
une seule interface) s'appelle en anglais \emph{Router on a stick}. Pour
illustrer ce concept, voici un diagramme de liaison L2 - L3 classique :

\begin{figure}[ht]
\hspace{-1cm}
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.25mm}
  \fbox{\includegraphics[width=18cm]{./images/router-no-vlan-trunk}}
  \caption{Liaison L2 - L3 « classique ».}
\end{figure}

On constate l'absence de \emph{trunk} sur les deux équipements. Ainsi, pour
chaque VLAN configuré sur le commutateur Ethernet, il est nécessaire d'avoir une
interface réseau dédiée sur le routeur et un port physique dédié sur le
commutateur. Les avantages sont très logiques,
puisque le débit de liaison entre un VLAN et un autre est maximal en
\emph{Full Duplex}. Cependant, les inconvénients sont nombreux et évidents :
cette méthode manque cruellement de souplesse et nécessite du matériel haut de
gamme, ou tout au moins proposant une capacité d'évolution matérielle
importante.

\clearpage

A contrario, voici une représentation d'une configuration \emph{Router on a
stick} :

\begin{figure}[ht]
\hspace{-1cm}
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.25mm}
  \fbox{\includegraphics[width=18cm]{./images/router-on-a-stick}}
  \caption{Liaison L2 - L3 \emph{Router on a stick}.}
\end{figure}

Cette configuration se révèle alors d'une grande souplesse, puisque le principal
avantage du \emph{trunking} est d'offrir une abstraction logicielle là où la
configuration précédente nécessite du matériel supplémentaire. Cependant, deux
problèmes se posent, un de performance, l'autre de sécurité/fiabilité. En effet,
même en profitant du plein débit symétrique grâce au \emph{Full Duplex}, cette
configuration est amenée à saturer si les besoins sur le réseau sont exigeants,
et/ou impliquent du routage entre plusieurs VLANs simultanément. Cela étant dit,
j'avais évalué qu'avec mes besoins relativement modestes de débit moyen, ce
problème s'effacerait quasiment complètement. De plus, si des transferts de
données ont lieu à haut débit, alors ce sera entre la DMZ (fichiers sur le
serveur) et une machine du LAN. Les machines du sous-réseau d'administration ADM
ne sont pas amenées à effectuer des transferts rapides, et concernant le WAN,
c'est la connexion ADSL qui sature. Dans la grande majorité des cas, c'est
uniquement une machine d'un VLAN et une autre machine d'un autre VLAN qui sont
impliquées. On reste donc dans une situation de débit maximal en \emph{Full
Duplex}.\\
L'autre problème est beaucoup plus préoccupant : en ne dépendant que d'un seul
lien logique et physique pour effectuer la liaison entre deux couches réseau
primordiales, on introduit alors un point unique de panne, ou \emph{Single Point
Of Failure}. Malheureusement, ce problème n'est pas facile à résoudre, et se
clotûre bien souvent par l'acceptation du risque à courir, en se basant sur une
fiabilité logicielle et matérielle de mieux en mieux maîtrisées.

L'extrait de la configuration du Cisco WS-2624-XL qui suit est plus générique,
puisqu'elle assure la liaison entre le commutateur et de simples machines, ou
alors avec le point d'accès WiFi et le commutateur de distribution du salon.
Toutefois, si les directives de liaison sont moins poussées, ce sont les
directives de sécurité qui sont plus développées.

\clearpage

\begin{small}
\lstinputlisting[firstline=66,lastline=103]{files/c2924.conf}
\end{small}

Au delà des directives communes à tous les ports, et celles qui ont déjà été
abordées précédemment, les ports \texttt{fa0/3} et \texttt{fa0/5} exposent des
directives de sécurité particulièrement efficaces et restrictives. En effet, les
machines connectées à ces deux ports du commutateur sont des machines sensibles,
et dont on souhaiterait protéger la connectivité. Ainsi, si un attaquant tente
d'effectuer une attaque du type \emph{Man in the Middle} en utilisant un
ordinateur portable comme passerelle entre la machine originellement connectée
et le commutateur. ou si tout simplement il se connecte à la place de la machine
prévue, le commutateur constate alors le changement d'adresse MAC et coupe
administrativement le port. rendant impossible l'utilisation du port, sauf sur
réactivation manuelle directement dans IOS.

Cette mesure de protection est activée par les directives des lignes 3 à 7 et 23
à 25. Il n'y a pas de délai d'expiration (directive
\texttt{port security aging}), puisque les machines sont assignées en dur à un
port. Ainsi un attaquant aurait pu dans le cas contraire débrancher la machine,
attendre une dizaine de minutes pour être certain que le commutateur « oublie »
l'ancienne MAC. Cette protection est renforcée par l'écriture d'une table ARP
statique sur la NVRAM du commutateur. Ainsi, même si l'attaquant redémarre le
commutateur, il ne pourra pas se connecter au port protégé. Cette table ARP
statique particulière (table \emph{secure}) est configurée via la directive
\texttt{mac-address-table secure}, et la table peut être visualisée via la
commande \texttt{show mac-address-table secure}. En voici la sortie actuelle :

\bigskip

\begin{footnotesize}
\begin{lstlisting}
c2924#show mac-address-table secure
Non-static Address Table:
Destination Address  Address Type  VLAN  Destination Port
-------------------  ------------  ----  --------------------
0000.0000.14b4       Secure           2  FastEthernet0/2
0000.0000.14b4       Secure           3  FastEthernet0/2
0000.0000.14b4       Secure           4  FastEthernet0/2
0000.0000.14b4       Secure          42  FastEthernet0/2
001f.e218.d52f       Secure          42  FastEthernet0/24
0022.3fd0.edaa       Secure           3  FastEthernet0/5
0030.18a9.57b0       Secure           4  FastEthernet0/3
c2924#
\end{lstlisting}
\end{footnotesize}

On pourra noter une subtilité dans la sécurisation du commutateur. En effet, la
table ci dessus comporte également les adresses MAC des différentes interfaces
du routeur, à raison d'une par VLAN. Cependant, la configuration du port
\texttt{fa0/2} ne comporte aucune directive de \texttt{port security}, et il
n'est pas possible d'en ajouter, car le port est un \emph{trunk} entre
différents VLANs, et il est plus que probable que pour chaque VLAN du
\emph{trunk} l'interface réseau connectée porte une adresse MAC différente des
autres. Il n'est donc pas possible de sécuriser un port du commutateur en mode
\emph{trunk} en forcant un nombre maximum d'adresses MAC simultanées. On note
également le cas particulier offert par le module kernel \emph{8021q}, qui crée
des interfaces logiques au dessus d'une interface physique. Côté routeur comme
commutateur, c'est donc la même adresse MAC qui est visible pour tous les VLANs
du \emph{trunk}, ici \texttt{0000.0000.14b4}.

Concernant la surveillance de la couche de liaison, un service \texttt{arpwatch}
tourne en tâche de fond sur le routeur, permettant d'envoyer un mail
immédiatement après l'apparition d'une nouvelle adresse MAC sur le réseau, si
pour une IP il ya plusieurs MAC différentes ou encore si le couple (@ IP, @ MAC)
a changé, etc.

\subsection{Autres équipements et tests}

Concernant les autres équipements réseau, le détail de leur configuration n'est
pas pertinent dans ce rapport pour plusieurs raisons. En effet, transcrire les
réglages effectués dans une interface web sans inonder le rapport de captures
d'écran n'est pas simple, et la configuration en elle-même est d'intérêt limité,
puisqu'elle est directement compatible avec celle du Cisco WS-2924-XL qui a été
longuement explicitée dans ce chapitre. On y retrouvera à peu de choses près les
mêmes configurations, la même table de VLANs, une ou plusieurs interfaces réseau
configurée(s) en \emph{trunk} à l'identique du port du commutateur au bout du
câble réseau, etc.

La page suivante présentera les résultats de deux tests concernant la couche de
liaison et la configuration du commutateur Ethernet de cœur.

\clearpage

\subsubsection*{VLAN : Isolation au niveau de la couche de liaison.}
\begin{small}
\begin{lstlisting}
18:31 root@gnaea ~# ip a s wlan0
3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP qlen 1000
    link/ether 00:21:5c:8b:0a:e1 brd ff:ff:ff:ff:ff:ff
    inet 192.168.144.130/27 brd 192.168.144.159 scope global wlan0
    inet6 fe80::221:5cff:fe8b:ae1/64 scope link
       valid_lft forever preferred_lft forever
18:32 root@gnaea ~# ifconfig wlan0 192.168.144.130/24
18:33 root@gnaea ~# ip a s wlan0
3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP qlen 1000
    link/ether 00:21:5c:8b:0a:e1 brd ff:ff:ff:ff:ff:ff
    inet 192.168.144.130/24 brd 192.168.144.255 scope global wlan0
    inet6 fe80::221:5cff:fe8b:ae1/64 scope link
       valid_lft forever preferred_lft forever
18:34 root@gnaea ~# arping -I wlan0  c2924
ARPING 192.168.144.229 from 192.168.144.130 wlan0
^CSent 3 probes (3 broadcast(s))
Received 0 response(s)
zsh: exit 1     arping -I wlan0 c2924
18:32 root@gnaea ~#
\end{lstlisting}
\end{small}

Dans ce test, je suis connecté en WiFi au sein du VLAN 3 (LAN). J'ai un masque
de sous-réseau de /27. Je le passe en /24, pour pouvoir communiquer avec
l'ensemble des machines du réseau, sans routage. Je suis donc censé pouvoir
communiquer au niveau L3 avec la machine c2924, mais sans routage, il est
nécessaire que les machines soient sur même segment réseau, c'est à dire avoir
une connectivité directe au niveau de la couche de liaison (L2).
Mon ordinateur portable et la machine c2924 étant sur des VLANs différents, si
les VLANs n'étaient pas isolés les uns vis-à-vis des autres, je devrais pouvoir
directement communiquer en L2, avec la commande \texttt{arping}. Or il n'en est
rien, puisque aucun paquet d'ARP REQUEST n'a atteint la machine censé être
voisine. Il y a donc bien une isolation au niveau de la couche de liaison, et
quelque soit la configuration des couches supérieures, il est nécessaire de
router les paquets par la couche de réseau.

\subsubsection*{IOS : comportement du \emph{storm-control}}
\begin{small}
\begin{lstlisting}
c2924#sh port storm-control broadcast 
Interface  Filter State   Trap State     Rising  Falling  Current  Traps Sent
---------  -------------  -------------  ------  -------  -------  ----------
Fa0/1      Forwarding     <inactive>        500      250        0           0
Fa0/2      Forwarding     <inactive>        500      250        0           0
Fa0/3      Forwarding     <inactive>        500      250        0           0
Fa0/4      Blocking       <inactive>         80       60       80           0
Fa0/5      Forwarding     <inactive>        500      250        0           0
Fa0/6      Forwarding     <inactive>        500      250        0           0
(...........................................................................)
Fa0/23     Forwarding     <inactive>        500      250        0           0
Fa0/24     Forwarding     <inactive>        500      250        0           0
\end{lstlisting}
\end{small}

Pour ce test, j'ai modifié les valeurs des seuils de déclenchement et de
relâchement du contrôle de flux appliqué au trafic de diffusion du port du
commutateur \texttt{fa0/4}, affecté au point d'accès wifi. Je génère alors un
trafic de diffusion d'environ 420 paquets par secondes, soit 85 paquets par 200
ms. La commande \texttt{show storm-control brodcast} permet d'observer que le
commutateur ne transmet plus le trafic de diffusion.

\clearpage

\section{Couche de réseau - L3}
\label{conf:net:L3}

Cette section exposera les configurations du routeur \emph{homemade} spécifiques
à la couche de niveau 3, la couche réseau. La plupart des bases qui permettent
au réseau de fonctionner sont exposées dans le chapitre précédent, cette section
présente essentiellement la configuration des variables du noyau Linux et le
script de routage/pare-feu reposant sur la technologie \emph{Netfilter} via
l'utilisation de l'utilitaire \emph{iptables} (et prochainement \emph{ip6tables}
pour filtrer l'IPv6).

\subsection{Configuration du noyau Linux}

Le routeur fonctionne avec un noyau linux endurci grâce au correctif
\emph{grsec}\footnote{\url{http://grsecurity.com/} pour plus de détails.}, qui
apporte de nombreuses améliorations de sécurité sur l'ensemble des
fonctionnalités du noyau. La version du noyau est 2.6.32-11, donc un noyau
récent. Au niveau de la configuration des variables de sécurité relatives à la
couche réseau du noyau, j'ai donc utilisé l'utilitaire \texttt{sysctl} et plus
spécifiquement en éditant le fichier \texttt{/etc/sysctl.conf}, dont voici les
lignes pertinentes :

\begin{small}
\begin{lstlisting}
# Spoof protection (reverse-path filter)
net.ipv4.conf.default.rp_filter = 1
net.ipv4.conf.all.rp_filter = 1

# Enable packet forwarding for both IPv4 and IPv6
net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding = 1

# Ignoring ICMP bogus errors
net.ipv4.icmp_ignore_bogus_error_responses = 1

# We don't accept any ICMP REDIRECT since WE are the router and main gateway.
net.ipv4.conf.all.accept_redirects = 0
net.ipv6.conf.all.accept_redirects = 0

# Connection Tracking Accounting
net.netfilter.nf_conntrack_acct = 1
\end{lstlisting}
\end{small}

\subsection{Pare-feu et transmission de paquets IP}

Les commandes \texttt{iptables} et \texttt{sysctl} qui permettent de mettre en
place la transmission de paquets IP et les règles de filtrage sont consignées
dans un script shell situé dans le dossier des scripts d'administration de
services (\texttt{/etc/init.d/firewall}). Ce script présente les
en-têtes standard des scripts d'init.d Debian. Cela permet de gérer
l'application des règles automatiquement au démarrage de la machine.

Le script expose également des actions d'administration
\texttt{start}, \texttt{stop}, \texttt{restart}, \texttt{status}. Ces actions
sont utilisables soit par \texttt{/etc/init.d/firewall <action>}, 
soit par \texttt{service firewall <action>}
(\texttt{service} est fourni par le paquet debian \texttt{sysvinit-utils}).

Dans les pages suivantes, chaque section importante du script sera détaillée.
Lorsqu'un protocole de couche supérieure à la couche transport (L4) est évoqué,
on fait uniquement référence au couple (protocole de transport, port standard
associé), comme (tcp, 80) pour HTTP, ou encore (tcp, 22) pour SSH. En effet le
pare-feu ne fait pas d'introspection au dessus de la couche de transport, à
quelques exceptions près (type de message ICMP).

\clearpage

\begin{small}
\lstinputlisting[firstline=1,lastline=35]{files/firewall}
\end{small}

L'en-tête du script contient les informations init et les variables utilisées
dans l'ensemble du script. Ces variables sont essentiellement les sources et
destinations dans les règles de filtrage.

\clearpage

\begin{small}
\lstinputlisting[firstline=37,lastline=80]{files/firewall}
\end{small}

Cette première partie de la fonction principale \texttt{start} effectue les
actions suivantes :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item{Activer la transmission de paquets IPv4 dans le noyau.}

  \item{Purge des reglès existantes}

  \item{Définition des politiques par défaut}

  \item{Ouverture de la boucle locale}

  \item{Diverses règles de protection spécifiques à la
  RFC1918\footnote{http://www.faqs.org/rfcs/rfc1918.html} : Une adresse IP
  privée ne peut en aucun cas provenir de la passerelle vers le réseau Internet
  (WAN).}

  \item{Rejet silencieux des paquets provenant de communications non initiées
  (pare-feu à état).}
\end{itemize}

\clearpage

\begin{small}
\lstinputlisting[firstline=81,lastline=102]{files/firewall}
\end{small}

Cette seconde partie concerne spécifiquement les règles d'accès au routeur
(INPUT), à la différence de règles de transmission (FORWARD) :

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item{Autorisation des messages ICMP de type ECHO REQUEST, dans la limite de
  10 paquets par seconde.}

  \item{Règle à état : autorisation du trafic préalablement établi.}

  \item{Règles autorisant en entrée les protocoles DNS, DHCP en provenance de
  toutes les interfaces sauf le WAN.}

  \item{Autorisation en entrée du protocole SSH uniquement pour les machines du
  sous-réseau d'administration ADM.}

  \item{Autorisation en entrée du protocole HTTP depuis les sous-réseaux LAN
  et ADM.}

  \item{Règle spéciale permettant le rebond SSH depuis le serveur de la DMZ pour
  pouvoir accéder au routeur depuis Internet. Notons le port de destination très
  élevé, permettant de masquer le port d'écoute du sshd du routeur par un peu de
  sécurité par l'obscurité. Cependant le port n'est pas choisi au hasard, il
  s'agit en effet d'un des ports les moins utilisés sur Internet (statistiques
  \emph{insecure.org}).}

  \item{La règle commentée permet de journaliser les tentatives de connexions
  intrusives. L'inconvénient est la quantité de logs générés, puisque
  très régulièrement le routeur subit des scans de ports depuis Internet.}

  \item{la dernière règle change la politique par défaut pour l'entrée sur le
  routeur si la machine est sur le réseau interne, afin de retourner des
  messages ICMP d'erreur plutôt que de tuer silencieusement la connexion.}
\end{itemize}

\clearpage

\begin{small}
\lstinputlisting[firstline=104,lastline=142]{files/firewall}
\end{small}

\begin{itemize}
\setlength{\itemsep}{8pt}
  \item{Règles à état : on rejette silencieusement les communications dans un
  état invalide.}

  \item{Ensemble de règles définissant les règles de transmission entre les
  différents sous-réseaux :}
  \begin{verbatim}
    ADM -> WAN  : OK      ADM -> LAN  : OK
    LAN -> WAN  : OK      DMZ -> WAN  : OK
    pa  -> be   : OK      *   -> DMZ  : OK\end{verbatim}

  \item{Autorisation de transmission du trafic déjà établi.}

  \item{la dernière règle change la politique par défaut pour le trafic transmis
  par le routeur si la source est sur le réseau interne, afin de retourner des
  messages ICMP d'erreur plutôt que de tuer silencieusement la connexion.}

  \item{Quelques règles de \emph{port forwarding}. Par exemple, depuis Internet,
  le trafic vers le port 22/tcp en entrée sur le routeur est altéré pour
  atteindre le port 22/tcp du serveur dans la DMZ : c'est du \emph{Destination
  NAT}, puisque c'est l'adresse de destination qui a été réécrite.}

  \item{On réécrit l'adresse source de tout le trafic sortant sur Internet.}

\end{itemize}
\clearpage

\begin{small}
\lstinputlisting[firstline=144,lastline=160]{files/firewall}
\end{small}

Cette fonction \texttt{stop} permet à l'utilisateur de stoper le pare-feu en
supprimant toutes les règles mises en place par la fonction \texttt{start}, et
réinitialise les politiques par défaut. Enfin, elle interdit la transmission de
paquets IPv4 par le noyau Linux.

\bigskip

\begin{small}
\lstinputlisting[firstline=161,lastline=185]{files/firewall}
\end{small}

La fonction \texttt{status} permet un affichage complet et compréhensible des
règles et du nombre de paquets traités et de pour toutes les tables de
Netfilter : filter, nat, mangle, raw.

\clearpage

\begin{small}
\lstinputlisting[firstline=187]{files/firewall}
\end{small}

Enfin, cette portion de code permet d'appeler la fonction correspondante au
premier argument saisi par l'utilisateur lors de l'appel du script.
