#!/usr/bin/env python
# -*- coding: utf-8 -*-

import textwrap as tw
import random as r

import data_generation as dat
import bit_multiplexing as bit
import error_generation as err

## DEBUG?
DEBUG = False
MORE_DEBUG = False
MORE_MORE_DEBUG = False
## SEPARATOR
SEP = ""

## CONSTANT VALUES FOR TDMA FRAME GENERATION
# 3 bits of tail bits
tail_bits = [1,1,1]
# training sequence of 26 bits
training_sequence = dat.gen_data(26)
# one stealing bit
stealing_bit = [1]
# How to modelise 8.25 bits? Let's consider them NULL for now
guard_bits = []

## OTHER USEFUL VALUES
null_slot = (  tail_bits +
                dat.gen_null_data(57) +
                stealing_bit +
                training_sequence +
                stealing_bit +
                dat.gen_null_data(57) +
                tail_bits +
                guard_bits  )


def gen_tdma_slot(data):
    """ Generate a GSM TDMA frame slot
    """

    # Parity interleaving
    slot_data = bit.bit_parity_interleaving(data)
    # Slot content
    tdma_slot = (   tail_bits +
                    slot_data[:57] +
                    stealing_bit +
                    training_sequence +
                    stealing_bit +
                    slot_data[57:] +
                    tail_bits +
                    guard_bits  )

    return tdma_slot


def channel_to_tdma(channel_sequences) :
    """ Return a list of 20 TDMA frames containing burst of data in half-rate
        communication mode.
    """

    # list of bit-interleaved binary sequences
    bi_sequences = [ bit.bit_interleaving(i) for i in channel_sequences ]

    ## DEBUG SECTION : OK
    #print bi_sequences
    # Expected value: 456
    #print len(bi_sequences[0])
    # Expected value: [0,..,0]
    #print err.data_cmp(bi_sequences[0], bit.bit_interleaving(s1))

    if DEBUG:
        for i in xrange(len(channel_sequences)):
            print "\nINPUT BIN SEQ %d: %d bits\n%s\n" \
                % (i+1, len(channel_sequences[i]),
                    tw.fill(SEP.join(map(str, channel_sequences[i])), 80))

            print "BIT INTERLEAVED BIN SEQ %d: %d bits" \
                % (i+1, len(bi_sequences[i]))
            for j in xrange(8):
                print "%d (57b): %s" \
                % (j+1, SEP.join(map(str, bi_sequences[i][j*57 : (j+1)*57])))

    return block_interleaving(bi_sequences)


def block_interleaving(bi_seq_list) :
    """ Return a list of complete TDMA frames built upon block interleaving.
    """

    ## INITIALIZATION OF TDMA FRAME LIST
    # 4 * 456 bits will need 20 TDMA frames to be transmitted
    # Generation of a list of TDMA frames composed by empty bursts
    tdma_frames = [ [null_slot for i in xrange(8)] for j in xrange(20) ]

    ## DEBUG SECTION : OK
    # Expected value:
    #
    #[1, 1, 1]
    #
    #[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #        0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    #
    #[1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0,
    #        1, 0, 1]
    #
    #[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #        0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    #
    #[1, 1, 1]
    #
    #148
    #print tdma_frames[ r.randint(0,19) ][ r.randint(0,7) ][:3]
    #print tdma_frames[ r.randint(0,19) ][ r.randint(0,7) ][3:60]
    #print tdma_frames[ r.randint(0,19) ][ r.randint(0,7) ][60:88]
    #print tdma_frames[ r.randint(0,19) ][ r.randint(0,7) ][88:145]
    #print tdma_frames[ r.randint(0,19) ][ r.randint(0,7) ][145:148]
    #print len(tdma_frames[0][0])

    ## WE USE ONLY SLOT N°2
    # Filling the second slot (burst) of every TDMA with communication data
    # from binary sequences curr-1 and curr, or curr and curr+1
    if MORE_DEBUG:
        print "\n", 80 * "#"

    for i in xrange(20):
        if i < 4:
            # Binary sequence we are working on
            curr = 0
            # Slot content
            tdma_frames[i][1] = gen_tdma_slot([
                                    dat.gen_null_data(57),
                                    bi_seq_list[curr][57*i : 57*(i+1)]
                                ])

        if i >= 4 and i < 8:
            # Binary sequence we are working on
            curr = 0
            # Slot content
            tdma_frames[i][1] = gen_tdma_slot([
                                    bi_seq_list[curr][57*i : 57*(i+1)],
                                    bi_seq_list[curr+1][57*(i-4) : 57*(i-3)]
                                ])

        if i >= 8 and i < 12:
            # Binary sequence we are working on
            curr = 1
            # Slot content
            tdma_frames[i][1] = gen_tdma_slot([
                                    bi_seq_list[curr][57*(i-4) : 57*(i-3)],
                                    bi_seq_list[curr+1][57*(i-8) : 57*(i-7)]
                                ])

        if i >= 12 and i < 16:
            # Binary sequence we are working on
            curr = 2
            # Slot content
            tdma_frames[i][1] = gen_tdma_slot([
                                    bi_seq_list[curr][57*(i-8) : 57*(i-7)],
                                    bi_seq_list[curr+1][57*(i-12) : 57*(i-11)]
                                ])

        if i >= 16 and i < 20:
            # Binary sequence we are working on
            curr = 3
            # Slot content
            tdma_frames[i][1] = gen_tdma_slot([
                                    bi_seq_list[curr][57*(i-12) : 57*(i-11)],
                                    dat.gen_null_data(57)
                                ])

        if MORE_DEBUG:
            r = tdma_frames[i][1]
            print """
TDMA %02d \t\t\t %db
SLOT 2: \t\t\t %db
tail\t\t\t\t 3b:\t %s
half-burst 1\t\t\t 57b:\t %s
sbit + training seq + sbint \t 28b:\t %s
half-burst 2 \t\t\t 57b:\t %s
tail\t\t\t\t 3b:\t %s
                """ % (i+1, 8*(len(r)), len(r),
                        SEP.join(map(str, r[:3])),
                        SEP.join(map(str, r[3:60])),
                        SEP.join(map(str, r[60:88])),
                        SEP.join(map(str, r[88:145])),
                        SEP.join(map(str, r[145:148])))

    return tdma_frames


def apply_error(frames, err_type, err_prob):
    """ Apply the provided type of error on binary stream with the provided
        probability.
    """

    if err_type == "null":
        return frames
    else:
        altered_frames = []
        bits = reduce(lambda a,b: a+b, frames, [])
        bits = reduce(lambda a,b: a+b, bits, [])
        abits = []
        abits.extend(bits)

        if err_type == "block":
            abits = err.block_trans_err(abits, err_prob)
        elif err_type == "bit":
            abits = err.bit_trans_err(abits, err_prob)
        else :
            return frames

        if MORE_MORE_DEBUG:
            for i in xrange(len(frames)):
                str = err.data_cmp(bits, abits)[0][i*1184 : (i+1)*1184]
                str = tw.fill(str, 80)

                print "TDMA %d : 0 for correct bit, 1 for altered bit\n%s\n"\
                        % (i+1, str)

            print 80 * "#"

        nb_f = len(frames)
        nb_s = len(frames[0])
        len_f = len(frames[0]) * len(frames[0][1])
        len_s = len_f / nb_s
        tmp_frame = []

        for i in xrange(nb_f):
            tmp_bits = abits[i*len_f : (i+1)*len_f]

            for j in xrange(nb_s):
                tmp_frame.append(tmp_bits[ j*len_s : (j+1)*len_s ])

            altered_frames.append(tmp_frame)
            tmp_frame = []

        return altered_frames


def tdma_to_channel(tdma_frames):
    """ Return a list of channel encoded binary sequences from a list of TDMA
        frames.
    """

    channel_sequences = [[], [], [], []]

    for i in xrange(20):
        if i < 4:
            # Binary sequence we are working on
            curr = 0
            # Parity deinterlacing
            data = bit.bit_parity_deinterlacing(
                    [tdma_frames[i][1][3:60], tdma_frames[i][1][88:145]])

            channel_sequences[curr] += data[57:]

        if i >= 4 and i < 8:
            # Binary sequence we are working on
            curr = 0
            # Parity deinterlacing
            data = bit.bit_parity_deinterlacing(
                    [tdma_frames[i][1][3:60], tdma_frames[i][1][88:145]])

            channel_sequences[curr] += data[:57]
            channel_sequences[curr+1] += data[57:]

        if i >= 8 and i < 12:
            # Binary sequence we are working on
            curr = 1
            # Parity deinterlacing
            data = bit.bit_parity_deinterlacing(
                    [tdma_frames[i][1][3:60], tdma_frames[i][1][88:145]])

            channel_sequences[curr] += data[:57]
            channel_sequences[curr+1] += data[57:]

        if i >= 12 and i < 16:
            # Binary sequence we are working on
            curr = 2
            # Parity deinterlacing
            data = bit.bit_parity_deinterlacing(
                    [tdma_frames[i][1][3:60], tdma_frames[i][1][88:145]])

            channel_sequences[curr] += data[:57]
            channel_sequences[curr+1] += data[57:]

        if i >= 16 and i < 20:
            # Binary sequence we are working on
            curr = 3
            # Parity deinterlacing
            data = bit.bit_parity_deinterlacing(
                    [tdma_frames[i][1][3:60], tdma_frames[i][1][88:145]])

            channel_sequences[curr] += data[:57]

    received_sequences = [ bit.bit_deinterlacing(s) for s in channel_sequences ]

    if DEBUG:
        for i in xrange(len(received_sequences)):
            print "\nRECEIVED BIN SEQ %d: %d bits\n%s\n" \
                % (i+1, len(received_sequences[i]),
                    tw.fill(SEP.join(map(str, received_sequences[i])), 80))

    return received_sequences


if __name__ == '__main__':
    DEBUG = True
    MORE_DEBUG = True
    input_channel_sequences = [ dat.gen_data(456) for i in xrange(4) ]

    transmitted_tdma_trames = channel_to_tdma(input_channel_sequences)
    received_channel_sequences = tdma_to_channel(transmitted_tdma_trames)

    ## DEBUG SECTION: OK
    #seq1 = input_channel_sequences[0]
    # Expected value: all zeros 000000000
    #print err.data_cmp(seq1, bit.bit_deinterlacing(bit.bit_interleaving(seq1)))
    # Expected value: all zeros 000000000
    #print err.data_cmp(seq1, received_channel_sequences[0])

