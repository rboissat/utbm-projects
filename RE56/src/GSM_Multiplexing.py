#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import optparse

import error_generation as err
import data_generation as dat
import tdma_multiplexing as tdma


def gsm_transmission(e_type="null", e_prob=0):
    """ Simulate a complete transmission from channel encoded data to TDMA
        with error handling
    """

    # We generate random input data for now.
    input_sequences = [ dat.gen_data(456) for i in xrange(4) ]

    # Coding data from channel to TDMA frames
    pre_transmission = tdma.channel_to_tdma(input_sequences)

    print 80 * "#"

    # Transmission error
    transmitted = tdma.apply_error(pre_transmission, e_type, e_prob)
    # Simulate a complete reception from TDMA frames to channel encoded data
    received = tdma.tdma_to_channel(transmitted)

    print 80 * "#", "\n"

    # Print a basic set of transmission results
    for i in xrange(4):
        error = err.data_cmp(input_sequences[i], received[i])

        if "1" in error[0]:
            print "FAIL!: Transmission of channel sequence %d : error %.02f %%"\
                % (i+1, error[1])
        else:
            print "SUCCESS!: Transmission of channel sequence %d" % (i+1)


if __name__ == '__main__':
    parser = optparse.OptionParser()

    # Do we need verbosity?
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose",
                    default=False, help="verbose output")

    parser.add_option("-V", "--vverbose", action="store_true", dest="vverbose",
                    default=False, help="VERY VERBOSE OUTPUT")

    # Transmission error probability
    parser.add_option("-p", "--errprob", dest="errprob", default=0, type="int",
                    help="error probability in %",
                    metavar="PERCENTAGE")

    # Transmission error type
    parser.add_option("-t", "--errtype", dest="errtype", default="null",
                    help="error type: bit or block, default NULL",
                    metavar="VALUE")

    (options, args) = parser.parse_args()

    # Input sanatizing
    if 0 > options.errprob or options.errprob > 100:
        parser.error("-p|--errprob VALUE, with 0 <= VALUE <= 100\n")

    if options.errtype not in ["null", "bit", "block"]:
        parser.error("-t|--errtype must be in 'null' or 'bit' or 'block'\n")

    # We want some fancy output.
    tdma.DEBUG = True
    tdma.MORE_DEBUG = options.verbose

    if options.vverbose:
        tdma.MORE_DEBUG = options.vverbose
        tdma.MORE_MORE_DEBUG = options.vverbose

    gsm_transmission(options.errtype, options.errprob/100.)

