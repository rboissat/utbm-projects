#!/usr/bin/env python
# -*- coding: utf-8 -*-

def bit_interleaving(sequence):
    """ Return a list of interleved bits from a list of 456 consecutive bits
        to 8 blocks of 57 bits interlaced in the following fashion:

            [0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 80, 88, 96, 104, 112, 120,
            128, 136, 144, 152, 160, 168, 176, 184, 192, 200, 208, 216, 224,
            232, 240, 248, 256, 264, 272, 280, 288, 296, 304, 312, 320, 328,
            336, 344, 352, 360, 368, 376, 384, 392, 400, 408, 416, 424, 432,
            440, 448]

            [1, 9, 17, 25, 33, 41, 49, 57, 65, 73, 81, 89, 97, 105, 113, 121,
            129, 137, 145, 153, 161, 169, 177, 185, 193, 201, 209, 217, 225,
            233, 241, 249, 257, 265, 273, 281, 289, 297, 305, 313, 321, 329,
            337, 345, 353, 361, 369, 377, 385, 393, 401, 409, 417, 425, 433,
            441, 449]

            [2, 10, 18, 26, 34, 42, 50, 58, 66, 74, 82, 90, 98, 106, 114, 122,
            130, 138, 146, 154, 162, 170, 178, 186, 194, 202, 210, 218, 226,
            234, 242, 250, 258, 266, 274, 282, 290, 298, 306, 314, 322, 330,
            338, 346, 354, 362, 370, 378, 386, 394, 402, 410, 418, 426, 434,
            442, 450]

            [..........]

            [7, 15, 23, 31, 39, 47, 55, 63, 71, 79, 87, 95, 103, 111, 119, 127,
            135, 143, 151, 159, 167, 175, 183, 191, 199, 207, 215, 223, 231,
            239, 247, 255, 263, 271, 279, 287, 295, 303, 311, 319, 327, 335,
            343, 351, 359, 367, 375, 383, 391, 399, 407, 415, 423, 431, 439,
            447, 455]
    """

    indexes = [range(i,456,8) for i in range(8)]
    return [ sequence[indexes[i][j]] for i in range(8) for j in range(57) ]


def bit_deinterlacing(sequence):
    """ Return the deinterlaced list previously interlaced by 
        bit_interleaving.
    """

    indexes = [range(i,456,57) for i in range(57)]
    return [ sequence[indexes[i][j]] for i in range(57) for j in range(8) ]


def bit_parity_interleaving(block_list):
    """ Return an bit-parity interleaved bit list
    """
    burst = []

    bit_pool = block_list[0] + block_list[1]

    odd_indexes = [ i for i in xrange(len(bit_pool)) if i % 2 == 1 ]
    even_indexes = [ i for i in xrange(len(bit_pool)) if i % 2 == 0 ]

    # Interleaving the two half-bursts
    for i in odd_indexes:
        burst.append(bit_pool[i])

    for i in even_indexes:
        burst.append(bit_pool[i])

    return burst

def bit_parity_deinterlacing(block_list):
    """ Return an bit-parity deinterlaced bit list
    """
    burst = []

    bit_pool = block_list[0] + block_list[1]

    odd_indexes = [ i for i in xrange(len(bit_pool)) if i % 2 == 1 ]
    even_indexes = [ i for i in xrange(len(bit_pool)) if i % 2 == 0 ]

    # Deinterlacing the two half-bursts
    for i in xrange(len(bit_pool)/2):
        burst.append(bit_pool[i+len(bit_pool)/2])
        burst.append(bit_pool[i])

    return burst

if __name__ == '__main__':
    block1 = [0, 1, 0, 1, 0, 1, 0, 1, 9, 9, 9, 9, 9]
    block2 = [2, 3, 2, 3, 2, 3, 2, 3, 4, 4, 4, 4, 4]

    l1 = len(block1)
    l2 = len(block2)

    int_half_bursts = bit_parity_interleaving([block1, block2])
    deint_half_bursts = bit_parity_deinterlacing([int_half_bursts[:l1],
        int_half_bursts[l1:]])

    print "original half-burst 1:\t\t\t %s" % block1
    print "original half-burst 2:\t\t\t %s\n" % block2

    print "parity interleaved half-burst 1:\t %s" % int_half_bursts[:l1]
    print "parity interleaved half-burst 2:\t %s\n" % int_half_bursts[l1:]

    print "parity deinterlaced half-burst 1:\t %s" % deint_half_bursts[:l1]
    print "parity deinterlaced half-burst 2:\t %s" % deint_half_bursts[l1:]
