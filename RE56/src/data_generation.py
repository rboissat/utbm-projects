#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random as r

def gen_data(length):
    """ Return a list of `length` random bits """
    return [r.randint(0,1) for x in xrange(length)]

def gen_null_data(length):
    """ Return a list of `length` null bits """
    return [0 for x in xrange(length)]

if __name__ == '__main__':
    print gen_data(42)
    print gen_null_data(42)
    print "expected 42 : %d" % len(gen_null_data(42))
