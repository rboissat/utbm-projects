#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random as r

SEP=""

def bit_trans_err(data, err_prob):
    """ Return a list of bits, altered at the bit level. """
    return [ data[i] ^ (r.random() <= err_prob) for i in xrange(len(data)) ]


def block_trans_err(data, err_prob):
    """ Return a list of bits, altered at the bit level, by block. """
    begin = int(r.randint(0, round((1 - err_prob) * len(data))))
    end = int(begin + round(err_prob * len(data)))
    result = []
    result.extend(data)

    for i in xrange(begin, end):
        result[i] = 1 ^ result[i]

    return result

def data_cmp(data1, data2):
    """ Compare two bit lists and return a bit string where :
        * result[i] = 0 means data1[i] == data2[i]
        * result[i] = 1 means data1[i] != data2[i]
    """
    if len(data1) != len(data2):
        return -1

    result = [ 0 ^ (data1[i] != data2[i]) for i in xrange(len(data1)) ]
    percentage = (result.count(1) / float(len(result))) * 100

    return (SEP.join(map( str, result)), percentage)

if __name__ == '__main__':
    data = [1 for i in xrange(42)]
    altered_data = bit_trans_err(data, 0.5)
    print "original data: \t\t %s" % SEP.join(map(str, data))
    print "bit altered data: \t %s" %SEP.join(map(str, altered_data))
    print "error: %.02f %%\n" % data_cmp(data, altered_data)[1]

    altered_data = block_trans_err(data, 0.5)
    print "original data: \t\t %s" % SEP.join(map(str, data))
    print "block altered data: \t %s" %SEP.join(map(str, altered_data))
    print "error: %.02f %%\n" % data_cmp(data, altered_data)[1]

